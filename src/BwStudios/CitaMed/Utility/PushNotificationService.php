<?php
/**
 * Created by PhpStorm.
 * User: ingridperez
 * Date: 7/04/16
 * Time: 4:52 PM
 */

namespace BwStudios\CitaMed\Utility;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Device;

//Symfony utilities
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;


class PushNotificationService
{
    private $repositoryDevice;
    private $em;


    /**
     *
     * @var EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryDevice= $this->em->getRepository(Constant::ENTITY_DEVICE);
    }

    public function addDevice(Request $request){


    }

    public function sendPushNotification($message, $devices){


    }
}