<?php

namespace BwStudios\CitaMed\Utility;

use Symfony\Component\BrowserKit\Request;

class MailService
{

    private $email;
    private $pass;
    private $name;
    private $nameDoctor;
    private $lastNameDoctor;
    private $gender;
    private $tipeMail;
    private $address;
    private $phone;
    private $day;
    private $month;
    private $year;
    private $time;
    private $dateid;
    private $date;
    private $email_doctor;
    private $enablePersonalTitle;
    private $isToday;
    private $isTomorrow;
    private $dateToEnd;
    private $subjectPersonalTitle;
    private $gebder_R;
    private $dayLetter;
    private $gduPatient;
    private $eduDoctor;
    private $appointment;
    private $gduDoctor;
    private $personalTitleDoctor;
    private $newUser;
    private $textNewUser;
    private $isAppointmentEdit;
    private $introductionTextForEmailThree;
    private $introductionTextForEmailTwo;
    private $previousMonth;
    private $previousDayLetter;
    private $previousDay;
    private $previousHour;

    /**
     * @return mixed
     */
    public function getPreviousHour()
    {
        return $this->previousHour;
    }

    /**
     * @param mixed $previousHour
     */
    public function setPreviousHour($previousHour)
    {
        $this->previousHour = $previousHour;
    }

    /**
     * @return mixed
     */
    public function getPreviousDay()
    {
        return $this->previousDay;
    }

    /**
     * @param mixed $previousDay
     */
    public function setPreviousDay($previousDay)
    {
        $this->previousDay = $previousDay;
    }

    /**
     * @return mixed
     */
    public function getPreviousDayLetter()
    {
        return $this->previousDayLetter;
    }

    /**
     * @param mixed $previousDayLetter
     */
    public function setPreviousDayLetter($previousDayLetter)
    {
        $this->previousDayLetter = $previousDayLetter;
    }

    /**
     * @return mixed
     */
    public function getPreviousMonth()
    {
        return $this->previousMonth;
    }

    /**
     * @param mixed $previousMonth
     */
    public function setPreviousMonth($previousMonth)
    {
        $this->previousMonth = $previousMonth;
    }

    /**
     * @return mixed
     */
    public function getIsAppointmentEdit()
    {
        return $this->isAppointmentEdit;
    }

    /**
     * @param mixed $isAppointmentEdit
     */
    public function setIsAppointmentEdit($isAppointmentEdit)
    {
        $this->isAppointmentEdit = $isAppointmentEdit;
    }

    /**
     * @return mixed
     */
    public function getNewUser()
    {
        return $this->newUser;
    }

    /**
     * @param mixed $newUser
     */
    public function setNewUser($newUser)
    {
        $this->newUser = $newUser;
    }

    /**
     * @return mixed
     */
    public function getPersonalTitleDoctor()
    {
        return $this->personalTitleDoctor;
    }

    /**
     * @param mixed $personalTitleDoctor
     */
    public function setPersonalTitleDoctor($personalTitleDoctor)
    {
        $this->personalTitleDoctor = $personalTitleDoctor;
    }

    /**
     * @return mixed
     */
    public function getDayLetter()
    {
        return $this->dayLetter;
    }

    /**
     * @param mixed $dayLetter
     */
    public function setDayLetter($dayLetter)
    {
        $this->dayLetter = $dayLetter;
    }

    /**
     * @return mixed
     */
    public function getGduPatient()
    {
        return $this->gduPatient;
    }

    /**
     * @param mixed $gduPatient
     */
    public function setGduPatient($gduPatient)
    {
        $this->gduPatient = $gduPatient;
    }

    /**
     * @return mixed
     */
    public function getEduDoctor()
    {
        return $this->eduDoctor;
    }

    /**
     * @param mixed $eduDoctor
     */
    public function setEduDoctor($eduDoctor)
    {
        $this->eduDoctor = $eduDoctor;
    }

    /**
     * @return mixed
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * @param mixed $appointment
     */
    public function setAppointment($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * @return mixed
     */
    public function getGduDoctor()
    {
        return $this->gduDoctor;
    }

    /**
     * @param mixed $gduDoctor
     */
    public function setGduDoctor($gduDoctor)
    {
        $this->gduDoctor = $gduDoctor;
    }


    /**
     * @return mixed
     */
    public function getDateToEnd()
    {
        return $this->dateToEnd;
    }

    /**
     * @param mixed $dateToEnd
     */
    public function setDateToEnd($dateToEnd)
    {
        $this->dateToEnd = $dateToEnd;
    }

    /**
     * @return mixed
     */
    public function getIsTomorrow()
    {
        return $this->isTomorrow;
    }

    /**
     * @param mixed $isTomorrow
     */
    public function setIsTomorrow($isTomorrow)
    {
        $this->isTomorrow = $isTomorrow;
    }

    /**
     * @return mixed
     */
    public function getIsToday()
    {
        return $this->isToday;
    }

    /**
     * @param mixed $isToday
     */
    public function setIsToday($isToday)
    {
        $this->isToday = $isToday;
    }

    /**
     * @return mixed
     */
    public function getEnablePersonalTitle()
    {
        return $this->enablePersonalTitle;
    }

    /**
     * @param mixed $enablePersonalTitle
     */
    public function setEnablePersonalTitle($enablePersonalTitle)
    {
        $this->enablePersonalTitle = $enablePersonalTitle;
    }

    /**
     * @return mixed
     */
    public function getEmailDoctor()
    {
        return $this->email_doctor;
    }

    /**
     * @param mixed $email_doctor
     */
    public function setEmailDoctor($email_doctor)
    {
        $this->email_doctor = $email_doctor;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNameDoctor()
    {
        return $this->nameDoctor;
    }

    /**
     * @param mixed $nameDoctor
     */
    public function setNameDoctor($nameDoctor)
    {
        $this->nameDoctor = $nameDoctor;
    }

    /**
     * @return mixed
     */
    public function getLastNameDoctor()
    {
        return $this->lastNameDoctor;
    }

    /**
     * @param mixed $lastNameDoctor
     */
    public function setLastNameDoctor($lastNameDoctor)
    {
        $this->lastNameDoctor = $lastNameDoctor;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTipeMail()
    {
        return $this->tipeMail;
    }

    /**
     * @param mixed $tipeMail
     */
    public function setTipeMail($tipeMail)
    {
        $this->tipeMail = $tipeMail;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param mixed $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getDateid()
    {
        return $this->dateid;
    }

    /**
     * @param mixed $dateid
     */
    public function setDateid($dateid)
    {
        $this->dateid = $dateid;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function setDataForEmailTwo()
    {


        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        $this->setAddress($this->getAppointment()->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $this->setDay($this->getAppointment()->getDate()->format('d'));
        $this->setMonth($months[$this->getAppointment()->getDate()->format('m') - 1]);
        $this->setYear($this->getAppointment()->getDate()->format('Y'));
        $this->setTime($this->getAppointment()->getDate()->format('h:i a'));
        $this->setName($this->getGduPatient()->getName());
        $this->setPhone($this->getAppointment()->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $this->setEmailDoctor($this->getGduDoctor()->getEmail());
        $this->setNameDoctor($this->getGduDoctor()->getName());
        $this->setLastNameDoctor($this->getGduDoctor()->getLastName());

        if ($this->getIsAppointmentEdit()) {
            $this->introductionTextForEmailTwo = '<div style="font-size:16px;margin: 0px; padding: 0px; "><b>ATENCIÓN: </b>
                             tu cita para el ' . $this->getPreviousDayLetter() . ' ' . $this->getPreviousDay() . ' de 
                             ' . $this->getPreviousMonth() . ' a las <br>' . $this->getPreviousHour() . ' <b>ha sido movida al
                             ' . $this->getDayLetter() . ' ' . $this->getDay() . ' de ' . $this->getMonth() . ' a las ' . $this->getTime() . ' de 
                             ' . $this->getYear() . '</b></div><br>';
        } else {
            $this->introductionTextForEmailTwo = '<h1 style="font-size:16px;margin: 0px; padding: 0px; ">Tienes una cita con <br> ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . '</h1>';
        }

        //$this->setEmail($generalDataPatient->getEmail());

        //$this->setDate($appointment->getDate());


        //$this->setDateid($appointment->getId());
        //$this->setTipeMail(2);

        //error_log('hola');
        //$appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        //$timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        //$dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        //$email->setDateToEnd($dateToEnd);

    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    public function setPersonalTitle()
    {
        if ($this->getPersonalTitleDoctor()) {
            if ($this->getPersonalTitleDoctor() == "1") {
                $this->subjectPersonalTitle = 'Dr.';
                $this->gebder_R = "el Doctor";
            } elseif ($this->getPersonalTitleDoctor() == "2") {
                $this->subjectPersonalTitle = 'Dra.';
                $this->gebder_R = "la Doctora";
            } elseif (($this->getPersonalTitleDoctor() == 0)) {
                $this->subjectPersonalTitle = '';
                $this->gebder_R = '';
            }
        } else {
            $this->subjectPersonalTitle = '';
            $this->gebder_R = '';
        }
    }

    public function setDataForEmailNine()
    {
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        $this->setDate($this->getAppointment()->getDate());
        $this->setDay($this->getAppointment()->getDate()->format('d'));
        $this->setMonth($months[$this->getAppointment()->getDate()->format('m') - 1]);
        $this->setYear($this->getAppointment()->getDate()->format('Y'));
        $this->setNameDoctor($this->getGduDoctor()->getName());
        $this->setLastNameDoctor($this->getGduDoctor()->getLastName());
        $this->setEmailDoctor($this->getGduDoctor()->getEmail());
        $this->setEmail($this->getGduPatient()->getEmail());
        $this->setAddress($this->getAppointment()->getUserPlaceConnectionId()->getPlaceId()->getAddress());
    }

    public function setDataForEmailThree()
    {
        if ($this->getNewUser()) {
            $this->textNewUser = '<h1 style="font-size:16px;margin: 0px; padding: 0px; ">ESTA ES UNA MUESTRA DEL MAIL QUE VERÁN TUS PACIENTES</h1><br><br>';
        }

        if ($this->getIsAppointmentEdit()) {
            $this->introductionTextForEmailThree = '<div style="font-size:16px;margin: 0px; padding: 0px; "><b>ATENCIÓN: </b>
                             tu cita para el ' . $this->getPreviousDayLetter() . ' ' . $this->getPreviousDay() . ' de 
                             ' . $this->getPreviousMonth() . ' a las <br>' . $this->getPreviousHour() . ' <b>ha sido movida al
                             ' . $this->getDayLetter() . ' ' . $this->getDay() . ' de ' . $this->getMonth() . ' a las ' . $this->getTime() . ' de 
                             ' . $this->getYear() . '</b></div>';
        } else {
            $this->introductionTextForEmailThree = '<h1 style="font-size:16px;margin: 0px; padding: 0px; ">Tienes una cita con <br> ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . '</h1>';
        }
    }

    public function buildIcsData()
    {
        $appointmentDate = new \DateTime($this->getAppointment()->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $this->getAppointment()->getAssignedTime()->format('H') . 'H' . $this->getAppointment()->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        $date = new \DateTime($this->getDate()->format('Y-m-d H:i:s'));
        $date->setTimezone(new \DateTimeZone('Africa/Abidjan'));
        $dateToEnd = new \DateTime($dateToEnd->format('Y-m-d H:i:s'));
        $dateToEnd->setTimezone(new \DateTimeZone('Africa/Abidjan'));
        $now = new \DateTime('now');
        $description = 'Recuerda que el día ' . $this->getDay() . ' de ' . $this->getMonth() . ' de ' . $this->getYear() . ' tienes una cita con ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor());
        $icsData = 'BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:' . $date->format('Ymd') . 'T' . $date->format('His') . 'Z
DTEND:' . $dateToEnd->format('Ymd') . 'T' . $dateToEnd->format('His') . 'Z
DTSTAMP:' . $now->format('Ymd') . 'T' . $now->format('His') . 'Z
ORGANIZER;CN=no-reply@todoc.co:mailto:no-reply@todoc.co
UID:no-reply@todoc.co
ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=' . $this->getEmail() . ';X-NUM-GUESTS=0:mailto:' . $this->getEmail() . '
ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=no-reply@todoc.co;X-NUM-GUESTS=0:mailto:no-reply@todoc.co
CREATED:' . $now->format('Ymd') . 'T' . $now->format('His') . 'Z
DESCRIPTION:' . $description . '
LOCATION:' . $this->getAddress() . '
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Cita Médica
TRANSP:TRANSPARENT
END:VEVENT
END:VCALENDAR';

        return $icsData;
    }


    public function getFullNameDoctor()
    {
        /*if ($this->getEduDoctor()->getEnablePersonalTitle()) {
            return $this->getEduDoctor()->getEnablePersonalTitle() . ' ' . ucfirst($this->getGduDoctor()->getName()) . ' ' . ucfirst($this->getGduDoctor()->getLastName());
        } else {
            return ucfirst($this->getGduDoctor()->getName()) . ' ' . ucfirst($this->getGduDoctor()->getLastName());
        }*/
        //error_log('fullNameDoctor');
        //error_log($this->getEnablePersonalTitle());
        if ($this->getPersonalTitleDoctor()) {
            if ($this->getPersonalTitleDoctor() == "1") {
                $this->gebder_R = "el Doctor";
                return 'Dr. ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor());
                //$this->subjectPersonalTitle = 'Dr.';
            } elseif ($this->getPersonalTitleDoctor() == "2") {
                $this->gebder_R = "la Doctora";
                return 'Dra. ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor());
                //$this->subjectPersonalTitle = 'Dra.';
            }
        } else {
            return ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor());
        }
    }

    public function dataValidation()
    {

        if ($this->getTipeMail() == 1) {

        } elseif ($this->getTipeMail() == 2) {
            if ($this->getGduPatient()->getEmail() && $this->getGduDoctor()->getEmail()) {
                return true;
            } else {
                return false;
            }
        } elseif ($this->getTipeMail() == 3) {

        } elseif ($this->getTipeMail() == 4) {

        } elseif ($this->getTipeMail() == 5) {

        } elseif ($this->getTipeMail() == 6) {

        } elseif ($this->getTipeMail() == 7) {

        } elseif ($this->getTipeMail() == 8) {

        }
    }

    public function sendEmail()
    {

        $emailToSend = $this->emailVerification($this->email);

        if ($emailToSend == true) {
            //$gebder_R = "";
            $welcome = "";
            $lonely = "";
            //$subjectPersonalTitle = "";
            //Modificar esta ruta para produccion
            $baseServer = 'http://' . $_SERVER['HTTP_HOST'];
            $environmetBaseUrl = '/api_dev';

            $this->setPersonalTitle();

            if ($this->getGender())
                if ($this->getGender() == "m") {
                    $welcome = 'Bienvenido';
                    $lonely = 'solo';
                } elseif ($this->getGender() == "f") {
                    $welcome = 'Bienvenida';
                    $lonely = 'sola';
                }

            if ($this->getPersonalTitleDoctor()) {
                if ($this->getPersonalTitleDoctor() == 1) {
                    $welcome = 'Bienvenido';
                    $lonely = 'solo';
                } else {
                    if ($this->getPersonalTitleDoctor() == 2) {
                        $welcome = 'Bienvenida';
                        $lonely = 'sola';
                    }
                }
            } else {
                $welcome = 'Bienvenido';
                $lonely = 'solo';
            }

            $transport = \Swift_SmtpTransport::newInstance()
                ->setHost('smtp.gmail.com')
                ->setPort('587')
                ->setEncryption('tls')
                ->setUsername('no-reply@todoc.co')
                ->setPassword('usuarios14');

            $mailer = \Swift_Mailer::newInstance($transport);

            //Email cuando un medico se registra
            if ($this->getTipeMail() == 1) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Bienvenido a Todoc')
                    ->setFrom(array('no-reply@todoc.co' => 'TODOC'))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setBody('
<div marginheight="0" marginwidth="0" style="background-color:#ececec"><div class="adM">
    <table style="max-width:1000px;margin:auto" align="center">
        <tbody>
            <tr>
                <td style="font-size:16px;font-weight:bold;padding-top:2px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                </td>
            </tr>
            <tr>
                <td style="padding-bottom:8px">
                    <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:18px;margin:auto" align="center" bgcolor="#ffffff" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top:20px;padding-bottom:1px;padding-left:20px;padding-right:20px" align="right" width="100%">
                                    <a href="" title="Todoc" target="_blank">
                                        <img alt="Todoc" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#1e80b6;padding-top:1px;padding-bottom:1px;padding-left:20px;padding-right:20px">
                                     <h2>Hola ' . ucfirst($this->getName()) . '</h2> 
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:30px">
                                Bienvenido a <strong>TODOC</strong>,  ' . $this->gebder_R . '  ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ' te ha dado acceso a su cuenta como <strong>Asistente</strong>
                                <br><br>
                                1. Descarga la aplicación TODOC del store
                                <br>
                                2. Inicia sesión con las siguientes credenciales:
                                <br><br>
                                Usuario: ' . $this->getEmail() . '
                                <br>
                                Contraseña: ' . $this->getPass() . '
                                 <br> <br>
                                 Y comencemos!
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: small;
    color: rgb(153, 153, 153);padding-left:20px;padding-right:20px;padding-bottom:30px;text-align: justify" colspan="2">
                                AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
                                </td>
                            </tr>
                            <tr>
                                <td style="height:5px" height="5"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
               <td style="font-size:16px;font-weight:bold;padding-top:20px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                </td>
            </tr>
        </tbody>
    </table>
</div>
', 'text/html');
                $mailer->send($message);
            } //Email notificando nueva cita
            elseif ($this->getTipeMail() == 2) {
                if ($this->dataValidation()) {
                    $this->setDataForEmailTwo();
                    //ob_start();
                    //include dirname(__DIR__) . '/ApiBundle/Resources/views/emailsTemplates/newAppointment.html';
                    //$content = ob_get_clean();
                    if ($this->getIsAppointmentEdit()) {
                        $subject = 'Tu cita ha sido reprogramada';
                    } else {
                        $subject = 'Nueva cita';
                    }
                    $message = \Swift_Message::newInstance()
                        ->setSubject($subject)
                        ->setFrom(array('no-reply@todoc.co' => $this->getFullNameDoctor()))
                        ->setTo(array($this->getGduPatient()->getEmail() => $this->getGduPatient()->getName()))
                        ->setReplyTo($this->getGduDoctor()->getEmail())
                        ->setBody('<div>
  <br>
</div>&nbsp;
<table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border: 0px; ">
  <tbody style="margin: 0; padding: 0;">
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        ' . $this->introductionTextForEmailTwo . '
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); color: rgb(153, 153, 153); font-size: 16px; line-height: 18px; margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; color: rgb(153, 153, 153); font-size: 16px; line-height: 18px; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        <div>Lugar ' . $this->getAddress() . '
                          <br style="margin: 0; padding: 0;">el día <strong style="margin: 0; padding: 0;">' . $this->getDayLetter() . ' ' . $this->getDay() . ' de '
                            . $this->getMonth() . ' de ' . $this->getYear() . '</strong>,</div>
                        <div>a las <strong style="margin: 0; padding: 0;">' . $this->getTime() . '</strong>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff" height="15">&nbsp;</td>
    </tr>
    <tr style="line-height: 1px; font-size: 1px; color: #fff; background-color: #2ca881; margin: 0; padding: 0; border: none;" bgcolor="#2CA881">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#00a0dc" height="10">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="width: 100%; margin: 0; padding: 0;">
              </td>
              <td style="margin: 0; padding: 0;" valign="top">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="font-size: 16px; line-height: 20px; color: rgb(44, 44, 44); white-space: nowrap; width: 1px; margin: 0px; padding: 0px 20px; " bgcolor="#eff5f6" height="43">
                        <a href="http://www.todoc.co">
                  <img alt="Relay Foods" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="width: 100%; text-align: left; margin: 0; padding: 0;" align="left">
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; margin: 0px; padding: 0px; border: 0px; " bgcolor="#eff5f6">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
                                  bgcolor="transparent">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <h2 style="color: rgb(44, 44, 44); font-size: 14px; text-align: left; margin: 0px; padding: 0px; " align="left">Hola ' . ucfirst($this->getName()) . ',</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="line-height: 1px; font-size: 1px; margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;" height="10">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; background-color: rgb(255, 255, 255); margin: 0px; padding: 0px 0px 50px; border-width: 0px 1px; " bgcolor="#fff">
                          <tbody>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                               
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" >
                                                <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;" >Recuerda que el día ' . $this->getDayLetter() . ' ' . $this->getDay() . ' de ' . $this->getMonth() . ' de ' . $this->getYear() . ' tienes una cita con ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ' a las ' . $this->getTime() . ' en la dirección: ' . $this->getAddress() . '. .
                                                 </p><br>
                                                 <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;"><b>Por favor confirma tu asistencia a través del correo electrónico que recibirás el día antes de la cita.</b>
                                                 </p><br>
                                                <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;">Si llega a surgir algún problema, no dudes en contactarnos al teléfono ' . $this->getPhone() . ' o escribirnos a ' . $this->getEmailDoctor() . '.
                                                  
                                                <br><br>  Te recomendamos que llegues con al menos 10 minutos de antelación a tu cita.
                                                 </p><br>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; font-size: 18px; line-height: 20px; color: rgb(51, 51, 51); margin: 0px auto; padding: 0px 0px 50px; border: 0px solid rgb(224, 224, 224); " bgcolor="#fff">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#EFF5F6">
                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td style="margin: 0; padding: 0;">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <h2 style="font-size: 16px; line-height: 26px; color: rgb(44, 44, 44); margin: 0px; padding: 0px; "><br>Por favor no respondas a este correo. Éste es un mensaje automático enviado por Todoc</h2>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: justified; font-size: 10px; line-height: 14px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; font-size: 16px; line-height: 30px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">Cuida tu salud y mantente siempre en contacto con tu médico de cabecera.
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
 
  </tbody>
</table>
  <div>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; min-width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 0px; text-align: center; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border-width: 10px 0px 0px; ">
      <tbody>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="transparent">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                 
                  <td style="margin: 0; padding: 0;" width="16">&nbsp;</td>
                  <td style="margin: 0; padding: 0;">
                    <a style="margin: 0; padding: 0; " href="https://www.facebook.com/todoc.co/?fref=ts">
                      <img alt="facebook" src="http://www6.dev-chop-chop.org/JIX8STE8/images/btn-facebook.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                    </a>
                  </td>
                  
                  
                </tr>
                 <tr><br><br><a style="align:center!important;" href="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es"><img src="https://todoc.co/images/azultransparentep.png" width="300" height="56" alt="Todoc para Android" longdesc="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es" /></a>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" bgcolor="#E1E7E8" height="8">&nbsp;</td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="#E1E7E8">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                  <td style="margin: 0; padding: 8 px 0 0 0;">
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">&nbsp;</td>
        </tr>
  
      </tbody>
    </table>
    <br>
    <!-- / wrap table -->
   
  </div>', 'text/html');
                    return $mailer->send($message);
                } else
                    return false;
            } //Email para confirmar la cita por parte del paciente
            elseif ($this->getTipeMail() == 3) {
                $this->setDataForEmailThree();
                //$icsData = $this->buildIcsData();
                if ($this->getIsToday()) {
                    $dateEmail = "hoy";
                } elseif ($this->getIsTomorrow()) {
                    $dateEmail = "mañana";
                } else {
                    $dateEmail = "el día " . $this->getDayLetter() . ' ' . $this->getDay() . " de " . $this->getMonth() . " de " . $this->getYear();
                }
                if ($this->getIsAppointmentEdit()) {
                    $subject = 'Tu cita ha sido reprogramada';
                } else {
                    $subject = 'Por favor confirma tu asistencia';
                }
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom(array('no-reply@todoc.co' => $this->subjectPersonalTitle . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor())))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setReplyTo($this->getEmailDoctor())
                    ->setBody('<div>
<div style="text-align: center">
  <br>' . $this->textNewUser . '
</div>
<table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border: 0px; ">
  <tbody style="margin: 0; padding: 0;">
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                        ' . $this->introductionTextForEmailThree . '
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; color: rgb(0, 159, 220); font-size: 16px; line-height: 18px; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td height="50" style="margin: 0; padding: 0;">
                        <p><strong>Es muy importante que nos confirmes tu asistencia</strong>
                        </p>
                      
              </td>
            </tr>
          </tbody>
        </table>
        <div align="center">
                                    <table cellspacing="20">
                                        <tr>
                                            <td>
                                                <a style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-decoration:none;padding:14px;font-weight:400;font-size:14px;color:#ffffff;background-color:#4CB944;border-radius:6px;border:2px solid #538a04" href="' . $baseServer . $environmetBaseUrl . '/date/mail/confirmed?date=' . base64_encode($this->getDateid()) . '&data=' . base64_encode($this->getDate()->format('Y-m-d')) . '" target="_blank">Confirmar</a>
                                            </td>
                                            <td>
                                                <a style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;text-decoration:none;padding:14px;font-weight:400;font-size:14px;color:#ffffff;background-color:#EB5160;border-radius:6px;border:2px solid #cc2f07" href="' . $baseServer . $environmetBaseUrl . '/date/mail/canceled?date=' . base64_encode($this->getDateid()) . '&data=' . base64_encode($this->getDate()->format('Y-m-d')) . '" target="_blank">Cancelar</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
      </td>
    </tr>
    <tr style="line-height: 1px; font-size: 1px; color: #fff; background-color: #2ca881; margin: 0; padding: 0; border: none;" bgcolor="#2CA881">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#00a0dc" height="10">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 90%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="width: 90%; margin: 0; padding: 0;" align="left"><h2 style="color: rgb(44, 44, 44); font-size: 14px; text-align: left; margin: 0px; padding: 0px; " align="left">Hola ' . ucfirst($this->getName()) . ',<br /></h2>
              </td>
              <td style="margin: 0; padding: 0;" valign="top">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="font-size: 14px; line-height: 20px; color: rgb(44, 44, 44); white-space: nowrap; width: 1px; margin: 0px; padding: 0px 20px; " bgcolor="#eff5f6" height="43">
                        <a href="http://www.todoc.co">
                  <img alt="Todoc" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; " bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" >
                                                <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;" ><br />Recuerda que ' . $dateEmail . ' tienes una cita con ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ' a las ' . $this->getTime() . ' en la dirección: ' . $this->getAddress() . '. 
                                                </p><br>
                                                <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;">Para nosotros es muy importante respetar tu tiempo. Por lo que rogamos confirmes tu asistencia haciendo clic en el botón verde o cancelarla haciendo clic en el botón rojo. 
                                                </p>
                                                 <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;">
                                <br>
                                <strong> Si deseas reagendar tu cita o llega a surgir algún problema, no dudes en
                                    contactarnos al teléfono: ' . $this->getPhone() . ' o escribirnos al correo: ' .
                        $this->getEmailDoctor() . '.</strong>
                                <br><br>
                                Te recomendamos que llegues con al menos 10 minutos de antelación a la cita indicada
                                <br><br></p>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                  </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
          <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#EFF5F6">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <h2 style="font-size: 16px; line-height: 26px; color: rgb(44, 44, 44); margin: 0px; padding: 0px; "><br>Por favor no respondas a este correo. Éste es un mensaje automático enviado por Todoc</h2>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: justified; font-size: 10px; line-height: 12px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;"><br />AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.<br />
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#EFF5F6">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; font-size: 16px; line-height: 30px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;"><br />Cuida tu salud y mantente siempre en contacto con tu médico de cabecera.
              </td>
            </tr>
            <tr><br><br><a style="align:center;" href="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es"><img src="https://todoc.co/images/azultransparentep.png" width="300" height="56" alt="Todoc para Android" longdesc="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es" /></a>
              </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
  <table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; min-width: 100%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 0px; text-align: center; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border-width: 10px 0px 0px; ">
      <tbody>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="transparent">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                  <td style="margin: 0; padding: 0;" align: "center";>
                    <a style="margin: 0; padding: 0; " href="https://www.facebook.com/todoc.co/?fref=ts">
                      <img alt="facebook" src="http://www6.dev-chop-chop.org/JIX8STE8/images/btn-facebook.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                    </a>
                  </td>
               </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" bgcolor="#E1E7E8" height="8">&nbsp;</td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">
          
          </td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">&nbsp;</td>
        </tr>
      </tbody>
    </table>
        </table>  
      </td>
    </tr>
 
</table>

<!-- / wrap table -->
</div>', 'text/html');
                //->attach(\Swift_Attachment::newInstance($icsData, 'cita.ics', 'text/calendar'));
                //FIN confirmacion cita
                return $mailer->send($message);
            } elseif ($this->getTipeMail() == 4) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Te invito a que conozcas TODOC ¡Es muy útil!')
                    ->setFrom(array('no-reply@todoc.co' => 'TODOC'))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setBody('
                    <div marginheight="0" marginwidth="0" style="background-color:#ececec"><div class="adM">
                        <table style="max-width:1000px;margin:auto" align="center">
                            <tbody>
                                <tr>
                                    <td style="font-size:16px;font-weight:bold;padding-top:2px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:8px">
                                        <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:18px;margin:auto" align="center" bgcolor="#ffffff" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-top:20px;padding-bottom:1px;padding-left:20px;padding-right:20px" align="right" width="100%">
                                                        <a href="" title="Todoc" target="_blank">
                                                            <img alt="Todoc" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" class="CToWUd">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#1e80b6;padding-top:1px;padding-bottom:1px;padding-left:20px;padding-right:20px">
                                                         <h2>Hola ' . ucfirst($this->getName()) . '</h2> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:30px">
                                                    Únete a todoc, la aplicación para agendar citas que hará ver tu negocio como una verdadera empresa.
                                                    <br><br>
                                                    Descárgala de las tiendas
                                                    <br>
                                                   
                                                    <br><br>
                                                    Googleplay: https://play.google.com/apps/publish/?dev_acc=01985136011631517451#MarketListingPlace:p=tmp.01985136011631517451.1463440545001
                                                    <br>
                                                    Appstore: https://play.google.com/apps/publish/?dev_acc=01985136011631517451#MarketListingPlace:p=tmp.01985136011631517451.1463440545001
                                                     <br> <br>
                                                     ¡Descubre el potencial del app y optimiza tu tiempo!
                                                    </td>
                                                </tr>
                                                <tr>
                                <td style="font-size: small;
    color: rgb(153, 153, 153);padding-left:20px;padding-right:20px;padding-bottom:30px;text-align: justify" colspan="2">
                                AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
                                </td>
                            </tr>
                                                <tr>
                                                    <td style="height:5px" height="5"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   <td style="font-size:16px;font-weight:bold;padding-top:20px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    ', 'text/html');
                $mailer->send($message);
            } elseif ($this->getTipeMail() == 5) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('TODOC cambio de contraseña')
                    ->setFrom(array('no-reply@todoc.co' => 'TODOC'))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setBody('
                    <div marginheight="0" marginwidth="0" style="background-color:#ececec"><div class="adM">
                        <table style="max-width:1000px;margin:auto" align="center">
                            <tbody>
                                <tr>
                                    <td style="font-size:16px;font-weight:bold;padding-top:2px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:8px">
                                        <table style="color:#353e4a;font-family:Arial,sans-serif;font-size:18px;margin:auto" align="center" bgcolor="#ffffff" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding-top:20px;padding-bottom:1px;padding-left:20px;padding-right:20px" align="right" width="100%">
                                                        <a href="" title="Todoc" target="_blank">
                                                            <img alt="Todoc" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" class="CToWUd">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#1e80b6;padding-top:1px;padding-bottom:1px;padding-left:20px;padding-right:20px">
                                                         <h2>Hola ' . ucfirst($this->getName()) . '</h2> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="line-height:24px;padding-left:20px;padding-right:20px;padding-bottom:30px">
                                                            Has solicitado cambiar tu contraseña en <strong>TODOC</strong>, 
                                                        <br>
                                                            Inicia sesión con las siguientes credenciales y crea una nueva contraseña:
                                                        <br><br>
                                                            Usuario: ' . $this->getEmail() . '
                                                        <br>
                                                            Contraseña: ' . $this->getPass() . '
                                                        <br> <br>
                                                            Atentamente,
                                                         <br>
                                                            Equipo de TODOC.
                                                    </td>
                                                </tr>
                                                <tr>
                                <td style="font-size: small;
    color: rgb(153, 153, 153);padding-left:20px;padding-right:20px;padding-bottom:30px;text-align: justify" colspan="2">
                                AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
                                </td>
                            </tr>
                                                <tr>
                                                    <td style="height:5px" height="5"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   <td style="font-size:16px;font-weight:bold;padding-top:20px;padding-bottom:20px;padding-left:25px;padding-right:25px;text-align:center;color:#353e4a;font-family:Arial,sans-serif" align="center" width="100%">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    ', 'text/html');
                $mailer->send($message);
            } elseif ($this->getTipeMail() == 6) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Bienvenido a TODOC ')
                    ->setFrom(array('no-reply@todoc.co' => 'TODOC'))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setBody('<div>
  <br>
</div>&nbsp;
<table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border: 0px; ">
  <tbody style="margin: 0; padding: 0;">
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        <h1 style="margin: 0px; color: rgb(0, 160, 220); padding: 0px; ">' . $welcome . ' a Todoc</h1>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); color: rgb(153, 153, 153); font-size: 24px; line-height: 36px; margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; color: rgb(153, 153, 153); font-size: 24px; line-height: 36px; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        <div>La aplicación móvil para gestionar <br>las citas de tu consultorio
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff" height="15">&nbsp;</td>
    </tr>
    <tr style="line-height: 1px; font-size: 1px; color: #fff; background-color: #2ca881; margin: 0; padding: 0; border: none;" bgcolor="#2CA881">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#00a0dc" height="10">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="width: 100%; margin: 0; padding: 0;">
              </td>
              <td style="margin: 0; padding: 0;" valign="top">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="font-size: 16px; line-height: 20px; color: rgb(44, 44, 44); white-space: nowrap; width: 1px; margin: 0px; padding: 0px 20px; " bgcolor="#eff5f6" height="43">
                        <a href="http://www.todoc.co">
                  <img alt="Relay Foods" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="width: 100%; text-align: left; margin: 0; padding: 0;" align="left">
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; margin: 0px; padding: 0px; border: 0px; " bgcolor="#eff5f6">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
                                  bgcolor="transparent">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <h2 style="color: rgb(44, 44, 44); font-size: 24px; text-align: left; margin: 0px; padding: 0px; " align="left">Hola ' . ucfirst($this->getName()) . ',</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="line-height: 1px; font-size: 1px; margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;" height="10">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; background-color: rgb(255, 255, 255); margin: 0px; padding: 0px 0px 50px; border-width: 0px 1px; " bgcolor="#fff">
                          <tbody>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                               
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;">
                                                <p style="font-size: 16px; line-height: 22px; text-align: left;  color: #666666; margin: 0; padding: 0;">' . $welcome . ', ahora eres parte de un grupo exclusivo de especialistas que están dando un paso adelante en la digitalización de sus procesos para la mejora del servicio al paciente y la eficiencia en la gestión de tu consultorio. 
Recuerda que con <strong>TODOC</strong> podrás gestionar eficientemente la creación y confirmación de citas médicas, gestionar los ingresos de tu consultorio, conocer la información y estadísticas de atención a tus pacientes y permitir que tu asistente dedique más tiempo a realizar aquellas tareas que añaden mayor valor a tu consultorio. 
                                                 </p><br>
                                                <p style="font-size: 16px; line-height: 22px; text-align: left; color: #666666; margin: 0; padding: 0;">En este proceso no estarás ' . $lonely . ', puedes contar con todo nuestro equipo de trabajo en cualquier momento para resolver dudas o ayudarte en todo lo que tenga que ver con nuestro servicio. 
<br><br>
Mi nombre es Milagros Pérez Díez y estoy para servirte en lo que necesites.
                                                  <br><br>
Atentamente<br><br>
Milagros Pérez Diez<br>
                                                Directora General Todoc<br><a href="mailto:milagros@todoc.co" target="_blank">milagros@todoc.co</a></p><br>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                        
                       
                              
                           
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; font-size: 18px; line-height: 20px; color: rgb(51, 51, 51); margin: 0px auto; padding: 0px 0px 50px; border: 0px solid rgb(224, 224, 224); " bgcolor="#fff">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#EFF5F6">
                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td style="margin: 0; padding: 0;">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
 <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; font-size: 24px; line-height: 30px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">' . $welcome . ' a la era digital de tu consultorio.
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0px; padding: 0px; " align="center">
                <img alt="welcome-image" src="https://todoc.co/images/milagros.png" style="display: inline-block; outline: 0; margin: 0 auto; padding: 0; border: 0;">
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); font-size: 16px; line-height: 24px; color: rgb(102, 102, 102); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <h3 style="color: rgb(44, 44, 44); margin: 0px; padding: 0px; ">Milagros Pérez Diez</h3>
              </td>
            </tr>
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">CEO de Todoc</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <h2 style="font-size: 24px; line-height: 26px; color: rgb(44, 44, 44); margin: 0px; padding: 0px; "><br>Por favor no respondas a este correo. <br>Éste es un mensaje automático enviado por Todoc</h2>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="trasparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: justified; font-size: 12px; line-height: 14px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
   
  <div>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; min-width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 0px; text-align: center; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border-width: 10px 0px 0px; ">
      <tbody>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="transparent">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                 
                  <td style="margin: 0; padding: 0;" width="16">&nbsp;</td>
                  <td style="margin: 0; padding: 0;">
                    <a style="margin: 0; padding: 0; " href="https://www.facebook.com/todoc.co/?fref=ts">
                      <img alt="facebook" src="http://www6.dev-chop-chop.org/JIX8STE8/images/btn-facebook.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                    </a>
                  </td>
                  
                  
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" bgcolor="#E1E7E8" height="8">&nbsp;</td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="#E1E7E8">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                  <td style="margin: 0; padding: 8 px 0 0 0;">
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">&nbsp;</td>
        </tr>
  
      </tbody>
    </table>
    <br>
    <!-- / wrap table -->
   
  </div>', 'text/html');
                $mailer->send($message);
            } //Cuando el medico o asistente cancelan una cita
            elseif ($this->getTipeMail() == 7) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Cancelación de cita')
                    ->setFrom(array('no-reply@todoc.co' => $this->subjectPersonalTitle . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor())))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setReplyTo($this->getEmailDoctor())
                    ->setBody('<div>
  <br>
</div>&nbsp;
<table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border: 0px; ">
  <tbody style="margin: 0; padding: 0;">
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        <h1 style="font-size:16px;margin: 0px; padding: 0px; ">Tu cita con ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ' ha sido cancelada</h1>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
<!--    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); color: rgb(153, 153, 153); font-size: 24px; line-height: 36px; margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="#fff">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; color: rgb(153, 153, 153); font-size: 24px; line-height: 36px; margin: 0px auto; padding: 0px; border: 0px; ">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="margin: 0; padding: 0;">
                        <div>Lugar \' . $this->getAddress() .
                                \'
                          
                          <br style="margin: 0; padding: 0;">el día <strong style="margin: 0; padding: 0;">\' . $this->getDay() . \' de \'
                                . $this->getMonth() . \' de \' . $this->getYear() . \'</strong>,</div>
                        <div>a las <strong style="margin: 0; padding: 0;">\' . $this->getTime() . \'</strong>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>-->
    <tr style="background-color: #fff; margin: 0; padding: 0; border: none;" bgcolor="#fff">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#fff" height="15">&nbsp;</td>
    </tr>
    <tr style="line-height: 1px; font-size: 1px; color: #fff; background-color: #2ca881; margin: 0; padding: 0; border: none;" bgcolor="#2CA881">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="#00a0dc" height="10">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="width: 100%; margin: 0; padding: 0;">
              </td>
              <td style="margin: 0; padding: 0;" valign="top">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="font-size: 16px; line-height: 20px; color: rgb(44, 44, 44); white-space: nowrap; width: 1px; margin: 0px; padding: 0px 20px; " bgcolor="#eff5f6" height="43">
                        <a href="http://www.todoc.co">
                  <img alt="Relay Foods" src="https://lh6.googleusercontent.com/-MrKmHDk55Jw/V0Zf7a7NmSI/AAAAAAAAAsk/G5AONlEJdlkk52Qo3f2OK8fuLN5Df95MwCL0B/w139-h38-no/todoc_logo_azul.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
      <td style="margin: 0; padding: 0;">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <table style="border-collapse: collapse; border-spacing: 0 !important; width: 100%; margin: 0; padding: 0; border: 0;">
                  <tbody style="margin: 0; padding: 0;">
                    <tr style="margin: 0; padding: 0;">
                      <td style="width: 100%; text-align: left; margin: 0; padding: 0;" align="left">
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; margin: 0px; padding: 0px; border: 0px; " bgcolor="#eff5f6">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
                                  bgcolor="transparent">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <h2 style="color: rgb(44, 44, 44); font-size: 14px; text-align: left; margin: 0px; padding: 0px; " align="left">Hola ' . ucfirst($this->getName()) . ',</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="line-height: 1px; font-size: 1px; margin: 0px; padding: 0px; " bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;" height="10">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; background-color: rgb(255, 255, 255); margin: 0px; padding: 0px 0px 50px; border-width: 0px 1px; " bgcolor="#fff">
                          <tbody>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                               
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#eff5f6">
                              <td style="margin: 0; padding: 0;">
                                <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-right-style: solid; border-left-color: rgb(224, 224, 224); border-left-style: solid; background-color: rgb(255, 255, 255); margin: 0px auto; padding: 0px 0px 50px; border-width: 0px 1px; "
                                  bgcolor="#fff">
                                  <tbody style="margin: 0; padding: 0;">
                                    <tr style="margin: 0; padding: 0;">
                                      <td style="margin: 0; padding: 0;">
                                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; margin: 0px auto; padding: 0px; border: 0px; ">
                                          <tbody style="margin: 0; padding: 0;">
                                            <tr style="margin: 0; padding: 0;">
                                              <td style="margin: 0; padding: 0;" >
                                                <p style="font-size: 14px; line-height: 22px; color: #666666; margin: 0; padding: 0;" >Te informamos que ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ', ha cancelado la cita del día ' . $this->getDay() . ' de ' . $this->getMonth() . ' de ' . $this->getYear() . '. Pronto se pondrán en contacto contigo para reagendarla.<BR><BR> Recuerda que puedes ponerte en contacto con ' . $this->gebder_R . ' ' . ucfirst($this->getNameDoctor()) . ' ' . ucfirst($this->getLastNameDoctor()) . ' llamando al teléfono ' . $this->getPhone() . ' o escribirle a ' . $this->getEmailDoctor() . '.
                                                  
                                                <br><br> 
                                                 </p><br>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; font-size: 18px; line-height: 20px; color: rgb(51, 51, 51); margin: 0px auto; padding: 0px 0px 50px; border: 0px solid rgb(224, 224, 224); " bgcolor="#fff">
                          <tbody style="margin: 0; padding: 0;">
                            <tr style="background-color: #eff5f6; margin: 0; padding: 0;" bgcolor="#EFF5F6">
                              <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                      <td style="margin: 0; padding: 0;">&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
          bgcolor="transparent">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">
                <h2 style="font-size: 16px; line-height: 26px; color: rgb(44, 44, 44); margin: 0px; padding: 0px; "><br>Por favor no respondas a este correo. Este es un mensaje automático enviado por Todoc</h2>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: justified; font-size: 10px; line-height: 14px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">AVISO LEGAL: Este mensaje va dirigido, de manera exclusiva, a su destinatario y contiene información confidencial y sujeta al secreto profesional cuya divulgación no está permitida por la ley. En caso de haber recibido este mensaje por error, le rogamos que, de forma inmediata, nos lo comunique mediante correo electrónico remitido a nuestra atención y proceda a su eliminación, así como a la de cualquier documento adjunto al mismo. Asimismo, le comunicamos que la distribución, copia o utilización de este mensaje, o de cualquier documento adjunto al mismo, cualquiera que fuera su finalidad, están prohibidas por la ley.
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; " bgcolor="transparent">
      <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
    </tr>
    <tr style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
      <td style="margin: 0px; padding: 0px; border: none; " bgcolor="transparent">
        <table style="border-collapse: collapse; border-spacing: 0 !important; width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center; font-size: 16px; line-height: 30px; color: #666; margin: 0 auto; padding: 0; border: 0;">
          <tbody style="align:center;margin: 0; padding: 0;">
            <tr style="margin: 0; padding: 0;">
              <td style="margin: 0; padding: 0;">Cuida tu salud y mantente siempre en contacto con tu médico de cabecera.
              </td>
            </tr>

          </tbody>
        </table>
      </td>
    </tr>
 
  </tbody>
</table>
  <div>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <table bgcolor="#EFF5F6" style="border-collapse: collapse; border-spacing: 0px !important; width: 100%; min-width: 95%; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 0px; text-align: center; background-color: rgb(239, 245, 246); margin: 0px; padding: 0px; border-width: 10px 0px 0px; ">
      <tbody>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0px; padding: 0px; " bgcolor="#EFF5F6">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: center; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="transparent">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                 
                  <td style="margin: 0; padding: 0;" width="16">&nbsp;</td>
                  <td style="margin: 0; padding: 0;">
                    <a style="margin: 0; padding: 0; " href="https://www.facebook.com/todoc.co/?fref=ts">
                      <img alt="facebook" src="http://www6.dev-chop-chop.org/JIX8STE8/images/btn-facebook.png" style="display: block; outline: 0; margin: 0; padding: 0; border: 0;">
                    </a>
                  </td>
                  
                  
                </tr>
                 <tr style="align:center;"><br><br><a style="align:center!important;" href="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es"><img src="https://todoc.co/images/azultransparentep.png" width="300" height="56" alt="Todoc para Android" longdesc="https://play.google.com/store/apps/details?id=co.todoc.android&amp;hl=es" /></a>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" height="20">&nbsp;</td>
        </tr>
        <tr style="margin: 0; padding: 0;">
          <td style="margin: 0; padding: 0;" bgcolor="#E1E7E8" height="8">&nbsp;</td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">
            <table style="border-collapse: collapse; border-spacing: 0px !important; width: 95%; text-align: left; border-right-color: rgb(224, 224, 224); border-left-color: rgb(224, 224, 224); margin: 0px auto; padding: 0px 0px 50px; border-style: none; border-width: 0px 1px; "
              bgcolor="#E1E7E8">
              <tbody style="margin: 0; padding: 0;">
                <tr style="margin: 0; padding: 0;">
                  <td style="margin: 0; padding: 8 px 0 0 0;">
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #e1e7e8; margin: 0; padding: 0;" bgcolor="#E1E7E8">
          <td style="margin: 0; padding: 0;">&nbsp;</td>
        </tr>
  
      </tbody>
    </table>
    <br>
    <!-- / wrap table -->
   
  </div>', 'text/html');
                return $mailer->send($message);
            } elseif ($this->getTipeMail() == 8) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Listado de pacientes para hoy')
                    ->setFrom(array('no-reply@todoc.co' => 'Informe diario Todoc'))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->attach(\Swift_Attachment::fromPath(dirname(__FILE__) . '/pdfTemp/RecordatorioCitas.pdf', '', 'application/pdf'))
                    ->setBody('Buenos días ' . $this->subjectPersonalTitle . ' ' . ucfirst($this->getNameDoctor()) . ',

A continuación le adjuntamos el listado de pacientes que tiene para atender el día de hoy.
Recuerde que puede ver y gestionar este listado desde la aplicación.

Que tenga un buen día. 

Equipo Todoc');
                return $mailer->send($message);
            } //Para enviar un mail al medico con el ical 
            elseif ($this->getTipeMail() == 9) {
                $this->setDataForEmailNine();
                $icsData = $this->buildIcsData();
                $message = \Swift_Message::newInstance()
                    ->setSubject('Paciente: ' . $this->getGduPatient()->getName())
                    ->setFrom(array('no-reply@todoc.co' => 'no-reply@todoc.co'))
                    ->setTo(array($this->getGduDoctor()->getEmail() => $this->getName()))
                    ->attach(\Swift_Attachment::newInstance($icsData, 'invite.ics', 'text/calendar'))
                    ->setBody($this->getTextDay() . ' ' . $this->subjectPersonalTitle . ' ' . $this->getNameDoctor() . ',

El día ' . $this->getDayLetter() . ' ' . $this->getDay() . ' de ' . $this->getMonth() . ' de ' . $this->getYear() . ' tiene una cita con el paciente ' . $this->getGduPatient()->getName() . ' a las ' . $this->getTime() . '.
Si desea agregarla a su agenda haga doble click en el archivo adjunto.

Muchas gracias

Equipo Customer Care
Todoc');
                return $mailer->send($message);
            } //Recomendar app
            elseif ($this->getTipeMail() == 10) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Te recomiendo Todoc. A mi me sirve muchísimo!')
                    ->setFrom(array('no-reply@todoc.co' => $this->subjectPersonalTitle . ' ' . $this->getNameDoctor() . ' ' . $this->getLastNameDoctor()))
                    ->setTo(array($this->getEmail() => $this->getName()))
                    ->setReplyTo('milagros@todoc.co')
                    ->setBody('Hola ' . $this->getName() . '

Te ha llegado este correo porque ' . $this->getNameDoctor() . ' ' . $this->getLastNameDoctor() . ' quiere recomendarte Todoc para que mantengas a tus pacientes informados sobre sus próximas citas. Esto reducirá sus ausencias y el tiempo al teléfono llamándolos o escribiéndoles por Whatsapp.
Además, podrás ver estadísticas útiles para la toma de decisiones informadas y te llegará un informe diario con la agenda del día.

¿Por qué ' . $this->getNameDoctor() . ' ' . $this->getLastNameDoctor() . ' te lo recomienda? Por que le está sirviendo mucho y sus pacientes están felices con el cambio!

Descárgate Todoc ahora mismo en tu teléfono Android o iPhone y empieza a usarlo. Tienes 50 citas gratis mensuales!
https://play.google.com/store/apps/details?id=co.todoc.android&hl=en. Si necesitas ayuda, al pie de este mensaje está nuestro número de contacto. 

En iPhone lo encuentras como TodocApp.

Gracias y esperamos verte entrar a la era digital pronto!

Milagros Pérez Diez
ToDoc
www.todoc.co
Tel: (+57) 300 831 6761
Skype: milagrospd');
                return $mailer->send($message);
            } else {
                return false;
            }
        }
    }

    /**
     * @param $doctorName
     * @param $currentAppointments
     * @param $appointmentLimit
     * @param $emailsNotificationAppointmentsPercent
     * @param $notificationAppointmentsPercent
     * @return int
     */
    public function sendNotificationAppointmentPercent($doctorName, $currentAppointments, $appointmentLimit,
                                                       $emailsNotificationAppointmentsPercent, $notificationAppointmentsPercent)
    {
        $transport = \Swift_SmtpTransport::newInstance()
            ->setHost('smtp.gmail.com')
            ->setPort('587')
            ->setEncryption('tls')
            ->setUsername('no-reply@todoc.co')
            ->setPassword('usuarios14');
        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance()
            ->setSubject('Notificacion de citas')
            ->setFrom(array('no-reply@todoc.co' => 'No reply TODOC'))
            ->setTo($emailsNotificationAppointmentsPercent)
            ->setBody('Hola, el usuario ' . $doctorName . ' ha consumido el ' . $notificationAppointmentsPercent . '% de las citas asignadas.
Citas actuales: ' . $currentAppointments . '
Limite de citas: ' . $appointmentLimit);
        return $mailer->send($message);
    }

    public function getTextDay()
    {
        $now = new \DateTime('now');
        if ($now->format('H') > '0' && $now->format('H') < '12') {
            return 'Buenos dias';
        } elseif ($now->format('H') > '11' && $now->format('H') < '7') {
            return 'Buenas tardes';
        } else {
            return 'Buenas noches';
        }
    }

    private function emailVerification($mail)
    {
        $syntax = '#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if (preg_match($syntax, $mail)) {
            return true;
        } else {
            return false;
        }
    }
}
