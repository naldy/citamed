<?php

namespace BwStudios\CitaMed\Utility;

//require_once dirname(__FILE__).'/../../../../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class PdfGenerator
{
    /**
     * @param $fullDoctorName
     * @param $fullDate
     * @param $appointments
     * @return bool
     */
    public function pdfGenerator($fullDoctorName, $fullDate, $appointments)
    {
        try {
            ob_start();
            include dirname(__DIR__) . '/ApiBundle/Resources/views/pdfTemplate/pdfTemplate.php';
            $content = ob_get_clean();

            $html2pdf = new \HTML2PDF('P', array(216, 279), 'es', true, 'UTF-8',array(15,25,15,15));

            $html2pdf->pdf->SetFont('roboto', '', '14', '', false);
            $html2pdf->pdf->SetFont('robotob', '', '14', '', false);
            $html2pdf->pdf->SetFont('robotob', '', '28', '', false);
            $html2pdf->writeHTML($content);
            $html2pdf->Output(dirname(__FILE__) . '/pdfTemp/RecordatorioCitas.pdf', 'F');
            return true;
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            error_log($formatter->getHtmlMessage());
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}