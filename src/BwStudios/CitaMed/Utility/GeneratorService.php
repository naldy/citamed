<?php

namespace BwStudios\CitaMed\Utility;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GeneratorService
{
	public function passwordGenerator(){
          
        $code = substr(md5(uniqid(rand(), true)), 0, 10);
        return $code;

    }
   
} 