<?php

namespace BwStudios\CitaMed\Constant;

class Constant
{

    //PARAMETERS_INTO_ENTITY(The Column)
    const USER_TYPE_DESCRIPTION = 'description';
    const EMAIL_DESCRIPTION = 'email';
    const SPECIALITY_DESCRIPTION = 'description';
    const GENDER_DESCRIPTION = 'description';
    const DOCUMENT_TYPE_DESCRIPTION = 'description';
    const RELATION_SHIP_DESCRIPTION = 'description';
    const GENERAL_DESCRIPTION_COLUMN = 'description';
    const EPS_DESCRIPTION = 'description';
    const IDENTITY_DESCRIPTION = 'identity';
    const GENERAL_ID_COLUMN = 'id';
    const DATE_DESCRIPTION = 'id';
    const PLACE_DESCRIPTION = 'id';
    const USER_ONE_CONNECTION_COLUMN = 'userOneId';
    const USER_TWO_CONNECTION_COLUMN = 'userTwoId';
    const USER_THREE_CONNECTION_COLUMN = 'userThreeId';
    const EXTRA_DATA_USER_ID = 'extraDataUserId';
    const GENERAL_DATA_USER_ID = 'generalDataUserId';

    /*JSON INFORMATION (PARAMETERS)*/

    //USER_INFORMATION
    const USER_ID = 'userId';
    const DOCTOR_ID = 'doctorId';
    const DOCTOR_NAME = 'doctorName';
    const DOCTOR_LAST_NAME = 'doctorLastName';
    const PATIENT_NAME = 'patientName';
    const PATIENT_LAST_NAME = 'patientLastName';
    const EMAIL_ARRAY = 'emailarray';
    const NAME = 'name';
    const LAST_NAME = 'lastName';
    const EMAIL = 'email';
    const CELL_PHONE = 'cellPhone';
    const HOME_PHONE = 'homePhone';
    const OLD_PASSWORD = 'oldPassword';
    const NEW_PASSWORD = 'newPassword';
    const USER_TYPE_ID = 'userTypeId';

    //OTHER ENTITIES
    const USER_TYPE = 'userType';
    const DOCUMENT_TYPE = 'documentType';
    const GENDER = 'gender';
    const GENDER_ID = 'genderId';
    const RELATION_SHIP = 'relationShip';
    const SPECIALITY = 'speciality';
    const PLACE_NAME = 'placeName';
    const USERNAME = 'username';

    //EXTRADATAUSER_INFORMATION
    const IDENTITY = 'identity';
    const BIRTH_DATE = 'birthDate';
    const PASSWORD = 'password';
    const EMERGENCY_NAME = 'emergencyName';
    const EMERGENCY_NUMBER = 'emergencyNumber';
    const URGENT = 'urgent';
    const COMMENT_EXTRA_DATA_USER = 'comment';
    const ARRIVAL_TIME = 'arrivalTime';
    const DEPART_TIME = 'departTime';
    const PHOTO = 'photo';
    const PERSONAL_TITLE = 'personalTitle';

    //PLACE
    const ADDRESS = 'address';
    const PHONE = 'phone';
    const IS_EPS = 'isEps';
    const IS_PRIVATE = 'isPrivate';
    const EPS_ID = 'epsId';
    const EPS_ARRAY = 'epsArray';
    const EPS = 'eps';
    const EPS_NAME = 'epsName';

    const PLACES = 'places';
    const PLACE = 'place';
    const PLACE_ID = 'placeId';
    const PLACE_ARRAY = 'placeArray';

    const USER_PLACE_CONNECTION_ID = 'userPlaceConnectionId';
    const USER_PLACE_USER_ID = 'userId';
    const USER_EPS_CONNECTION = 'userEpsConnection';


    //DATE
    const DATE = 'date';
    const TIME_FRAME = 'timeFrame';
    const ASSIGNED_TIME = 'assignedTime';
    const COMMENT_DATE = 'comment';
    const TIME_EXPIRED_NO_ATTEND = 24;
    const DATE_ID = 'dateId';
    const DATE_MONTH = 'dateMonth';
    const DATE_YEAR = 'dateYear';
    const DATE_USER_ONE_ID = 'userOneId';
    const CREATION_DATE = 'creationDate';
    const IS_CANCELED = 'canceled';
    const IS_EDIT = 'isEdit';

    //TIME FOR DATE
    const MID_DAY = '12:00:00';
    const TIME = 'time';

    //STATISTIC VALUES
    const CURRENT_DATE = 'currentDate';
    const TOTAL_APPOINTMENTS_TODAY = 'totalAppointmentsToday';
    const TOTAL_USERS_ATTENDED = 'totalUsersAttended';
    const TOTAL_USER_CANCELED = 'totalCanceledUsers';
    const TOTAL_NOT_ARRIVED_USERS = 'totalNotArrivedUsers';
    const TOTAL_ONE_TIME_PATIENTS = 'totalOneTimePatients';
    const TOTAL_PATIENTS = 'totalPatients';
    const PATIENT_NUMBER = 'patientNumber';
    const AGE = 'age';

    //STATES
    const CONFIRMED = 'confirmed';
    const CANCELED = 'canceled';
    const ATTENDED = 'attended';
    const ARRIVED = 'arrived';
    //DATE APPOINTMENT USER
    Const USER_ONE_ID = 'userOneId';
    const USER_TWO_ID = 'userTwoId';
    Const USER_THREE_ID = 'userThreeId';
    //PAYMENT
    const PAYMENT_METHOD = 'paymentMethod';
    const IS_PAID = 'isPaid';
    const AMOUNT = 'amount';
    const EPS_PAYMENT_CODE = 'paymentCodeBonus';
    const EPS_BONUS = 'eps bonus';

    /*JSON_RESPONSE*/

    //RESPONSE_GENERAL_NAME
    const STATUS_NAME = 'status';
    const STATUS = 'status';
    const MESSAGE_NAME = 'message';
    const MESSAGE = 'message';

    //RESPONSE_SUCCESSFUL & //RESPONSE_NOT_SUCCESSFUL(CREATE USER)
    const STATUS_SUCCESSFUL = 'successful';
    const STATUS_NOT_SUCCESSFUL = 'not successful';

    //RESPONSE_SUCCESSFUL
    const STATUS_EMAIL_SEND = 'email send';
    const STATUS_NOT_EMAIL_SEND = 'not send';

    //RESPONSE_SUCCESSFUL & //RESPONSE_NOT_SUCCESSFUL(GET USER)
    const STATUS_FOUND = 'data found';
    const STATUS_NOT_FOUND = 'not found';
    const STATUS_DOCUMENT = 'document';
    const STATUS_CORREO = 'correo';

    //RESPONSE_NO_AUTH & RESPONSE_PROBLEM
    const STATUS_DESCRIPTION = 'failure';
    const MESSAGE_ERROR_SERVER = 'internal server error';
    const MESSAGE_NOT_AUTHENTICATED = 'request not authenticated';

    //VALIDATE_LOGIN:ANSWER
    const VALIDATE_BOOL_T = 'true';
    const VALIDATE_BOOL_F = 'false';
    const VALIDATE = 'validate';

    //VALIDATE_JSON
    const WRONG_JSON_FORMATTER = 'Invalid';

    /*EXTRA PARAMETERS */

    //PARAMETERS_FOR_USERS
    const IDENTITY_USER_TYPE_DOCTOR = 'identityUserTypeDoctor';
    const EMAIL_USER_TYPE_ASSISTANT = 'emailUserTypeAssistant';
    const IDENTITY_USER_TYPE_PATIENT = 'identityUserTypePatient';


    //DIFERENTS USERS TYPES
    const USER_TYPE_DOCTOR = 'doctor';
    const USER_TYPE_ASSISTANT = 'assistant';
    const USER_TYPE_PATIENT = 'patient';
    const USER_TYPE_RELATIVE = 'relative';

    //DIFFERENT GENDER DESCRIPTION
    const MALE_GENDER = 'm';
    const FEMALE_GENDER = 'f';


    //ENTITIES_CONSTANTS
    const ENTITY_USER = 'Entity:User';
    const ENTITY_USER_TYPE = 'Entity:UserType';
    const ENTITY_EXTRA_DATA_USER = 'Entity:ExtraDataUser';
    const ENTITY_GENERAL_DATA_USER = 'Entity:GeneralDataUser';
    const ENTITY_PLACE = 'Entity:Place';
    const ENTITY_SPECIALITY = 'Entity:Speciality';
    const ENTITY_DOCUMENT_TYPE = 'Entity:DocumentType';
    const ENTITY_RELATION_SHIP = 'Entity:RelationShip';
    const ENTITY_GENDER = 'Entity:Gender';
    const ENTITY_EPS = 'Entity:Eps';
    const ENTITY_USER_CONNECTION_ONE_TWO = 'Entity:UserConnectionOneTwo';
    const ENTITY_USER_CONNECTION_ONE_THREE = 'Entity:UserConnectionOneThree';
    const ENTITY_APPOINTMENT = 'Entity:Appointment';
    const ENTITY_USER_CONNECTION_THREE_FOUR = 'Entity:UserConnectionThreeFour';
    const ENTITY_PLACE_CONNECTION = 'Entity:UserPlaceConnection';
    const ENTITY_DEVICE = 'Entity:Device';
    const ENTITY_USER_EPS_CONNECTION = 'Entity:UserEpsConnection';
    const ENTITY_PAYMENT_METHOD = 'Entity:PaymentMethod';
    const ENTITY_PAYMENT = 'Entity:Payment';
    const ENTITY_LOGIN = 'Entity:Login';
    const ENTITY_NOTIFICATION = 'Entity:Notification';
    const ENTITY_NOTIFICATION_TYPE = 'Entity:NotificationType';
    const ENTITY_MEMBERSHIP = 'Entity:Membership';
    const ENTITY_CALENDAR = 'Calendar:Calendar';
    const ENTITY_CALENDAR_EVENT = 'Calendar:CalendarEvent';
    const ENTITY_COUNTRY = 'Entity:Country';
    const ENTITY_TRANSACTION = 'Entity:Transaction';

    /*Entity NotificationType*/
    const NOTIFICATION_TYPE_TYPE = 'type';
    const NOTIFICATION_TYPE_IS_SEND = 'isSend';
    const NOTIFICATION_TYPE_APPOINTMENT = 'appointment';

    const DEFAULT_IDENTITY = 'c.c.';
    const DEFAULT_EPS = '16';
    const EMAILS_RECOMMEND = 'toRecommend';
    const OWE = 'owe';
    const IS_OWE = 'isOwe';
    const SEND_NOTIFICATIONS = 'sendNotifications';
    const NEW_DATE = 'newDate';
    const NEW_ASSIGNED_TIME = 'newAssignedTime';
    const NEW_PLACE_ID = 'newPlaceId';
    const NEW_EPS_ID = 'newEpsId';
    const APPOINTMENT = 'appointment';
    const TYPE = 'type';
    const IS_SEND = 'isSend';
    const USER = 'user';
    const APPOINTMENTS_LIMIT = 'appointmentsLimit';
    const TYPE_DOCTOR = 1;
    const TYPE_ASSISTANT = 2;
    const APPOINTMENT_ID = 'appointmentId';
    const IS_GOOGLE_TOKEN = 'isGoogleToken';
    const IS_OUTLOOK_TOKEN = 'isOutlookToken';
    const ACCESS_TOKEN = 'access_token';
    const TOKEN_TYPE = 'token_type';
    const EXPIRES_IN = 'expires_in';
    const REFRESH_TOKEN = 'refresh_token';
    const ID_TOKEN = 'id_token';
    const CREATED = 'created';
    const ID = 'id';
    const USER_EMAIL = 'userEmail';
    const EVENT_ID = 'eventId';
    const CANCELED_BY_PATIENT = 'canceledByPatient';
    const PREVIOUS_DATE = 'previousDate';
    const COMMENTS = 'comments';
    const COUNTRY = 'country';
    const DESCRIPTION = 'description';
    const CODE = 'code';

    //Para sincronizacion
    const _ID = 'Id';
    const _USER_TYPE_ID = 'UserTypeId';
    const _OWNER_USER_ID = 'OwnerUserId';
    const _FIRST_NAME = 'FirstName';
    const _LAST_NAME = 'LastName';
    const _DATE = 'Date';
    const _USER = 'User';
    const _NAME = 'Name';
    const _SPECIALIZATIONS = 'Specializations';
    const _HEALTH_PROMOTERS = 'HealthPromoters';
    const _PATIENTS = 'Patients';
    const _APPOINTMENTS = 'Appointments';
    const _APPOINTMENTS_BY_NOTIFICATION = 'AppointmentsByNotification';
    const _USER_ID = 'UserId';
    const _EMAIL = 'Email';
    const _CELLPHONE = 'CellPhone';
    const _HOMEPHONE = 'HomePhone';
    const _GENDER_ID = 'GenderId';
    const _BIRTH_DATE = 'BirthDate';
    const _DOCUMENT_TYPE_ID = 'DocumentTypeId';
    const _DOCUMENT_ID = 'DocumentId';
    const _EMERGENCY_NAME = 'EmergencyName';
    const _EMERGENCY_NUMBER = 'EmergencyNumber';
    const _COMMENTS = 'Comments';
    const _PATIENT_ID = 'PatientId';
    const _PLACE_ID = 'PlaceId';
    const _HEALTH_PROMOTER_ID = 'HealthPromoterId';
    const _ASSIGNED_TIME = 'AssignedTime';
    const _SEND_NOTIFICATIONS = 'SendNotifications';
    const _CONFIRMED = 'Confirmed';
    const _CANCELED = 'Canceled';
    const _CANCELED_BY_PATIENT = 'CanceledByPatient';
    const _ATTENDED = 'Attended';
    const _APPOINTMENT_ID = 'AppointmentId';
    const _NOTIFICATION_ID = 'NotificationId';
    const _NOTIFICATION_TYPE_ID = 'NotificationTypeId';
    const _IS_SEND = 'IsSent';
    const _PASSWORD = 'Password';
    const _TEMPORARY_PASSWORD = 'TemporaryPassword';
    const _PERSONAL_TITLE_ID = 'PersonalTitleId';
    const _ARRIVAL_TIME = 'ArrivalTime';
    const _DEPART_TIME = 'DepartTime';
    const _SPECIALIZATION_ID = 'SpecializationId';
    const _TOKEN = 'Token';
    const _FULL_NAME = 'FullName';
    const _ADDRESS = 'Address';
    const _PHONE = 'Phone';
    const _PLACES = 'Places';
    const _EPS = 'Eps';
    const _LOCAL_ID = 'LocalId';
    const _PATIENT_LOCAL_ID = "PatientLocalId";
    const _CODE = 'Code';
    const _TEXT = 'Text';
    const _ERROR = 'Error';
    const _ERRORS = 'Errors';
    const _SINGLE_DATA = 'SingleData';
    const _NEW_PASSWORD = 'NewPassword';
    const _ACTIVE = 'Active';
    const _ASSISTANTS = 'Assistants';
    const _EXPIRATION_DATE = 'ExpirationDate';
    const _IS_PAID = 'IsPaid';
    const _APPOINTMENTS_LIMIT = 'AppointmentsLimit';
    const _APPOINTMENTS_COUNT = 'AppointmentsCount';
    const _MEMBERSHIP = 'Membership';
    const _RECOMMENDEDS = 'Recommendeds';
    const _DESCRIPTION = 'Description';
    const _COUNTRIES = 'Countries';
    const _COUNTRY_ID = 'CountryId';
    const _TRANSACTION_ID = 'TransactionId';
}
