<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="User")
 * @ORM\Entity
 */
class User
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var UserType @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\UserType", inversedBy="user")
     * @ORM\JoinColumn(name="user_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userTypeId;

    /**
     *
     * @var GeneralDataUser @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\GeneralDataUser", inversedBy="user")
     * @ORM\JoinColumn(name="general_data_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $generalDataUserId;

    /**
     *
     * @var ExtraDataUser @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\ExtraDataUser", inversedBy="user" )
     * @ORM\JoinColumn(name="extra_data_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $extraDataUserId;

    /**
     *
     * @var boolean @ORM\Column(name="temporary", type="boolean")
     */
    private $temporary;

    /**
     *
     * @ORM\Column(name="creation_date", type="datetime",nullable=true)
     */
    private $creationDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="sync_date", type="datetime", nullable=false)
     */
    private $sync_date;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set temporary
     *
     * @param boolean $temporary
     *
     * @return User
     */
    public function setTemporary($temporary)
    {
        $this->temporary = $temporary;

        return $this;
    }

    /**
     * Get temporary
     *
     * @return boolean
     */
    public function getTemporary()
    {
        return $this->temporary;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return User
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set syncDate
     *
     * @param \DateTime $syncDate
     *
     * @return User
     */
    public function setSyncDate($syncDate)
    {
        $this->sync_date = $syncDate;

        return $this;
    }

    /**
     * Get syncDate
     *
     * @return \DateTime
     */
    public function getSyncDate()
    {
        return $this->sync_date;
    }

    /**
     * Set userTypeId
     *
     * @param \BwStudios\CitaMed\Entity\UserType $userTypeId
     *
     * @return User
     */
    public function setUserTypeId(\BwStudios\CitaMed\Entity\UserType $userTypeId = null)
    {
        $this->userTypeId = $userTypeId;

        return $this;
    }

    /**
     * Get userTypeId
     *
     * @return \BwStudios\CitaMed\Entity\UserType
     */
    public function getUserTypeId()
    {
        return $this->userTypeId;
    }

    /**
     * Set generalDataUserId
     *
     * @param \BwStudios\CitaMed\Entity\GeneralDataUser $generalDataUserId
     *
     * @return User
     */
    public function setGeneralDataUserId(\BwStudios\CitaMed\Entity\GeneralDataUser $generalDataUserId = null)
    {
        $this->generalDataUserId = $generalDataUserId;

        return $this;
    }

    /**
     * Get generalDataUserId
     *
     * @return \BwStudios\CitaMed\Entity\GeneralDataUser
     */
    public function getGeneralDataUserId()
    {
        return $this->generalDataUserId;
    }

    /**
     * Set extraDataUserId
     *
     * @param \BwStudios\CitaMed\Entity\ExtraDataUser $extraDataUserId
     *
     * @return User
     */
    public function setExtraDataUserId(\BwStudios\CitaMed\Entity\ExtraDataUser $extraDataUserId = null)
    {
        $this->extraDataUserId = $extraDataUserId;

        return $this;
    }

    /**
     * Get extraDataUserId
     *
     * @return \BwStudios\CitaMed\Entity\ExtraDataUser
     */
    public function getExtraDataUserId()
    {
        return $this->extraDataUserId;
    }
}
