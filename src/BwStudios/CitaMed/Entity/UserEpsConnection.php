<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserEpsConnection
 *
 * @ORM\Table(name="UserEpsConnection")
 * @ORM\Entity
 */
class UserEpsConnection
{

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var ExtraDataUser @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\ExtraDataUser")
     * @ORM\JoinColumn(name="extra_data_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $extraDataUserId;

    /**
     *
     * @var Eps @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Eps")
     * @ORM\JoinColumn(name="eps_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $epsId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set extraDataUserId
     *
     * @param \BwStudios\CitaMed\Entity\ExtraDataUser $extraDataUserId
     *
     * @return UserEpsConnection
     */
    public function setExtraDataUserId(\BwStudios\CitaMed\Entity\ExtraDataUser $extraDataUserId = null)
    {
        $this->extraDataUserId = $extraDataUserId;

        return $this;
    }

    /**
     * Get extraDataUserId
     *
     * @return \BwStudios\CitaMed\Entity\ExtraDataUser
     */
    public function getExtraDataUserId()
    {
        return $this->extraDataUserId;
    }

    /**
     * Set epsId
     *
     * @param \BwStudios\CitaMed\Entity\Eps $epsId
     *
     * @return UserEpsConnection
     */
    public function setEpsId(\BwStudios\CitaMed\Entity\Eps $epsId = null)
    {
        $this->epsId = $epsId;

        return $this;
    }

    /**
     * Get epsId
     *
     * @return \BwStudios\CitaMed\Entity\Eps
     */
    public function getEpsId()
    {
        return $this->epsId;
    }
}
