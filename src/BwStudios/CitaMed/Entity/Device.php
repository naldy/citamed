<?php
/**
 * Created by PhpStorm.
 * User: ingridperez
 * Date: 7/04/16
 * Time: 4:37 PM
 */

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Device")
 */
class Device
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="token_register")
     */
    protected $tokenRegister;

    /**
     * @ORM\Column(type="string", name="user_email")
     */
    protected $userEmail;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tokenRegister
     *
     * @param string $tokenRegister
     *
     * @return Device
     */
    public function setTokenRegister($tokenRegister)
    {
        $this->tokenRegister = $tokenRegister;

        return $this;
    }

    /**
     * Get tokenRegister
     *
     * @return string
     */
    public function getTokenRegister()
    {
        return $this->tokenRegister;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     *
     * @return Device
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }
}
