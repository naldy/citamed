<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraDataUser
 *
 * @ORM\Table(name="ExtraDataUser")
 * @ORM\Entity
 */
class ExtraDataUser
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var \DateTime @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    private $birthDate;

    /**
     *
     * @var DocumentType @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\DocumentType")
     * @ORM\JoinColumn(name="document_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $documentTypeId;

    /**
     *
     * @var string @ORM\Column(name="identity", type="string" ,nullable=true)
     */
    private $identity;

    /**
     *
     * @var Gender @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Gender")
     * @ORM\JoinColumn(name="gender_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $genderId;

    /**
     *
     * @var string @ORM\Column(name="emergency_name", type="string", nullable=true)
     */
    private $emergencyName;

    /**
     *
     * @var integer @ORM\Column(name="emergency_number", type="string", nullable=true)
     */
    private $emergencyNumber;

    /**
     *
     * @var boolean @ORM\Column(name="urgent", type="boolean", nullable=true)
     */
    private $urgent;

    /**
     *
     * @var RelationShip @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\RelationShip")
     * @ORM\JoinColumn(name="relation_ship_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $relationShipId;

    /**
     *
     * @var Speciality @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Speciality")
     * @ORM\JoinColumn(name="speciality_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $specialityId;

    /**
     *
     * @var boolean @ORM\Column(name="personal_title", type="string", nullable=true)
     */
    private $personalTitle;

    /**
     *
     * @var string @ORM\Column(name="password", type="string", nullable=true)
     */
    private $password;

    /**
     *
     * @var boolean @ORM\Column(name="is_banned", type="boolean", nullable=true)
     */
    private $isBanned;

    /**
     *
     * @var boolean @ORM\Column(name="is_approved", type="boolean", nullable=true)
     */
    private $isApproved;

    /**
     *
     * @var boolean @ORM\Column(name="is_first_access", type="boolean", nullable=true)
     */
    private $isFirstAccess;

    /**
     *
     * @var time @ORM\Column(name="arrival_time", type="time", nullable=true)
     */
    private $arrivalTime;

    /**
     *
     * @var time @ORM\Column(name="depart_time", type="time", nullable=true)
     */
    private $departTime;

    /**
     *
     * @var string @ORM\Column(name="comment", type="string", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="extraDataUserId")
     */
    protected $user;

    /**
     *
     * @var bigint @ORM\Column(name="photo", type="bigint", nullable=true)
     */
    private $photo;

    /**
     *
     * @var string @ORM\Column(name="mailDay", type="string", nullable=true)
     */
    private $mailDays;

    /**
     *
     * @var string @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     *
     * @var Country @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return ExtraDataUser
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set identity
     *
     * @param string $identity
     *
     * @return ExtraDataUser
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Get identity
     *
     * @return string
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set emergencyName
     *
     * @param string $emergencyName
     *
     * @return ExtraDataUser
     */
    public function setEmergencyName($emergencyName)
    {
        $this->emergencyName = $emergencyName;

        return $this;
    }

    /**
     * Get emergencyName
     *
     * @return string
     */
    public function getEmergencyName()
    {
        return $this->emergencyName;
    }

    /**
     * Set emergencyNumber
     *
     * @param string $emergencyNumber
     *
     * @return ExtraDataUser
     */
    public function setEmergencyNumber($emergencyNumber)
    {
        $this->emergencyNumber = $emergencyNumber;

        return $this;
    }

    /**
     * Get emergencyNumber
     *
     * @return string
     */
    public function getEmergencyNumber()
    {
        return $this->emergencyNumber;
    }

    /**
     * Set urgent
     *
     * @param boolean $urgent
     *
     * @return ExtraDataUser
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;

        return $this;
    }

    /**
     * Get urgent
     *
     * @return boolean
     */
    public function getUrgent()
    {
        return $this->urgent;
    }

    /**
     * Set personalTitle
     *
     * @param string $personalTitle
     *
     * @return ExtraDataUser
     */
    public function setPersonalTitle($personalTitle)
    {
        $this->personalTitle = $personalTitle;

        return $this;
    }

    /**
     * Get personalTitle
     *
     * @return string
     */
    public function getPersonalTitle()
    {
        return $this->personalTitle;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return ExtraDataUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     *
     * @return ExtraDataUser
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     *
     * @return ExtraDataUser
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * Set isFirstAccess
     *
     * @param boolean $isFirstAccess
     *
     * @return ExtraDataUser
     */
    public function setIsFirstAccess($isFirstAccess)
    {
        $this->isFirstAccess = $isFirstAccess;

        return $this;
    }

    /**
     * Get isFirstAccess
     *
     * @return boolean
     */
    public function getIsFirstAccess()
    {
        return $this->isFirstAccess;
    }

    /**
     * Set arrivalTime
     *
     * @param \DateTime $arrivalTime
     *
     * @return ExtraDataUser
     */
    public function setArrivalTime($arrivalTime)
    {
        $this->arrivalTime = $arrivalTime;

        return $this;
    }

    /**
     * Get arrivalTime
     *
     * @return \DateTime
     */
    public function getArrivalTime()
    {
        return $this->arrivalTime;
    }

    /**
     * Set departTime
     *
     * @param \DateTime $departTime
     *
     * @return ExtraDataUser
     */
    public function setDepartTime($departTime)
    {
        $this->departTime = $departTime;

        return $this;
    }

    /**
     * Get departTime
     *
     * @return \DateTime
     */
    public function getDepartTime()
    {
        return $this->departTime;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ExtraDataUser
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set photo
     *
     * @param integer $photo
     *
     * @return ExtraDataUser
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return integer
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set mailDays
     *
     * @param string $mailDays
     *
     * @return ExtraDataUser
     */
    public function setMailDays($mailDays)
    {
        $this->mailDays = $mailDays;

        return $this;
    }

    /**
     * Get mailDays
     *
     * @return string
     */
    public function getMailDays()
    {
        return $this->mailDays;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return ExtraDataUser
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set documentTypeId
     *
     * @param \BwStudios\CitaMed\Entity\DocumentType $documentTypeId
     *
     * @return ExtraDataUser
     */
    public function setDocumentTypeId(\BwStudios\CitaMed\Entity\DocumentType $documentTypeId = null)
    {
        $this->documentTypeId = $documentTypeId;

        return $this;
    }

    /**
     * Get documentTypeId
     *
     * @return \BwStudios\CitaMed\Entity\DocumentType
     */
    public function getDocumentTypeId()
    {
        return $this->documentTypeId;
    }

    /**
     * Set genderId
     *
     * @param \BwStudios\CitaMed\Entity\Gender $genderId
     *
     * @return ExtraDataUser
     */
    public function setGenderId(\BwStudios\CitaMed\Entity\Gender $genderId = null)
    {
        $this->genderId = $genderId;

        return $this;
    }

    /**
     * Get genderId
     *
     * @return \BwStudios\CitaMed\Entity\Gender
     */
    public function getGenderId()
    {
        return $this->genderId;
    }

    /**
     * Set relationShipId
     *
     * @param \BwStudios\CitaMed\Entity\RelationShip $relationShipId
     *
     * @return ExtraDataUser
     */
    public function setRelationShipId(\BwStudios\CitaMed\Entity\RelationShip $relationShipId = null)
    {
        $this->relationShipId = $relationShipId;

        return $this;
    }

    /**
     * Get relationShipId
     *
     * @return \BwStudios\CitaMed\Entity\RelationShip
     */
    public function getRelationShipId()
    {
        return $this->relationShipId;
    }

    /**
     * Set specialityId
     *
     * @param \BwStudios\CitaMed\Entity\Speciality $specialityId
     *
     * @return ExtraDataUser
     */
    public function setSpecialityId(\BwStudios\CitaMed\Entity\Speciality $specialityId = null)
    {
        $this->specialityId = $specialityId;

        return $this;
    }

    /**
     * Get specialityId
     *
     * @return \BwStudios\CitaMed\Entity\Speciality
     */
    public function getSpecialityId()
    {
        return $this->specialityId;
    }

    /**
     * Set user
     *
     * @param \BwStudios\CitaMed\Entity\User $user
     *
     * @return ExtraDataUser
     */
    public function setUser(\BwStudios\CitaMed\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set country
     *
     * @param \BwStudios\CitaMed\Entity\Country $country
     *
     * @return ExtraDataUser
     */
    public function setCountry(\BwStudios\CitaMed\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \BwStudios\CitaMed\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
