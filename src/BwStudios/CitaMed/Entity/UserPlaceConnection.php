<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPlaceConnection
 *
 * @ORM\Table(name="UserPlaceConnection")
 * @ORM\Entity
 */
class UserPlaceConnection
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userId;

    /**
     *
     * @var Place @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Place")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $placeId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param \BwStudios\CitaMed\Entity\User $userId
     *
     * @return UserPlaceConnection
     */
    public function setUserId(\BwStudios\CitaMed\Entity\User $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set placeId
     *
     * @param \BwStudios\CitaMed\Entity\Place $placeId
     *
     * @return UserPlaceConnection
     */
    public function setPlaceId(\BwStudios\CitaMed\Entity\Place $placeId = null)
    {
        $this->placeId = $placeId;

        return $this;
    }

    /**
     * Get placeId
     *
     * @return \BwStudios\CitaMed\Entity\Place
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }
}
