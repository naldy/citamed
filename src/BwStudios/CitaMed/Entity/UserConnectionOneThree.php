<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserConnectionOneThree
 *
 * @ORM\Table(name="UserConnectionOneThree")
 * @ORM\Entity
 */
class UserConnectionOneThree
{
	/**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_one_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userOneId;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_three_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userThreeId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userOneId
     *
     * @param \BwStudios\CitaMed\Entity\User $userOneId
     *
     * @return UserConnectionOneThree
     */
    public function setUserOneId(\BwStudios\CitaMed\Entity\User $userOneId = null)
    {
        $this->userOneId = $userOneId;

        return $this;
    }

    /**
     * Get userOneId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserOneId()
    {
        return $this->userOneId;
    }

    /**
     * Set userThreeId
     *
     * @param \BwStudios\CitaMed\Entity\User $userThreeId
     *
     * @return UserConnectionOneThree
     */
    public function setUserThreeId(\BwStudios\CitaMed\Entity\User $userThreeId = null)
    {
        $this->userThreeId = $userThreeId;

        return $this;
    }

    /**
     * Get userThreeId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserThreeId()
    {
        return $this->userThreeId;
    }
}
