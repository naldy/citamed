<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eps
 *
 * @ORM\Table(name="Eps")
 * @ORM\Entity
 */
class Eps
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     *
     * @var \DateTime @ORM\Column(name="sync_date", type="datetime", nullable=false)
     */
    private $sync_date;

    /**
     *
     * @var Country @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     *
     * @var boolean @ORM\Column(name="is_default", type="boolean", nullable=false, options={"default"=false})
     */
    private $isDefault;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Eps
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set syncDate
     *
     * @param \DateTime $syncDate
     *
     * @return Eps
     */
    public function setSyncDate($syncDate)
    {
        $this->sync_date = $syncDate;

        return $this;
    }

    /**
     * Get syncDate
     *
     * @return \DateTime
     */
    public function getSyncDate()
    {
        return $this->sync_date;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return Eps
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set country
     *
     * @param \BwStudios\CitaMed\Entity\Country $country
     *
     * @return Eps
     */
    public function setCountry(\BwStudios\CitaMed\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \BwStudios\CitaMed\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
