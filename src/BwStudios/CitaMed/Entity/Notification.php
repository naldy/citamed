<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="Notification")
 * @ORM\Entity
 */
class Notification
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Appointment")
     */
    private $appointment;

    /**
     * @ORM\Column(name="date_to_send", type="datetime", nullable=false)
     */
    private $dateToSend;

    /**
     * @ORM\ManyToOne(targetEntity="NotificationType")
     */
    private $type;

    /**
     * @ORM\Column(name="is_send", type="boolean", nullable=false)
     */
    private $isSend;

    /**
     *
     * @var \DateTime @ORM\Column(name="sync_date", type="datetime", nullable=false)
     */
    private $sync_date;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateToSend
     *
     * @param \DateTime $dateToSend
     *
     * @return Notification
     */
    public function setDateToSend($dateToSend)
    {
        $this->dateToSend = $dateToSend;

        return $this;
    }

    /**
     * Get dateToSend
     *
     * @return \DateTime
     */
    public function getDateToSend()
    {
        return $this->dateToSend;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return Notification
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set syncDate
     *
     * @param \DateTime $syncDate
     *
     * @return Notification
     */
    public function setSyncDate($syncDate)
    {
        $this->sync_date = $syncDate;

        return $this;
    }

    /**
     * Get syncDate
     *
     * @return \DateTime
     */
    public function getSyncDate()
    {
        return $this->sync_date;
    }

    /**
     * Set appointment
     *
     * @param \BwStudios\CitaMed\Entity\Appointment $appointment
     *
     * @return Notification
     */
    public function setAppointment(\BwStudios\CitaMed\Entity\Appointment $appointment = null)
    {
        $this->appointment = $appointment;

        return $this;
    }

    /**
     * Get appointment
     *
     * @return \BwStudios\CitaMed\Entity\Appointment
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * Set type
     *
     * @param \BwStudios\CitaMed\Entity\NotificationType $type
     *
     * @return Notification
     */
    public function setType(\BwStudios\CitaMed\Entity\NotificationType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \BwStudios\CitaMed\Entity\NotificationType
     */
    public function getType()
    {
        return $this->type;
    }
}
