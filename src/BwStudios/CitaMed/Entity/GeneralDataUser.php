<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeneralDataUser
 *
 * @ORM\Table(name="GeneralDataUser")
 * @ORM\Entity
 */
class GeneralDataUser
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     *
     * @var string @ORM\Column(name="last_name", type="string", nullable=true)
     */
    private $lastName;

    /**
     *
     * @var string @ORM\Column(name="email", type="string", length=60)
     */
    private $email;

    /**
     *
     * @var integer @ORM\Column(name="cell_phone", type="string", nullable=true)
     */
    private $cellPhone;

    /**
     *
     * @var integer @ORM\Column(name="home_phone", type="string", nullable=true)
     */
    private $homePhone;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="generalDataUserId")
     */
    protected $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GeneralDataUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return GeneralDataUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return GeneralDataUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cellPhone
     *
     * @param integer $cellPhone
     *
     * @return GeneralDataUser
     */
    public function setCellPhone($cellPhone)
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return integer
     */
    public function getCellPhone()
    {
        return $this->cellPhone;
    }

    /**
     * Set user
     *
     * @param \BwStudios\CitaMed\Entity\User $user
     *
     * @return GeneralDataUser
     */
    public function setUser(\BwStudios\CitaMed\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set homePhone
     *
     * @param integer $homePhone
     *
     * @return GeneralDataUser
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return integer
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }
}
