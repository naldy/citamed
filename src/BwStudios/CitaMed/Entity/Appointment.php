<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Appointment
 *
 * @ORM\Table(name="Appointment")
 * @ORM\Entity
 */
class Appointment
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_one_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userOneId;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_three_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userThreeId;

    /**
     *
     * @var \DateTime @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     *
     * @var boolean @ORM\Column(name="confirmed", type="boolean", nullable=true)
     */
    private $confirmed;

    /**
     *
     * @var boolean @ORM\Column(name="waiting_queue", type="boolean", nullable=true)
     */
    private $waitingQueue;

    /**
     *
     * @var boolean @ORM\Column(name="attended", type="boolean", nullable=true)
     */
    private $attended;

    /**
     *
     * @var boolean @ORM\Column(name="canceled", type="boolean", nullable=true)
     */
    private $canceled;

    /**
     *
     * @var boolean @ORM\Column(name="arrived", type="boolean", nullable=true)
     */
    private $arrived;

    /**
     *
     * @var Time @ORM\Column(name="time_frame", type="time", nullable=true)
     */
    private $timeFrame;

    /**
     *
     * @var Time @ORM\Column(name="assigned_time", type="time")
     */
    private $assignedTime;

    /**
     *
     * @var \DateTime @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="confirmation_date", type="datetime", nullable=true)
     */
    private $confirmationDate;

    /**
     *
     * @var integer @ORM\Column(name="priority", type="integer", nullable=true, nullable=true)
     */
    private $priority;

    /**
     *
     * @var string @ORM\Column(name="comment", type="string", length=200, nullable=true)
     */
    private $comment;

    /**
     *
     * @var UserPlaceConnection @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\UserPlaceConnection")
     * @ORM\JoinColumn(name="user_place_connection_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userPlaceConnectionId;

    /**
     *
     * @var eps @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Eps")
     * @ORM\JoinColumn(name="eps_id", referencedColumnName="id")
     */
    private $eps;

    /**
     * @ORM\Column(name="canceled_by_patient", type="boolean", nullable=true)
     */
    private $canceledByPatient;

    /**
     * @ORM\Column(name="send_notifications", type="boolean", nullable=true)
     */
    private $sendNotifications;

    /**
     *
     * @var \DateTime @ORM\Column(name="sync_date", type="datetime", nullable=false)
     */
    private $sync_date;

    /**
     *
     * @var string @ORM\Column(name="g_calendar_id", type="string", nullable=true)
     */
    private $googleCalendarId;

    /**
     *
     * @var string @ORM\Column(name="o_calendar_id", type="string", nullable=true)
     */
    private $outlookCalendarId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Appointment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     *
     * @return Appointment
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set waitingQueue
     *
     * @param boolean $waitingQueue
     *
     * @return Appointment
     */
    public function setWaitingQueue($waitingQueue)
    {
        $this->waitingQueue = $waitingQueue;

        return $this;
    }

    /**
     * Get waitingQueue
     *
     * @return boolean
     */
    public function getWaitingQueue()
    {
        return $this->waitingQueue;
    }

    /**
     * Set attended
     *
     * @param boolean $attended
     *
     * @return Appointment
     */
    public function setAttended($attended)
    {
        $this->attended = $attended;

        return $this;
    }

    /**
     * Get attended
     *
     * @return boolean
     */
    public function getAttended()
    {
        return $this->attended;
    }

    /**
     * Set canceled
     *
     * @param boolean $canceled
     *
     * @return Appointment
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return boolean
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set arrived
     *
     * @param boolean $arrived
     *
     * @return Appointment
     */
    public function setArrived($arrived)
    {
        $this->arrived = $arrived;

        return $this;
    }

    /**
     * Get arrived
     *
     * @return boolean
     */
    public function getArrived()
    {
        return $this->arrived;
    }

    /**
     * Set timeFrame
     *
     * @param \DateTime $timeFrame
     *
     * @return Appointment
     */
    public function setTimeFrame($timeFrame)
    {
        $this->timeFrame = $timeFrame;

        return $this;
    }

    /**
     * Get timeFrame
     *
     * @return \DateTime
     */
    public function getTimeFrame()
    {
        return $this->timeFrame;
    }

    /**
     * Set assignedTime
     *
     * @param \DateTime $assignedTime
     *
     * @return Appointment
     */
    public function setAssignedTime($assignedTime)
    {
        $this->assignedTime = $assignedTime;

        return $this;
    }

    /**
     * Get assignedTime
     *
     * @return \DateTime
     */
    public function getAssignedTime()
    {
        return $this->assignedTime;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Appointment
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set confirmationDate
     *
     * @param \DateTime $confirmationDate
     *
     * @return Appointment
     */
    public function setConfirmationDate($confirmationDate)
    {
        $this->confirmationDate = $confirmationDate;

        return $this;
    }

    /**
     * Get confirmationDate
     *
     * @return \DateTime
     */
    public function getConfirmationDate()
    {
        return $this->confirmationDate;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Appointment
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Appointment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set canceledByPatient
     *
     * @param boolean $canceledByPatient
     *
     * @return Appointment
     */
    public function setCanceledByPatient($canceledByPatient)
    {
        $this->canceledByPatient = $canceledByPatient;

        return $this;
    }

    /**
     * Get canceledByPatient
     *
     * @return boolean
     */
    public function getCanceledByPatient()
    {
        return $this->canceledByPatient;
    }

    /**
     * Set sendNotifications
     *
     * @param boolean $sendNotifications
     *
     * @return Appointment
     */
    public function setSendNotifications($sendNotifications)
    {
        $this->sendNotifications = $sendNotifications;

        return $this;
    }

    /**
     * Get sendNotifications
     *
     * @return boolean
     */
    public function getSendNotifications()
    {
        return $this->sendNotifications;
    }

    /**
     * Set syncDate
     *
     * @param \DateTime $syncDate
     *
     * @return Appointment
     */
    public function setSyncDate($syncDate)
    {
        $this->sync_date = $syncDate;

        return $this;
    }

    /**
     * Get syncDate
     *
     * @return \DateTime
     */
    public function getSyncDate()
    {
        return $this->sync_date;
    }

    /**
     * Set googleCalendarId
     *
     * @param string $googleCalendarId
     *
     * @return Appointment
     */
    public function setGoogleCalendarId($googleCalendarId)
    {
        $this->googleCalendarId = $googleCalendarId;

        return $this;
    }

    /**
     * Get googleCalendarId
     *
     * @return string
     */
    public function getGoogleCalendarId()
    {
        return $this->googleCalendarId;
    }

    /**
     * Set outlookCalendarId
     *
     * @param string $outlookCalendarId
     *
     * @return Appointment
     */
    public function setOutlookCalendarId($outlookCalendarId)
    {
        $this->outlookCalendarId = $outlookCalendarId;

        return $this;
    }

    /**
     * Get outlookCalendarId
     *
     * @return string
     */
    public function getOutlookCalendarId()
    {
        return $this->outlookCalendarId;
    }

    /**
     * Set userOneId
     *
     * @param \BwStudios\CitaMed\Entity\User $userOneId
     *
     * @return Appointment
     */
    public function setUserOneId(\BwStudios\CitaMed\Entity\User $userOneId = null)
    {
        $this->userOneId = $userOneId;

        return $this;
    }

    /**
     * Get userOneId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserOneId()
    {
        return $this->userOneId;
    }

    /**
     * Set userThreeId
     *
     * @param \BwStudios\CitaMed\Entity\User $userThreeId
     *
     * @return Appointment
     */
    public function setUserThreeId(\BwStudios\CitaMed\Entity\User $userThreeId = null)
    {
        $this->userThreeId = $userThreeId;

        return $this;
    }

    /**
     * Get userThreeId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserThreeId()
    {
        return $this->userThreeId;
    }

    /**
     * Set userPlaceConnectionId
     *
     * @param \BwStudios\CitaMed\Entity\UserPlaceConnection $userPlaceConnectionId
     *
     * @return Appointment
     */
    public function setUserPlaceConnectionId(\BwStudios\CitaMed\Entity\UserPlaceConnection $userPlaceConnectionId = null)
    {
        $this->userPlaceConnectionId = $userPlaceConnectionId;

        return $this;
    }

    /**
     * Get userPlaceConnectionId
     *
     * @return \BwStudios\CitaMed\Entity\UserPlaceConnection
     */
    public function getUserPlaceConnectionId()
    {
        return $this->userPlaceConnectionId;
    }

    /**
     * Set eps
     *
     * @param \BwStudios\CitaMed\Entity\Eps $eps
     *
     * @return Appointment
     */
    public function setEps(\BwStudios\CitaMed\Entity\Eps $eps = null)
    {
        $this->eps = $eps;

        return $this;
    }

    /**
     * Get eps
     *
     * @return \BwStudios\CitaMed\Entity\Eps
     */
    public function getEps()
    {
        return $this->eps;
    }
}
