<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserConnectionThreeFour
 *
 * @ORM\Table(name="UserConnectionThreeFour")
 * @ORM\Entity
 */
class UserConnectionThreeFour
{
	/**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_three_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userThreeId;

    /**
     *
     * @var User @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_four_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userFourId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userThreeId
     *
     * @param \BwStudios\CitaMed\Entity\User $userThreeId
     *
     * @return UserConnectionThreeFour
     */
    public function setUserThreeId(\BwStudios\CitaMed\Entity\User $userThreeId = null)
    {
        $this->userThreeId = $userThreeId;

        return $this;
    }

    /**
     * Get userThreeId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserThreeId()
    {
        return $this->userThreeId;
    }

    /**
     * Set userFourId
     *
     * @param \BwStudios\CitaMed\Entity\User $userFourId
     *
     * @return UserConnectionThreeFour
     */
    public function setUserFourId(\BwStudios\CitaMed\Entity\User $userFourId = null)
    {
        $this->userFourId = $userFourId;

        return $this;
    }

    /**
     * Get userFourId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserFourId()
    {
        return $this->userFourId;
    }
}
