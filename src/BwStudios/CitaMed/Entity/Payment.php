<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @ORM\Table(name="Payment")
 * @ORM\Entity
 */
class Payment
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var boolean @ORM\Column(name="is_paid", type="boolean")
     */
    private $isPaid;

    /**
     *
     * @var integer @ORM\Column(name="amount", type="integer") 
     */
    private $amount;

    /**
     *
     * @var integer @ORM\Column(name="owe", type="integer")
     */
    private $owe;

    /**
     *
     * @var PaymentMethod @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $paymentMethod;

    /**
     *
     * @var appointment @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\Appointment")
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appointmentId;

    /**
     *
     * @var string @ORM\Column(name="eps_payment_code", type="string", nullable=true)
     */
    private $epsPaymentCode;

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isPaid
     *
     * @param boolean $isPaid
     *
     * @return Payment
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set owe
     *
     * @param integer $owe
     *
     * @return Payment
     */
    public function setOwe($owe)
    {
        $this->owe = $owe;

        return $this;
    }

    /**
     * Get owe
     *
     * @return integer
     */
    public function getOwe()
    {
        return $this->owe;
    }

    /**
     * Set epsPaymentCode
     *
     * @param string $epsPaymentCode
     *
     * @return Payment
     */
    public function setEpsPaymentCode($epsPaymentCode)
    {
        $this->epsPaymentCode = $epsPaymentCode;

        return $this;
    }

    /**
     * Get epsPaymentCode
     *
     * @return string
     */
    public function getEpsPaymentCode()
    {
        return $this->epsPaymentCode;
    }

    /**
     * Set paymentMethod
     *
     * @param \BwStudios\CitaMed\Entity\PaymentMethod $paymentMethod
     *
     * @return Payment
     */
    public function setPaymentMethod(\BwStudios\CitaMed\Entity\PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \BwStudios\CitaMed\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set appointmentId
     *
     * @param \BwStudios\CitaMed\Entity\Appointment $appointmentId
     *
     * @return Payment
     */
    public function setAppointmentId(\BwStudios\CitaMed\Entity\Appointment $appointmentId = null)
    {
        $this->appointmentId = $appointmentId;

        return $this;
    }

    /**
     * Get appointmentId
     *
     * @return \BwStudios\CitaMed\Entity\Appointment
     */
    public function getAppointmentId()
    {
        return $this->appointmentId;
    }
}
