<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserConnectionOneTwo
 *
 * @ORM\Table(name="UserConnectionOneTwo")
 * @ORM\Entity
 */
class UserConnectionOneTwo
{
	/**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_one_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userOneId;

    /**
     *
     * @var User @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_two_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $userTwoId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userOneId
     *
     * @param \BwStudios\CitaMed\Entity\User $userOneId
     *
     * @return UserConnectionOneTwo
     */
    public function setUserOneId(\BwStudios\CitaMed\Entity\User $userOneId = null)
    {
        $this->userOneId = $userOneId;

        return $this;
    }

    /**
     * Get userOneId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserOneId()
    {
        return $this->userOneId;
    }

    /**
     * Set userTwoId
     *
     * @param \BwStudios\CitaMed\Entity\User $userTwoId
     *
     * @return UserConnectionOneTwo
     */
    public function setUserTwoId(\BwStudios\CitaMed\Entity\User $userTwoId = null)
    {
        $this->userTwoId = $userTwoId;

        return $this;
    }

    /**
     * Get userTwoId
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUserTwoId()
    {
        return $this->userTwoId;
    }
}
