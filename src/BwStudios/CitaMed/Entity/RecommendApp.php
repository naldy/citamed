<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eps
 *
 * @ORM\Table(name="RecommendApp")
 * @ORM\Entity
 */
class RecommendApp
{
    /**
     *
     * @var integer @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var User @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     *
     * @var string @ORM\Column(name="email_recommended", type="string", nullable=true)
     */
    private $emailRecommended;

    /**
     *
     * @var string @ORM\Column(name="name_recommended", type="string")
     */
    private $nameRecommended;

    /**
     *
     * @var string @ORM\Column(name="phone_recommended", type="string", nullable=true)
     */
    private $phoneRecommended;
    
    /**
     *
     * @var string @ORM\Column(name="date_recommended", type="datetime")
     */
    private $dateRecommended;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailRecommended
     *
     * @param string $emailRecommended
     *
     * @return RecommendApp
     */
    public function setEmailRecommended($emailRecommended)
    {
        $this->emailRecommended = $emailRecommended;

        return $this;
    }

    /**
     * Get emailRecommended
     *
     * @return string
     */
    public function getEmailRecommended()
    {
        return $this->emailRecommended;
    }

    /**
     * Set nameRecommended
     *
     * @param string $nameRecommended
     *
     * @return RecommendApp
     */
    public function setNameRecommended($nameRecommended)
    {
        $this->nameRecommended = $nameRecommended;

        return $this;
    }

    /**
     * Get nameRecommended
     *
     * @return string
     */
    public function getNameRecommended()
    {
        return $this->nameRecommended;
    }

    /**
     * Set phoneRecommended
     *
     * @param string $phoneRecommended
     *
     * @return RecommendApp
     */
    public function setPhoneRecommended($phoneRecommended)
    {
        $this->phoneRecommended = $phoneRecommended;

        return $this;
    }

    /**
     * Get phoneRecommended
     *
     * @return string
     */
    public function getPhoneRecommended()
    {
        return $this->phoneRecommended;
    }

    /**
     * Set dateRecommended
     *
     * @param \DateTime $dateRecommended
     *
     * @return RecommendApp
     */
    public function setDateRecommended($dateRecommended)
    {
        $this->dateRecommended = $dateRecommended;

        return $this;
    }

    /**
     * Get dateRecommended
     *
     * @return \DateTime
     */
    public function getDateRecommended()
    {
        return $this->dateRecommended;
    }

    /**
     * Set user
     *
     * @param \BwStudios\CitaMed\Entity\User $user
     *
     * @return RecommendApp
     */
    public function setUser(\BwStudios\CitaMed\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}