<?php

namespace BwStudios\CitaMed\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Membership
 *
 * @ORM\Table(name="Membership")
 * @ORM\Entity
 */
class Membership
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(name="start_period", type="datetime")
     */
    private $startPeriod;

    /**
     * @ORM\Column(name="end_period", type="datetime")
     */
    private $endPeriod;

    /**
     * @ORM\Column(name="payMembership", type="boolean")
     */
    private $payMembership;

    /**
     * @ORM\Column(name="current_appointments", type="integer")
     */
    private $currentAppointments;

    /**
     * @ORM\Column(name="appointments_limit", type="integer")
     */
    private $appointmentsLimit;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startPeriod
     *
     * @param \DateTime $startPeriod
     *
     * @return Membership
     */
    public function setStartPeriod($startPeriod)
    {
        $this->startPeriod = $startPeriod;

        return $this;
    }

    /**
     * Get startPeriod
     *
     * @return \DateTime
     */
    public function getStartPeriod()
    {
        return $this->startPeriod;
    }

    /**
     * Set endPeriod
     *
     * @param \DateTime $endPeriod
     *
     * @return Membership
     */
    public function setEndPeriod($endPeriod)
    {
        $this->endPeriod = $endPeriod;

        return $this;
    }

    /**
     * Get endPeriod
     *
     * @return \DateTime
     */
    public function getEndPeriod()
    {
        return $this->endPeriod;
    }

    /**
     * Set payMembership
     *
     * @param boolean $payMembership
     *
     * @return Membership
     */
    public function setPayMembership($payMembership)
    {
        $this->payMembership = $payMembership;

        return $this;
    }

    /**
     * Get payMembership
     *
     * @return boolean
     */
    public function getPayMembership()
    {
        return $this->payMembership;
    }

    /**
     * Set currentAppointments
     *
     * @param integer $currentAppointments
     *
     * @return Membership
     */
    public function setCurrentAppointments($currentAppointments)
    {
        $this->currentAppointments = $currentAppointments;

        return $this;
    }

    /**
     * Get currentAppointments
     *
     * @return integer
     */
    public function getCurrentAppointments()
    {
        return $this->currentAppointments;
    }

    /**
     * Set appointmentsLimit
     *
     * @param integer $appointmentsLimit
     *
     * @return Membership
     */
    public function setAppointmentsLimit($appointmentsLimit)
    {
        $this->appointmentsLimit = $appointmentsLimit;

        return $this;
    }

    /**
     * Get appointmentsLimit
     *
     * @return integer
     */
    public function getAppointmentsLimit()
    {
        return $this->appointmentsLimit;
    }

    /**
     * Set user
     *
     * @param \BwStudios\CitaMed\Entity\User $user
     *
     * @return Membership
     */
    public function setUser(\BwStudios\CitaMed\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
