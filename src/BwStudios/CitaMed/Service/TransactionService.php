<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Transaction;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class TransactionService
{

    private $repositoryTransaction;
    private $em;

    /**
     * TransactionService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryTransaction = $this->em->getRepository(Constant::ENTITY_TRANSACTION);
    }

    /**
     * @param $criteria
     * @return null|object
     */
    public function findOneBy($criteria)
    {
        try {
            return $this->repositoryTransaction->findOneBy($criteria);
        } catch (ORMException $e) {
            error_log('Exception in findBy - TransactionService');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Transaction $transaction
     * @return bool|object
     */
    public function persistAndFlush(Transaction $transaction)
    {
        try {
            $this->em->persist($transaction);
            $this->em->flush();
            return true;
        } catch (ORMException $e) {
            error_log('Exception in persistAndFlush - TransactionService');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();
        var_dump($object);
        $contents = ob_get_contents();
        ob_end_clean();
        error_log($contents);
    }
}