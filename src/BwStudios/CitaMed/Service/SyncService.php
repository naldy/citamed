<?php

namespace BwStudios\CitaMed\Service;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Eps;
use BwStudios\CitaMed\Entity\Notification;
use BwStudios\CitaMed\Entity\NotificationType;
use BwStudios\CitaMed\Entity\Payment;
use BwStudios\CitaMed\Entity\Speciality;
use BwStudios\CitaMed\Entity\Transaction;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserEpsConnection;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;
use BwStudios\CitaMed\Entity\Place;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use co\todoc\PublicAPIBundle\Service\CalendarService;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Proxies\__CG__\BwStudios\CitaMed\Entity\Membership;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\Entity;


class SyncService
{

    private $em;
    private $specialityService;
    private $epsService;
    private $userService;
    private $patientService;
    private $appointmentService;
    private $dateService;
    private $notificationService;
    private $assistantService;
    private $membershipService;
    private $countryService;
    private $transactionService;

    /**
     * SyncService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;

        //Servicios
        $this->specialityService = new SpecialityService($this->em);
        $this->epsService = new EpsService($this->em);
        $this->userService = new UserService($this->em);
        $this->patientService = new PatientService($this->em);
        $this->appointmentService = new AppointmentService($this->em);
        $this->notificationService = new NotificationService($this->em);
        $this->dateService = new DateService($this->em);
        $this->assistantService = new AssistantService($this->em);
        $this->membershipService = new MembershipService($this->em);
        $this->countryService = new CountryService($this->em);
        $this->transactionService = new TransactionService($this->em);
    }

    /**
     * @param Request $req
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @param $hashAlgorithm
     * @param $notificationAppointmentsPercent
     * @param $emailsNotificationAppointmentsPercent
     * @return object
     */
    public function syncDataV1(Request $req, $freeAppointments, $timeLapseForFreeAppointments, $hashAlgorithm,
                               $notificationAppointmentsPercent, $emailsNotificationAppointmentsPercent)
    {
        try {
            //Lo que llega
            //error_log('Lo que llega');
            //$this->var_error_log($req->request->all());

            //Codigo de sincronizacion
            $transactionIdRequest = $req->request->get(Constant::_TRANSACTION_ID);

            //Fecha para la sincronizacion
            $dateSync = $req->request->get(Constant::_DATE);

            //Informacion del usuario que hace la request
            $dataUser = $req->request->get(Constant::_USER);

            //Informacion de pacientes
            $dataArrayPatients = $req->request->get(Constant::_PATIENTS);

            //Informacion de citas
            $dataArrayAppointments = $req->request->get(Constant::_APPOINTMENTS);

            //Informacion de Asistentes
            $dataArrayAssistants = $req->request->get(Constant::_ASSISTANTS);

            //Informacion de recomendaciones
            $dataArrayRecommendeds = $req->request->get(Constant::_RECOMMENDEDS);

            $dateResponse = null;
            $userResponse = (object)array();
            $arrayHealthPromotersResponse = array();
            $arraySpecializationsResponse = array();
            $arrayPatientsResponse = array();
            $arrayAppointmentsResponse = array();
            $arrayAppointmentsByNotificationsResponse = array();
            $arrayAssistantResponse = array();
            $arrayRecommendsResponse = array();
            $arrayErrorsResponse = array();
            $arrayCountriesResponse = array();
            $membershipInfo = (object)array();

            //Si dateSync y dataUser llegan vacios se tiene que devolver la lista de eps y especializaciones
            if (empty($dateSync) && count($dataUser) == 0) {

                $tempArrayHealthPromotersResponse = $this->epsService->buildArrayHealthPromoters();
                if (isset($tempArrayHealthPromotersResponse->Error)) {
                    return $tempArrayHealthPromotersResponse;
                } else {
                    $arrayHealthPromotersResponse = $tempArrayHealthPromotersResponse;
                }

                $tempArraySpecializationsResponse = $this->specialityService->buildArraySpecializations();
                if (isset($tempArraySpecializationsResponse->Error)) {
                    return $tempArraySpecializationsResponse;
                } else {
                    $arraySpecializationsResponse = $tempArraySpecializationsResponse;
                }

                $tempArrayCountriesResponse = $this->countryService->buildArrayCountries();
                if (isset($tempArrayCountriesResponse->Error)) {
                    return $tempArrayCountriesResponse;
                } else {
                    $arrayCountriesResponse = $tempArrayCountriesResponse;
                }

                $jsonResponse = $this->buildJsonResponse($transactionIdRequest, $dateResponse, $userResponse, $arraySpecializationsResponse,
                    $arrayHealthPromotersResponse, $arrayPatientsResponse, $arrayAppointmentsResponse,
                    $arrayAppointmentsByNotificationsResponse, $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                    $membershipInfo, $arrayCountriesResponse);

            } else {
                //Cuando llega solo date
                if ($dateSync && count($dataUser) == 0) {
                    $dateResponse = new \DateTime('now');

                    $tempArrayHealthPromotersResponse = $this->epsService->buildArrayHealthPromotersSync($dateSync);
                    if (isset($tempArrayHealthPromotersResponse->Error)) {
                        return $tempArrayHealthPromotersResponse;
                    } else {
                        $arrayHealthPromotersResponse = $tempArrayHealthPromotersResponse;
                    }

                    $tempArraySpecializationsResponse = $this->specialityService->buildArraySpecializationsSync($dateSync);
                    if (isset($tempArraySpecializationsResponse->Error)) {
                        return $tempArraySpecializationsResponse;
                    } else {
                        $arraySpecializationsResponse = $tempArraySpecializationsResponse;
                    }

                    $tempArrayCountriesResponse = $this->countryService->buildArrayCountriesSync($dateSync);
                    if (isset($tempArrayCountriesResponse->Error)) {
                        return $tempArrayCountriesResponse;
                    } else {
                        $arrayCountriesResponse = $tempArrayCountriesResponse;
                    }

                    $jsonResponse = $this->buildJsonResponse($transactionIdRequest, $dateResponse->format(\DateTime::ISO8601), $userResponse, $arraySpecializationsResponse,
                        $arrayHealthPromotersResponse, $arrayPatientsResponse, $arrayAppointmentsResponse,
                        $arrayAppointmentsByNotificationsResponse, $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                        $membershipInfo, $arrayCountriesResponse);
                } else {
                    //Cuando solo llega el usuario y la fecha no
                    if (empty($dateSync) && count($dataUser) > 0) {
                        $dateResponse = new \DateTime('now');

                        $tempArrayHealthPromotersResponse = $this->epsService->buildArrayHealthPromoters();
                        if (isset($tempArrayHealthPromotersResponse->Error)) {
                            return $tempArrayHealthPromotersResponse;
                        } else {
                            $arrayHealthPromotersResponse = $tempArrayHealthPromotersResponse;
                        }

                        $tempArraySpecializationsResponse = $this->specialityService->buildArraySpecializations();
                        if (isset($tempArraySpecializationsResponse->Error)) {
                            return $tempArraySpecializationsResponse;
                        } else {
                            $arraySpecializationsResponse = $tempArraySpecializationsResponse;
                        }

                        $tempArrayCountriesResponse = $this->countryService->buildArrayCountries();
                        if (isset($tempArrayCountriesResponse->Error)) {
                            return $tempArrayCountriesResponse;
                        } else {
                            $arrayCountriesResponse = $tempArrayCountriesResponse;
                        }

                        if (isset($dataUser[Constant::_ID])) {
                            //Validamos que el doctor exista
                            $userEntityDoctor = $this->userService->find($dataUser[Constant::_ID]);
                            if (isset($userEntityDoctor->Error)) {
                                return $userEntityDoctor;
                            } else {
                                if (isset($userEntityDoctor)) {
                                    $tempArrayPatientsResponse = $this->patientService->buildArrayPatients($dataUser[Constant::_ID]);
                                    if (isset($tempArrayPatientsResponse->Error)) {
                                        return $tempArrayPatientsResponse;
                                    } else {
                                        $arrayPatientsResponse = $tempArrayPatientsResponse;
                                    }

                                    $tempArrayAppointmentsResponse = $this->appointmentService->buildArrayAppointment($dataUser[Constant::_ID]);
                                    if (isset($tempArrayAppointmentsResponse->Error)) {
                                        return $tempArrayAppointmentsResponse;
                                    } else {
                                        $arrayAppointmentsResponse = $tempArrayAppointmentsResponse;
                                    }

                                    $tempArrayAppointmentsByNotificationsResponse = $this->notificationService->buildArrayNotifications($arrayAppointmentsResponse);
                                    if (isset($tempArrayAppointmentsByNotificationsResponse->Error)) {
                                        return $tempArrayAppointmentsByNotificationsResponse;
                                    } else {
                                        $arrayAppointmentsByNotificationsResponse = $tempArrayAppointmentsByNotificationsResponse;
                                    }

                                    $tempArrayAssistantsResponse = $this->assistantService->buildArrayAssistants($dataUser[Constant::_ID]);
                                    if (isset($tempArrayAssistantsResponse->Error)) {
                                        return $tempArrayAssistantsResponse;
                                    } else {
                                        $arrayAssistantResponse = $tempArrayAssistantsResponse;
                                    }

                                    $tempMembershipInfo = $this->membershipService->getInfoSync($userEntityDoctor->getId(),
                                        $freeAppointments, $timeLapseForFreeAppointments);
                                    if (isset($tempMembershipInfo->Error)) {
                                        return $tempMembershipInfo;
                                    } else {
                                        $membershipInfo = $tempMembershipInfo;
                                    }

                                    $jsonResponse = $this->buildJsonResponse($transactionIdRequest, $dateResponse->format(\DateTime::ISO8601), $userResponse, $arraySpecializationsResponse,
                                        $arrayHealthPromotersResponse, $arrayPatientsResponse, $arrayAppointmentsResponse,
                                        $arrayAppointmentsByNotificationsResponse, $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                                        $membershipInfo, $arrayCountriesResponse);
                                } else {
                                    return $this->buildErrorObject(1);
                                }
                            }
                        } else {
                            return $this->buildErrorObject(2);
                        }
                    } else {
                        //Cuando viene fecha y user
                        if (!empty($dateSync) && count($dataUser) > 0) {
                            if (isset($dataUser[Constant::_ID])) {
                                $validateUser = true;
                                //Verificamos si es medico o assistente
                                if (isset($dataUser[Constant::_OWNER_USER_ID]) && $dataUser[Constant::_ID] != $dataUser[Constant::_OWNER_USER_ID]) {
                                    //Es asistente
                                    //TODO: validar que asistente exista
                                    //Validamos que asistente exista
                                    //$userEntityAssistant = $this->userService->find($dataUser[Constant::_ID]);
                                    //if ()
                                    $doctorId = $dataUser[Constant::_OWNER_USER_ID];
                                    $assistantEmail = $dataUser[Constant::_ID];
                                } else {
                                    //Es medico
                                    $doctorId = $dataUser[Constant::_ID];
                                    $assistantEmail = null;
                                }

                                //Validamos que el doctor exista
                                $userEntityDoctor = $this->userService->find($doctorId);
                                if (isset($userEntityDoctor->Error)) {
                                    return $userEntityDoctor;
                                } else
                                    if (isset($userEntityDoctor)) {

                                        $transactionCode = null;
                                        $transactionIdRequestIsNull = true;

                                        //Buscamos el syncCode en base de datos, si ya esta no se procesa nada
                                        if ($transactionIdRequest != null || $transactionIdRequest != '') {
                                            $transactionCode = $this->transactionService->findOneBy(array(Constant::CODE => $transactionIdRequest,
                                                Constant::USER => $dataUser[Constant::_ID]));
                                            $transactionIdRequestIsNull = false;
                                        }

                                        if (isset($transactionCode) == null) {

                                            if ($transactionIdRequestIsNull == false) {
                                                //Almacenamos el codigo de procesamiento
                                                $syncCodeObject = new Transaction();
                                                $syncCodeObject->setCode($transactionIdRequest);
                                                $syncCodeObject->setUser($userEntityDoctor);
                                                $syncCodeSaveResult = $this->transactionService->persistAndFlush($syncCodeObject);
                                            } else {
                                                $syncCodeSaveResult = true;
                                            }

                                            if ($syncCodeSaveResult == true) {

                                                //Primero se tiene que procesar la informacion que viene

                                                //Pacientes
                                                $tempNotifications = array();
                                                $errorAppointments = false;
                                                $appointmentLimit = false;
                                                //Iniciamos la transaccion
                                                $this->em->getConnection()->beginTransaction();
                                                foreach ($dataArrayPatients as $patientObject) {
                                                    if ($errorAppointments == false) {
                                                        if (isset($patientObject[Constant::_ID])) {
                                                            //Actualizar paciente
                                                            if ($patientObject[Constant::_ID] > 0) {
                                                                $tempPatientObject = $this->userService->updatePatientSync($patientObject, $doctorId, $assistantEmail, null);
                                                                if (isset($tempPatientObject->Error)) {
                                                                    $this->em->getConnection()->rollBack();
                                                                    $arrayPatientsResponse = array();
                                                                    $arrayAppointmentsResponse = array();
                                                                    $tempPatientObject->Error->Type = 1;
                                                                    //Capturamos el error en errors y detenemos el foreach
                                                                    array_push($arrayErrorsResponse, $tempPatientObject->Error);
                                                                    break;
                                                                }
                                                            } else {
                                                                //Nuevo paciente
                                                                if ($patientObject[Constant::_ID] == 0 && isset($patientObject[Constant::_LOCAL_ID]) &&
                                                                    $patientObject[Constant::_LOCAL_ID] > 0
                                                                ) {
                                                                    //Si el paciente ya existe se debe devolver la info de ese paciente
                                                                    //almacenar esa data en $arrayPatientsResponse
                                                                    $patientAlreadyExist = false;

                                                                    $tempObjectUser = $this->userService->validateUserSync($patientObject[Constant::_DOCUMENT_ID],
                                                                        $patientObject[Constant::_DOCUMENT_TYPE_ID], $patientObject[Constant::_FIRST_NAME],
                                                                        $patientObject[Constant::_LAST_NAME], $patientObject[Constant::_CELLPHONE],
                                                                        $patientObject[Constant::_EMAIL], $userEntityDoctor->getId(),
                                                                        $patientObject[Constant::_LOCAL_ID]);

                                                                    if ($tempObjectUser != null) {
                                                                        if (isset($tempObjectUser->Error)) {
                                                                            $this->em->getConnection()->rollBack();
                                                                            $arrayPatientsResponse = array();
                                                                            $arrayAppointmentsResponse = array();
                                                                            $tempObjectUser->Error->Type = 1;
                                                                            array_push($arrayErrorsResponse, $tempObjectUser->Error);
                                                                            break;
                                                                        } else {
                                                                            $patientAlreadyExist = true;
                                                                            array_push($arrayPatientsResponse, $tempObjectUser);
                                                                        }
                                                                    } else {
                                                                        $tempObjectUser = $this->userService->createUserSync($patientObject, $doctorId, $assistantEmail, $patientObject[Constant::_LOCAL_ID]);
                                                                        if (isset($tempObjectUser->Error)) {
                                                                            $this->em->getConnection()->rollBack();
                                                                            $arrayPatientsResponse = array();
                                                                            $arrayAppointmentsResponse = array();
                                                                            $tempObjectUser->Error->Type = 1;
                                                                            array_push($arrayErrorsResponse, $tempObjectUser->Error);
                                                                            break;
                                                                        } else {
                                                                            array_push($arrayPatientsResponse, $tempObjectUser);
                                                                        }
                                                                    }

                                                                    //buscamos citas nuevas para esos pacientes
                                                                    $tempObjectUser = (array)$tempObjectUser;
                                                                    if ($errorAppointments == false || $appointmentLimit == false) {
                                                                        foreach ($dataArrayAppointments as $appointmentObject) {
                                                                            if (isset($appointmentObject[Constant::_ID]) && isset($appointmentObject[Constant::_PATIENT_ID])) {
                                                                                if ($appointmentObject[Constant::_ID] == 0 && $appointmentObject[Constant::_PATIENT_ID] == 0) {
                                                                                    if (isset($patientObject[Constant::_LOCAL_ID]) && isset($appointmentObject[Constant::_PATIENT_LOCAL_ID])) {
                                                                                        if ($patientObject[Constant::_LOCAL_ID] == $appointmentObject[Constant::_PATIENT_LOCAL_ID]) {
                                                                                            $tempObjectAppointment = $this->dateService->createDateSync($tempObjectUser[Constant::_ID],
                                                                                                $appointmentObject, $freeAppointments, $timeLapseForFreeAppointments,
                                                                                                $appointmentObject[Constant::_LOCAL_ID], $appointmentObject[Constant::_PATIENT_LOCAL_ID]);
                                                                                            if (isset($tempObjectAppointment->Error)) {
                                                                                                $tempObjectAppointment->Error->Type = 2;
                                                                                                if ($tempObjectAppointment->Error->Code == 6) {
                                                                                                    $appointmentLimit = true;
                                                                                                    array_push($arrayErrorsResponse, $tempObjectAppointment->Error);
                                                                                                    break;
                                                                                                } else {
                                                                                                    $errorAppointments = true;
                                                                                                    $this->em->getConnection()->rollBack();
                                                                                                    if ($patientAlreadyExist == false) {
                                                                                                        $arrayPatientsResponse = array();
                                                                                                    }
                                                                                                    $arrayAppointmentsResponse = array();
                                                                                                    array_push($arrayErrorsResponse, $tempObjectAppointment->Error);
                                                                                                    break;
                                                                                                }
                                                                                            } else {
                                                                                                array_push($tempNotifications, (object)array(
                                                                                                    Constant::APPOINTMENT_ID => $tempObjectAppointment->Id,
                                                                                                    Constant::IS_EDIT => false,
                                                                                                ));
                                                                                                array_push($arrayAppointmentsResponse, $tempObjectAppointment);
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        $errorAppointments = true;
                                                                                        $this->em->getConnection()->rollBack();
                                                                                        $arrayPatientsResponse = array();
                                                                                        $arrayAppointmentsResponse = array();
                                                                                        $tempError = $this->buildErrorObject(2);
                                                                                        $tempError->Error->Type = 2;
                                                                                        array_push($arrayErrorsResponse, $tempError->Error);
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                $errorAppointments = true;
                                                                                $this->em->getConnection()->rollBack();
                                                                                $arrayPatientsResponse = array();
                                                                                $arrayAppointmentsResponse = array();
                                                                                $tempError = $this->buildErrorObject(2);
                                                                                $tempError->Error->Type = 2;
                                                                                array_push($arrayErrorsResponse, $tempError->Error);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    $this->em->getConnection()->rollBack();
                                                                    $arrayPatientsResponse = array();
                                                                    $arrayAppointmentsResponse = array();
                                                                    $tempError = $this->buildErrorObject(15);
                                                                    $tempError->Error->Type = 1;
                                                                    array_push($arrayErrorsResponse, $tempError->Error);
                                                                    break;
                                                                }
                                                            }
                                                        } else {
                                                            $this->em->getConnection()->rollBack();
                                                            $arrayPatientsResponse = array();
                                                            $arrayAppointmentsResponse = array();
                                                            //Capturamos el error en errors y detenemos el foreach
                                                            $tempError = $this->buildErrorObject(2);
                                                            $tempError->Error->Type = 1;
                                                            array_push($arrayErrorsResponse, $tempError->Error);
                                                            break;
                                                        }
                                                    } else {
                                                        break;
                                                    }
                                                }
                                                if ($this->em->getConnection()->isTransactionActive())
                                                    $this->em->getConnection()->commit();


                                                //Citas nuevas de pacientes que ya existen y editar citas de pacientes que ya existen
                                                //Iniciamos la transaccion
                                                $this->em->getConnection()->beginTransaction();
                                                if ($errorAppointments == false || $appointmentLimit == false) {
                                                    foreach ($dataArrayAppointments as $appointmentObject) {
                                                        //Cita nueva de paciente que ya existe
                                                        if ($appointmentObject[Constant::_PATIENT_ID] > 0 && $appointmentObject[Constant::_ID] == 0) {
                                                            $tempObjectAppointment = $this->dateService->createDateSync($appointmentObject[Constant::_PATIENT_ID],
                                                                $appointmentObject, $freeAppointments, $timeLapseForFreeAppointments,
                                                                $appointmentObject[Constant::_LOCAL_ID], 0);
                                                            if (isset($tempObjectAppointment->Error)) {
                                                                $tempObjectAppointment->Error->Type = 2;
                                                                if ($tempObjectAppointment->Error->Code == 6) {
                                                                    array_push($arrayErrorsResponse, $tempObjectAppointment->Error);
                                                                    break;
                                                                } else {
                                                                    $this->em->getConnection()->rollBack();
                                                                    array_push($arrayErrorsResponse, $tempObjectAppointment->Error);
                                                                    break;
                                                                }
                                                            } else {
                                                                array_push($tempNotifications, (object)array(
                                                                    Constant::APPOINTMENT_ID => $tempObjectAppointment->Id,
                                                                    Constant::IS_EDIT => false,
                                                                ));
                                                                array_push($arrayAppointmentsResponse, $tempObjectAppointment);
                                                            }
                                                        } else {
                                                            //Editar cita
                                                            if ($appointmentObject[Constant::_PATIENT_ID] > 0 && $appointmentObject[Constant::_ID] > 0) {
                                                                $tempResult = $this->dateService->editSync($appointmentObject);
                                                                if (isset($tempResult->Error)) {
                                                                    $this->em->getConnection()->rollBack();
                                                                    $tempResult->Error->Type = 2;
                                                                    array_push($arrayErrorsResponse, $tempResult->Error);
                                                                    break;
                                                                } else {
                                                                    if (count($tempResult) > 0)
                                                                        array_push($tempNotifications, $tempResult);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if ($this->em->getConnection()->isTransactionActive())
                                                    $this->em->getConnection()->commit();

                                                //Operaciones con asistentes
                                                //Iniciamos nueva transaccion
                                                $this->em->getConnection()->beginTransaction();
                                                $arrayAssistantResponse = array();
                                                if ($dataArrayAssistants) {
                                                    foreach ($dataArrayAssistants as $assistantObject) {
                                                        //Un asistente nuevo
                                                        if ($assistantObject[Constant::_ID] == 0) {
                                                            $tempCreateAssistantSync = $this->userService->createAssistantSync($assistantObject, $userEntityDoctor->getId(),
                                                                $hashAlgorithm, $assistantObject[Constant::_LOCAL_ID]);
                                                            if (isset($tempCreateAssistantSync->Error)) {
                                                                $this->em->getConnection()->rollBack();
                                                                $arrayAssistantResponse = array();
                                                                $tempCreateAssistantSync->Error->Type = 3;
                                                                array_push($arrayErrorsResponse, $tempCreateAssistantSync->Error);
                                                                break;
                                                            } else {
                                                                array_push($arrayAssistantResponse, $tempCreateAssistantSync);
                                                            }
                                                        } else {
                                                            //Editar asistente
                                                            if ($assistantObject[Constant::_ID] > 0) {
                                                                $tempResult = $this->userService->updateAssistantSync($assistantObject);
                                                                if (isset($tempResult->Error)) {
                                                                    $this->em->getConnection()->rollBack();
                                                                    $arrayAssistantResponse = array();
                                                                    $tempResult->Error->Type = 3;
                                                                    array_push($arrayErrorsResponse, $tempResult->Error);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if ($this->em->getConnection()->isTransactionActive())
                                                    $this->em->getConnection()->commit();


                                                //Recommendations
                                                if ($dataArrayRecommendeds) {
                                                    foreach ($dataArrayRecommendeds as $recommendObject) {
                                                        $tempResult = $this->userService->recommendAppSync($recommendObject, $userEntityDoctor);
                                                        if (isset($tempResult->Error)) {
                                                            $tempResult->Error->Type = 4;
                                                            array_push($arrayErrorsResponse, $tempResult->Error);
                                                            break;
                                                        } else {
                                                            array_push($arrayRecommendsResponse, $tempResult);
                                                        }
                                                    }
                                                }

                                                //Informacion para devolver
                                                $dateResponse = new \DateTime('now');

                                                $userResponse = $this->userService->buildUserObjectSync($userEntityDoctor, $dateSync);
                                                if (isset($userResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $userResponse;
                                                    }
                                                }

                                                $arrayHealthPromotersResponse = $this->epsService->buildArrayHealthPromotersSync($dateSync);
                                                if (isset($arrayHealthPromotersResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $arrayHealthPromotersResponse;
                                                    }
                                                }

                                                $arraySpecializationsResponse = $this->specialityService->buildArraySpecializationsSync($dateSync);
                                                if (isset($arraySpecializationsResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $arraySpecializationsResponse;
                                                    }
                                                }

                                                $arrayCountriesResponse = $this->countryService->buildArrayCountriesSync($dateSync);
                                                if (isset($arrayCountriesResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $arrayCountriesResponse;
                                                    }
                                                }

                                                $tempArrayPatientsResponse = $this->patientService->buildArrayPatientsSync($dataUser[Constant::_ID], $dateSync);
                                                if (isset($tempArrayPatientsResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $tempArrayPatientsResponse;
                                                    }
                                                } else {
                                                    foreach ($tempArrayPatientsResponse as $patientObject) {
                                                        $status = false;
                                                        foreach ($arrayPatientsResponse as $patientObjectResponse) {
                                                            if ($patientObject->Id == $patientObjectResponse->Id)
                                                                $status = true;
                                                        }
                                                        if (!$status)
                                                            array_push($arrayPatientsResponse, $patientObject);
                                                    }
                                                }

                                                $tempArrayAppointmentsResponse = $this->appointmentService->buildArrayAppointmentSync($dataUser[Constant::_ID], $dateSync);
                                                if (isset($tempArrayAppointmentsResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $tempArrayAppointmentsResponse;
                                                    }
                                                } else {
                                                    foreach ($tempArrayAppointmentsResponse as $appointmentObject) {
                                                        $status = false;
                                                        foreach ($arrayAppointmentsResponse as $appointmentObjectResponse) {
                                                            if ($appointmentObject->Id == $appointmentObjectResponse->Id)
                                                                $status = true;
                                                        }
                                                        if (!$status)
                                                            array_push($arrayAppointmentsResponse, $appointmentObject);
                                                    }
                                                }

                                                $tempArrayAssistantsResponse = $this->assistantService->buildArrayAssistantsSync($dataUser[Constant::_ID], $dateSync);
                                                if (isset($tempArrayAssistantsResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $tempArrayAssistantsResponse;
                                                    }
                                                } else {
                                                    foreach ($tempArrayAssistantsResponse as $assistantObject) {
                                                        $status = false;
                                                        foreach ($arrayAssistantResponse as $assistantObjectResponse) {
                                                            if ($assistantObject->Id == $assistantObjectResponse->Id) {
                                                                $status = true;
                                                            }
                                                        }
                                                        if (!$status)
                                                            array_push($arrayAssistantResponse, $assistantObject);
                                                    }
                                                }

                                                $arrayAppointmentsByNotificationsResponse = $this->notificationService->buildArrayNotificationsSync($arrayAppointmentsResponse, $dateSync);
                                                if (isset($arrayAppointmentsByNotificationsResponse->Error)) {
                                                    if (count($dataArrayPatients) == 0 && count($dataArrayAppointments) == 0 &&
                                                        count($dataArrayAssistants) == 0 && count($dataArrayRecommendeds) == 0
                                                    ) {
                                                        return $arrayAppointmentsByNotificationsResponse;
                                                    }
                                                }

                                                $tempMembershipInfo = $this->membershipService->getInfoSync($userEntityDoctor->getId(),
                                                    $freeAppointments, $timeLapseForFreeAppointments);
                                                if (isset($tempMembershipInfo->Error)) {
                                                    return $tempMembershipInfo;
                                                } else {
                                                    $membershipInfo = $tempMembershipInfo;
                                                }

                                                $jsonResponse = $this->buildJsonResponse($transactionIdRequest, $dateResponse->format(\DateTime::ISO8601), $userResponse, $arraySpecializationsResponse,
                                                    $arrayHealthPromotersResponse, $arrayPatientsResponse, $arrayAppointmentsResponse,
                                                    $arrayAppointmentsByNotificationsResponse, $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                                                    $membershipInfo, $arrayCountriesResponse);

                                                //recorro las notificaciones para enviarlas
                                                //Iniciamos nueva transaccion
                                                $this->em->getConnection()->beginTransaction();
                                                $tempStatusSendNotifications = $this->notificationService->sendEmailsNewAppointments($tempNotifications);
                                                if (isset($tempStatusSendNotifications->Error)) {
                                                    $this->em->getConnection()->rollBack();
                                                    array_push($arrayErrorsResponse, $tempStatusSendNotifications->Error);
                                                }
                                                if ($this->em->getConnection()->isTransactionActive())
                                                    $this->em->getConnection()->commit();

                                                //Validamos las membresias para enviar mail si algun medico ha llegado
                                                //al porcentaje de citas establecido
                                                $this->membershipService->verifyAppointmentsPercent($notificationAppointmentsPercent,
                                                    $emailsNotificationAppointmentsPercent, $userEntityDoctor);
                                            } else {
                                                return $syncCodeSaveResult;
                                            }
                                        } else {
                                            return $this->buildJsonResponse($transactionIdRequest, null, null, $arraySpecializationsResponse,
                                                $arrayHealthPromotersResponse, $arrayPatientsResponse, $arrayAppointmentsResponse,
                                                $arrayAppointmentsByNotificationsResponse, $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                                                $membershipInfo, $arrayCountriesResponse);
                                        }
                                    } else {
                                        return $this->buildErrorObject(1);
                                    }
                            } else {
                                return $this->buildErrorObject(2);
                            }
                        }
                    }
                }
            }
            //error_log('Lo que responde el server');
            //$this->var_error_log($jsonResponse);
            return $jsonResponse;
        } catch (\Exception $e) {
            error_log('trace074');
            error_log('Error: ' . $e->getMessage());
            $tempError = $this->buildErrorObject(-1);
            array_push($arrayErrorsResponse, $tempError->Error);
            return $tempError;
        }
    }


    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case 1:
                $text = 'User not found';
                break;
            case 2:
                $text = 'Missing parameters';
                break;
            case 3:
                $text = 'Missing document type';
                break;
            case 4:
                $text = 'Patient already exist';
                break;
            case 5:
                $text = 'Relationship not found';
                break;
            case 6:
                $text = 'Appointments limit';
                break;
            case 7:
                $text = 'Appointment not found';
                break;
            case 8:
                $text = 'Eps not found';
                break;
            case 9:
                $text = 'Place relationship not found';
                break;
            case 10:
                $text = 'Invalid email or password';
                break;
            case 12:
                $text = 'Missing user type';
                break;
            case 13:
                $text = 'Doctor already exist';
                break;
            case 14:
                $text = 'Assistant already exist';
                break;
            case 15:
                $text = 'Invalid data';
                break;
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }

    /**
     * @param $transactionIdRequest
     * @param $date
     * @param $user
     * @param $arraySpecializationsResponse
     * @param $arrayHealthPromotersResponse
     * @param $arrayPatientsResponse
     * @param $arrayAppointmentsResponse
     * @param $arrayAppointmentsByNotificationsResponse
     * @param $arrayAssistantResponse
     * @param $arrayErrorsResponse
     * @param $arrayRecommendsResponse
     * @param $membershipInfo
     * @param $arrayCountriesResponse
     * @return object
     */
    public function buildJsonResponse($transactionIdRequest, $date, $user, $arraySpecializationsResponse, $arrayHealthPromotersResponse
        , $arrayPatientsResponse, $arrayAppointmentsResponse, $arrayAppointmentsByNotificationsResponse,
                                      $arrayAssistantResponse, $arrayErrorsResponse, $arrayRecommendsResponse,
                                      $membershipInfo, $arrayCountriesResponse)
    {
        return (object)array(
            Constant::_TRANSACTION_ID => $transactionIdRequest,
            Constant::_DATE => $date,
            Constant::_USER => $user,
            Constant::_SPECIALIZATIONS => $arraySpecializationsResponse,
            Constant::_COUNTRIES => $arrayCountriesResponse,
            Constant::_HEALTH_PROMOTERS => $arrayHealthPromotersResponse,
            Constant::_PATIENTS => $arrayPatientsResponse,
            Constant::_APPOINTMENTS => $arrayAppointmentsResponse,
            Constant::_APPOINTMENTS_BY_NOTIFICATION => $arrayAppointmentsByNotificationsResponse,
            Constant::_ASSISTANTS => $arrayAssistantResponse,
            Constant::_RECOMMENDEDS => $arrayRecommendsResponse,
            Constant::_ERRORS => $arrayErrorsResponse,
            Constant::_MEMBERSHIP => $membershipInfo
        );
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}