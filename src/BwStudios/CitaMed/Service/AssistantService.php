<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use Doctrine\ORM\EntityManager;

class AssistantService
{

    private $repositoryUser;
    private $repositoryGeneralDataUser;
    private $repositoryExtraDataUser;

    private $em;

    /**
     * AssistantService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
    }

    public function buildArrayAssistants($doctorId)
    {
        try {
            $arrayAssistantsResponse = array();
            $ucoTwoService = new UserConnectionOneTwoService($this->em);
            $ucoTwoArray = $ucoTwoService->findBy(array(Constant::USER_ONE_ID => $doctorId));
            if (isset($ucoTwoArray->Error)) {
                return $ucoTwoArray;
            } else {
                if (count($ucoTwoArray) > 0)
                    foreach ($ucoTwoArray as $ucoTwoEntity) {
                        $userEntityAssistant = $ucoTwoEntity->getUserTwoId();
                        $generalDataUserAssistant = $userEntityAssistant->getGeneralDataUserId();
                        $extraDataUserAssistant = $userEntityAssistant->getExtraDataUserId();
                        if (isset($userEntityAssistant) && isset($generalDataUserAssistant) && isset($extraDataUserAssistant)) {
                            $tempResponse = $this->buildAssistantObject($userEntityAssistant, $generalDataUserAssistant,
                                $extraDataUserAssistant, 0);
                            if (isset($tempResponse->Error)) {
                                return $tempResponse;
                            } else {
                                array_push($arrayAssistantsResponse, $tempResponse);
                            }
                        } else {
                            return $this->buildErrorObject(-1);
                        }
                    }

                return $arrayAssistantsResponse;
            }
        } catch (\Exception $e) {
            error_log('Catch de buildArrayAssistants');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    public function buildArrayAssistantsSync($doctorId, $dateSync)
    {
        try {
            $arrayAssistantsResponse = array();
            $ucoTwoService = new UserConnectionOneTwoService($this->em);
            $ucoTwoArray = $ucoTwoService->findBy(array(Constant::USER_ONE_ID => $doctorId));

            $dateSyncRequest = new \DateTime($dateSync);

            foreach ($ucoTwoArray as $ucoTwoEntity) {
                $userEntityAssistant = $this->repositoryUser->find($ucoTwoEntity->getUserTwoId()->getId());
                if ($userEntityAssistant->getSyncDate() > $dateSyncRequest) {
                    $generalDataUserAssistant = $userEntityAssistant->getGeneralDataUserId();
                    $extraDataUserAssistant = $userEntityAssistant->getExtraDataUserId();

                    $tempResponse = $this->buildAssistantObject($userEntityAssistant, $generalDataUserAssistant,
                        $extraDataUserAssistant, 0);
                    if (isset($tempResponse->Error)) {
                        return $tempResponse;
                    } else {
                        array_push($arrayAssistantsResponse, $tempResponse);
                    }
                }
            }
            return $arrayAssistantsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    public function buildAssistantObject(User $userEntityAssistant, GeneralDataUser $generalDataUserAssistant,
                                         ExtraDataUser $extraDataUserAssistant, $localId)
    {
        try {
            return (object)array(
                Constant::_ID => $userEntityAssistant->getId(),
                Constant::_FIRST_NAME => $generalDataUserAssistant->getName(),
                Constant::_LAST_NAME => $generalDataUserAssistant->getLastName(),
                Constant::_EMAIL => $generalDataUserAssistant->getEmail(),
                Constant::_ACTIVE => true
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}