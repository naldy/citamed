<?php

namespace BwStudios\CitaMed\Service;


use BwStudios\CitaMed\Constant\Constant;

class RecommendService
{

    public function buildRecommendObject($recommendId, $recommendObject)
    {
        try {
            return (object)array(
                Constant::_ID => $recommendId,
                Constant::_LOCAL_ID => $recommendObject->LocalId,
                Constant::_NAME => $recommendObject->Name,
                Constant::_CELLPHONE => $recommendObject->CellPhone,
                Constant::_EMAIL => $recommendObject->Email
            );
        } catch (\Exception $e) {
            error_log('catch buildRecommendObject');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }

        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}