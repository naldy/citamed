<?php

namespace BwStudios\CitaMed\Service;

//Constant
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;

//Symfony utilities
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\Entity;

class ScheduleService
{

    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionThreeFour;
    private $repositoryAppointment;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $repositoryPayment;
    private $em;

    /**
     *
     * @var EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
        $this->repositoryPayment = $this->em->getRepository(Constant::ENTITY_PAYMENT);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getDaysMorning(Request $request)
    {
        //Json Information
        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_MONTH => $request->request->get(Constant::DATE_MONTH),
            Constant::DATE_YEAR => $request->request->get(Constant::DATE_YEAR)
        );

        $jsonResponse = array();
        $negativeResponse = null;

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {

            $data_user = $this->repositoryExtraDataUser->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($data_user) > 0) {

                //User data
                $user = $data_user->getUser();
                $user_type = $user->getUserTypeId();
                $user_id = $user->getId();
                $arrival_time = $data_user->getArrivalTime()->format('H:i:s');//format
                $depart_time = $data_user->getDepartTime()->format('H:i:s');

                //Data Time
                $midday = Constant::MID_DAY;
                $arrayCitas = array();
                $state_morning = 0;
                $state_afternoon = 0;

                // User Type Data
                $user_type = $this->repositoryUserType->find($user_type);
                $user_type_id = $user_type->getDescription();
                if ($user_type_id == Constant::USER_TYPE_DOCTOR) {

                    //consult appointments in this that user
                    $data_date = $this->repositoryAppointment->findBy(array(Constant::DATE_USER_ONE_ID => $user_id), array());

                    if (count($data_date) > 0) {

                        //format to type date
                        $dteStart = new \DateTime($arrival_time);
                        $dteEnd = new \DateTime($midday);

                        //differences between dates
                        $dteDiff = $dteStart->diff($dteEnd);
                        $hour_morning = $dteDiff->format("%H:%I:%S");

                        //format to type date
                        $dteStart = new \DateTime($midday);
                        $dteEnd = new \DateTime($depart_time);

                        //differences between dates
                        $dteDiff = $dteStart->diff($dteEnd);
                        $hour_afternoon = $dteDiff->format("%H:%I:%S");

                        //check the number of days of the month
                        $month = mktime(0, 0, 0, $data_array[Constant::DATE_MONTH], 1, $data_array[Constant::DATE_YEAR]);
                        setlocale('LC_ALL', 'fr_FR');

                        //see all appointments in the month are received by the JSON information
                        /*$sql = 'SELECT `id`, `user_one_id`, `user_three_id`, `user_place_connection_id`, `date`, `confirmed`, `waiting_queue`, `attended`, `canceled`, `arrived`, `time_frame`, `assigned_time`, `creation_date`, `confirmation_date`, `priority`, `comment`, `User_Eps_Connection_id` FROM `Appointment` WHERE MONTH(date) = :value AND user_one_id = :idvalue';
                         $stm = $this->em->getConnection()->prepare($sql);
                         $stm->bindParam('value', $data_array[Constant::DATE_MONTH]);
                         $stm->bindParam('idvalue', $user_id);
                         $stm->execute();
                         $valors = $stm->fetchAll();*/
                        $data_month = $this->repositoryAppointment->createQueryBuilder('o')
                            ->where('MONTH(o.date) = :value')
                            ->andWhere('o.userOneId = :idvalue')
                            ->setParameter('value', $data_array[Constant::DATE_MONTH])
                            ->setParameter('idvalue', $user_id)
                            ->orderBy('o.date', 'ASC')
                            ->getQuery();
                        $valors = $data_month->getResult();

                        //travels the number of months
                        for ($i = 1; $i <= date("t", $month); $i++) {

                            //pociciones assigned to an array
                            $arrayCitas[$i] = array();

                            //through each pocicon
                            foreach ($valors as $value) {
                                $date_day = $value->getDate()->format('d');

                                //validates that the day of the appointment corresponds to the day of the month
                                if ($i == $date_day) {

                                    array_push($arrayCitas[$i], array(
                                        Constant::DATE => $value->getDate(),
                                        Constant::TIME => $value->getAssignedTime()
                                    ));
                                }
                            }
                        }
                        //each arrayCitas
                        foreach ($arrayCitas as $key => $value) {
                            foreach ($value as $keye => $values) {

                                //get values of arrayCitas
                                $date_hours = $values[Constant::DATE]->format('H:i:s');
                                //estimated time
                                $time_hours = $values[Constant::TIME]->format('H:i:s') * 60;
                                $time_minutes = $values[Constant::TIME]->format('i');
                                $time_total = $time_hours + $time_minutes;

                                //valid ranges hours
                                if ($date_hours > "00:00:00" && $date_hours <= "12:00:00") {

                                    //subtract hours while Dr.
                                    $date = date_create($hour_morning);
                                    date_add($date, date_interval_create_from_date_string('- ' . $time_total . ' minutes'));
                                    $hour_Total = date_format($date, 'H:i:s');
                                    $hour_morning = $hour_Total;

                                } elseif ($date_hours > "00:00:00" && $date_hours > "12:00:00" && $date_hours <= "24:00:00") {

                                    $date = date_create($hour_afternoon);
                                    date_add($date, date_interval_create_from_date_string('- ' . $time_total . ' minutes'));
                                    $hour_Total = date_format($date, 'H:i:s');
                                    $hour_afternoon = $hour_Total;

                                } else {

                                }

                            }
                            //validates time availability
                            if ($hour_morning <= '00:10:00') {
                                $state_morning = 0;
                            } else {
                                $state_morning = 1;
                            }
                            if ($hour_afternoon <= '00:10:00') {
                                $state_afternoon = 0;
                            } else {
                                $state_afternoon = 1;
                            }
                            array_push($jsonResponse, array(
                                Constant::DATE => $key . "-" . $data_array[Constant::DATE_MONTH] . "-" . $data_array[Constant::DATE_YEAR],
                                'disponibilidadManana' => $state_morning,
                                'disponibilidadTarde' => $state_afternoon
                            ));
                            $state_afternoon = 0;
                            $state_morning = 0;
                            $hour_afternoon = $dteDiff->format("%H:%I:%S");
                            $hour_morning = $dteDiff->format("%H:%I:%S");
                        }
                    }
                    return $jsonResponse;
                }
            }
        } elseif ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $data_user = $this->repositoryGeneralDataUser->findOneBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));
            if (count($data_user) > 0) {
                $user = $data_user->getUser();

                $user_id = $this->repositoryUser->find($user);
                $user_type = $user_id->getUserTypeId();
                $user_type_id = $user_type->getDescription();

                //Data Time
                $midday = Constant::MID_DAY;
                $arrayCitas = array();
                $state_morning = 0;
                $state_afternoon = 0;

                if ($user_type_id == Constant::USER_TYPE_ASSISTANT) {

                    //consult Entity UserConnectionOneTwo
                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));
                    $doctorId = $takeOfConnection->getUserOneId();

                    $doctor_id = $doctorId->getId();
                    $data_doctor = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctor_id));

                    if (count($data_doctor) > 0) {

                        $arrival_time = $data_doctor->getArrivalTime()->format('H:i:s');//format
                        $depart_time = $data_doctor->getDepartTime()->format('H:i:s');


                        $date_data = $this->repositoryAppointment->findOneBy(array(Constant::DATE_USER_ONE_ID => $doctor_id));

                        if (count($date_data) > 0) {

                            //format to type date
                            $dteStart = new \DateTime($arrival_time);
                            $dteEnd = new \DateTime($midday);

                            //differences between dates
                            $dteDiff = $dteStart->diff($dteEnd);
                            $hour_morning = $dteDiff->format("%H:%I:%S");

                            //format to type date
                            $dteStart = new \DateTime($midday);
                            $dteEnd = new \DateTime($depart_time);

                            //differences between dates
                            $dteDiff = $dteStart->diff($dteEnd);
                            $hour_afternoon = $dteDiff->format("%H:%I:%S");

                            //check the number of days of the month
                            $month = mktime(0, 0, 0, $data_array[Constant::DATE_MONTH], 1, $data_array[Constant::DATE_YEAR]);
                            setlocale('LC_ALL', 'fr_FR');

                            //see all appointments in the month are received by the JSON information
                            /* $sql = 'SELECT * FROM Appointment WHERE MONTH(date) = :value AND user_one_id = :idvalue';
                             $stm = $this->em->getConnection()->prepare($sql);
                             $stm->bindParam('value', $data_array[Constant::DATE_MONTH]);
                             $stm->bindParam('idvalue', $doctorId);
                             $stm->execute();
                             $valors = $stm->fetchAll();*/

                            $data_month = $this->repositoryAppointment->createQueryBuilder('o')
                                ->where('MONTH(o.date) = :value')
                                ->andWhere('o.userOneId = :idvalue')
                                ->setParameter('value', $data_array[Constant::DATE_MONTH])
                                ->setParameter('idvalue', $doctorId)
                                ->orderBy('o.date', 'ASC')
                                ->getQuery();
                            $valors = $data_month->getResult();

                            //travels the number of months
                            for ($i = 1; $i <= date("t", $month); $i++) {

                                //pociciones assigned to an array
                                $arrayCitas[$i] = array();

                                //through each pocicon
                                foreach ($valors as $value) {
                                    $date_day = $value->getDate()->format('d');

                                    //validates that the day of the appointment corresponds to the day of the month
                                    if ($i == $date_day) {

                                        array_push($arrayCitas[$i], array(
                                            Constant::DATE => $value->getDate(),
                                            Constant::TIME => $value->getAssignedTime()
                                        ));
                                    }
                                }
                            }
                            //each arrayCitas
                            foreach ($arrayCitas as $key => $value) {
                                foreach ($value as $keye => $values) {

                                    //get values of arrayCitas
                                    $date_hours = $values[Constant::DATE]->format('H:i:s');
                                    //estimated time
                                    $time_hours = $values[Constant::TIME]->format('H:i:s') * 60;
                                    $time_minutes = $values[Constant::TIME]->format('i');
                                    $time_total = $time_hours + $time_minutes;

                                    //valid ranges hours
                                    if ($date_hours > "00:00:00" && $date_hours <= "12:00:00") {

                                        //subtract hours while Dr.
                                        $date = date_create($hour_morning);
                                        date_add($date, date_interval_create_from_date_string('- ' . $time_total . ' minutes'));
                                        $hour_Total = date_format($date, 'H:i:s');
                                        $hour_morning = $hour_Total;

                                    } elseif ($date_hours > "00:00:00" && $date_hours > "12:00:00" && $date_hours <= "24:00:00") {

                                        $date = date_create($hour_afternoon);
                                        date_add($date, date_interval_create_from_date_string('- ' . $time_total . ' minutes'));
                                        $hour_Total = date_format($date, 'H:i:s');
                                        $hour_afternoon = $hour_Total;

                                    } else {

                                    }

                                }
                                //validates time availability
                                if ($hour_morning <= '00:10:00') {
                                    $state_morning = 0;
                                } else {
                                    $state_morning = 1;
                                }
                                if ($hour_afternoon <= '00:10:00') {
                                    $state_afternoon = 0;
                                } else {
                                    $state_afternoon = 1;
                                }
                                array_push($jsonResponse, array(
                                    Constant::DATE => $key . "-" . $data_array[Constant::DATE_MONTH] . "-" . $data_array[Constant::DATE_YEAR],
                                    'disponibilidadManana' => $state_morning,
                                    'disponibilidadTarde' => $state_afternoon
                                ));
                                $state_afternoon = 0;
                                $state_morning = 0;
                                $hour_afternoon = $dteDiff->format("%H:%I:%S");
                                $hour_morning = $dteDiff->format("%H:%I:%S");
                            }
                        }
                        return $jsonResponse;
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getListAllDates(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        //json Information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );
        //evaluated first condition
        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);
            if ($user) {
                //consult entity Appointment
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                //get rows where $user exist
                foreach ($dateId as $date) {
                    // TODO: validacion innecesaria
                    if ($date->getUserOneId() == $user) {
                        /**/
                        //Get office data
                        $placeId = $date->getUserPlaceConnectionId();

                        $placeUser = $this->repositoryUserPlaceConnection->find($placeId);
                        $placeIdOffice = $placeUser->getPlaceId();

                        $epsDescription = $date->getEps()->getDescription();

                        //Consult entity Place
                        $place = $this->repositoryPlace->find($placeIdOffice);


                        // Get patient data
                        $userThree = $date->getUserThreeId();
                        //Consult entity User
                        $patientData = $this->repositoryUser->find($userThree);
                        $patientId = $patientData->getGeneralDataUserId();
                        //Consult entity GeneralDataUser
                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                        if ($date->getTimeFrame() != null) {
                            $timeFrame = $date->getTimeFrame()->format('H:i:s');
                        } else {
                            $timeFrame = '00:00:00';
                        }

                        $isPaid = $this->repositoryPayment->findOneBy(array('appointmentId' => $date->getId()));
                        if (count($isPaid) > 0) {
                            $paymentChecker = $isPaid->getIsPaid();
                            $amount = $isPaid->getAmount();
                            $paymentMethod = $isPaid->getPaymentMethod()->getDescription();
                            $epsBonus = $isPaid->getEpsPaymentCode();
                            if ($isPaid->getOwe() > 0) {
                                $isOwe = true;
                            } else {
                                $isOwe = false;
                            }
                            $owe = $isPaid->getOwe();
                        } else {
                            $paymentChecker = null;
                            $amount = null;
                            $paymentMethod = null;
                            $epsBonus = null;
                            $owe = null;
                            $isOwe = null;
                        }

                        $isCanceledByPatient = $date->getCanceledByPatient();
                        $isCanceled = $date->getCanceled();
                        if ($isCanceled != true) {
                            //TODO: refactorizar
                            if ($isCanceledByPatient == null || $isCanceledByPatient == false) {
                                $isCanceled = false;
                            } else
                                $isCanceled = true;
                            //json information Output
                            array_push($jsonResponse, array(
                                //general appointment data
                                Constant::DATE_ID => $date->getId(),
                                Constant::DATE => $date->getDate()->format('Y-m-d H:i'),
                                Constant::ASSIGNED_TIME => $date->getAssignedTime()->format('H:i'),
                                Constant::CONFIRMED => $date->getConfirmed(),
                                Constant::ATTENDED => $date->getAttended(),
                                Constant::TIME_FRAME => $timeFrame,
                                Constant::PLACE => $place->getAddress(),
                                Constant::CREATION_DATE => $date->getCreationDate()->format('Y-m-d H:i'),
                                //patient data
                                Constant::PATIENT_NAME => $patient->getName(),
                                Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                Constant::CELL_PHONE => $patient->getCellPhone(),
                                Constant::HOME_PHONE => $patient->getHomePhone(),
                                Constant::EMAIL => $patient->getEmail(),
                                Constant::COMMENT_DATE => $date->getComment(),
                                Constant::EPS_NAME => $epsDescription,
                                Constant::IS_PAID => $paymentChecker,
                                Constant::AMOUNT => $amount,
                                Constant::PAYMENT_METHOD => $paymentMethod,
                                Constant::EPS_BONUS => $epsBonus,
                                Constant::OWE => $owe,
                                Constant::IS_OWE => $isOwe,
                                Constant::USER_ID => $userThree->getId(),
                                Constant::IS_CANCELED => $isCanceled,
                                Constant::SEND_NOTIFICATIONS => $date->getSendNotifications()
                            ));
                        }
                    } else {
                        array_push($jsonResponse, array(
                            //doctor's data
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'not allowed'
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'invalid doctor identity or does not exist'
                ));
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {

                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {

                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {

                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {
                                    $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $userDoctor), array());
                                    //get rows where $user exist
                                    foreach ($dateId as $date) {

                                        /**/
                                        //Get office data
                                        $placeId = $date->getUserPlaceConnectionId();

                                        $placeUser = $this->repositoryUserPlaceConnection->find($placeId);
                                        $placeIdOffice = $placeUser->getPlaceId();

                                        $epsDescription = $date->getEps()->getDescription();

                                        //Consult entity Place
                                        $place = $this->repositoryPlace->find($placeIdOffice);


                                        // Get patient data
                                        $userThree = $date->getUserThreeId();
                                        //Consult entity User
                                        $patientData = $this->repositoryUser->find($userThree);
                                        $patientId = $patientData->getGeneralDataUserId();
                                        //Consult entity GeneralDataUser
                                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                                        if ($date->getTimeFrame() != null) {
                                            $timeFrame = $date->getTimeFrame()->format('H:i:s');
                                        } else {
                                            $timeFrame = '00:00:00';
                                        }

                                        $isPaid = $this->repositoryPayment->findOneBy(array('appointmentId' => $date->getId()));
                                        if (count($isPaid) > 0) {
                                            $paymentChecker = $isPaid->getIsPaid();
                                            $amount = $isPaid->getAmount();
                                            $paymentMethod = $isPaid->getPaymentMethod()->getDescription();
                                            $epsBonus = $isPaid->getEpsPaymentCode();
                                            if ($isPaid->getOwe() > 0) {
                                                $isOwe = true;
                                            } else {
                                                $isOwe = false;
                                            }
                                            $owe = $isPaid->getOwe();
                                        } else {
                                            $paymentChecker = null;
                                            $amount = null;
                                            $paymentMethod = null;
                                            $epsBonus = null;
                                            $owe = null;
                                            $isOwe = null;
                                        }

                                        $isCanceled = $date->getCanceled();
                                        //evaluate if row is canceled
                                        if ($isCanceled == null || $isCanceled == false) {
                                            $isCanceled = false;
                                        } else
                                            $isCanceled = true;
                                        //json information Output
                                        array_push($jsonResponse, array(
                                            //general appointment data
                                            Constant::DATE_ID => $date->getId(),
                                            Constant::DATE => $date->getDate()->format('Y-m-d H:i'),
                                            Constant::ASSIGNED_TIME => $date->getAssignedTime()->format('H:i'),
                                            Constant::CONFIRMED => $date->getConfirmed(),
                                            Constant::ATTENDED => $date->getAttended(),
                                            Constant::TIME_FRAME => $timeFrame,
                                            Constant::PLACE => $place->getAddress(),
                                            Constant::CREATION_DATE => $date->getCreationDate()->format('Y-m-d H:i'),
                                            //patient data
                                            Constant::PATIENT_NAME => $patient->getName(),
                                            Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                            Constant::CELL_PHONE => $patient->getCellPhone(),
                                            Constant::HOME_PHONE => $patient->getHomePhone(),
                                            Constant::EMAIL => $patient->getEmail(),
                                            Constant::COMMENT_DATE => $date->getComment(),
                                            Constant::EPS_NAME => $epsDescription,
                                            Constant::IS_PAID => $paymentChecker,
                                            Constant::AMOUNT => $amount,
                                            Constant::PAYMENT_METHOD => $paymentMethod,
                                            Constant::EPS_BONUS => $epsBonus,
                                            Constant::OWE => $owe,
                                            Constant::IS_OWE => $isOwe,
                                            Constant::USER_ID => $userThree->getId(),
                                            Constant::IS_CANCELED => $isCanceled,
                                            Constant::SEND_NOTIFICATIONS => $date->getSendNotifications()
                                        ));
                                    }
                                }
                            }
                        }
                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'User not found'
                        ));
                    }
                }
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing parameters'
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function availableTime(Request $request)
    {
        //Json Information
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_MONTH => $request->request->get(Constant::DATE_MONTH),
            Constant::DATE_YEAR => $request->request->get(Constant::DATE_YEAR)
        );

        $jsonResponse = array();

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $ExtraDataUser = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::DOCTOR_ID]));

            if (count($ExtraDataUser) > 0) {

                $user = $this->repositoryUser->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $ExtraDataUser->getId()));
                $userId = $user->getId();

                $userType = $this->repositoryUserType->findOneBy(array(Constant::GENERAL_ID_COLUMN => $user->getUserTypeId()));

                if ($userType->getDescription() == Constant::USER_TYPE_DOCTOR) {

                    $arrivalTime = $ExtraDataUser->getArrivalTime();
                    $departTime = $ExtraDataUser->getDepartTime();
                    $departTime1 = $ExtraDataUser->getDepartTime()->format('H:i:s');


                    $dep = new \DateTime($departTime1);
                    $disponibleDay = true;
                    $beforeToMidDay = '11:59:59';
                    $twelveHour = '12:00:00';

                    $maxMorning = new\DateTime($beforeToMidDay);
                    $midDay = new\DateTime($twelveHour);

                    $morningHours = $maxMorning->diff($arrivalTime);
                    $workDay = $departTime->diff($arrivalTime);
                    $afternoonHours = $dep->diff($midDay);
                    $zeroTime = '00:10:00';
                    $timeCounter = new\DateTime($zeroTime);
                    $timeCounter->format('%H:%i:%s');

                    //MESES DEL AÑO
                    for ($i = 1; $i <= 12; $i++) {

                        $days = cal_days_in_month(CAL_GREGORIAN, $i, $data_array[Constant::DATE_YEAR]);

                        //DIAS DEL MES
                        for ($j = 1; $j <= $days; $j++) {
                            $disponiblafternoon = true;
                            $disponiblemrning = true;
                            $dia = array();
                            $todayToValidDate = new \DateTime($data_array[Constant::DATE_YEAR] . '-' . $i . '-' . $j);
                            $hoy = $todayToValidDate->format('Y-m-d');
                            $queryDay = $this->repositoryAppointment->createQueryBuilder('D')
                                ->where('D.date LIKE :today')
                                ->andWhere('D.userOneId =:userId')
                                ->setParameter('today', $hoy . '%')
                                ->setParameter('userId', $userId)
                                ->getQuery();
                            $dayResult = $queryDay->getResult();

                            if ($dayResult == null) {
                                $dia = array(
                                    'morning availability' => $disponiblemrning,
                                    'afternoon availability' => $disponiblafternoon
                                );
                                array_push($jsonResponse, array(
                                    'date' => $hoy,
                                    'availability' => $dia
                                ));
                            } else {

                                $workingHoursInMorning = $morningHours->format('%H:%I:%S');//horas que trabaja en la mañana
                                $workingHoursAfternoon = $afternoonHours->format('%H:%I:%S');//horas que trabaje en la tarde
                                $hourToWork = $workDay->format('%H:%I:%S');//horas que trabaja to-do el dia

                                $countMorning = date_create($workingHoursInMorning);
                                $countAfternnon = date_create($workingHoursAfternoon);
                                $count = date_create($hourToWork);
                                $citaMañana = 0;
                                $citaTarde = 0;
                                $citaFueraDerango = 0;
                                foreach ($dayResult as $date) {
                                    $dateHour = $date->getDate()->format('H:i:s');
                                    $totalAssignedtime = $date->getAssignedTime()->format('H:i:s');
                                    $assignedTimeInHours = $date->getAssignedTime()->format('H');
                                    $assignedTimeInMinutes = $date->getAssignedTime()->format('i');
                                    $assignedTimeInSeconds = $date->getAssignedTime()->format('s');

                                    date_add($count, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));


                                    if (($dateHour < $departTime->format('H:i:s') && ($maxMorning->format('H:i:s') < $dateHour))) {
                                        $citaTarde++;
                                        date_add($countAfternnon, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));

                                        if ($countAfternnon < $timeCounter) {
                                            $disponiblafternoon = false;

                                        } else {
                                            $disponiblafternoon = true;

                                        }

                                    } else if (($dateHour < $maxMorning->format('H:i:s')) && ($dateHour >= $arrivalTime->format('H:i:s'))) {
                                        $citaMañana++;
                                        date_add($countMorning, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));

                                        if ($countMorning <= $timeCounter) {
                                            $disponiblemrning = false;

                                        } else {
                                            $disponiblemrning = true;

                                        }
                                    } else {
                                        $citaFueraDerango++;
                                    }

                                    $dia = array(
                                        'morning availability' => $disponiblemrning,
                                        'afternoon availability' => $disponiblafternoon
                                    );

                                }
                                array_push($jsonResponse, array(
                                    'date' => $hoy,
                                    'availability' => $dia
                                ));

                            }

                        }
                    }
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => ' user is not a doctor '
                    ));
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND
                ));
            }
        } elseif ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantIdentity = $this->repositoryGeneralDataUser->findOneBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));
            $doctorId = null;
            if (count($assistantIdentity) > 0) {
                $userEntity = $assistantIdentity->getUser();

                try {
                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $userEntity));
                    $doctorId = $takeOfConnection->getUserOneId();
                } catch (\Doctrine\ORM\ORMException $e) {
                    error_log($e->getMessage());
                } finally {
                    error_log("finally");
                }
            }
            if (count($doctorId) > 0) {

                $userFinder = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctorId));
                $extraDataFinder = $userFinder->getExtraDataUserId();
                $identity = $extraDataFinder->getIdentity();

                $ExtraDataUser = $this->repositoryExtraDataUser->findOneBy(array(Constant::IDENTITY => $identity));

                if (count($ExtraDataUser) > 0) {

                    $user = $this->repositoryUser->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $ExtraDataUser->getId()));
                    $userId = $user->getId();

                    $userType = $this->repositoryUserType->findOneBy(array(Constant::GENERAL_ID_COLUMN => $user->getUserTypeId()));

                    if ($userType->getDescription() == Constant::USER_TYPE_DOCTOR) {

                        $arrivalTime = $ExtraDataUser->getArrivalTime();
                        $departTime = $ExtraDataUser->getDepartTime();
                        $departTime1 = $ExtraDataUser->getDepartTime()->format('H:i:s');


                        $dep = new \DateTime($departTime1);
                        $disponibleDay = true;
                        $beforeToMidDay = '11:59:59';
                        $twelveHour = '12:00:00';

                        $maxMorning = new\DateTime($beforeToMidDay);
                        $midDay = new\DateTime($twelveHour);

                        $morningHours = $maxMorning->diff($arrivalTime);
                        $workDay = $departTime->diff($arrivalTime);
                        $afternoonHours = $dep->diff($midDay);
                        $zeroTime = '00:10:00';
                        $timeCounter = new\DateTime($zeroTime);
                        $timeCounter->format('%H:%i:%s');

                        //MESES DEL AÑO
                        for ($i = 1; $i <= 12; $i++) {

                            $days = cal_days_in_month(CAL_GREGORIAN, $i, $data_array[Constant::DATE_YEAR]);

                            //DIAS DEL MES
                            for ($j = 1; $j <= $days; $j++) {
                                $disponiblafternoon = true;
                                $disponiblemrning = true;
                                $dia = array();
                                $todayToValidDate = new \DateTime($data_array[Constant::DATE_YEAR] . '-' . $i . '-' . $j);
                                $hoy = $todayToValidDate->format('Y-m-d');
                                $queryDay = $this->repositoryAppointment->createQueryBuilder('D')
                                    ->where('D.date LIKE :today')
                                    ->andWhere('D.userOneId =:userId')
                                    ->setParameter('today', $hoy . '%')
                                    ->setParameter('userId', $userId)
                                    ->getQuery();
                                $dayResult = $queryDay->getResult();

                                if ($dayResult == null) {
                                    $dia = array(
                                        'morning availability' => $disponiblemrning,
                                        'afternoon availability' => $disponiblafternoon
                                    );
                                    array_push($jsonResponse, array(
                                        'date' => $hoy,
                                        'availability' => $dia
                                    ));
                                } else {

                                    $workingHoursInMorning = $morningHours->format('%H:%I:%S');//horas que trabaja en la mañana
                                    $workingHoursAfternoon = $afternoonHours->format('%H:%I:%S');//horas que trabaje en la tarde
                                    $hourToWork = $workDay->format('%H:%I:%S');//horas que trabaja to-do el dia

                                    $countMorning = date_create($workingHoursInMorning);
                                    $countAfternnon = date_create($workingHoursAfternoon);
                                    $count = date_create($hourToWork);
                                    $citaMañana = 0;
                                    $citaTarde = 0;
                                    $citaFueraDerango = 0;
                                    foreach ($dayResult as $date) {
                                        $dateHour = $date->getDate()->format('H:i:s');
                                        $totalAssignedtime = $date->getAssignedTime()->format('H:i:s');
                                        $assignedTimeInHours = $date->getAssignedTime()->format('H');
                                        $assignedTimeInMinutes = $date->getAssignedTime()->format('i');
                                        $assignedTimeInSeconds = $date->getAssignedTime()->format('s');

                                        date_add($count, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));


                                        if (($dateHour < $departTime->format('H:i:s') && ($maxMorning->format('H:i:s') < $dateHour))) {
                                            $citaTarde++;
                                            date_add($countAfternnon, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));

                                            if ($countAfternnon < $timeCounter) {
                                                $disponiblafternoon = false;

                                            } else {
                                                $disponiblafternoon = true;

                                            }

                                        } else if (($dateHour < $maxMorning->format('H:i:s')) && ($dateHour >= $arrivalTime->format('H:i:s'))) {
                                            $citaMañana++;
                                            date_add($countMorning, date_interval_create_from_date_string('- ' . $assignedTimeInHours . ' hours - ' . $assignedTimeInMinutes . ' minutes - ' . $assignedTimeInSeconds . ' seconds'));

                                            if ($countMorning <= $timeCounter) {
                                                $disponiblemrning = false;

                                            } else {
                                                $disponiblemrning = true;

                                            }
                                        } else {
                                            $citaFueraDerango++;
                                        }


                                        $dia = array(
                                            'morning availability' => $disponiblemrning,
                                            'afternoon availability' => $disponiblafternoon
                                        );

                                    }
                                    array_push($jsonResponse, array(
                                        'date' => $hoy,
                                        'availability' => $dia
                                    ));

                                }

                            }
                        }


                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => ' user is not a doctor '
                        ));
                    }

                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND
                    ));
                }

            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'user is no an assistant'
                ));
            }

        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing parameters'
            ));
        }
        return $jsonResponse;
    }

}
