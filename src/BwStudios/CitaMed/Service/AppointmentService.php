<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Appointment;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppointmentService
{

    private $repositoryAppointment;

    private $userService;

    private $em;

    /**
     * AppointmentService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);

        $this->userService = new UserService($this->em);
    }

    /**
     * @param $doctorId
     * @return array|object
     */
    public function buildArrayAppointment($doctorId)
    {
        try {
            $arrayAppointmentsResponse = array();
            $allAppointmentsEntity = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctorId));

            if (count($allAppointmentsEntity) > 0)
                foreach ($allAppointmentsEntity as $appointmentEntity) {
                    $tempResponse = $this->buildAppointmentObject($doctorId, $appointmentEntity, 0, 0);
                    if (isset($tempResponse->Error)) {
                        return $tempResponse;
                    } else {
                        array_push($arrayAppointmentsResponse, $tempResponse);
                    }
                }
            return $arrayAppointmentsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $doctorId
     * @param $dateSync
     * @return array
     */
    public function buildArrayAppointmentSync($doctorId, $dateSync)
    {
        try {
            $arrayAppointmentsResponse = array();
            $allAppointmentsEntity = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctorId));

            $dateSyncRequest = new \DateTime($dateSync);

            foreach ($allAppointmentsEntity as $appointmentEntity) {
                if ($appointmentEntity->getSyncDate() > $dateSyncRequest)
                    array_push($arrayAppointmentsResponse, $this->buildAppointmentObject($doctorId, $appointmentEntity, 0, 0));
            }
            return $arrayAppointmentsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $doctorId
     * @param Appointment $appointment
     * @param $localId
     * @param $patientLocalId
     * @return object
     */
    public function buildAppointmentObject($doctorId, Appointment $appointment, $localId, $patientLocalId)
    {
        try {
            $confirmed = $appointment->getConfirmed();
            if (!isset($confirmed)) {
                $confirmed = false;
            }

            $canceled = $appointment->getCanceled();
            if (!isset($canceled)) {
                $canceled = false;
            }

            $canceledByPatient = $appointment->getCanceledByPatient();
            if (!isset($canceledByPatient)) {
                $canceledByPatient = false;
            }

            $attended = $appointment->getAttended();
            if (!isset($attended)) {
                $attended = false;
            }

            if ($appointment->getUserPlaceConnectionId()) {
                $placeId = $appointment->getUserPlaceConnectionId()->getPlaceId()->getId();
            } else {
                return $this->buildErrorObject(-1);
            }

            return (object)array(
                Constant::_ID => $appointment->getId(),
                Constant::_LOCAL_ID => $localId,
                Constant::_PATIENT_ID => $appointment->getUserThreeId()->getId(),
                Constant::_PATIENT_LOCAL_ID => $patientLocalId,
                Constant::_USER_ID => $doctorId,
                Constant::_PLACE_ID => $placeId,
                Constant::_HEALTH_PROMOTER_ID => $appointment->getEps()->getId(),
                Constant::_DATE => $appointment->getDate()->format(\DateTime::ISO8601),
                Constant::_ASSIGNED_TIME => $appointment->getAssignedTime()->format(\DateTime::ISO8601),
                Constant::_COMMENTS => $appointment->getComment(),
                Constant::_SEND_NOTIFICATIONS => $appointment->getSendNotifications(),
                Constant::_CONFIRMED => $confirmed,
                Constant::_CANCELED => $canceled,
                Constant::_CANCELED_BY_PATIENT => $canceledByPatient,
                Constant::_ATTENDED => $attended
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        } catch (\ErrorException $e2) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}