<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Eps;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\Config\Definition\Exception\Exception;

class EpsService
{

    private $repositoryEps;
    private $repositoryUserEpsConnection;
    private $countryService;
    private $em;

    /**
     * EpsService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant::ENTITY_USER_EPS_CONNECTION);
        $this->countryService = new CountryService($this->em);
    }

    /**
     * @return array|object
     */
    public function findAll()
    {
        try {
            return $this->repositoryEps->findAll();
        } catch (ORMException $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return array|object
     */
    public function buildArrayHealthPromoters()
    {
        try {
            $arrayHealthPromotersResponse = array();
            $allEpsEntity = $this->findAll();
            if (isset($allEpsEntity->Error)) {
                return $this->buildErrorObject(-1);
            } else {
                if (count($allEpsEntity) > 0) {
                    foreach ($allEpsEntity as $epsEntity) {
                        $tempResponse = $this->buildHealthPromoterObject($epsEntity);
                        if (isset($tempResponse->Error)) {
                            return $tempResponse;
                        } else {
                            array_push($arrayHealthPromotersResponse, $tempResponse);
                        }
                    }
                }
            }
            return $arrayHealthPromotersResponse;
        } catch (\Exception $e) {
            error_log('catch buildArrayHealthPromoters');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $dateSync
     * @return array|object
     */
    public function buildArrayHealthPromotersSync($dateSync)
    {
        try {
            $dateSyncRequest = new \DateTime($dateSync);
            $arrayHealthPromotersResponse = array();
            $tempEps = $this->repositoryEps->createQueryBuilder('q')
                ->where('q.sync_date >= \'' . $dateSyncRequest->format('Y-m-d H:i:s') . '\'')
                ->getQuery();
            $allEpsEntity = $tempEps->getResult();
            if (count($allEpsEntity) > 0) {
                foreach ($allEpsEntity as $epsEntity) {
                    $tempResponse = $this->buildHealthPromoterObject($epsEntity);
                    if (isset($tempResponse->Error)) {
                        return $tempResponse;
                    } else {
                        array_push($arrayHealthPromotersResponse, $tempResponse);
                    }
                }
            }
            return $arrayHealthPromotersResponse;
        } catch (\Exception $e) {
            error_log('catch buildArrayHealthPromotersSync');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Eps $eps
     * @return object
     */
    public function buildHealthPromoterObject(Eps $eps)
    {
        try {
            return (object)array(
                Constant::_ID => $eps->getId(),
                Constant::_NAME => $eps->getDescription(),
                Constant::_COUNTRY_ID => $eps->getCountry()->getId()
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }

    /**
     * @param $doctorId
     * @return array|object
     */
    public function getAllEpsByDoctor($doctorId)
    {
        try {
            $allUserEpsConnection = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $doctorId));
            $allEpsResponse = array();
            foreach ($allUserEpsConnection as $ueconnection) {
                $tempResponse = $this->buildHealthPromoterObject($ueconnection->getEpsId());
                if (isset($tempResponse->Error)) {
                    return $tempResponse;
                } else {
                    array_push($allEpsResponse, $tempResponse);
                }
            }
            return $allEpsResponse;
        } catch (Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}