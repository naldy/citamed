<?php

namespace BwStudios\CitaMed\Service;

//Constant
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;

//Symfony utilities
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\Entity;


class StatisticService
{
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionOneThree;
    private $repositoryUserConnectionThreeFour;
    private $repositoryAppointment;
    private $repositoryUserPlaceConnection;
    private $repositoryLogin;
    private $em;

    /**
     *
     * @var EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryLogin = $this->em->getRepository(Constant:: ENTITY_LOGIN);


    }

    /**
     * Obtener cantidad de pacientes agendados en el día
     * @param Request $request
     * @return array|null
     */
    public function getCurrentPatientNumber(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        //json information input
        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );
        //evaluated first condition GO!
        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            //consult Entity ExtraDataUser
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                //consult Entity Appointment
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                $appointmentCounter = 0;
                //get rows where $user exist
                foreach ($dateId as $date) {
                    //interest data
                    $currentDate = $date->getDate()->format('Y-m-d');
                    $isCanceled = $date->getCanceled();
                    //evaluate condition
                    if ($currentDate == ($data_array[Constant::CURRENT_DATE]) && ($isCanceled == null || $isCanceled == 0)) {
                        $appointmentCounter++;
                    }
                    //$totalAppointments = $appointmentCounter;
                }
                // json information output
                array_push($jsonResponse, array(
                    Constant::TOTAL_APPOINTMENTS_TODAY => $appointmentCounter
                ));
                return $jsonResponse;

            } else {
                return $negativeResponse;
            }

            // evaluated second condition Go!
        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            // consult Entity GeneralDataUser
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));
            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();

                try {
                    //consult Entity UserConnectionOneTwo
                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));
                    if (count($takeOfConnection) > 0) {
                        $doctorId = $takeOfConnection->getUserOneId();

                        $doctor = $doctorId->getId();
                        //consult entity Appointment
                        $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctor), array());
                        //get rows where $user exist
                        $appointmentCounter = 0;
                        foreach ($dateId as $date) {
                            // interest data
                            $currentDate = $date->getDate()->format('Y-m-d');
                            $isCanceled = $date->getCanceled();
                            // evaluate condition
                            if ($currentDate == ($data_array[Constant::CURRENT_DATE]) && ($isCanceled == null || $isCanceled == 0)) {
                                $appointmentCounter++;
                            }
                            //$totalAppointments = $appointmentCounter;
                        }
                        // json information output
                        array_push($jsonResponse, array(
                            Constant::TOTAL_APPOINTMENTS_TODAY => $appointmentCounter
                        ));
                        return $jsonResponse;
                    }
                    //$doctorId = $takeOfConnection->getUserOneId();
                } catch (\Doctrine\ORM\ORMException $e) {
                    error_log($e->getMessage());
                } finally {
                    error_log("finally");
                }

            } else {
                return $negativeResponse;
            }
        }
    }

    /**
     * Cantidad pacientes atendidos
     * @param Request $request
     * @return array|null
     */
    public function getAttendedUsers(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                $appointmentCounter = 0;

                foreach ($dateId as $appointment) {
                    $attended = $appointment->getAttended();
                    $currentDate = $appointment->getDate()->format('Y-m-d');

                    $month = $appointment->getDate()->format('Y-m');

                    $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                    $thisMonth = date_format($currentMonth, 'Y-m');

                    if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($attended == 1)) {
                        $appointmentCounter++;
                    }
                }
                array_push($jsonResponse, array(
                    Constant::TOTAL_USERS_ATTENDED => $appointmentCounter
                ));
                return $jsonResponse;

            } else {
                return $negativeResponse;
            }

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            // consult Entity GeneralDataUser
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));
            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();

                try {
                    //consult Entity UserConnectionOneTwo
                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));
                    if (count($takeOfConnection) > 0) {
                        $doctorId = $takeOfConnection->getUserOneId();

                        $doctor = $doctorId->getId();
                        //consult entity Appointment
                        $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctor), array());
                        $appointmentCounter = 0;

                        //get rows where $user exist
                        foreach ($dateId as $appointment) {
                            $attended = $appointment->getAttended();
                            $currentDate = $appointment->getDate()->format('Y-m-d');

                            $month = $appointment->getDate()->format('Y-m');

                            $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                            $thisMonth = date_format($currentMonth, 'Y-m');

                            if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($attended == 1)) {
                                $appointmentCounter++;
                            }
                        }
                        array_push($jsonResponse, array(
                            Constant::TOTAL_USERS_ATTENDED => $appointmentCounter
                        ));
                        return $jsonResponse;
                    }
                    //$doctorId = $takeOfConnection->getUserOneId();
                } catch (\Doctrine\ORM\ORMException $e) {
                    error_log($e->getMessage());
                } finally {
                    error_log("finally");
                }
            } else {
                return $negativeResponse;
            }
        }
    }

    /**
     * Tiempo medio que pasan los pacientes en el consultorio
     * @param Request $request
     * @return array
     */
    public function getAverage(Request $request)
    {
        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::DATE_YEAR => $request->request->get(Constant::DATE_YEAR)
        );
        $jsonResponse = array();
        $negativeResponse = null;
        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {

            $data_user = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($data_user) > 0) {
                $user = $data_user->getUserId();
                $user_id = $user->getId();
                $user_type = $user->getUserTypeId();

                $user_type_data = $this->repositoryUserType->find($user_type);
                $user_type_description = $user_type_data->getDescription();

                if ($user_type_description == Constant::USER_TYPE_DOCTOR) {
                    $attended = 1;
                    for ($i = 1; $i <= 12; $i++) {
                        $sql = 'SELECT SEC_TO_TIME(AVG(TIME_TO_SEC(time_frame))) AS average FROM `Appointment` WHERE user_one_id = :id AND attended = :att AND MONTH(date) = :mon AND YEAR(date)= :ye';
                        $stm = $this->em->getConnection()->prepare($sql);
                        $stm->bindParam('id', $user_id);
                        $stm->bindParam('att', $attended);
                        $stm->bindParam('mon', $i);
                        $stm->bindParam('ye', $data_array[Constant::DATE_YEAR]);
                        $stm->execute();
                        $res = $stm->fetchAll();

                        foreach ($res as $key => $value) {
                            foreach ($value as $values) {
                                array_push($jsonResponse, array(
                                    Constant::DATE_MONTH => $i,
                                    Constant::GENERAL_DESCRIPTION_COLUMN => $values
                                ));
                            }
                        }
                    }
                }
                return $jsonResponse;
            }
        }
    }

    /**
     * Calcular citas canceladas
     * @param Request $request
     * @return array
     */
    public function getCanceled(Request $request)
    {

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::DATE_YEAR => $request->request->get(Constant::DATE_YEAR)
        );
        $jsonResponse = array();
        $negativeResponse = null;

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {

            $data_user = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($data_user) > 0) {
                $user = $data_user->getUserId();
                $user_id = $user->getId();
                $user_type = $user->getUserTypeId();
                $canceled_e = 1;

                $user_type_id = $this->repositoryUserType->find($user_type);
                $user_description = $user_type_id->getDescription();

                if ($user_description == Constant::USER_TYPE_DOCTOR) {

                    for ($i = 1; $i <= 12; $i++) {

                        $sql = 'SELECT COUNT(canceled) AS canceled FROM Appointment WHERE user_one_id = :idUser AND MONTH(date) = :month AND YEAR(date) = :year AND canceled = :cancel';
                        $stm = $this->em->getConnection()->prepare($sql);
                        $stm->bindParam('idUser', $user_id);
                        $stm->bindParam('month', $i);
                        $stm->bindParam('year', $data_array[Constant::DATE_YEAR]);
                        $stm->bindParam('cancel', $canceled_e);
                        $stm->execute();
                        $resulset = $stm->fetchAll();

                        /*$date_canceled = $this->repositoryAppointment->createQueryBuilder('d')
                            ->select('COUNT(d.canceled) AS canceled')
                            ->where('d.userOneId = :idUser')
                            ->andWhere('MONTH(d.date) = :month')
                            ->andWhere('YEAR(d.date) = :year')
                            ->andWhere('d.canceled = :cancel')
                            ->setParameter('idUser', $user_id)
                            ->setParameter('month', $i)
                            ->setParameter('year', $data_array[Constant::DATE_YEAR])
                            ->setParameter('cancel', 1)
                            ->getQuery();
                        $resulset = $date_canceled->getResult();*/

                        foreach ($resulset as $key => $value) {
                            foreach ($value as $keys => $values) {
                                array_push($jsonResponse, array(
                                    Constant::DATE_MONTH => $i,
                                    Constant::CANCELED => $values
                                ));
                            }
                        }
                    }
                }
            }
        }
        return $jsonResponse;
    }

    /**
     * Calcular pacientes atendidos
     * @param Request $request
     * @return array
     */
    public function getAttend(Request $request)
    {

        $data_array = array(

            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::DATE_YEAR => $request->request->get(Constant::DATE_YEAR)
        );

        $jsonResponse = array();
        $negativeResponse = null;

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {

            $data_user = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));
            if (count($data_user) > 0) {
                $user = $data_user->getUserId();
                $user_id = $user->getId();
                $user_type = $user->getUserTypeId();
                $attended_e = 1;
                $user_type_data = $this->repositoryUserType->find($user_type);
                $user_type_description = $user_type_data->getDescription();


                if ($user_type_description == Constant::USER_TYPE_DOCTOR) {

                    for ($i = 1; $i <= 12; $i++) {

                        $sql = 'SELECT COUNT(attended) AS attended FROM Appointment WHERE user_one_id = :idUser AND MONTH(date) = :month AND YEAR(date) = :year AND attended = :attended';
                        $stm = $this->em->getConnection()->prepare($sql);
                        $stm->bindParam('idUser', $user_id);
                        $stm->bindParam('month', $i);
                        $stm->bindParam('year', $data_array[Constant::DATE_YEAR]);
                        $stm->bindParam('attended', $attended_e);
                        $stm->execute();
                        $resulset = $stm->fetchAll();

                        foreach ($resulset as $values) {
                            foreach ($values as $value) {
                                array_push($jsonResponse, array(
                                    Constant::DATE_MONTH => $i,
                                    Constant::ATTENDED => $value
                                ));
                            }
                        }
                    }
                }
            }
            return $jsonResponse;
        }
    }

    /**
     * Cantidad pacientes(citas) que cancelaron
     * @param Request $request
     * @return array
     */
    public function getCanceledUsers(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {

            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                $appointmentCounter = 0;
                foreach ($dateId as $appointment) {
                    $isCanceled = $appointment->getCanceled();
                    $currentDate = $appointment->getDate()->format('Y-m-d');

                    $month = $appointment->getDate()->format('Y-m');

                    $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                    $thisMonth = date_format($currentMonth, 'Y-m');

                    if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($isCanceled == 1)) {
                        $appointmentCounter++;
                    }
                }
                array_push($jsonResponse, array(
                    Constant::TOTAL_USER_CANCELED => $appointmentCounter,
                ));
                return $jsonResponse;

            } else {
                return $negativeResponse;
            }

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $emailAssistant = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($emailAssistant) > 0) {
                $user = $emailAssistant->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {
                    //$takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));


                    //consult Entity UserConnectionOneTwo
                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));
                    if (count($takeOfConnection) > 0) {
                        $doctorId = $takeOfConnection->getUserOneId();

                        $doctor = $doctorId->getId();
                        //consult entity Appointment
                        $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctor), array());
                        //get rows where $user exist
                        $appointmentCounter = 0;
                        foreach ($dateId as $appointment) {
                            $isCanceled = $appointment->getCanceled();
                            $currentDate = $appointment->getDate()->format('Y-m-d');

                            $month = $appointment->getDate()->format('Y-m');

                            $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                            $thisMonth = date_format($currentMonth, 'Y-m');

                            if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($isCanceled == 1)) {
                                $appointmentCounter++;
                            }
                        }
                        array_push($jsonResponse, array(
                            Constant::TOTAL_USER_CANCELED => $appointmentCounter,
                        ));
                        return $jsonResponse;
                    }
                }
            } else {
                return $negativeResponse;
            }
        }
        return $jsonResponse;
    }

    /**
     * Cantidad pacientes que no asistieron mes actual
     * @param Request $request
     * @return array
     */
    public function getNotArrived(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                $appointmentCounter = 0;
                foreach ($dateId as $appointment) {
                    $arrived = $appointment->getArrived();
                    $canceled = $appointment->getCanceled();
                    $currentDate = $appointment->getDate()->format('Y-m-d');

                    $month = $appointment->getDate()->format('Y-m');

                    $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                    $thisMonth = date_format($currentMonth, 'Y-m');

                    if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($arrived != true) && ($canceled != true)) {
                        $appointmentCounter++;
                    }
                }
                array_push($jsonResponse, array(
                    Constant::TOTAL_NOT_ARRIVED_USERS => $appointmentCounter
                ));
                return $jsonResponse;

            } else {
                return $negativeResponse;
            }

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {

                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));

                    if (count($takeOfConnection) > 0) {
                        $doctorId = $takeOfConnection->getUserOneId();

                        $doctor = $doctorId->getId();
                        //consult entity Appointment
                        $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctor), array());
                        //get rows where $user exist
                        $appointmentCounter = 0;
                        foreach ($dateId as $appointment) {
                            $arrived = $appointment->getArrived();
                            $canceled = $appointment->getCanceled();
                            $currentDate = $appointment->getDate()->format('Y-m-d');

                            $month = $appointment->getDate()->format('Y-m');

                            $currentMonth = new\DateTime($data_array[Constant::CURRENT_DATE]);
                            $thisMonth = date_format($currentMonth, 'Y-m');

                            if (($month == $thisMonth) && ($currentDate < $data_array[Constant::CURRENT_DATE]) && ($arrived != true) && ($canceled != true)) {
                                $appointmentCounter++;
                            }
                        }
                        array_push($jsonResponse, array(
                            Constant::TOTAL_NOT_ARRIVED_USERS => $appointmentCounter
                        ));
                        return $jsonResponse;
                    }
                }

            }

        }

    }


    /**
     * Calcular género de pacientes
     * @param Request $request
     * @return array
     */
    public function getPatientGender(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                //$id = $this->repositoryUser->find($user);
                //$idDoctor = $user->getId();

                $takeOfConnection = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $user), array());

                if (count($takeOfConnection) > 0) {

                    $genderMaleCounter = 0;
                    $genderFemaleCounter = 0;
                    $genderUndefinedCounter = 0;

                    foreach ($takeOfConnection as $patient) {
                        $patientId = $patient->getUserThreeId();

                        $patientData = $this->repositoryUser->find($patientId);
                        $patientId = $patientData->getExtraDataUserId();

                        $patientExtraData = $this->repositoryExtraDataUser->find($patientId);
                        $patientGender = $patientExtraData->getGenderId();
                        $genderDescription = $this->repositoryGender->find($patientGender);
                        $gender = $genderDescription->getDescription();

                        if ($gender == Constant::MALE_GENDER) {
                            $genderMaleCounter++;
                        } else {
                            $genderFemaleCounter++;
                        }
                    }
                    array_push($jsonResponse, array(
                        Constant::MALE_GENDER => $genderMaleCounter,
                        Constant::FEMALE_GENDER => $genderFemaleCounter
                    ));
                }
            }
            return $jsonResponse;

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {


                    $takeOfConnectionUserOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));

                    if (count($takeOfConnectionUserOneTwo) > 0) {
                        $doctorId = $takeOfConnectionUserOneTwo->getUserOneId();
                        $myDoctor = $doctorId->getId();


                        $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $myDoctor), array());

                        if (count($takeOfConnectionUserOneThree) > 0) {

                            $genderMaleCounter = 0;
                            $genderFemaleCounter = 0;
                            $genderUndefinedCounter = 0;

                            foreach ($takeOfConnectionUserOneThree as $patient) {
                                $patientId = $patient->getUserThreeId();

                                $patientData = $this->repositoryUser->find($patientId);
                                $patientId = $patientData->getExtraDataUserId();

                                $patientExtraData = $this->repositoryExtraDataUser->find($patientId);
                                $patientGender = $patientExtraData->getGenderId();
                                $genderDescription = $this->repositoryGender->find($patientGender);
                                $gender = $genderDescription->getDescription();

                                if ($gender == Constant::MALE_GENDER) {
                                    $genderMaleCounter++;
                                } else {
                                    $genderFemaleCounter++;
                                }
                            }
                            array_push($jsonResponse, array(
                                Constant::MALE_GENDER => $genderMaleCounter,
                                Constant::FEMALE_GENDER => $genderFemaleCounter
                            ));
                        }
                    }
                }
            }
            return $jsonResponse;
        }
    }

    /**
     * Calcular edades de pacientes
     * @param Request $request
     * @return array
     */
    public function getPatientAge(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();

                $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $user), array());

                if (count($takeOfConnectionUserOneThree) > 0) {
                    $patientAge = null;
                    $currentDate = new \DateTime();
                    $differenceFormat = '%y';
                    $patientCounter = 0;

                    foreach ($takeOfConnectionUserOneThree as $patient) {
                        $patientId = $patient->getUserThreeId();

                        $patientData = $this->repositoryUser->find($patientId);
                        $patientId = $patientData->getExtraDataUserId();

                        $patientExtraData = $this->repositoryExtraDataUser->find($patientId);

                        if ($patientExtraData->getBirthDate()) {
                            $birthDate = $patientExtraData->getBirthDate();
                            $age = date_diff($currentDate, $birthDate);
                            $patientCounter++;

                            array_push($jsonResponse, array(
                                Constant::PATIENT_NUMBER => $patientCounter,
                                Constant::AGE => $age->format($differenceFormat)
                            ));
                        }
                    }

                }
            }
            return $jsonResponse;

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {

                    try {
                        $takeOfConnectionUserOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));

                        if (count($takeOfConnectionUserOneTwo) > 0) {
                            $doctorId = $takeOfConnectionUserOneTwo->getUserOneId();
                            $myDoctor = $doctorId->getId();

                            $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $myDoctor), array());

                            if (count($takeOfConnectionUserOneThree) > 0) {
                                $patientAge = null;
                                $currentDate = new \DateTime();
                                $differenceFormat = '%y';
                                $patientCounter = 0;

                                foreach ($takeOfConnectionUserOneThree as $patient) {
                                    $patientId = $patient->getUserThreeId();

                                    $patientData = $this->repositoryUser->find($patientId);
                                    $patientId = $patientData->getExtraDataUserId();

                                    $patientExtraData = $this->repositoryExtraDataUser->find($patientId);

                                    if ($patientExtraData->getBirthDate()) {
                                        $birthDate = $patientExtraData->getBirthDate();
                                        $age = date_diff($currentDate, $birthDate);
                                        $patientCounter++;

                                        array_push($jsonResponse, array(
                                            Constant::PATIENT_NUMBER => $patientCounter,
                                            Constant::AGE => $age->format($differenceFormat)
                                        ));
                                    }
                                }
                            }
                        }
                    } catch (\Doctrine\ORM\ORMException $e) {
                        error_log($e->getMessage());
                    } finally {
                        error_log('finally');
                    }
                }
            }
            return $jsonResponse;
        }
    }

    /**
     * Calcular pacientes que no asistieron
     * @param Request $request
     * @return array
     */
    public function getAllNotArrived(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUserId();
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                $todayIs = new\DateTime();
                $currentDay = date_format($todayIs, 'Y-m-d');
                $appointmentCounter = 0;
                foreach ($dateId as $appointment) {
                    $arrived = $appointment->getArrived();
                    $canceled = $appointment->getCanceled();
                    $appointmentDate = $appointment->getDate()->format('Y-m-d');

                    if (($appointmentDate < $currentDay) && ($arrived != true) && ($canceled != true)) {
                        $appointmentCounter++;
                    }
                }
                array_push($jsonResponse, array(
                    Constant::TOTAL_NOT_ARRIVED_USERS => $appointmentCounter
                ));
                return $jsonResponse;

            } else {
                return $negativeResponse;
            }

        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {

                    $takeOfConnection = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));

                    if (count($takeOfConnection) > 0) {
                        $doctorId = $takeOfConnection->getUserOneId();

                        $doctor = $doctorId->getId();
                        //consult entity Appointment
                        $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $doctor), array());
                        //get rows where $user exist
                        $todayIs = new\DateTime();
                        $currentDay = date_format($todayIs, 'Y-m-d');
                        $appointmentCounter = 0;
                        foreach ($dateId as $appointment) {
                            $arrived = $appointment->getArrived();
                            $canceled = $appointment->getCanceled();
                            $appointmentDate = $appointment->getDate()->format('Y-m-d');

                            if (($appointmentDate < $currentDay) && ($arrived != true) && ($canceled != true)) {
                                $appointmentCounter++;
                            }
                        }
                        array_push($jsonResponse, array(
                            Constant::TOTAL_NOT_ARRIVED_USERS => $appointmentCounter
                        ));
                        return $jsonResponse;
                    }
                }

            }

        }
    }

    /**
     * Calcular porcentaje pacientes solo han asistido una vez
     * @param Request $request
     * @return array
     */
    public function getOneTimePatient(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $doctorId = $this->repositoryExtraDataUser->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));

            if (count($doctorId) > 0) {
                $user = $doctorId->getUser();
                $userId = $user->getId();

                $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $user), array());

                if (count($takeOfConnectionUserOneThree) > 0) {
                    $patientCounter = 0;
                    $patientOneDate = 0;

                    foreach ($takeOfConnectionUserOneThree as $patient) {
                        $patientCounter++;
                        $patientId = $patient->getUserThreeId();
                        $thisPatient = $this->repositoryUser->find($patientId);
                        $thisPatientId = $thisPatient->getId();

                        $thisPatientCount = $this->repositoryAppointment->createQueryBuilder('c')
                            ->select('count(c.userThreeId) as attendedDate')
                            ->where('c.userThreeId =:patient')
                            ->andWhere('c.userOneId =:doctor')
                            ->andWhere('c.attended =:value')
                            ->setParameter('patient', $thisPatientId)
                            ->setParameter('doctor', $userId)
                            ->setParameter('value', 1)
                            ->getQuery();
                        $count = $thisPatientCount->getResult();

                        foreach ($count as $value) {
                            foreach ($value as $values) {
                                if ($values == 1) {
                                    $patientOneDate++;
                                }
                            }
                        }
                    }
                    array_push($jsonResponse, array(
                        Constant::TOTAL_PATIENTS => $patientCounter,
                        Constant::TOTAL_ONE_TIME_PATIENTS => $patientOneDate
                    ));
                }
                return $jsonResponse;
            }
        } else if ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $assistantEmail = $this->repositoryLogin->findOneBy(array(Constant::USERNAME => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            if (count($assistantEmail) > 0) {
                $user = $assistantEmail->getUserId();
                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isAssistant = $userType->getDescription();

                if ($isAssistant == Constant::USER_TYPE_ASSISTANT) {

                    $takeOfConnectionUserOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $user));

                    if (count($takeOfConnectionUserOneTwo) > 0) {
                        $doctorId = $takeOfConnectionUserOneTwo->getUserOneId();
                        $myDoctor = $doctorId->getId();

                        $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $myDoctor), array());

                        if (count($takeOfConnectionUserOneThree) > 0) {
                            $patientCounter = 0;
                            $patientOneDate = 0;

                            foreach ($takeOfConnectionUserOneThree as $patient) {
                                $patientCounter++;
                                $patientId = $patient->getUserThreeId();
                                $thisPatient = $this->repositoryUser->find($patientId);
                                $thisPatientId = $thisPatient->getId();

                                $thisPatientCount = $this->repositoryAppointment->createQueryBuilder('c')
                                    ->select('count(c.userThreeId) as attendedDate')
                                    ->where('c.userThreeId =:patient')
                                    ->andWhere('c.userOneId =:doctor')
                                    ->andWhere('c.attended =:value')
                                    ->setParameter('patient', $thisPatientId)
                                    ->setParameter('doctor', $myDoctor)
                                    ->setParameter('value', 1)
                                    ->getQuery();
                                $count = $thisPatientCount->getResult();

                                foreach ($count as $value) {
                                    foreach ($value as $values) {
                                        if ($values == 1) {
                                            $patientOneDate++;
                                        }
                                    }
                                }
                            }
                            array_push($jsonResponse, array(
                                Constant::TOTAL_PATIENTS => $patientCounter,
                                Constant::TOTAL_ONE_TIME_PATIENTS => $patientOneDate
                            ));
                        }
                    }
                }
            }
            return $jsonResponse;
        }
    }

    public function datesPerDayLastThirty($request)
    {
        $data = $request->request->all();
        $jsonResponse = array();
        if (!empty($data['doctorId'])) {
            if ($data['doctorId']) {
                $doctorId = $data['doctorId'];
                $today = new \DateTime('today');
                $lastThirtyDays = new \DateTime(strtolower($today->format('Y-m-d') . ' -30 days'));

                //Se trean todas las citas desde los ultimo 30 dias
                $queryAppointment = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.date >=' . $lastThirtyDays . ' and q.date<=' . $today . ' and q.userOneId=' . $doctorId)
                    ->orderBy('q.date', 'ASC')
                    ->getQuery();
                $allAppointmentsPerDoctor = $queryAppointment->getResult();

                //Con todas las citas para ese medico calculamos el numero de citas por dia
                $data = array();
                foreach ($allAppointmentsPerDoctor as $appoinment) {

                    if (sizeof($data) == 0) {
                        $data['' . $appoinment->getDate()->format('m-d')] = array('x' => $appoinment->getDate()->format('d'),
                            'y' => 1);
                    }

                    //foreach ($data as $tmpData)
                    //if($appoinment->getDate()->format('d'))
                }

            } else {
                array_push($jsonResponse, (object)array('status' => false,
                    'message' => 'Validacion de datos incorrecta'));
            }
        } else {
            array_push($jsonResponse, (object)array('status' => false,
                'message' => 'Validacion de datos incorrecta'));
        }
        return $jsonResponse;
    }

    /**
     * @param $req
     * @return object
     */
    public function statisticsPerMonth($req)
    {

        $data = $req->request->all();
        if (!empty($data['doctorId']) && !empty($data['date']) && $data['doctorId'] != null && $data['date'] != null) {
            //Valido que el medico exista
            $doctorUserEntity = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => '1',
                Constant::GENERAL_ID_COLUMN => $data['doctorId']));

            if (sizeof($doctorUserEntity) > 0) {

                $starDayOfMonth = new \DateTime($data['date']);
                $endDayOfMonth = new \DateTime($data['date']);
                $endDayOfMonth->modify('last day of this month');
                $endDayOfMonth = new \DateTime($endDayOfMonth->format('Y-m-d') . ' 23:59:59');

                $queryScheduledAppointments = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.date >= \'' . $starDayOfMonth->format('Y-m-d H:i:s') .
                        '\' and q.date <= \'' . $endDayOfMonth->format('Y-m-d H:i:s') .
                        '\' and q.userOneId=' . $data['doctorId'])
                    ->getQuery();

                $allScheduledAppointments = $queryScheduledAppointments->getResult();

                $countScheduledAppointments = 0;
                $countConfirmedAppointments = 0;
                $countCanceledAppointments = 0;

                foreach ($allScheduledAppointments as $appointment) {

                    if ($appointment->getCanceled() == null || ($appointment->getCanceled() && $appointment->getCanceled() == false)) {
                        $countScheduledAppointments++;
                        if ($appointment->getConfirmed() && $appointment->getConfirmed() == true) {
                            $countConfirmedAppointments++;
                        } else {
                            if ($appointment->getCanceledByPatient() && $appointment->getCanceledByPatient() == true)
                                $countCanceledAppointments++;
                        }
                    }
                }

                $jsonResponse = (object)array('scheduledAppointments' => $countScheduledAppointments,
                    'confirmedAppointments' => $countConfirmedAppointments,
                    'canceledAppointments' => $countCanceledAppointments,
                    'status' => true);

            } else {
                $jsonResponse = (object)array('status' => false,
                    'message' => 'User not found');
            }
        } else {
            $jsonResponse = (object)array('status' => false,
                'message' => 'Validacion de datos incorrecta');
        }
        return $jsonResponse;
    }

    /**
     * @param $req
     * @return object
     */
    public function statisticsPerDay($req)
    {

        $data = $req->request->all();
        if (!empty($data['doctorId']) && !empty($data['date']) && $data['doctorId'] != null && $data['date'] != null) {
            //Valido que el medico exista
            $doctorUserEntity = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => '1',
                Constant::GENERAL_ID_COLUMN => $data['doctorId']));

            if (sizeof($doctorUserEntity) > 0) {

                $starDay = new \DateTime($data['date']);
                $endDay = new \DateTime($data['date'] . '23:59:59');

                $queryScheduledAppointments = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.date >= \'' . $starDay->format('Y-m-d H:i:s') .
                        '\' and q.date <= \'' . $endDay->format('Y-m-d H:i:s') .
                        '\' and q.userOneId=' . $data['doctorId'])
                    ->getQuery();

                $allScheduledAppointments = $queryScheduledAppointments->getResult();

                $countScheduledAppointments = 0;
                $countConfirmedAppointments = 0;
                $countCanceledAppointments = 0;

                foreach ($allScheduledAppointments as $appointment) {

                    if ($appointment->getCanceled() == null || ($appointment->getCanceled() && $appointment->getCanceled() == false)) {
                        $countScheduledAppointments++;
                        if ($appointment->getConfirmed() && $appointment->getConfirmed() == true) {
                            $countConfirmedAppointments++;
                        } else {
                            if ($appointment->getCanceledByPatient() && $appointment->getCanceledByPatient() == true)
                                $countCanceledAppointments++;
                        }
                    }
                }

                $jsonResponse = (object)array('scheduledAppointments' => $countScheduledAppointments,
                    'confirmedAppointments' => $countConfirmedAppointments,
                    'canceledAppointments' => $countCanceledAppointments,
                    'status' => true);

            } else {
                $jsonResponse = (object)array('status' => false,
                    'message' => 'User not found');
            }
        } else {
            $jsonResponse = (object)array('status' => false,
                'message' => 'Validacion de datos incorrecta');
        }
        return $jsonResponse;
    }


    public function mainStatisticsPerMonth($req)
    {
        $data = $req->request->all();
        if ($data['doctorId'] && $data['date']) {
            //Busco el medico
            $userEntity = $this->repositoryUser->find($data['doctorId']);

            if ($userEntity) {
                $date = new \DateTime($data['date']);

                $queryAppointments = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.userOneId = :doctorId')
                    ->andWhere('YEAR(q.date) = :year')
                    ->andWhere('MONTH(q.date) = :month')
                    ->andWhere('q.canceled IS NULL OR q.canceled=0');

                $queryAppointments->setParameter('doctorId', $userEntity->getId())
                    ->setParameter('year', $date->format('Y'))
                    ->setParameter('month', $date->format('m'));

                $allAppointments = $queryAppointments->getQuery()->getResult();

                $epsArray = array();
                if ($allAppointments) {
                    foreach ($allAppointments as $appointment) {

                        $flag = false;
                        foreach ($epsArray as $eps) {
                            if ($eps->epsName == $appointment->getEps()->getDescription()) {
                                $flag = true;
                                $eps->appointments = $eps->appointments + 1;
                            }
                        }

                        if (!$flag) {
                            array_push($epsArray, (object)array('epsName' => $appointment->getEps()->getDescription(),
                                'appointments' => 1));
                        }
                    }
                    $jsonResponse = (object)array('status' => true,
                        'data' => $epsArray);
                }
            } else {
                $jsonResponse = (object)array('status' => false,
                    'message' => 'User not fount');
            }
        } else {
            $jsonResponse = (object)array('status' => false,
                'message' => 'Validacion de datos incorrecta');
        }
        return $jsonResponse;
    }


    public function mainStatisticsPerYear($req)
    {

        $data = $req->request->all();
        if ($data['doctorId'] && $data['year']) {
            //Busco el medico
            $userEntity = $this->repositoryUser->find($data['doctorId']);

            if ($userEntity) {

                $canceledArray = array();
                $attendedArray = array();
                $absencesArray = array();
                $createdArray = array();
                $averageAttentionTimeArray = array();

                for ($i = 1; $i <= 12; $i++) {

                    $queryAppointments = $this->repositoryAppointment->createQueryBuilder('q')
                        ->where('q.userOneId = :doctorId')
                        ->andWhere('YEAR(q.date) = :year')
                        ->andWhere('MONTH(q.date) = :month')
                        ->andWhere('(q.canceled=1 AND q.canceledByPatient=1) OR q.canceled IS NULL');

                    $queryAppointments->setParameter('doctorId', $userEntity->getId())
                        ->setParameter('year', $data['year'])
                        ->setParameter('month', $i);

                    $allAppointments = $queryAppointments->getQuery()->getResult();

                    $created = sizeof($allAppointments);
                    $canceled = 0;
                    $absences = 0;
                    $attended = 0;
                    $averageAttentionTime = 0;
                    $contador = 0;

                    foreach ($allAppointments as $appointment) {

                        if ($appointment->getCanceledByPatient())
                            $canceled++;

                        if ($appointment->getTimeFrame()) {
                            $attended++;
                            $averageAttentionTime += (int)$appointment->getTimeFrame()->format('G') * 60;
                            $averageAttentionTime += (int)$appointment->getTimeFrame()->format('i');
                            $contador++;
                        } else
                            $absences++;

                    }

                    //Termino de calcular para ese mes y agrego el mes a cada item
                    array_push($createdArray, (object)array('month' => $i,
                        'created' => $created));

                    array_push($absencesArray, (object)array('month' => $i,
                        'absences' => $absences));

                    array_push($attendedArray, (object)array('month' => $i,
                        'attended' => $attended));

                    array_push($canceledArray, (object)array('month' => $i,
                        'canceled' => $canceled));

                    array_push($averageAttentionTimeArray, (object)array('month' => $i,
                        'average' => ($contador == 0 ? $averageAttentionTime : $averageAttentionTime / $contador)));
                }

                $jsonResponse = (object)array('status' => true,
                    'data' => (object)array(
                        'created' => $createdArray,
                        'absences' => $absencesArray,
                        'attended' => $attendedArray,
                        'canceled' => $canceledArray,
                        'averageAttentionTime' => $averageAttentionTimeArray
                    ));
            } else {
                $jsonResponse = (object)array('status' => false,
                    'message' => 'User not fount');
            }
        } else {
            $jsonResponse = (object)array('status' => false,
                'message' => 'Validacion de datos incorrecta');
        }
        return $jsonResponse;
    }


    public function mainStatisticsAllTimes($req)
    {

        $data = $req->request->all();
        if ($data['doctorId']) {
            //Busco el medico
            $userEntity = $this->repositoryUser->find($data['doctorId']);

            if ($userEntity) {

                $takeOfConnection = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $userEntity));

                if (count($takeOfConnection) > 0) {

                    $genderMaleCounter = 0;
                    $genderFemaleCounter = 0;

                    $patientAge = null;
                    $currentDate = new \DateTime();
                    $differenceFormat = '%y';
                    $patientCounter = 0;

                    $ageArray = array();

                    foreach ($takeOfConnection as $patient) {

                        //Calculo de las edades
                        $patientId = $patient->getUserThreeId();

                        $patientData = $this->repositoryUser->find($patientId);
                        $patientId = $patientData->getExtraDataUserId();

                        $patientExtraData = $this->repositoryExtraDataUser->find($patientId);

                        if ($patientExtraData->getBirthDate()) {
                            $birthDate = $patientExtraData->getBirthDate();
                            $age = date_diff($currentDate, $birthDate);
                            $patientCounter++;

                            array_push($ageArray, array(
                                Constant::PATIENT_NUMBER => $patientCounter,
                                Constant::AGE => $age->format($differenceFormat)
                            ));
                        }
                        //Calculo de las edades

                        //Calcular el genero
                        $patientId = $patient->getUserThreeId();

                        $patientData = $this->repositoryUser->find($patientId);
                        $patientId = $patientData->getExtraDataUserId();

                        $patientExtraData = $this->repositoryExtraDataUser->find($patientId);
                        $patientGender = $patientExtraData->getGenderId();
                        $genderDescription = $this->repositoryGender->find($patientGender);
                        $gender = $genderDescription->getDescription();

                        if ($gender == Constant::MALE_GENDER) {
                            $genderMaleCounter++;
                        } else {
                            $genderFemaleCounter++;
                        }
                    }
                    $genderObject = (object)array(
                        Constant::MALE_GENDER => $genderMaleCounter,
                        Constant::FEMALE_GENDER => $genderFemaleCounter
                    );
                }
            }
        }
        $jsonResponse = (object)array('status' => true,
            'data' => (object)array(
                'gender' => $genderObject,
                'age' => $ageArray
            ));
        return $jsonResponse;
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}