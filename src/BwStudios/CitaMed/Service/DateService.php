<?php

namespace BwStudios\CitaMed\Service;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Notification;
use BwStudios\CitaMed\Entity\NotificationType;
use BwStudios\CitaMed\Entity\Payment;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserEpsConnection;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;
use BwStudios\CitaMed\Entity\Place;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use co\todoc\PublicAPIBundle\Service\CalendarService;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Proxies\__CG__\BwStudios\CitaMed\Entity\Membership;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\Entity;


class DateService
{
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionOneThree;
    private $repositoryUserConnectionThreeFour;
    private $repositoryAppointment;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $repositoryPayment;
    private $repositoryPaymentMethod;
    private $repositoryNotification;
    private $repositoryNotificationType;
    private $repositoryMembership;
    private $notificationService;

    private $em;

    /**
     *
     * @var EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->notificationService = new NotificationService($this->em);
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
        $this->repositoryPayment = $this->em->getRepository(Constant::ENTITY_PAYMENT);
        $this->repositoryPaymentMethod = $this->em->getRepository(Constant::ENTITY_PAYMENT_METHOD);
        $this->repositoryNotification = $this->em->getRepository(Constant::ENTITY_NOTIFICATION);
        $this->repositoryNotificationType = $this->em->getRepository(Constant::ENTITY_NOTIFICATION_TYPE);
        $this->repositoryMembership = $this->em->getRepository(Constant::ENTITY_MEMBERSHIP);
    }

    /**
     * @param Request $request
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @return array
     */
    public function createDate(Request $request, $freeAppointments, $timeLapseForFreeAppointments)
    {

        $data = array(
            Constant::DATE => $request->request->get(Constant::DATE),
            Constant::COMMENT_DATE => $request->request->get(Constant::COMMENT_DATE),
            Constant::ASSIGNED_TIME => $request->request->get(Constant::ASSIGNED_TIME),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::PLACE_ID => $request->request->get(Constant::PLACE_ID),
            Constant::EPS_ID => $request->request->get(Constant::EPS_ID),
            Constant::IDENTITY_USER_TYPE_PATIENT => $request->request->get(Constant::IDENTITY_USER_TYPE_PATIENT),
            Constant::USER_ID => $request->request->get(Constant::USER_ID),
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::SEND_NOTIFICATIONS => $request->request->get(Constant::SEND_NOTIFICATIONS)
        );

        $jsonResponse = array();
        $isDoctor = false;
        $isAssistant = false;

        //Buscamos el doctor por el id
        if (isset($data[Constant::DOCTOR_ID])) {

            $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);

            if ($userDoctor && $userDoctor->getUserTypeId()->getId() == Constant::TYPE_DOCTOR) {
                //Validator es true si el medico puede crear citas
                $validatorMembership = false;
                //Valido que el medico tenga una membresia valida
                $this->validateMembership($userDoctor, $freeAppointments, $timeLapseForFreeAppointments);
                $membership = $this->repositoryMembership->findOneBy(array(Constant::USER => $userDoctor->getId()));

                //Se valida si el medico es soft o heavy
                if ($membership->getPayMembership()) {
                    $validatorMembership = true;
                } else {
                    if ($membership->getCurrentAppointments() < $membership->getAppointmentsLimit()) {
                        $validatorMembership = true;
                    }
                }
                if ($validatorMembership) {

                    /*Validamos si viene el campo asistente y si la asistente existe, de lo contrario se determina que la request
                    fue enviada por un medico*/
                    if (isset($data[Constant::EMAIL_USER_TYPE_ASSISTANT])) {

                        $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                        if (sizeof($gduAssistants) > 0) {

                            $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                            foreach ($ucoTwo as $ucot) {
                                foreach ($gduAssistants as $gduAssistant) {
                                    if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {
                                        $isAssistant = true;
                                    }
                                }
                            }
                        } else {
                            $jsonResponse = array(
                                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                Constant::MESSAGE_NAME => 'User not found'
                            );
                        }
                    } else {
                        $isDoctor = true;
                    }

                    if ($isDoctor || $isAssistant) {

                        $generaDataDoctor = $this->repositoryGeneralDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userDoctor->getGeneralDataUserId()));
                        $extraDataDoctor = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userDoctor->getExtraDataUserId()));

                        //Se busca el paciente
                        $patientUser = $this->repositoryUser->find($data[Constant::USER_ID]);

                        //Si el paciente se encontro
                        if ($patientUser) {

                            //Se valida que el usuario sea de tipo paciente
                            if ($patientUser->getUserTypeId()->getDescription() == Constant::USER_TYPE_PATIENT) {

                                //Se valida que el usuario y el doctor esten relacionados
                                $isInRelation = $this->repositoryUserConnectionOneThree->findOneBy(array(Constant::USER_ONE_ID => $userDoctor->getId(), Constant::USER_THREE_CONNECTION_COLUMN => $patientUser->getId()));

                                if ($isInRelation) {

                                    $generalDataPatient = $patientUser->getGeneralDataUserId();

                                    if (isset($data[Constant::DATE]) && isset($data[Constant::ASSIGNED_TIME]) &&
                                        isset($data[Constant::PLACE_ID]) && isset($data[Constant::EPS_ID]) && isset($data[Constant::SEND_NOTIFICATIONS])
                                    ) {

                                        $appointment = new Appointment();
                                        $appointment->setUserOneId($userDoctor);
                                        $appointment->setUserThreeId($patientUser);
                                        $appointment->setDate(new \DateTime($data[Constant::DATE]));
                                        $appointment->setComment($data[Constant::COMMENT_DATE]);
                                        $appointment->setAssignedTime(new \DateTime($data[Constant::ASSIGNED_TIME]));

                                        $userPlaceId = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data[Constant::PLACE_ID]));
                                        $appointment->setUserPlaceConnectionId($userPlaceId);

                                        $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data[Constant::EPS_ID]));
                                        if ($eps) {
                                            $appointment->setEps($eps);
                                        } else {
                                            $eps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                                            $appointment->setEps($eps);
                                        }
                                        $appointment->setCreationDate(new \DateTime());
                                        $appointment->setSendNotifications($data[Constant::SEND_NOTIFICATIONS]);
                                        $appointment->setSyncDate(new \DateTime('now'));
                                        $this->em->persist($appointment);
                                        $this->em->flush();
                                        //Sumo la cita a las citas actuales en la tabla membership
                                        $membership->setCurrentAppointments($membership->getCurrentAppointments() + 1);
                                        $this->em->flush($membership);

                                        $this->setDateNotificationForAppointments($appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, false, $appointment->getSendNotifications(), null);

                                        //Se crea la cita en los calendarios que tenga habilitados
                                        $calendarService = new CalendarService($this->em);
                                        //createCalendarEvent
                                        $calendarService->createCalendarEvent($userDoctor, null, $appointment, null);

                                        array_push($jsonResponse, array(
                                            Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL,
                                            Constant::DATE_ID => $appointment->getId(),
                                            'membership' => (object)array(
                                                'payMembership' => $membership->getPayMembership(),
                                                'startPeriod' => $membership->getStartPeriod(),
                                                'endPeriod' => $membership->getEndPeriod(),
                                                'currentAppointments' => $membership->getCurrentAppointments())
                                        ));
                                    } else {
                                        array_push($jsonResponse, array(
                                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                            Constant::MESSAGE_NAME => 'Parameters missing'
                                        ));
                                    }
                                } else {
                                    array_push($jsonResponse, array(
                                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                        Constant::MESSAGE_NAME => 'User and doctor are not in relationship'
                                    ));
                                }
                            } else {
                                array_push($jsonResponse, array(
                                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                    Constant::MESSAGE_NAME => 'User type not match'
                                ));
                            }
                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                Constant::MESSAGE_NAME => 'Patient not found'
                            ));
                        }
                    } else {
                        $jsonResponse = array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'User not found'
                        );
                    }
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::APPOINTMENTS_LIMIT => true,
                        Constant::MESSAGE_NAME => 'Appointments limit reached'
                    ));
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'User not found'
                ));
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'Missing parameters'
            ));
        }
        return $jsonResponse;
    }


    /**
     * @param $patientId
     * @param $appointmentObject
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @param $localId
     * @param $patientLocalId
     * @return null
     */
    public function createDateSync($patientId, $appointmentObject, $freeAppointments, $timeLapseForFreeAppointments, $localId, $patientLocalId)
    {
        try {
            $data = array(
                Constant::USER_ID => $patientId,
                Constant::DOCTOR_ID => $appointmentObject[Constant::_USER_ID],
                Constant::PLACE_ID => $appointmentObject[Constant::_PLACE_ID],
                Constant::EPS_ID => $appointmentObject[Constant::_HEALTH_PROMOTER_ID],
                Constant::DATE => $appointmentObject[Constant::_DATE],
                Constant::ASSIGNED_TIME => $appointmentObject[Constant::_ASSIGNED_TIME],
                Constant::COMMENT_DATE => $appointmentObject[Constant::_COMMENTS],
                Constant::SEND_NOTIFICATIONS => $appointmentObject[Constant::_SEND_NOTIFICATIONS],
                Constant::CONFIRMED => $appointmentObject[Constant::_CONFIRMED],
                Constant::CANCELED => $appointmentObject[Constant::_CANCELED],
                Constant::CANCELED_BY_PATIENT => $appointmentObject[Constant::_CANCELED_BY_PATIENT],
                Constant::ATTENDED => $appointmentObject[Constant::_ATTENDED]
            );

            $isDoctor = false;
            $isAssistant = false;

            //Buscamos el doctor por el id
            if (isset($data[Constant::DOCTOR_ID])) {

                $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);

                if ($userDoctor && $userDoctor->getUserTypeId()->getId() == Constant::TYPE_DOCTOR) {
                    //Validator es true si el medico puede crear citas
                    $validatorMembership = false;
                    //Valido que el medico tenga una membresia valida
                    $this->validateMembership($userDoctor, $freeAppointments, $timeLapseForFreeAppointments);
                    $membership = $this->repositoryMembership->findOneBy(array(Constant::USER => $userDoctor->getId()));

                    //Se valida si el medico es soft o heavy
                    if ($membership->getPayMembership()) {
                        $validatorMembership = true;
                    } else {
                        if ($membership->getCurrentAppointments() < $membership->getAppointmentsLimit()) {
                            $validatorMembership = true;
                        }
                    }
                    if ($validatorMembership) {

                        $generaDataDoctor = $this->repositoryGeneralDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userDoctor->getGeneralDataUserId()));
                        $extraDataDoctor = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userDoctor->getExtraDataUserId()));

                        //Se busca el paciente
                        $patientUser = $this->repositoryUser->find($data[Constant::USER_ID]);

                        //Si el paciente se encontro
                        if ($patientUser) {

                            //Se valida que el usuario sea de tipo paciente
                            if ($patientUser->getUserTypeId()->getDescription() == Constant::USER_TYPE_PATIENT) {

                                //Se valida que el usuario y el doctor esten relacionados
                                $isInRelation = $this->repositoryUserConnectionOneThree->findOneBy(array(Constant::USER_ONE_ID => $userDoctor->getId(), Constant::USER_THREE_CONNECTION_COLUMN => $patientUser->getId()));

                                if ($isInRelation) {

                                    $generalDataPatient = $patientUser->getGeneralDataUserId();

                                    if (isset($data[Constant::DATE]) && isset($data[Constant::ASSIGNED_TIME]) &&
                                        isset($data[Constant::PLACE_ID]) && isset($data[Constant::EPS_ID]) &&
                                        isset($data[Constant::SEND_NOTIFICATIONS])
                                    ) {

                                        $dateAppointment = new \DateTime($data[Constant::DATE]);
                                        /*Validamos que no haya una cita para el mismo dia a la misma hora y para el mismo paciente
                                        y que no este cancelada*/
                                        $queryAppointmentValidation = $this->repositoryAppointment->createQueryBuilder('q')
                                            ->where('q.userThreeId =' . $patientUser->getId())
                                            ->andWhere('q.date =\'' . $dateAppointment->format('Y-m-d H:i:s') . '\'')
                                            ->andWhere('q.canceled = false OR q.canceled IS NULL')
                                            ->getQuery();

                                        $appointmentValidation = $queryAppointmentValidation->getResult();

                                        if (sizeof($appointmentValidation) == 0) {

                                            $appointment = new Appointment();
                                            $appointment->setUserOneId($userDoctor);
                                            $appointment->setUserThreeId($patientUser);
                                            $appointment->setDate($dateAppointment);
                                            $appointment->setComment($data[Constant::COMMENT_DATE]);
                                            $appointment->setAssignedTime(new \DateTime($data[Constant::ASSIGNED_TIME]));

                                            $userPlaceId = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data[Constant::PLACE_ID], Constant::USER_ID => $userDoctor));
                                            if ($userPlaceId) {
                                                $appointment->setUserPlaceConnectionId($userPlaceId);
                                            } else {
                                                return $this->buildErrorObject(9);
                                            }

                                            $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data[Constant::EPS_ID]));
                                            if ($eps) {
                                                $appointment->setEps($eps);
                                            } else {
                                                $eps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                                                $appointment->setEps($eps);
                                            }
                                            $appointment->setCreationDate(new \DateTime());
                                            $appointment->setConfirmed($data[Constant::CONFIRMED]);
                                            $appointment->setCanceled($data[Constant::CANCELED]);
                                            $appointment->setCanceledByPatient($data[Constant::CANCELED_BY_PATIENT]);
                                            $appointment->setAttended($data[Constant::ATTENDED]);
                                            $appointment->setSendNotifications($data[Constant::SEND_NOTIFICATIONS]);
                                            $appointment->setSyncDate(new \DateTime('now'));
                                            $this->em->persist($appointment);
                                            $this->em->flush();

                                            //Sumo la cita a las citas actuales en la tabla membership
                                            $membership->setCurrentAppointments($membership->getCurrentAppointments() + 1);
                                            $this->em->flush($membership);

                                            $this->setDateNotificationForAppointmentsSync($appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, false, $appointment->getSendNotifications());

                                            //Se crea la cita en los calendarios que tenga habilitados
                                            $calendarService = new CalendarService($this->em);
                                            //createCalendarEvent
                                            $calendarService->createCalendarEvent($userDoctor, null, $appointment, null);

                                            $appointmentService = new AppointmentService($this->em);

                                            return $appointmentService->buildAppointmentObject($userDoctor->getId(),
                                                $appointment, $localId, $patientLocalId);
                                        } else {
                                            $appointmentService = new AppointmentService($this->em);
                                            $tempResponse = $appointmentService->buildAppointmentObject($userDoctor->getId(),
                                                $appointmentValidation[0], $localId, $patientLocalId);
                                            $tempResponse->AlreadyExists = true;
                                            return $tempResponse;
                                            //return $this->buildErrorObject(15);
                                        }
                                    } else {
                                        return $this->buildErrorObject(2);
                                    }
                                } else {
                                    return $this->buildErrorObject(5);
                                }
                            } else {
                                return $this->buildErrorObject(1);
                            }
                        } else {
                            return $this->buildErrorObject(1);
                        }
                    } else {
                        return $this->buildErrorObject(6);
                    }
                } else {
                    return $this->buildErrorObject(1);
                }
            } else {
                return $this->buildErrorObject(2);
            }
        } catch (\Exception $e) {
            error_log('catch createDateSync');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $user
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     */
    public function validateMembership($user, $freeAppointments, $timeLapseForFreeAppointments)
    {
        //Busco el registro en la membresia
        $membership = $this->repositoryMembership->findOneBy(array('user' => $user->getId()));

        $today = new \DateTime('today');

        if ($membership) {

            if ($today > $membership->getEndPeriod()) {
                //Se genera un nuevo periodo
                $flagWhile = true;
                $startPeriod = new \DateTime($membership->getEndPeriod()->format('Y-m-d'));
                do {
                    $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                    if ($today < $endPeriod) {
                        $flagWhile = false;
                    } else {
                        $startPeriod = $endPeriod;
                    }
                } while ($flagWhile);

                if ($startPeriod && $endPeriod) {
                    $membership->setCurrentAppointments(0);
                    $membership->setEndPeriod($endPeriod);
                    $membership->setStartPeriod($startPeriod);
                    $membership->setPayMembership(false);
                    $membership->setAppointmentsLimit($freeAppointments);
                    $this->em->flush();
                }
            }
        } else {
            //Deberia tener membresia pero si no la tiene se crea

            //Deberia tener fecha de creacion pero si no lo tiene se toma hoy
            if ($user->getCreationDate() == null) {
                $user->setCreationDate($today);
            }

            $flagWhile = true;
            $startPeriod = new \DateTime($user->getCreationDate()->format('Y-m-d'));
            do {
                $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                if ($today < $endPeriod) {
                    $flagWhile = false;
                } else {
                    $startPeriod = $endPeriod;
                }
            } while ($flagWhile);

            if ($startPeriod && $endPeriod) {
                $membership = new Membership();
                $membership->setUser($user);
                $membership->setCurrentAppointments(0);
                $membership->setEndPeriod($endPeriod);
                $membership->setStartPeriod($startPeriod);
                $membership->setPayMembership(false);
                $membership->setAppointmentsLimit($freeAppointments);
                $this->em->persist($membership);
                $this->em->flush();
            }
        }
    }

    /**
     * @param Appointment $appointment
     * @param GeneralDataUser $generalDataPatient
     * @param GeneralDataUser $generaDataDoctor
     * @param ExtraDataUser $extraDataDoctor
     * @param $isAppointmentEdit
     * @param $sendNotifications
     * @param $previousDate
     */
    private function setDateNotificationForAppointments(Appointment $appointment, GeneralDataUser $generalDataPatient, GeneralDataUser $generaDataDoctor, ExtraDataUser $extraDataDoctor, $isAppointmentEdit, $sendNotifications, $previousDate)
    {
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $appointmentDate = new \DateTime($appointment->getDate()->format("Y-m-d"));

        //Regla de negocio
        //Cita creada para el mísmo día o para el día siguiente a la cita
        if ($appointmentDate->format("Y-m-d") == $today->format("Y-m-d") ||
            $appointmentDate->format("Y-m-d") == $tomorrow->format("Y-m-d")
        ) {
            //Se obtiene el id del registro type=2 de la tabla NotificationType
            $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));

            if (count($notificationType) > 0) {
                $notification = new Notification();
                $notification->setAppointment($appointment);
                $notification->setDateToSend($today);
                $notification->setIsSend(false);
                $notification->setType($notificationType);
                $notification->setSyncDate(new \DateTime('now'));
                $this->em->persist($notification);
                $this->em->flush();

                if ($sendNotifications)
                    $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate);

            } else {
                //TODO: inconsistencia de datos
            }
        } else {

            //Valores para pruebas
            //$today = new \DateTime('2016-08-27');

            $differenceDate = date_diff($today, $appointmentDate);

            //Segunda regla de negocio
            //Si la cita se crea el sabado y la cita es para el Lunes, se debe enviar mail de confirmacion inmediatamente
            if ($differenceDate->format('%a') == 2 && $today->format('w') == 6 && $appointmentDate->format('w') == 1) {
                //Se obtiene el id del registro type=2 de la tabla NotificationType
                $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));

                if (count($notificationType) > 0) {
                    $notification = new Notification();
                    $notification->setAppointment($appointment);
                    $notification->setDateToSend($today);
                    $notification->setIsSend(false);
                    $notification->setType($notificationType);
                    $notification->setSyncDate(new \DateTime('now'));
                    $this->em->persist($notification);
                    $this->em->flush();

                    if ($sendNotifications)
                        $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor,
                            $notification, $isAppointmentEdit, $previousDate);

                    //TODO: aca se puede implementar una logica para que en caso de que no se envie el mail se notifique a otro mail
                } else {
                    //TODO: inconsistencia de datos
                }
            } else {

                //Tercera regla de negocio
                //dos o mas dias y menos de un mes
                $todayPlusTwoDays = new \DateTime(strtolower($today->format('Y-m-d') . ' +2 days'));
                $todayPlusOneMonth = new \DateTime(strtolower($today->format('Y-m-d') . ' +1 month'));

                if ($appointmentDate >= $todayPlusTwoDays && $appointmentDate <= $todayPlusOneMonth) {
                    //Notificacion SMS 1 dia antes
                    //Se obtiene el id del registro type=5 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 5));

                    if ($notificationType) {
                        $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                        //Si la notificacion cae un Domingo se corre para el Sabado
                        if ($appointmentDateLessOneDay->format('w') == 0)
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($appointmentDateLessOneDay);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();
                    }

                    //Notificacion email
                    //Se obtiene el id del registro type=1 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                    if ($notificationType) {
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($today);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();

                        if ($sendNotifications)
                            $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate);
                        //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                        //Setear fecha para el segundo email (de confirmacion)
                        //Se obtiene el id del registro type=2 de la tabla NotificationType
                        $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                        if (count($notificationTypeTwo) > 0) {
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                            //Si la notificacion cae un Domingo se corre para el Sabado
                            if ($appointmentDateLessOneDay->format('w') == 0)
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessOneDay);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeTwo);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                        } else {
                            //TODO: inconsistencia de datos
                        }
                    } else {
                        //TODO: inconsistencia de datos
                    }
                } else {

                    //Cuarta regla de negocio. Citas para mas de 1 mes
                    if ($appointmentDate > $todayPlusOneMonth) {

                        //Se obtiene el id del registro type=1 de la tabla NotificationType
                        $notificationTypeOne = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                        if (count($notificationTypeOne) > 0) {
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($today);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                            //Se envia el email uno
                            if ($sendNotifications)
                                $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate);
                            //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                            //Setear fecha para otro mail de informacion y sms 10 dias antes
                            /*$appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -10 days'));
                            //Si la tonificacion cae un Domingo
                            if ($appointmentDateLessTenDays->format('w') == 0)
                                $appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDateLessTenDays->format('Y-m-d') . ' -1 days'));
                            //Para Email
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessTenDays);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $this->em->persist($notification);
                            $this->em->flush();

                            //Para SMS
                            $notificationTypeSMS = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 3));
                            $SMSNotification = new Notification();
                            $SMSNotification->setAppointment($appointment);
                            $SMSNotification->setDateToSend($appointmentDateLessTenDays);
                            $SMSNotification->setIsSend(false);
                            $SMSNotification->setType($notificationTypeSMS);
                            $this->em->persist($SMSNotification);
                            $this->em->flush();*/

                            //Setear fecha para el segundo email (de confirmacion)
                            //Se obtiene el id del registro type=2 de la tabla NotificationType
                            $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                            if (count($notificationTypeTwo) > 0) {
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                                //Si la notificacion cae un Domingo se corre para el Sabado
                                if ($appointmentDateLessOneDay->format('w') == 0)
                                    $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                                $notification = new Notification();
                                $notification->setAppointment($appointment);
                                $notification->setDateToSend($appointmentDateLessOneDay);
                                $notification->setIsSend(false);
                                $notification->setType($notificationTypeTwo);
                                $notification->setSyncDate(new \DateTime('now'));
                                $this->em->persist($notification);
                                $this->em->flush();
                            } else {
                                //TODO: inconsistencia de datos
                            }
                        } else {

                        }
                    }
                }
            }
        }
    }

    /**
     * @param Appointment $appointment
     * @param GeneralDataUser $generalDataPatient
     * @param GeneralDataUser $generaDataDoctor
     * @param ExtraDataUser $extraDataDoctor
     * @param $isAppointmentEdit
     * @param $sendNotifications
     */
    private function setDateNotificationForAppointmentsSync(Appointment $appointment, GeneralDataUser $generalDataPatient, GeneralDataUser $generaDataDoctor, ExtraDataUser $extraDataDoctor, $isAppointmentEdit, $sendNotifications)
    {
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $appointmentDate = new \DateTime($appointment->getDate()->format("Y-m-d"));

        //Regla de negocio
        //Cita creada para el mísmo día o para el día siguiente a la cita
        if ($appointmentDate->format("Y-m-d") == $today->format("Y-m-d") ||
            $appointmentDate->format("Y-m-d") == $tomorrow->format("Y-m-d")
        ) {
            //Se obtiene el id del registro type=2 de la tabla NotificationType
            $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));

            if (count($notificationType) > 0) {
                $notification = new Notification();
                $notification->setAppointment($appointment);
                $notification->setDateToSend($today);
                $notification->setIsSend(false);
                $notification->setType($notificationType);
                $notification->setSyncDate(new \DateTime('now'));
                $this->em->persist($notification);
                $this->em->flush();

                //if ($sendNotifications)
                //    $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);

            } else {
                //TODO: inconsistencia de datos
            }
        } else {

            //Valores para pruebas
            //$today = new \DateTime('2016-08-27');

            $differenceDate = date_diff($today, $appointmentDate);

            //Segunda regla de negocio
            //Si la cita se crea el sabado y la cita es para el Lunes, se debe enviar mail de confirmacion inmediatamente
            if ($differenceDate->format('%a') == 2 && $today->format('w') == 6 && $appointmentDate->format('w') == 1) {
                //Se obtiene el id del registro type=2 de la tabla NotificationType
                $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));

                if (count($notificationType) > 0) {
                    $notification = new Notification();
                    $notification->setAppointment($appointment);
                    $notification->setDateToSend($today);
                    $notification->setIsSend(false);
                    $notification->setType($notificationType);
                    $notification->setSyncDate(new \DateTime('now'));
                    $this->em->persist($notification);
                    $this->em->flush();

                    //if ($sendNotifications)
                    //    $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);

                    //TODO: aca se puede implementar una logica para que en caso de que no se envie el mail se notifique a otro mail
                } else {
                    //TODO: inconsistencia de datos
                }
            } else {

                //Tercera regla de negocio
                //dos o mas dias y menos de un mes
                $todayPlusTwoDays = new \DateTime(strtolower($today->format('Y-m-d') . ' +2 days'));
                $todayPlusOneMonth = new \DateTime(strtolower($today->format('Y-m-d') . ' +1 month'));

                if ($appointmentDate >= $todayPlusTwoDays && $appointmentDate <= $todayPlusOneMonth) {
                    //Notificacion SMS 1 dia antes
                    //Se obtiene el id del registro type=5 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 5));

                    if ($notificationType) {
                        $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                        //Si la notificacion cae un Domingo se corre para el Sabado
                        if ($appointmentDateLessOneDay->format('w') == 0)
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($appointmentDateLessOneDay);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();
                    }

                    //Notificacion email
                    //Se obtiene el id del registro type=1 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                    if ($notificationType) {
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($today);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();

                        //if ($sendNotifications)
                        //   $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);
                        //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                        //Setear fecha para el segundo email (de confirmacion)
                        //Se obtiene el id del registro type=2 de la tabla NotificationType
                        $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                        if (count($notificationTypeTwo) > 0) {
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                            //Si la notificacion cae un Domingo se corre para el Sabado
                            if ($appointmentDateLessOneDay->format('w') == 0)
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessOneDay);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeTwo);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                        } else {
                            //TODO: inconsistencia de datos
                        }
                    } else {
                        //TODO: inconsistencia de datos
                    }
                } else {

                    //Cuarta regla de negocio. Citas para mas de 1 mes
                    if ($appointmentDate > $todayPlusOneMonth) {

                        //Se obtiene el id del registro type=1 de la tabla NotificationType
                        $notificationTypeOne = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                        if (count($notificationTypeOne) > 0) {
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($today);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                            //Se envia el email uno
                            //if ($sendNotifications)
                            //    $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);
                            //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                            //Setear fecha para otro mail de informacion y sms 10 dias antes
                            /*$appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -10 days'));
                            //Si la tonificacion cae un Domingo
                            if ($appointmentDateLessTenDays->format('w') == 0)
                                $appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDateLessTenDays->format('Y-m-d') . ' -1 days'));
                            //Para Email
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessTenDays);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $this->em->persist($notification);
                            $this->em->flush();

                            //Para SMS
                            $notificationTypeSMS = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 3));
                            $SMSNotification = new Notification();
                            $SMSNotification->setAppointment($appointment);
                            $SMSNotification->setDateToSend($appointmentDateLessTenDays);
                            $SMSNotification->setIsSend(false);
                            $SMSNotification->setType($notificationTypeSMS);
                            $this->em->persist($SMSNotification);
                            $this->em->flush();*/

                            //Setear fecha para el segundo email (de confirmacion)
                            //Se obtiene el id del registro type=2 de la tabla NotificationType
                            $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                            if (count($notificationTypeTwo) > 0) {
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                                //Si la notificacion cae un Domingo se corre para el Sabado
                                if ($appointmentDateLessOneDay->format('w') == 0)
                                    $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                                $notification = new Notification();
                                $notification->setAppointment($appointment);
                                $notification->setDateToSend($appointmentDateLessOneDay);
                                $notification->setIsSend(false);
                                $notification->setType($notificationTypeTwo);
                                $notification->setSyncDate(new \DateTime('now'));
                                $this->em->persist($notification);
                                $this->em->flush();
                            } else {
                                //TODO: inconsistencia de datos
                            }
                        } else {

                        }
                    }
                }
            }
        }
    }

    /**
     * @param $gduPatient
     * @param $eduDoctor
     * @param $appointment
     * @param $gduDoctor
     * @param $notification
     * @param $isAppointmentEdit
     * @param $previousDate
     */
    public function sendInformationEmail($gduPatient, $eduDoctor, $appointment, $gduDoctor, $notification, $isAppointmentEdit, $previousDate)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($gduPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($gduDoctor->getName());
        $email->setLastNameDoctor($gduDoctor->getLastName());
        $email->setEmailDoctor($gduDoctor->getEmail());
        $email->setPersonalTitleDoctor($eduDoctor->getPersonalTitle());
        $email->setEmail($gduPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate());
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        if ($previousDate) {
            $email->setPreviousMonth($months[$previousDate->format('m') - 1]);
            $email->setPreviousDayLetter($days[$previousDate->format('w')]);
            $email->setPreviousDay($previousDate->format('d'));
            $email->setPreviousHour($previousDate->format('h:i a'));
        }

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        $email->setDateToEnd($dateToEnd);

        $email->setGduPatient($gduPatient);
        $email->setEdUDoctor($eduDoctor);
        $email->setAppointment($appointment);
        $email->setGduDoctor($gduDoctor);
        $email->setTipeMail(2);

        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $gduPatient
     * @param $eduDoctor
     * @param $appointment
     * @param $gduDoctor
     * @param $notification
     */
    public function sendIcsEmail($gduPatient, $eduDoctor, $appointment, $gduDoctor, $notification)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $email = new MailService();


        $email->setDay($appointment->getDate()->format('d'));
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));

        $email->setGduPatient($gduPatient);
        $email->setEdUDoctor($eduDoctor);
        $email->setAppointment($appointment);
        $email->setGduDoctor($gduDoctor);
        $email->setEmail($gduPatient->getEmail());
        $email->setPersonalTitleDoctor($eduDoctor->getPersonalTitle());
        $email->setTime($appointment->getDate()->format('h:i a'));

        $email->setTipeMail(9);
        $email->sendEmail();
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     * @param $isAppointmentEdit
     * @param $previousDate
     */
    public function sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate)
    {

        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        if ($previousDate) {
            $email->setPreviousMonth($months[$previousDate->format('m') - 1]);
            $email->setPreviousDayLetter($days[$previousDate->format('w')]);
            $email->setPreviousDay($previousDate->format('d'));
            $email->setPreviousHour($previousDate->format('h:i a'));

            $email->setDay($appointment->getDate()->format('d'));
            $email->setDayLetter($days[$appointment->getDate()->format('w')]);
            $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
            $email->setYear($appointment->getDate()->format('Y'));
        }

        if ($today->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
            $email->setIsToday(true);
        } else {
            if ($tomorrow->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
                $email->setIsTomorrow(true);
            } else {
                $email->setDay($appointment->getDate()->format('d'));
                $email->setDayLetter($days[$appointment->getDate()->format('w')]);
                $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
                $email->setYear($appointment->getDate()->format('Y'));
            }
        }

        $email->setDate($appointment->getDate());

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));

        $email->setDateToEnd($dateToEnd);
        $email->setDateid($appointment->getId());
        $email->setTipeMail(3);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     *
     */
    public function sendNotifications()
    {
        $today = new \DateTime('today');
        $notifications = $this->repositoryNotification->findBy(array('dateToSend' => $today, 'isSend' => false));

        if ($notifications) {

            foreach ($notifications as $notification) {
                $appointment = $this->repositoryAppointment->find($notification->getAppointment());

                if (($appointment->getCanceled() == false || $appointment->getCanceled() == null)
                    && ($appointment->getCanceledByPatient() == false || $appointment->getCanceledByPatient() == null)
                    && ($appointment->getSendNotifications() == true)
                ) {
                    //Datos del doctor
                    $userDoctor = $this->repositoryUser->find($appointment->getUserOneId());
                    $extraDataDoctor = $userDoctor->getExtraDataUserId();
                    $generaDataDoctor = $userDoctor->getGeneralDataUserId();

                    //Datos paciente
                    $userPatient = $this->repositoryUser->find($appointment->getUserThreeId());
                    $generalDataPatient = $userPatient->getGeneralDataUserId();

                    if ($notification->getType()->getType() == 1) {
                        $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, false, null);
                    } else {
                        if ($notification->getType()->getType() == 2) {
                            $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, false, null);
                        }
                    }
                }
            }
        } else {
            //TODO:
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function updateDate(Request $request)
    {
        $data_array = array(
            Constant::DATE => $request->request->get(Constant::DATE),
            Constant::COMMENT_DATE => $request->request->get(Constant::COMMENT_DATE),
            Constant::ASSIGNED_TIME => $request->request->get(Constant::ASSIGNED_TIME),
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
            Constant::USER_PLACE_CONNECTION_ID => $request->request->get(Constant::USER_PLACE_CONNECTION_ID),
            Constant::PLACE_ID => $request->request->get(Constant::PLACE_ID),
            Constant::EPS_ID => $request->request->get(Constant::EPS_ID)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::DOCTOR_ID]));

            if ($user && $user->getUserTypeId()->getId() == 1) {

                $data_date = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                if (count($data_date) > 0) {

                    if ($user == $data_date->getUserOneId()) {

                        $userPlaceId = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data_array[Constant::PLACE_ID]));

                        if (count($userPlaceId) > 0) {

                            $data_date->setDate(new \DateTime($data_array[Constant::DATE]));
                            $data_date->setAssignedTime(new \DateTime($data_array[Constant::ASSIGNED_TIME]));
                            $data_date->setComment($data_array[Constant::COMMENT_DATE]);
                            $data_date->setUserPlaceConnectionId($userPlaceId);
                            $data_date->setSyncDate(new \DateTime('now'));
                            $eps = $this->repositoryEps->find($data_array[Constant::EPS_ID]);
                            if ($eps) {
                                $data_date->setEps($eps);
                            } else {
                                $eps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                                $data_date->setEps($eps);
                            }
                            $this->em->flush();
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            if ($data_array[Constant::DATE_ID]) {

                $appointment = $this->repositoryAppointment->find($data_array[Constant::DATE_ID]);

                if ($appointment) {

                    $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

                    if ($userDoctor == $appointment->getUserOneId()) {
                        $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                        if (sizeof($ucoTwo) > 0) {

                            $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                            if (sizeof($gduAssistants) > 0) {

                                foreach ($ucoTwo as $ucot) {
                                    foreach ($gduAssistants as $gduAssistant) {
                                        $userAssistant = $gduAssistant->getUser();
                                        if ($ucot->getUserTwoId() == $userAssistant) {

                                            $userPlaceId = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data_array[Constant::PLACE_ID]));

                                            if (count($userPlaceId) > 0) {

                                                $appointment->setDate(new \DateTime($data_array[Constant::DATE]));
                                                $appointment->setAssignedTime(new \DateTime($data_array[Constant::ASSIGNED_TIME]));
                                                $appointment->setComment($data_array[Constant::COMMENT_DATE]);
                                                $appointment->setUserPlaceConnectionId($userPlaceId);
                                                $appointment->setSyncDate(new \DateTime('now'));
                                                $eps = $this->repositoryEps->find($data_array[Constant::EPS_ID]);
                                                if ($eps) {
                                                    $appointment->setEps($eps);
                                                } else {
                                                    $eps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                                                    $appointment->setEps($eps);
                                                }
                                                $this->em->flush();
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getDate(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        //Json information Input
        $data_array = array(
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::DOCTOR_ID] != null && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {

                $appointment = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                if ($appointment) {

                    if ($appointment->getUserOneId() == $userDoctor) {
                        /**/
                        //Get office data
                        $placeId = $appointment->getUserPlaceConnectionId();

                        $placeUser = $this->repositoryUserPlaceConnection->find($placeId);
                        $placeIdOffice = $placeUser->getPlaceId();

                        $eps = $appointment->getEps();
                        $epsDescription = $eps->getDescription();

                        //Consult entity Place
                        $place = $this->repositoryPlace->find($placeIdOffice);


                        // Get patient data
                        $userThree = $appointment->getUserThreeId();
                        //Consult entity User
                        $patientData = $this->repositoryUser->find($userThree);
                        $patientId = $patientData->getGeneralDataUserId();
                        //Consult entity GeneralDataUser
                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                        if ($appointment->getTimeFrame() != null) {
                            $timeFrame = $appointment->getTimeFrame()->format('H:i:s');
                        } else {
                            $timeFrame = '00:00:00';
                        }

                        $isPaid = $this->repositoryPayment->findOneBy(array('appointmentId' => $appointment->getId()));
                        if (count($isPaid) > 0) {
                            $paymentChecker = $isPaid->getIsPaid();
                            $amount = $isPaid->getAmount();
                            $paymentMethod = $isPaid->getPaymentMethod()->getDescription();
                            $epsBonus = $isPaid->getEpsPaymentCode();
                            if ($isPaid->getOwe() > 0) {
                                $isOwe = true;
                            } else {
                                $isOwe = false;
                            }
                            $owe = $isPaid->getOwe();
                        } else {
                            $paymentChecker = null;
                            $amount = null;
                            $paymentMethod = null;
                            $epsBonus = null;
                            $owe = null;
                            $isOwe = null;
                        }

                        //json information Output
                        array_push($jsonResponse, array(
                            //doctor's data
                            Constant::DOCTOR_NAME => $userDoctor->getGeneralDataUserId()->getName(),
                            Constant::DOCTOR_LAST_NAME => $userDoctor->getGeneralDataUserId()->getLastName(),
                            //general appointment data
                            Constant::DATE => $appointment->getDate()->format('Y-m-d H:i'),
                            Constant::ASSIGNED_TIME => $appointment->getAssignedTime()->format('H:i'),
                            Constant::TIME_FRAME => $timeFrame,
                            Constant::PLACE => $place->getAddress(),
                            Constant::CREATION_DATE => $appointment->getCreationDate()->format('Y-m-d H:i'),
                            Constant::DATE_ID => $appointment->getId(),
                            //patient data
                            Constant::PATIENT_NAME => $patient->getName(),
                            Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                            Constant::CELL_PHONE => $patient->getCellPhone(),
                            Constant::HOME_PHONE => $patient->getHomePhone(),
                            Constant::EMAIL => $patient->getEmail(),
                            Constant::COMMENT_DATE => $appointment->getComment(),
                            Constant::EPS_NAME => $epsDescription,
                            Constant::IS_PAID => $paymentChecker,
                            Constant::AMOUNT => $amount,
                            Constant::PAYMENT_METHOD => $paymentMethod,
                            Constant::EPS_BONUS => $epsBonus,
                            Constant::OWE => $owe,
                            Constant::IS_OWE => $isOwe,
                            Constant::USER_ID => $userThree->getId(),
                            Constant::SEND_NOTIFICATIONS => $appointment->getSendNotifications()
                        ));
                    } else {
                        array_push($jsonResponse, array(
                            //doctor's data
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'not allowed'
                        ));
                    }
                } else {
                    return $negativeResponse = null;
                }
            }
        } else if ($data_array[Constant::DOCTOR_ID] != null && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT] != null) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

            foreach ($ucoTwo as $ucot) {
                foreach ($gduAssistants as $gduAssistant) {
                    if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {

                        $appointment = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                        if ($appointment) {

                            if ($ucot->getUserOneId() == $appointment->getUserOneId()) {

                                $userOne = $appointment->getUserOneId();
                                //consult entity User relative to doctor's data
                                $doctorData = $this->repositoryUser->find($userOne);
                                //consult entity GeneralDataUser relative to doctor's data
                                $doctor = $this->repositoryGeneralDataUser->find($doctorData);

                                //office data
                                $placeId = $appointment->getUserPlaceConnectionId();

                                $eps_id = $appointment->getEps();
                                //Consult entity Place relative relative to office data
                                $place = $this->repositoryPlace->find($placeId);

                                //patient data
                                $userThree = $appointment->getUserThreeId();
                                //consult entity User relative to patient data
                                $patientData = $this->repositoryUser->find($userThree);
                                //consult entity GeneralDataUser relative to patient data
                                $patientId = $patientData->getGeneralDataUserId();
                                $patient = $this->repositoryGeneralDataUser->find($patientId);

                                if ($appointment->getTimeFrame() != null) {
                                    $timeFrame = $appointment->getTimeFrame()->format('H:i:s');
                                } else {
                                    $timeFrame = '00:00:00';
                                }

                                $isPaid = $this->repositoryPayment->findOneBy(array('appointmentId' => $appointment->getId()));
                                if (count($isPaid) > 0) {
                                    $paymentChecker = $isPaid->getIsPaid();
                                    $amount = $isPaid->getAmount();
                                    $paymentMethod = $isPaid->getPaymentMethod()->getDescription();
                                    $epsBonus = $isPaid->getEpsPaymentCode();
                                    if ($isPaid->getOwe() > 0) {
                                        $isOwe = true;
                                    } else {
                                        $isOwe = false;
                                    }
                                    $owe = $isPaid->getOwe();
                                } else {
                                    $paymentChecker = null;
                                    $amount = null;
                                    $paymentMethod = null;
                                    $epsBonus = null;
                                    $owe = null;
                                    $isOwe = null;
                                }

                                //json information Output
                                array_push($jsonResponse, array(
                                    //doctor's data
                                    Constant::DOCTOR_NAME => $doctor->getName(),
                                    Constant::DOCTOR_LAST_NAME => $doctor->getLastName(),
                                    //general appointment data
                                    Constant::DATE => $appointment->getDate()->format('Y-m-d H:i'),
                                    Constant::ASSIGNED_TIME => $appointment->getAssignedTime()->format('H:i'),
                                    Constant::TIME_FRAME => $timeFrame,
                                    Constant::PLACE => $place->getAddress(),
                                    Constant::CREATION_DATE => $appointment->getCreationDate()->format('Y-m-d H:i'),
                                    Constant::DATE_ID => $appointment->getId(),
                                    //patient data
                                    Constant::PATIENT_NAME => $patient->getName(),
                                    Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                    Constant::CELL_PHONE => $patient->getCellPhone(),
                                    Constant::EMAIL => $patient->getEmail(),
                                    Constant::COMMENT_DATE => $appointment->getComment(),
                                    Constant::EPS_NAME => $eps_id->getDescription(),
                                    Constant::IS_PAID => $paymentChecker,
                                    Constant::AMOUNT => $amount,
                                    Constant::PAYMENT_METHOD => $paymentMethod,
                                    Constant::EPS_BONUS => $epsBonus,
                                    Constant::OWE => $owe,
                                    Constant::IS_OWE => $isOwe,
                                    Constant::USER_ID => $userThree->getId(),
                                    Constant::SEND_NOTIFICATIONS => $appointment->getSendNotifications()
                                ));
                                break;
                            } else {
                                array_push($jsonResponse, array(
                                    //doctor's data
                                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                    Constant::MESSAGE_NAME => 'not allowed'
                                ));
                            }
                        } else {
                            array_push($jsonResponse, array(
                                //doctor's data
                                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND
                            ));
                        }
                    }
                }
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'Missing Parameters'
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function createTimeFrame(Request $request)
    {
        //json information Input
        $data_array = array(
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
            Constant::TIME_FRAME => $request->request->get(Constant::TIME_FRAME),
        );
        //Consult entity Appointment
        $dateId = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

        if (count($dateId) > 0) {
            // set time_frame in table appointment
            $time_frame = $dateId->getTimeFrame();
            if ($time_frame != null) {
                $hour_m = $dateId->getTimeFrame()->format('H') * 60;
                $minute = $dateId->getTimeFrame()->format('i');
                $total_minute = $hour_m + $minute;
                $time_a = $data_array[Constant::TIME_FRAME];
                $date = date_create($time_a);
                date_add($date, date_interval_create_from_date_string('+ ' . $total_minute . ' minutes'));
                $hour_Total = date_format($date, 'H:i:s');
                $dateId->setTimeFrame(new \DateTime($hour_Total));
                $dateId->setSyncDate(new \DateTime('now'));
                $this->em->flush();
                return true;
            } else {
                $dateId->setTimeFrame(new \DateTime($data_array[Constant::TIME_FRAME]));
                $dateId->setAttended(true);
                $dateId->setArrived(true);
                $dateId->setSyncDate(new \DateTime('now'));
                $this->em->flush();
                return true;
            }
        }
    }

    /**
     * Confirmacion desde una app
     * @param Request $request
     * @return bool
     */
    public function confirmDate(Request $request)
    {
        //json information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {

                if ($userDoctor->getUserTypeId()->getId() == 1) {

                    $appointment = $this->repositoryAppointment->find(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                    if ($appointment) {

                        if ($userDoctor == $appointment->getUserOneId()) {
                            $appointment->setConfirmed(1);// // set 1 as true in 'confirmed' field
                            $appointment->setWaitingQueue(0);// set 1 as true in 'waiting_queue' field
                            $appointment->setCanceledByPatient(false);
                            $appointment->setConfirmationDate(new \DateTime());//set new date in 'confirmation_date' field
                            $appointment->setSyncDate(new \DateTime('now'));
                            $this->em->flush();
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

            foreach ($ucoTwo as $ucot) {
                foreach ($gduAssistants as $gduAssistant) {
                    if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {

                        $appointment = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                        if ($appointment) {

                            if ($ucot->getUserOneId() == $appointment->getUserOneId()) {
                                $appointment->setConfirmed(1);// set 1 as true in 'confirmed' field
                                $appointment->setWaitingQueue(0);// set 1 as true in 'waiting_queue' field
                                $appointment->setConfirmationDate(new \DateTime());//set new date in 'confirmation_date' field
                                $appointment->setSyncDate(new \DateTime('now'));
                                $this->em->flush();
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Appointment $appointment
     * @param $previousCancelStatus
     * @return object
     */
    public function confirmDateSync(Appointment $appointment, $previousCancelStatus)
    {
        try {
            $userDoctor = $appointment->getUserOneId();

            $appointment->setConfirmed(1);
            $appointment->setWaitingQueue(0);
            $appointment->setConfirmationDate(new \DateTime('now'));
            $appointment->setSyncDate(new \DateTime('now'));
            $this->em->flush();

            return (object)array(
                Constant::APPOINTMENT_ID => $appointment->getId(),
                Constant::IS_EDIT => true
            );
        } catch (ORMException $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * Cancelacion por parte del medico o asistente
     * @param Request $request
     * @return bool
     */
    public function cancelDate(Request $request)
    {
        //json information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
        );
        //Si es doctor
        if ($data_array[Constant::DOCTOR_ID] != null && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor->getUserTypeId()->getId() == 1) {

                $appointment = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                if ($appointment) {

                    if ($appointment->getUserOneId() == $userDoctor) {

                        $appointment->setCanceled(true);//set 1 as true  in 'canceled' field
                        $appointment->setWaitingQueue(false);// set 1 as true in 'waiting_queue' field
                        $appointment->setSyncDate(new \DateTime('now'));
                        $this->em->flush();

                        //Se crea la notificacion tipo cancellation email
                        //Tipo de notificacion cancellation
                        $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 4));
                        $notification = new Notification();
                        $notification->setType($notificationType);
                        $notification->setDateToSend(new \DateTime('today'));
                        $notification->setAppointment($appointment);
                        $notification->setIsSend(false);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();

                        //Envio mail solo si la cita no ha sido cancelada por el paciente previamente y si el campo
                        //sendNotifications es true
                        if (!$appointment->getCanceledByPatient() && $appointment->getSendNotifications() == true) {
                            //Consulto los valores que se necesitan para armar el email
                            $patientEntityUser = $this->repositoryUser->find($appointment->getUserThreeId());
                            $generalDataPatient = $this->repositoryGeneralDataUser->find($patientEntityUser->getGeneralDataUserId());
                            $extraDataDoctor = $userDoctor->getExtraDataUserId();
                            $generaDataDoctor = $userDoctor->getGeneralDataUserId();
                            $this->sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                        }

                        $eventId = $appointment->getGoogleCalendarId();
                        //Elimino el evento en los calendarios respectivos
                        $calendarService = new CalendarService($this->em);
                        $calendarService->deleteGoogleEvent($eventId, $userDoctor, null);
                        return true;
                    }
                }
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

            $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

            foreach ($ucoTwo as $ucot) {
                foreach ($gduAssistants as $gduAssistant) {
                    if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {

                        $appointment = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                        if ($appointment) {

                            if ($userDoctor == $appointment->getUserOneId()) {
                                $appointment->setCanceled(true);//set 1 as true  in 'canceled' field
                                $appointment->setWaitingQueue(false);// set 1 as true in 'waiting_queue' field
                                $appointment->setSyncDate(new \DateTime('now'));
                                $this->em->flush();

                                //Se crea la notificacion tipo cancellation email
                                //Tipo de notificacion cancellation
                                $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 4));
                                $notification = new Notification();
                                $notification->setType($notificationType);
                                $notification->setDateToSend(new \DateTime('today'));
                                $notification->setAppointment($appointment);
                                $notification->setIsSend(false);
                                $notification->setSyncDate(new \DateTime('now'));
                                $this->em->persist($notification);
                                $this->em->flush();

                                if (!$appointment->getCanceledByPatient() && $appointment->getSendNotifications() == true) {
                                    //Consulto los valores que se necesitan para armar el email
                                    $patientEntityUser = $this->repositoryUser->find($appointment->getUserThreeId());
                                    $generalDataPatient = $this->repositoryGeneralDataUser->find($patientEntityUser->getGeneralDataUserId());
                                    $extraDataDoctor = $userDoctor->getExtraDataUserId();
                                    $generaDataDoctor = $userDoctor->getGeneralDataUserId();
                                    $this->sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                                }

                                //Elimino el evento en los calendarios respectivos
                                $eventId = $appointment->getGoogleCalendarId();
                                $calendarService = new CalendarService($this->em);
                                $calendarService->deleteGoogleEvent($eventId, $userDoctor, null);

                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Appointment $appointment
     * @param $previousCancelStatus
     * @param $sendNotifications
     * @return object
     */
    public function cancelDateSync(Appointment $appointment, $previousCancelStatus, $sendNotifications)
    {
        try {
            $userDoctor = $appointment->getUserOneId();

            $appointment->setCanceled(true);
            $appointment->setWaitingQueue(false);// set 1 as true in 'waiting_queue' field
            $appointment->setSyncDate(new \DateTime('now'));
            $appointment->setSendNotifications($sendNotifications);
            $this->em->persist($appointment);
            $this->em->flush();

            if (($previousCancelStatus == false || $previousCancelStatus == null) &&
                ($sendNotifications == true)
            ) {
                //Se crea la notificacion tipo cancellation email
                //Tipo de notificacion cancellation
                $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 4));
                $notification = new Notification();
                $notification->setType($notificationType);
                $notification->setDateToSend(new \DateTime('today'));
                $notification->setAppointment($appointment);
                $notification->setIsSend(false);
                $notification->setSyncDate(new \DateTime('now'));
                $this->em->persist($notification);
                $this->em->flush();

                //Envio mail solo si la cita no ha sido cancelada por el paciente previamente y si el campo
                //sendNotifications es true
                /*if (!$appointment->getCanceledByPatient() && $appointment->getSendNotifications() == true) {
                    //Consulto los valores que se necesitan para armar el email
                    $patientEntityUser = $this->repositoryUser->find($appointment->getUserThreeId());
                    $generalDataPatient = $this->repositoryGeneralDataUser->find($patientEntityUser->getGeneralDataUserId());
                    $extraDataDoctor = $userDoctor->getExtraDataUserId();
                    $generaDataDoctor = $userDoctor->getGeneralDataUserId();
                    $this->sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                }*/
            }

            //Elimino el evento en los calendarios respectivos
            $eventId = $appointment->getGoogleCalendarId();
            $calendarService = new CalendarService($this->em);
            $calendarService->deleteGoogleEvent($eventId, $userDoctor, null);

            return (object)array(
                Constant::APPOINTMENT_ID => $appointment->getId(),
                Constant::IS_CANCELED => true
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function appointmentPayment(Request $request)
    {
        $data = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID),
            Constant::IS_PAID => $request->request->get(Constant::IS_PAID),
            Constant::PAYMENT_METHOD => $request->request->get(Constant::PAYMENT_METHOD),
            Constant::AMOUNT => $request->request->get(Constant::AMOUNT),
            Constant::EPS_PAYMENT_CODE => $request->request->get(Constant::EPS_PAYMENT_CODE),
            Constant::OWE => $request->request->get(Constant::OWE)
        );

        $jsonResponse = array();
        $isDoctor = false;
        $isAssistant = false;

        //TODO:: este es el codigo que debe quedar al final, se comenta por ahora
        //Buscamos el doctor por el id
        /*if (isset($data[Constant::DOCTOR_ID])) {

            $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);

            if ($userDoctor && $userDoctor->getUserTypeId()->getId() == Constant::TYPE_DOCTOR) {

                //Validamos si viene el campo asistente y si la asistente existe, de lo contrario se determina que la request
                //fue enviada por un medico
                if (isset($data[Constant::EMAIL_USER_TYPE_ASSISTANT])) {

                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {

                        $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {
                                    $isAssistant = true;
                                }
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    $isDoctor = true;
                }

                if ($isDoctor || $isAssistant) {

                    $appointmentEntity = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data[Constant::DATE_ID]));

                    if ($appointmentEntity) {

                        if ($userDoctor == $appointmentEntity->getUserOneId()) {

                            $paymentMethod = $this->repositoryPaymentMethod->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data[Constant::PAYMENT_METHOD]));

                            if ($paymentMethod) {

                                $paymentDescription = $paymentMethod->getDescription();

                                $paymentFinder = $this->repositoryPayment->findOneBy(array(Constant::APPOINTMENT_ID => $appointmentEntity->getId()));

                                if ($paymentFinder) {
                                    if ($paymentDescription == Constant::EPS_BONUS) {
                                        $paymentFinder->setIsPaid($data[Constant::IS_PAID]);
                                        $paymentFinder->setAmount($data[Constant::AMOUNT]);
                                        $paymentFinder->setPaymentMethod($paymentMethod);
                                        $paymentFinder->setAppointmentId($appointmentEntity);
                                        $paymentFinder->setEpsPaymentCode($data[Constant::EPS_PAYMENT_CODE]);
                                        $paymentFinder->setOwe($data[Constant::OWE]);
                                        $this->em->persist($paymentFinder);
                                        $this->em->flush();
                                    } else {
                                        $paymentFinder->setIsPaid($data[Constant::IS_PAID]);
                                        $paymentFinder->setAmount($data[Constant::AMOUNT]);
                                        $paymentFinder->setPaymentMethod($paymentMethod);
                                        $paymentFinder->setAppointmentId($appointmentEntity);
                                        $paymentFinder->setOwe($data[Constant::OWE]);
                                        $this->em->persist($paymentFinder);
                                        $this->em->flush();
                                    }
                                    return true;
                                } else {
                                    $paymentEntity = new Payment();

                                    if ($paymentDescription == Constant::EPS_BONUS) {
                                        $paymentEntity->setIsPaid($data[Constant::IS_PAID]);
                                        $paymentEntity->setAmount($data[Constant::AMOUNT]);
                                        $paymentEntity->setPaymentMethod($paymentMethod);
                                        $paymentEntity->setAppointmentId($appointmentEntity);
                                        $paymentEntity->setEpsPaymentCode($data[Constant::EPS_PAYMENT_CODE]);
                                        $paymentEntity->setOwe($data[Constant::OWE]);
                                        $this->em->persist($paymentEntity);
                                        $this->em->flush();
                                    } else {
                                        $paymentEntity->setIsPaid($data[Constant::IS_PAID]);
                                        $paymentEntity->setAmount($data[Constant::AMOUNT]);
                                        $paymentEntity->setPaymentMethod($paymentMethod);
                                        $paymentEntity->setAppointmentId($appointmentEntity);
                                        $paymentEntity->setOwe($data[Constant::OWE]);
                                        $this->em->persist($paymentEntity);
                                        $this->em->flush();
                                    }
                                    return true;
                                }
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }*/

        if (isset($data[Constant::DOCTOR_ID])) {

            $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);

            if ($userDoctor && $userDoctor->getUserTypeId()->getId() == Constant::TYPE_DOCTOR) {

                $isDoctor = true;

            } else {
                return false;
            }
        } else {
            //Validamos si viene el campo asistente y si la asistente existe
            if (isset($data[Constant::EMAIL_USER_TYPE_ASSISTANT])) {
                $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                if (sizeof($gduAssistants) > 0) {
                    foreach ($gduAssistants as $gduAssistant) {
                        if ($gduAssistant->getUser()->getUserTypeId()->getId() == 2) {
                            $ucoTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_ID => $gduAssistant));
                            if ($ucoTwo) {
                                $userDoctor = $ucoTwo->getUserOneId();
                                $isAssistant = true;
                                break;
                            } else {
                                return false;
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        if ($isDoctor || $isAssistant) {

            $appointmentEntity = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data[Constant::DATE_ID]));

            if ($appointmentEntity) {

                if ($userDoctor == $appointmentEntity->getUserOneId()) {

                    $paymentMethod = $this->repositoryPaymentMethod->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data[Constant::PAYMENT_METHOD]));

                    if ($paymentMethod) {

                        $paymentDescription = $paymentMethod->getDescription();

                        $paymentFinder = $this->repositoryPayment->findOneBy(array(Constant::APPOINTMENT_ID => $appointmentEntity->getId()));

                        if ($paymentFinder) {
                            if ($paymentDescription == Constant::EPS_BONUS) {
                                $paymentFinder->setIsPaid($data[Constant::IS_PAID]);
                                $paymentFinder->setAmount($data[Constant::AMOUNT]);
                                $paymentFinder->setPaymentMethod($paymentMethod);
                                $paymentFinder->setAppointmentId($appointmentEntity);
                                $paymentFinder->setEpsPaymentCode($data[Constant::EPS_PAYMENT_CODE]);
                                $paymentFinder->setOwe($data[Constant::OWE]);
                                $this->em->persist($paymentFinder);
                                $this->em->flush();
                            } else {
                                $paymentFinder->setIsPaid($data[Constant::IS_PAID]);
                                $paymentFinder->setAmount($data[Constant::AMOUNT]);
                                $paymentFinder->setPaymentMethod($paymentMethod);
                                $paymentFinder->setAppointmentId($appointmentEntity);
                                $paymentFinder->setOwe($data[Constant::OWE]);
                                $this->em->persist($paymentFinder);
                                $this->em->flush();
                            }
                            return true;
                        } else {
                            $paymentEntity = new Payment();

                            if ($paymentDescription == Constant::EPS_BONUS) {
                                $paymentEntity->setIsPaid($data[Constant::IS_PAID]);
                                $paymentEntity->setAmount($data[Constant::AMOUNT]);
                                $paymentEntity->setPaymentMethod($paymentMethod);
                                $paymentEntity->setAppointmentId($appointmentEntity);
                                $paymentEntity->setEpsPaymentCode($data[Constant::EPS_PAYMENT_CODE]);
                                $paymentEntity->setOwe($data[Constant::OWE]);
                                $this->em->persist($paymentEntity);
                                $this->em->flush();
                            } else {
                                $paymentEntity->setIsPaid($data[Constant::IS_PAID]);
                                $paymentEntity->setAmount($data[Constant::AMOUNT]);
                                $paymentEntity->setPaymentMethod($paymentMethod);
                                $paymentEntity->setAppointmentId($appointmentEntity);
                                $paymentEntity->setOwe($data[Constant::OWE]);
                                $this->em->persist($paymentEntity);
                                $this->em->flush();
                            }
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Confirmacion por usuario (correo)
     * @param Request $request
     * @return bool
     */
    public function confirmedDataEmail(Request $request)
    {

        //get('date') trae el id codificado en base64
        //get('data') trae la fecha de la cita codificado en base64
        $var = $request->query->get('date');
        $data = $request->query->get('data');
        $date_id = base64_decode($var);
        $date_data = base64_decode($data);
        $current_data = date('Y-m-d');
        $dateId = $this->repositoryAppointment->findOneBy(array(Constant::GENERAL_ID_COLUMN => $date_id));
        $confirmed = $dateId->getConfirmed();
        $statusCanceled = $dateId->getCanceled();
        $statusCanceledByPatient = $dateId->getCanceledByPatient();
        //Si confirmed = true,o sea que la cita ya fue confirmada previamente
        if ($statusCanceled == true) {
            return 5;
        } else {
            if ($confirmed == 1) {
                return 4;
            } elseif (count($dateId) > 0) {
                //Si la cita ya caduco
                if ($current_data > $date_data) {
                    return 3;
                } else {
                    if ($statusCanceledByPatient == true) {
                        $dateId->setCanceledByPatient(false);
                    }
                    $dateId->setConfirmed(true);// // set 1 as true in 'confirmed' field
                    $dateId->setWaitingQueue(false);// set 1 as true in 'waiting_queue' field
                    $dateId->setConfirmationDate(new \DateTime());//set new date in 'confirmation_date' field
                    $dateId->setSyncDate(new \DateTime('now'));
                    $this->em->flush();
                    return 1;
                }
            } else {
                //Cita que no existe
                return 2;
            }
        }
    }

    /**
     * Cancelacion por parte del paciente
     * @param Request $request
     * @return bool
     */
    public function cancelDataEmail(Request $request)
    {
        $var = $request->query->get('date');
        $data = $request->query->get('data');
        $date_id = base64_decode($var);
        $date_data = base64_decode($data);
        $current_data = date('Y-m-d');
        $dateId = $this->repositoryAppointment->findOneBy(array(Constant::GENERAL_ID_COLUMN => $date_id));
        $confirmed = $dateId->getConfirmed();
        $statusCanceled = $dateId->getCanceled();
        $statusCanceledByPatient = $dateId->getCanceledByPatient();
        //Si la cita ya la confirmo esta logica indica que no la puede cancelar
        if ($statusCanceledByPatient == true || $statusCanceled == true) {
            //Cita que ya fue cancelada
            return 5;
        } elseif (count($dateId) > 0) {
            if ($current_data > $date_data) {
                //Cita caducada
                return 3;
            } else {
                if ($confirmed == true) {
                    $dateId->setConfirmed(false);
                }
                $dateId->setCanceledByPatient(true);// // set 1 as true in 'confirmed' field
                //Si la cancela el paciente, la cita aun sigue en lista de espera segun esta logica
                $dateId->setWaitingQueue(true);// set 1 as true in 'waiting_queue' field
                $dateId->setSyncDate(new \DateTime('now'));
                $this->em->flush();
                return 1;
            }
        } else {
            //No encontro la cita
            return 2;
        }
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     */
    public function sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate()->format('Y-m-d'));
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());
        $email->setTipeMail(7);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    /**
     * @param $userId
     * @return array
     */
    public function resendsms($userId)
    {
        $responseService = array();

        //Verificar si existe el usuario
        $userEntity = $this->repositoryUser->find($userId);

        if ($userEntity && ($userEntity->getUserTypeId()->getId() === 1 || $userEntity->getUserTypeId()->getId() === 2)) {

            if ($userEntity->getUserTypeId()->getId() === 1) {

                $extraDataUser = $userEntity->getExtraDataUserId();

                if ($extraDataUser != null) {
                    $dataArray = $this->getAllSMSNotificationByDoctor($userEntity, $extraDataUser);
                    $responseService = array(
                        Constant::STATUS_NAME => true,
                        'data' => $dataArray
                    );
                } else {
                    $responseService = array(
                        Constant::STATUS_NAME => false,
                        Constant::MESSAGE_NAME => 'User not found'
                    );
                }
            } elseif ($userEntity->getUserTypeId()->getId() === 2) {

                $userConnectionOneTwoEntity = $this->repositoryUserConnectionOneTwo->findOneBy(array("userTwoId" => $userEntity->getId()));

                if ($userConnectionOneTwoEntity) {

                    $userDoctorEntity = $userConnectionOneTwoEntity->getUserOneId();

                    if ($userDoctorEntity) {

                        $extraDataUser = $userDoctorEntity->getExtraDataUserId();

                        if ($extraDataUser) {
                            $dataArray = $this->getAllSMSNotificationByDoctor($userDoctorEntity, $extraDataUser);
                            $responseService = array(
                                Constant::STATUS_NAME => true,
                                'data' => $dataArray
                            );
                        } else {
                            $responseService = array(
                                Constant::STATUS_NAME => false,
                                Constant::MESSAGE_NAME => 'ExtraDataUser para el doctor no encontrado'
                            );
                        }
                    } else {
                        $responseService = array(
                            Constant::STATUS_NAME => false,
                            Constant::MESSAGE_NAME => 'Doctor no encontrado'
                        );
                    }
                } else {
                    $responseService = array(
                        Constant::STATUS_NAME => false,
                        Constant::MESSAGE_NAME => 'Doctor no encontrado para este asistente'
                    );
                }
            }
        } else {
            $responseService = array(
                Constant::STATUS_NAME => false,
                Constant::MESSAGE_NAME => 'User not found'
            );
        }
        return $responseService;
    }

    /**
     * @param $user
     * @param $extraDatauser
     * @return array
     */
    public function getAllSMSNotificationByDoctor($user, $extraDatauser)
    {
        $today = new \DateTime('today');
        $now = new \DateTime('now');
        $dataArray = array();
        $fiveDaysBefore = new \DateTime(strtolower($today->format('Y-m-d') . ' -5 days'));

        //Para las notificaciones sms de un dia antes (tipo 5), solo se permitira el envio de estas hasta las 7 pm
        if ($now->format('H:i') <= '19:00') {
            $SQLWhere = '((q.' . Constant::NOTIFICATION_TYPE_TYPE . '=5 AND q.dateToSend = \'' . $today->format('Y-m-d H:i:s') . '\') OR (q.' . Constant::NOTIFICATION_TYPE_TYPE . '=3 AND q.dateToSend>=\'' . $fiveDaysBefore->format('Y-m-d H:i:s') . '\' AND q.dateToSend<= \'' . $today->format('Y-m-d H:i:s') . '\') OR (q.' . Constant::NOTIFICATION_TYPE_TYPE . '=6 AND q.dateToSend= \'' . $today->format('Y-m-d H:i:s') . '\')) AND q.isSend=false';
        } else {
            $SQLWhere = '((q.' . Constant::NOTIFICATION_TYPE_TYPE . '=3 AND q.dateToSend>=\'' . $fiveDaysBefore->format('Y-m-d H:i:s') . '\' AND q.dateToSend<= \'' . $today->format('Y-m-d H:i:s') . '\') OR (q.' . Constant::NOTIFICATION_TYPE_TYPE . '=6 AND q.dateToSend= \'' . $today->format('Y-m-d H:i:s') . '\')) AND q.isSend=false';
        }
        //Se trae todas las notificaciones para hoy
        $queryNotifications = $this->repositoryNotification->createQueryBuilder('q')
            ->where($SQLWhere)
            ->getQuery();

        $notifications = $queryNotifications->getResult();

        foreach ($notifications as $notification) {
            //Se filtran las notificaciones solo para ese medico
            if ($notification->getAppointment()->getUserOneId()->getId() == $user->getId()) {

                if (($notification->getAppointment()->getCanceled() == false || $notification->getAppointment()->getCanceled() == null)
                    && ($notification->getAppointment()->getCanceledByPatient() == false || $notification->getAppointment()->getCanceledByPatient() == null)
                    && ($notification->getAppointment()->getSendNotifications() == true)
                ) {
                    $place = $notification->getAppointment()->getUserPlaceConnectionId()->getPlaceId();

                    //Entidad user para paciente
                    $patient = $this->repositoryUser->find($notification->getAppointment()->getUserThreeId());
                    $patientGeneralDataUser = $this->repositoryGeneralDataUser->find($patient->getGeneralDataUserId());

                    array_push($dataArray, (object)array(
                        'notificationId' => $notification->getId(),
                        'smsTypeNotification' => $notification->getType()->getId(),
                        'name' => $patientGeneralDataUser->getName(),
                        'lastName' => $patientGeneralDataUser->getLastName(),
                        'patientId' => $patient->getId(),
                        'date' => $notification->getAppointment()->getDate()->format('Y-m-d H:i:s'),
                        'cellPhone' => $patientGeneralDataUser->getCellPhone(),
                        'dateId' => $notification->getAppointment()->getId(),
                        'officePhone' => $place->getPhone()
                    ));
                }
            }
        }
        return $dataArray;
    }

    /**
     * @param $request
     * @return bool
     */
    public function setToSendSMS($request)
    {
        $notificationsArray = $request->request->all();
        $generalStatus = false;
        if (count($notificationsArray) > 0) {
            foreach ($notificationsArray as $notification) {
                $generalStatus = false;
                $notificationEntity = $this->repositoryNotification->findOneBy(array('id' => $notification, Constant::NOTIFICATION_TYPE_IS_SEND => false));
                if ($notificationEntity) {
                    $notificationEntity->setIsSend(true);
                    $notificationEntity->setSyncDate(new \DateTime('now'));
                    $this->em->flush($notificationEntity);
                    $generalStatus = true;
                } else {
                    $generalStatus = true;
                }
                //TODO: mejorar asignacion de generalStatus
            }
            return $generalStatus;
        }
    }

    /**
     * @param $request
     * @return array|mixed
     */
    public function listAllByPatient($request)
    {
        $jsonResponse = array();
        $data = $request->request->all();
        if (!empty($data['userId'])) {
            if ($data['userId']) {
                $userId = $data['userId'];
                $queryAppointments = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.userThreeId=' . $userId . ' and (q.canceled=false OR q.canceled IS NULL) and (q.canceledByPatient=false OR q.canceledByPatient IS NULL)')
                    ->orderBy('q.date', 'DESC')
                    ->getQuery();

                $appointmentsByUser = $queryAppointments->getResult();
                $listOfAppointments = array();
                if ($appointmentsByUser && sizeof($appointmentsByUser) > 0) {
                    foreach ($appointmentsByUser as $appointment) {
                        /*array_push($listOfAppointments, (object)array(
                            'id' => $appointment->getId(),
                            'userOneId' => $appointment->getUserOneId()->getId(),
                            'userThreeId' => $appointment->getUserThreeId()->getId(),
                            'date' => $appointment->getDate(),
                            'confirmed' => $appointment->getConfirmed(),
                            'waitingQueue' => $appointment->getWaitingQueue(),
                            'attended' => $appointment->getAttended(),
                            'canceled' => $appointment->getCanceled(),
                            'arrived' => $appointment->getArrived(),
                            'timeFrame' => $appointment->getTimeFrame(),
                            'assignedTime' => $appointment->getAssignedTime(),
                            'creationDate' => $appointment->getCreationDate(),
                            'confirmationDate' => $appointment->getConfirmationDate(),
                            'canceledByPatient' => $appointment->getCanceledByPatient(),
                            'place' => array('address' => $appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress(),
                                'phone' => $appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone()),
                        ));*/
                        array_push($listOfAppointments, array(
                            'dateId' => $appointment->getId(),
                            'date' => $appointment->getDate(),
                            'assignedTime' => $appointment->getAssignedTime()->format('H:i'),
                            'confirmed' => $appointment->getConfirmed(),
                            'attended' => $appointment->getAttended(),
                            'timeFrame' => $appointment->getTimeFrame(),
                            'place' => $appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress(),
                            'creationDate' => $appointment->getCreationDate(),
                            'patientName' => $appointment->getUserThreeId()->getGeneralDataUserId()->getName(),
                            'patientLastName' => $appointment->getUserThreeId()->getGeneralDataUserId()->getLastName(),
                            'cellPhone' => $appointment->getUserThreeId()->getGeneralDataUserId()->getCellPhone(),
                            'homePhone' => $appointment->getUserThreeId()->getGeneralDataUserId()->getHomePhone(),
                            'email' => $appointment->getUserThreeId()->getGeneralDataUserId()->getEmail(),
                            'comment' => $appointment->getUserThreeId()->getExtraDataUserId()->getComment(),
                            'epsName' => $appointment->getEps()->getDescription(),
                            'userId' => $appointment->getUserThreeId()->getId(),
                        ));
                    }
                    array_push($jsonResponse, (object)array('status' => true, 'data' => $listOfAppointments));
                    $jsonResponse = $jsonResponse[0];
                }
                //$jsonResponse = $listOfAppointments;
            } else {
                array_push($jsonResponse, (object)array(
                    'status' => false,
                    'message' => 'Validacion de datos incorrecta',
                ));
            }
        } else {
            array_push($jsonResponse, (object)array(
                'status' => false,
                'message' => 'Validacion de datos incorrecta',
            ));
        }
        return $jsonResponse;
    }

    /**
     * @return bool
     */
    public function setEpsId()
    {
        set_time_limit(0);
        //Obtengo todas las citas
        $allAppointments = $this->repositoryAppointment->findAll();

        foreach ($allAppointments as $appointment) {
            //capturo el eps entity actual de UserEpsConnection
            if ($appointment->getEps() == null) {
                $epsEntity = null;

                $epsEntity = $appointment->getEps();

                //Si hay relacion actualizo la cita actual
                if ($epsEntity) {
                    $appointment->setEps($epsEntity);
                    $this->em->persist($appointment);
                    $this->em->flush();
                } else {
                    //De lo contrario pongo por defecto la 16 = Particular
                    $epsParticular = $this->repositoryEps->find('16');

                    if ($epsParticular) {
                        $appointment->setEps($epsParticular);
                        $this->em->persist($appointment);
                        $this->em->flush();
                    }
                }
            } else {
                if ($appointment->getUserEpsConnection()) {

                    $epsEntity = $appointment->getUserEpsConnection()->getEpsId();

                    if ($epsEntity) {
                        $appointment->setEps($epsEntity);
                        $this->em->persist($appointment);
                        $this->em->flush();
                    } else {
                        //De lo contrario pongo por defecto la 16 = Particular
                        $epsParticular = $this->repositoryEps->find('16');

                        if ($epsParticular) {
                            $appointment->setEps($epsParticular);
                            $this->em->persist($appointment);
                            $this->em->flush();
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $req
     * @return array
     */
    public function edit(Request $req)
    {
        $response = array();
        $data = array(
            Constant::DOCTOR_ID => $req->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $req->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $req->request->get(Constant::DATE_ID),
            Constant::NEW_DATE => $req->request->get(Constant::NEW_DATE),
            Constant::NEW_ASSIGNED_TIME => $req->request->get(Constant::NEW_ASSIGNED_TIME),
            Constant::NEW_PLACE_ID => $req->request->get(Constant::NEW_PLACE_ID),
            Constant::NEW_EPS_ID => $req->request->get(Constant::NEW_EPS_ID)
        );

        //Buscamos el medico
        $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);
        $validUsers = false;
        if ($userDoctor) {

            if ($data[Constant::DOCTOR_ID] && $data[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

                $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                foreach ($ucoTwo as $ucot) {
                    foreach ($gduAssistants as $gduAssistant) {
                        if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {
                            $validUsers = true;
                        }
                    }
                }
            } else
                $validUsers = true;

            if ($validUsers) {
                //Buscamos la cita
                $appointment = $this->repositoryAppointment->find($data[Constant::DATE_ID]);
                if ($appointment) {

                    $eps = $this->repositoryEps->find($data[Constant::NEW_EPS_ID]);

                    if ($eps) {

                        $uPConnection = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data[Constant::NEW_PLACE_ID], Constant::USER_ID => $userDoctor));

                        if ($uPConnection) {

                            $previousDate = clone $appointment->getDate();

                            $appointment->setDate(new \DateTime($data[Constant::NEW_DATE]));
                            $appointment->setAssignedTime(new \DateTime($data[Constant::NEW_ASSIGNED_TIME]));
                            $appointment->setUserPlaceConnectionId($uPConnection);
                            $appointment->setEps($eps);
                            $appointment->setSyncDate(new \DateTime('now'));
                            $this->em->persist($appointment);
                            $this->em->flush();

                            if ($appointment->getDate() != $previousDate) {
                                //Buscamos las notificaciones actuales y las eliminamos
                                $allNotifications = $this->repositoryNotification->findBy(array(Constant::APPOINTMENT => $appointment));

                                if (sizeof($allNotifications) > 0) {
                                    foreach ($allNotifications as $notification) {
                                        $this->em->remove($notification);
                                        $this->em->flush();
                                    }
                                }

                                $generalDataPatient = $appointment->getUserThreeId()->getGeneralDataUserId();
                                $generaDataDoctor = $appointment->getUserOneId()->getGeneralDataUserId();
                                $extraDataDoctor = $appointment->getUserOneId()->getExtraDataUserId();
                                $this->setDateNotificationForAppointments($appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, true,
                                    $appointment->getSendNotifications(), $previousDate);

                            }

                            //Se edita la cita en los calendarios que tenga habilitados
                            if ($appointment->getGoogleCalendarId()) {
                                $calendarService = new CalendarService($this->em);
                                //Edit Calendar Event
                                $calendarService->editGoogleEvent($appointment, null, null);
                            }

                            $response = array(
                                Constant::STATUS => true,
                                Constant::MESSAGE => 'Successful'
                            );
                        } else {
                            $response = array(
                                Constant::STATUS => false,
                                Constant::MESSAGE => 'Place connection not found'
                            );
                        }
                    } else {
                        $response = array(
                            Constant::STATUS => false,
                            Constant::MESSAGE => 'EPS not found'
                        );
                    }
                } else {
                    $response = array(
                        Constant::STATUS => false,
                        Constant::MESSAGE => 'Date not found'
                    );
                }
            } else {
                $response = array(
                    Constant::STATUS => false,
                    Constant::MESSAGE => 'Assistant not found'
                );
            }
        } else {
            $response = array(
                Constant::STATUS => false,
                Constant::MESSAGE => 'User not found'
            );
        }
        return $response;
    }

    /**
     * @param $appointmentObject
     * @return object
     */
    public function editSync($appointmentObject)
    {
        try {
            $data = array(
                Constant::DATE_ID => $appointmentObject[Constant::_ID],
                Constant::DOCTOR_ID => $appointmentObject[Constant::_USER_ID],
                Constant::_PATIENT_ID => $appointmentObject[Constant::_PATIENT_ID],
                Constant::NEW_DATE => $appointmentObject[Constant::_DATE],
                Constant::NEW_ASSIGNED_TIME => $appointmentObject[Constant::_ASSIGNED_TIME],
                Constant::NEW_PLACE_ID => $appointmentObject[Constant::_PLACE_ID],
                Constant::NEW_EPS_ID => $appointmentObject[Constant::_HEALTH_PROMOTER_ID],
                Constant::SEND_NOTIFICATIONS => $appointmentObject[Constant::_SEND_NOTIFICATIONS],
                Constant::CONFIRMED => $appointmentObject[Constant::_CONFIRMED],
                Constant::CANCELED => $appointmentObject[Constant::_CANCELED],
                Constant::CANCELED_BY_PATIENT => $appointmentObject[Constant::_CANCELED_BY_PATIENT],
                Constant::ATTENDED => $appointmentObject[Constant::_ATTENDED],
                Constant::COMMENTS => $appointmentObject[Constant::_COMMENTS],
            );

            $response = array();

            if (isset($data[Constant::DATE_ID]) && isset($data[Constant::DOCTOR_ID]) &&
                isset($data[Constant::_PATIENT_ID])
            ) {
                //Buscamos el medico
                $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);

                if ($userDoctor) {

                    //Buscamos el paciente
                    $userPatient = $this->repositoryUser->find($data[Constant::_PATIENT_ID]);

                    if ($userPatient) {

                        //Buscamos la cita
                        $appointment = $this->repositoryAppointment->findOneBy(array(Constant::ID => $data[Constant::DATE_ID],
                            Constant::USER_ONE_ID => $userDoctor, Constant::USER_THREE_ID => $userPatient));
                        if ($appointment) {
                            $eps = $this->repositoryEps->find($data[Constant::NEW_EPS_ID]);

                            if ($eps == null) {
                                $eps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                            }

                            if ($eps) {
                                $uPConnection = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $data[Constant::NEW_PLACE_ID], Constant::USER_ID => $userDoctor));

                                if ($uPConnection) {
                                    $previousCancelStatus = $appointment->getCanceled();

                                    if ($data[Constant::CANCELED]) {
                                        $tempResponse = $this->cancelDateSync($appointment, $previousCancelStatus, $data[Constant::SEND_NOTIFICATIONS]);
                                        return $tempResponse;
                                    }

                                    $previousDate = clone $appointment->getDate();

                                    $appointment->setDate(new \DateTime($data[Constant::NEW_DATE]));
                                    $appointment->setAssignedTime(new \DateTime($data[Constant::NEW_ASSIGNED_TIME]));
                                    $appointment->setUserPlaceConnectionId($uPConnection);
                                    $appointment->setEps($eps);
                                    $appointment->setSyncDate(new \DateTime('now'));
                                    $appointment->setConfirmed($data[Constant::CONFIRMED]);
                                    $appointment->setCanceled($data[Constant::CANCELED]);
                                    $appointment->setCanceledByPatient($data[Constant::CANCELED_BY_PATIENT]);
                                    $appointment->setAttended($data[Constant::ATTENDED]);
                                    $appointment->setSendNotifications($data[Constant::SEND_NOTIFICATIONS]);
                                    $appointment->setComment($data[Constant::COMMENTS]);
                                    $this->em->persist($appointment);
                                    $this->em->flush();

                                    if ($data[Constant::CONFIRMED]) {
                                        $appointment->setConfirmed(true);
                                        $appointment->setWaitingQueue(false);
                                        $appointment->setConfirmationDate(new \DateTime('now'));
                                        $appointment->setSyncDate(new \DateTime('now'));
                                        $this->em->flush();
                                    }

                                    if ($appointment->getDate() != $previousDate) {
                                        //Buscamos las notificaciones actuales y las eliminamos
                                        $allNotifications = $this->repositoryNotification->findBy(array(Constant::APPOINTMENT => $appointment));

                                        if (sizeof($allNotifications) > 0) {
                                            foreach ($allNotifications as $notification) {
                                                $this->em->remove($notification);
                                                $this->em->flush();
                                            }
                                        }

                                        if ($previousCancelStatus == false || $previousCancelStatus == null) {
                                            $generalDataPatient = $appointment->getUserThreeId()->getGeneralDataUserId();
                                            $generaDataDoctor = $appointment->getUserOneId()->getGeneralDataUserId();
                                            $extraDataDoctor = $appointment->getUserOneId()->getExtraDataUserId();
                                            $this->setDateNotificationForAppointmentsSync($appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, true,
                                                $appointment->getSendNotifications());

                                            $response = (object)array(
                                                Constant::APPOINTMENT_ID => $appointment->getId(),
                                                Constant::IS_EDIT => true,
                                                Constant::PREVIOUS_DATE => $previousDate);
                                        }
                                    }

                                    //Se edita la cita en los calendarios que tenga habilitados
                                    if ($appointment->getGoogleCalendarId()) {
                                        $calendarService = new CalendarService($this->em);
                                        //Edit Calendar Event
                                        $calendarService->editGoogleEvent($appointment, null, null);
                                    }

                                    return $response;
                                } else {
                                    return $this->buildErrorObject(9);
                                }
                            } else {
                                return $this->buildErrorObject(8);
                            }
                        } else {
                            return $this->buildErrorObject(7);
                        }
                    } else {
                        return $this->buildErrorObject(1);
                    }
                } else {
                    return $this->buildErrorObject(1);
                }
            } else {
                return $this->buildErrorObject(2);
            }
        } catch (\Exception $e2) {
            error_log('catch editSync');
            error_log('Error: ' . $e2->getMessage());
            return $this->buildErrorObject(-1);
        } catch (ORMException $e) {
            error_log('catch editSync ORMException');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return bool
     */
    public function removeNotifications()
    {
        $allNotifications = $this->repositoryNotification->findBy(array(Constant::TYPE => 1, Constant::IS_SEND => false));
        foreach ($allNotifications as $notification) {
            $this->em->remove($notification);
            $this->em->flush();
        }
        return true;
    }

    /**
     * @param $req
     * @return array|object
     */
    public function setSendNotifications($req)
    {
        $response = array();
        $data = array(
            Constant::DOCTOR_ID => $req->request->get(Constant::DOCTOR_ID),
            Constant::DATE_ID => $req->request->get(Constant::DATE_ID),
            Constant::SEND_NOTIFICATIONS => $req->request->get(Constant::SEND_NOTIFICATIONS),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $req->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        $userDoctor = null;
        //Buscamos el medico
        if (isset($data[Constant::DOCTOR_ID])) {
            $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);
        } else {
            $response = (object)array(
                Constant::STATUS => false,
                Constant::MESSAGE => 'Missing parameters'
            );
        }
        $validUsers = false;
        if ($userDoctor) {

            if ($data[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

                $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                foreach ($ucoTwo as $ucot) {
                    foreach ($gduAssistants as $gduAssistant) {
                        if ($ucot->getUserTwoId()->getGeneralDataUserId() == $gduAssistant) {
                            $validUsers = true;
                        }
                    }
                }
            } else
                $validUsers = true;

            if ($validUsers) {
                //Buscamos la cita
                $appointment = $this->repositoryAppointment->find($data[Constant::DATE_ID]);

                if ($appointment) {
                    if (isset($data[Constant::SEND_NOTIFICATIONS])) {
                        $appointment->setSendNotifications($data[Constant::SEND_NOTIFICATIONS]);
                        $appointment->setSyncDate(new \DateTime('now'));
                        $this->em->persist($appointment);
                        $this->em->flush();

                        $response = (object)array(
                            Constant::STATUS => true,
                            Constant::MESSAGE => Constant::STATUS_SUCCESSFUL
                        );
                    } else {
                        $response = (object)array(
                            Constant::STATUS => false,
                            Constant::MESSAGE => 'Parameter not found'
                        );
                    }
                } else {
                    $response = (object)array(
                        Constant::STATUS => false,
                        Constant::MESSAGE => 'Appointment not found'
                    );
                }
            } else {
                $response = (object)array(
                    Constant::STATUS => false,
                    Constant::MESSAGE => 'Assistant not found'
                );
            }
        } else {
            $response = (object)array(
                Constant::STATUS => false,
                Constant::MESSAGE => 'Doctor not found'
            );
        }
        return $response;
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case 1:
                $text = 'User not found';
                break;
            case 2:
                $text = 'Missing parameters';
                break;
            case 3:
                $text = 'Missing document type';
                break;
            case 4:
                $text = 'Patient already exist';
                break;
            case 5:
                $text = 'Relationship not found';
                break;
            case 6:
                $text = 'Appointments limit';
                break;
            case 7:
                $text = 'Appointment not found';
                break;
            case 8:
                $text = 'Eps not found';
                break;
            case 9:
                $text = 'Place relationship not found';
                break;
            case 10:
                $text = 'Invalid email or password';
                break;
            case 12:
                $text = 'Missing user type';
                break;
            case 13:
                $text = 'Doctor already exist';
                break;
            case 14:
                $text = 'Assistant already exist';
                break;
            case 15:
                $text = 'Appointment already exist';
                break;
            case -1:
                $text = 'Internal Server Error';
                break;
        }

        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}