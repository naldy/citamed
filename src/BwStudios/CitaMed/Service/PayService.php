<?php

namespace BwStudios\CitaMed\Service;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Login;
use BwStudios\CitaMed\Entity\Membership;
use BwStudios\CitaMed\Entity\RecommendApp;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Speciality;
use BwStudios\CitaMed\Entity\Gender;
use BwStudios\CitaMed\Entity\DocumentType;
use BwStudios\CitaMed\Entity\RelationShip;
use BwStudios\CitaMed\Entity\Eps;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;
use BwStudios\CitaMed\Entity\UserPlaceConnection;
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\UserEpsConnection;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use Doctrine\ORM\EntityManager;
use BwStudios\CitaMed\Utility\PdfGenerator;
use DoctrineExtensions\Query\Mysql\Cos;
//use Proxies\__CG__\BwStudios\CitaMed\Entity\UserEpsConnection;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;


class PayService
{
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryAppointment;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionOneThree;
    private $repositoryUserConnectionThreeFour;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $repositoryMembership;
    private $em;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
        $this->repositoryMembership = $this->em->getRepository(Constant::ENTITY_MEMBERSHIP);
    }

    public function payUStepOne($req)
    {
        return array(
            'merchantId' => 'merchantId',
            'accountId' => 'accountId',
            'description' => 'description',
            'referenceCode' => 'referenceCode',
            'amount' => 'amount',
            'tax' => 'tax',
            'taxReturnBase' => 'taxReturnBase',
            'currency' => 'currency',
            'signature' => 'signature',
            'test' => 'test',
            'buyerEmail' => 'buyerEmail',
            'responseUrl' => 'responseUrl',
            'confirmationUrl' => 'confirmationUrl');
    }

}