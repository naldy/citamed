<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Country;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class CountryService
{

    private $repositoryCountry;
    private $em;

    /**
     * CountryService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryCountry = $this->em->getRepository(Constant::ENTITY_COUNTRY);
    }

    /**
     * @param $criteria
     * @return array|object
     */
    public function findBy($criteria)
    {
        try {
            return $this->repositoryCountry->findBy($criteria);
        } catch (ORMException $e) {
            error_log('catch findBy');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return array|object
     */
    public function findAll()
    {
        try {
            return $this->repositoryCountry->findAll();
        } catch (ORMException $e) {
            error_log('catch findAll');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $column
     * @return \Doctrine\ORM\QueryBuilder|object
     */
    public function findAllOrderBy($column)
    {
        try {
            return $this->repositoryCountry->findBy(array(), array($column => 'ASC'));
        } catch (ORMException $e) {
            error_log('catch findAll');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return array|\Doctrine\ORM\QueryBuilder|object
     */
    public function buildArrayCountries()
    {
        $allCountries = $this->findAllOrderBy(Constant::DESCRIPTION);
        $allCountriesResponse = array();
        if (isset($allCountries->Error)) {
            return $allCountries;
        } else {
            foreach ($allCountries as $country) {
                $tempCountryObject = $this->buildCountryObject($country);
                if (isset($tempCountryObject->Error)) {
                    return $tempCountryObject;
                } else {
                    array_push($allCountriesResponse, $tempCountryObject);
                }
            }
            return $allCountriesResponse;
        }
    }

    /**
     * @param $dateSync
     * @return array|object
     */
    public function buildArrayCountriesSync($dateSync)
    {
        try {
            $dateSync = new \DateTime($dateSync);
            $tempQuery = $this->repositoryCountry->createQueryBuilder('q')
                ->where('q.syncDate >= \'' . $dateSync->format('Y-m-d H:i:s') . '\'')
                ->getQuery();
            $allCountries = $tempQuery->getResult();
            $allCountriesResponse = array();
            if (isset($allCountries->Error)) {
                return $allCountries;
            } else {
                foreach ($allCountries as $country) {
                    $tempCountryObject = $this->buildCountryObject($country);
                    if (isset($tempCountryObject->Error)) {
                        return $tempCountryObject;
                    } else {
                        array_push($allCountriesResponse, $tempCountryObject);
                    }
                }
                return $allCountriesResponse;
            }
        } catch (\Exception $e) {
            error_log('catch buildArrayCountriesSync');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Country $countryEntity
     * @return object
     */
    public function buildCountryObject(Country $countryEntity)
    {
        try {
            return (object)array(
                Constant::_ID => $countryEntity->getId(),
                Constant::_NAME => $countryEntity->getDescription()
            );
        } catch (\Exception $e) {
            error_log('catch buildCountryObject');
            error_log('Error' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }

        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}