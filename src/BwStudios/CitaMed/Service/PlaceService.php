<?php

namespace BwStudios\CitaMed\Service;

//Constant
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\DependencyInjection\Dump\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\Entity;

class PlaceService
{

    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionThreeFour;
    private $repositoryAppointment;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $em;

    /**
     *
     * @var EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {

        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function getAddress(Request $request)
    {
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
        );

        $jsonResponse = array();
        $negativeResponse = null;

        if ($data_array[Constant::DOCTOR_ID]) {
            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {
                $user_type = $user->getUserTypeId();
                $user_id = $user->getId();

                $user_type_id = $this->repositoryUserType->find($user_type);
                $user_description = $user_type_id->getDescription();

                if ($user_description == Constant::USER_TYPE_DOCTOR) {

                    $user_place = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $user_id), array());

                    foreach ($user_place as $values) {
                        $user_place_con = $values->getId();
                        $place_con = $values->getPlaceId();
                        $place_id = $this->repositoryPlace->find($place_con);
                        array_push($jsonResponse, array(
                            Constant::GENERAL_ID_COLUMN => $user_place_con,
                            Constant::ADDRESS => $place_id->getAddress(),
                            Constant::PHONE => $place_id->getPhone()
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => false,
                    Constant::MESSAGE_NAME => 'User not fount'
                ));
            }
            return $jsonResponse;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getEps(Request $request)
    {
        $list_eps = $this->repositoryEps->findBy(array(Constant::COUNTRY => 1));
        $jsonResponse = array();
        foreach ($list_eps as $values) {
            array_push($jsonResponse, array(
                Constant::GENERAL_ID_COLUMN => $values->getId(),
                Constant::EPS => $values->getDescription()
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getUserEps(Request $request)
    {
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
        );
        $jsonResponse = array();
        $negativeResponse = null;
        if ($data_array[Constant::DOCTOR_ID]) {
            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);
            if ($user) {
                $user_type = $user->getUserTypeId();
                $user_id = $user->getId();
                $user_extra = $user->getExtraDataUserId();

                $user_type_id = $this->repositoryUserType->find($user_type);
                $user_description = $user_type_id->getDescription();

                if ($user_description == Constant::USER_TYPE_DOCTOR) {

                    $user_eps = $this->repositoryUserEpsConnection->findBy(array('extraDataUserId' => $user_extra), array());

                    foreach ($user_eps as $values) {
                        $userEpsId = $values->getId();
                        $eps_con = $values->getEpsId();
                        $eps_id = $this->repositoryEps->find($eps_con);
                        array_push($jsonResponse, array(
                            Constant::EPS_ID => $userEpsId,
                            Constant::EPS_NAME => $eps_id->getDescription()
                        ));
                    }
                    return $jsonResponse;
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function tellFriend(Request $request)
    {

        $emailArray = $request->request->get(Constant::EMAIL_DESCRIPTION);
        $data_array = array(
            Constant::EMAIL_ARRAY => $emailArray,
            Constant::EMAIL_DESCRIPTION => $request->request->get(Constant::EMAIL_DESCRIPTION),
            Constant::NAME => $request->request->get(Constant::NAME)
        );
        $jsonResponse = array();
        $negativeResponse = null;
        if ($request->request->get(Constant::EMAIL_DESCRIPTION)) {
            foreach ($data_array[Constant::EMAIL_ARRAY] as $email) {
                $friend_email = $email['email'];
                $friend_name = $email['name'];
                $email = new MailService();
                $email->setEmail($friend_email);
                $email->setName($friend_name);
                $email->setTipeMail(4);
                $email->sendEmail();

                array_push($jsonResponse, array(
                    Constant::EMAIL_DESCRIPTION => $friend_email,
                    Constant::NAME => $friend_name
                ));
            }
        } else {
            return $negativeResponse;
        }
        return $jsonResponse;
    }

    /**
     * @param Place $placeEntity
     * @param $localId
     * @param $userId
     * @return object
     */
    public function buildPlaceObject(Place $placeEntity, $localId, $userId)
    {
        return (object)array(
            Constant::_ID => $placeEntity->getId(),
            Constant::_LOCAL_ID => $localId,
            Constant::_USER_ID => $userId,
            Constant::_ADDRESS => $placeEntity->getAddress(),
            Constant::_PHONE => $placeEntity->getPhone()
        );
    }


    public function getAllPlacesByDoctor($doctorId)
    {
        try {
            $allUserPlaceConnection = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_ID => $doctorId));
            $arrayPlacesByDoctor = array();
            foreach ($allUserPlaceConnection as $userPlaceConnection) {
                array_push($arrayPlacesByDoctor, $this->buildPlaceObject($userPlaceConnection->getPlaceId(),
                    0, $doctorId));
            }
            return $arrayPlacesByDoctor;
        } catch (\Exception $e) {
            error_log('catch getAllPlacesByDoctor');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }

        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}