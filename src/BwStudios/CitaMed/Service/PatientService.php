<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use Doctrine\ORM\EntityManager;

class PatientService
{

    private $repositoryUser;
    private $repositoryGeneralDataUser;
    private $repositoryExtraDataUser;

    private $em;

    /**
     * PatientService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
    }

    public function buildArrayPatients($doctorId)
    {
        try {
            $arrayPatientsResponse = array();
            $ucoThreeService = new UserConnectionOneThreeService($this->em);
            $ucoThreeArray = $ucoThreeService->findBy(array(Constant::USER_ONE_ID => $doctorId));
            if (isset($ucoThreeArray->Error)) {
                return $ucoThreeArray;
            } else {
                if (count($ucoThreeArray) > 0)
                    foreach ($ucoThreeArray as $ucoThreeEntity) {
                        $userEntityPatient = $ucoThreeEntity->getUserThreeId();
                        $generalDataUserPatient = $userEntityPatient->getGeneralDataUserId();
                        $extraDataUserPatient = $userEntityPatient->getExtraDataUserId();

                        if (isset($userEntityPatient) && isset($generalDataUserPatient) && isset($extraDataUserPatient)) {
                            $tempResponse = $this->buildPatientObject($doctorId, $userEntityPatient, $generalDataUserPatient,
                                $extraDataUserPatient, 0);
                            if (isset($tempResponse->Error)) {
                                return $tempResponse;
                            } else {
                                array_push($arrayPatientsResponse, $tempResponse);
                            }
                        } else {
                            return $this->buildErrorObject(-1);
                        }
                    }

                return $arrayPatientsResponse;
            }
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    public function buildArrayPatientsSync($doctorId, $dateSync)
    {
        try {
            $arrayPatientsResponse = array();
            $ucoThreeService = new UserConnectionOneThreeService($this->em);
            $ucoThreeArray = $ucoThreeService->findBy(array(Constant::USER_ONE_ID => $doctorId));

            $dateSyncRequest = new \DateTime($dateSync);

            foreach ($ucoThreeArray as $ucoThreeEntity) {
                $userEntityPatient = $this->repositoryUser->find($ucoThreeEntity->getUserThreeId()->getId());
                if ($userEntityPatient->getSyncDate() > $dateSyncRequest) {
                    $generalDataUserPatient = $userEntityPatient->getGeneralDataUserId();
                    $extraDataUserPatient = $userEntityPatient->getExtraDataUserId();

                    $tempResponse = $this->buildPatientObject($doctorId, $userEntityPatient, $generalDataUserPatient,
                        $extraDataUserPatient, 0);
                    if (isset($tempResponse->Error)) {
                        return $tempResponse;
                    } else {
                        array_push($arrayPatientsResponse, $tempResponse);
                    }
                }
            }
            return $arrayPatientsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    /**
     * @param $doctorId
     * @param User $userEntityPatient
     * @param GeneralDataUser $generalDataUserPatient
     * @param ExtraDataUser $extraDataUser
     * @param $localId
     * @return object
     */
    public function buildPatientObject($doctorId, User $userEntityPatient, GeneralDataUser $generalDataUserPatient,
                                       ExtraDataUser $extraDataUser, $localId)
    {
        try {
            $genderId = null;
            if ($extraDataUser->getGenderId())
                $genderId = $extraDataUser->getGenderId()->getId();

            $birthDate = null;
            if ($extraDataUser->getBirthDate())
                $birthDate = $extraDataUser->getBirthDate()->format(\DateTime::ISO8601);

            $documentTypeId = null;
            if ($extraDataUser->getDocumentTypeId())
                $documentTypeId = $extraDataUser->getDocumentTypeId()->getId();

            return (object)array(
                Constant::_ID => $userEntityPatient->getId(),
                Constant::_LOCAL_ID => $localId,
                Constant::_USER_ID => $doctorId,
                Constant::_FIRST_NAME => $generalDataUserPatient->getName(),
                Constant::_LAST_NAME => $generalDataUserPatient->getLastName(),
                Constant::_EMAIL => $generalDataUserPatient->getEmail(),
                Constant::_CELLPHONE => $generalDataUserPatient->getCellPhone(),
                Constant::_HOMEPHONE => $generalDataUserPatient->getHomePhone(),
                Constant::_GENDER_ID => $genderId,
                Constant::_BIRTH_DATE => $birthDate,
                Constant::_DOCUMENT_TYPE_ID => $documentTypeId,
                Constant::_DOCUMENT_ID => $extraDataUser->getIdentity(),
                Constant::_EMERGENCY_NAME => $extraDataUser->getEmergencyName(),
                Constant::_EMERGENCY_NUMBER => $extraDataUser->getEmergencyNumber(),
                Constant::_COMMENTS => $extraDataUser->getComment()
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}