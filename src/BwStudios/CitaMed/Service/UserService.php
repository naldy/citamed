<?php

namespace BwStudios\CitaMed\Service;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Notification;
use BwStudios\CitaMed\Entity\NotificationType;
use BwStudios\CitaMed\Entity\Login;
use BwStudios\CitaMed\Entity\Membership;
use BwStudios\CitaMed\Entity\Appointment;
use BwStudios\CitaMed\Entity\RecommendApp;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Speciality;
use BwStudios\CitaMed\Entity\Gender;
use BwStudios\CitaMed\Entity\DocumentType;
use BwStudios\CitaMed\Entity\RelationShip;
use BwStudios\CitaMed\Entity\Eps;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;
use BwStudios\CitaMed\Entity\UserPlaceConnection;
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\UserEpsConnection;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use ClassesWithParents\D;
use co\todoc\Entity\GCalendarToken;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManager;
use BwStudios\CitaMed\Utility\PdfGenerator;
use Doctrine\ORM\ORMException;
use DoctrineExtensions\Query\Mysql\Cos;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

use co\todoc\PublicAPIBundle\Service\GCalendarTokenService;


class UserService
{
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryAppointment;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionOneThree;
    private $repositoryUserConnectionThreeFour;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $repositoryLogin;
    private $repositoryNotification;
    private $repositoryNotificationType;
    private $repositoryMembership;

    //Servicios
    private $gCalendarTokenService;
    private $placeService;
    private $epsService;
    private $em;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
        $this->repositoryLogin = $this->em->getRepository(Constant:: ENTITY_LOGIN);
        $this->repositoryNotification = $this->em->getRepository(Constant::ENTITY_NOTIFICATION);
        $this->repositoryNotificationType = $this->em->getRepository(Constant::ENTITY_NOTIFICATION_TYPE);
        $this->repositoryMembership = $this->em->getRepository(Constant::ENTITY_MEMBERSHIP);

        //Servicios
        $this->gCalendarTokenService = new GCalendarTokenService($this->em);
        $this->placeService = new PlaceService($this->em);
        $this->epsService = new EpsService($this->em);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function loginUser(Request $request, $hashAlgorithm)
    {
        //Json Information
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::EMAIL),
            Constant::PASSWORD => $request->request->get(Constant::PASSWORD)
        );
        $jsonResponse = array();

        if ($data_array[Constant::EMAIL] == null && $data_array[Constant::PASSWORD] == null) {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing parameters'
            ));
        }

        //Busco todos los usuarios con ese mail
        $allGdu = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

        //Consult entity Login
        $hashPassword = hash($hashAlgorithm, $data_array[Constant::PASSWORD]);
        $foundStatus = false;
        foreach ($allGdu as $gdu) {

            $edu = $gdu->getUser()->getExtraDataUserId();

            if ($edu->getPassword() == $hashPassword) {
                $foundStatus = true;
                if ($gdu->getUser()->getUserTypeId()->getDescription() == Constant::USER_TYPE_DOCTOR) {
                    $generalDataUser = $gdu;
                    $extraDataUser = $edu;
                    //$genderData = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $extraDataUser->getGenderId()));
                    $placeConnections = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $gdu->getUser()));
                    $epsConnections = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUser));

                    //TODO: devolver el password nulo
                    array_push($jsonResponse, array(
                        'isAssistant' => false,
                        //doctor`s id
                        Constant::USER_ID => $gdu->getUser()->getId(),
                        //ExtraDataUser Entity
                        Constant::PASSWORD => null,
                        Constant::BIRTH_DATE => $extraDataUser->getBirthDate(),
                        Constant::IDENTITY => $extraDataUser->getIdentity(),
                        Constant::ARRIVAL_TIME => $extraDataUser->getArrivalTime(),
                        Constant::DEPART_TIME => $extraDataUser->getDepartTime(),
                        Constant::PHOTO => $extraDataUser->getPhoto(),
                        //Entities
                        Constant::SPECIALITY => $extraDataUser->getSpecialityId()->getDescription(),
                        //GeneralDataUser Entity
                        Constant::NAME => $generalDataUser->getName(),
                        Constant::LAST_NAME => $generalDataUser->getLastName(),
                        Constant::EMAIL => $generalDataUser->getEmail(),
                        Constant::CELL_PHONE => $generalDataUser->getCellPhone(),
                        Constant::HOME_PHONE => $generalDataUser->getHomePhone(),
                        Constant::IDENTITY => $extraDataUser->getIdentity(),
                        Constant::STATUS_NAME => $gdu->getUser()->getTemporary()
                    ));

                    foreach ($placeConnections as $placeConnection) {
                        $placeId = $placeConnection->getId();
                        $place = $this->repositoryPlace->findOneBy(array(Constant::GENERAL_ID_COLUMN => $placeId));

                        array_push($jsonResponse, array(
                            //place
                            Constant::ADDRESS => $place->getAddress(),
                            Constant::PHONE => $place->getPhone()
                        ));
                    }

                    foreach ($epsConnections as $epsConnection) {
                        $epsId = $epsConnection->getEpsId();
                        $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $epsId));

                        array_push($jsonResponse, array(
                            //eps
                            Constant::EPS => $eps->getDescription()
                        ));
                    }
                } elseif ($gdu->getUser()->getUserTypeId()->getDescription() == Constant::USER_TYPE_ASSISTANT) {

                    $userConnectionOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $gdu->getUser()->getId()));
                    $doctor = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userConnectionOneTwo->getUserOneId()));

                    //assistant's data
                    $generalDataUser = $gdu;

                    //doctor's data
                    $generalDataUserDoctor = $this->repositoryGeneralDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctor->getGeneralDataUserId()));
                    $extraDataUserDoctor = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctor->getExtraDataUserId()));
                    //$genderDataDoctor = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $extraDataUserDoctor->getGenderId()));
                    $placeConnectionsDoctor = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $doctor->getId()));
                    $epsConnectionsDoctor = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUserDoctor));

                    array_push($jsonResponse, array(
                        //doctor`s id
                        'isAssistant' => true,
                        Constant::USER_ID => $doctor->getId(),
                        //assistant
                        Constant::NAME => $generalDataUser->getName(),
                        Constant::EMAIL => $generalDataUser->getEmail(),
                        Constant::LAST_NAME => $generalDataUser->getLastName(),

                        //doctor
                        'doctorName' => $generalDataUserDoctor->getName(),
                        'doctorEmail' => $generalDataUserDoctor->getEmail(),
                        'doctorLastName' => $generalDataUserDoctor->getLastName(),
                        'doctorCellPhone' => $generalDataUserDoctor->getCellPhone(),

                        Constant::HOME_PHONE => $generalDataUserDoctor->getHomePhone(),
                        Constant::IDENTITY => $extraDataUserDoctor->getIdentity(),
                        Constant::ARRIVAL_TIME => $extraDataUserDoctor->getArrivalTime(),
                        Constant::DEPART_TIME => $extraDataUserDoctor->getDepartTime(),
                        //Assistant status
                        Constant::STATUS_NAME => $gdu->getUser()->getTemporary()

                    ));

                    foreach ($placeConnectionsDoctor as $placeConnection) {
                        $placeId = $placeConnection->getId();
                        $place = $this->repositoryPlace->findOneBy(array(Constant::GENERAL_ID_COLUMN => $placeId));

                        array_push($jsonResponse, array(
                            //place
                            Constant::ADDRESS => $place->getAddress(),
                            Constant::PHONE => $place->getPhone()
                        ));
                    }

                    foreach ($epsConnectionsDoctor as $epsConnection) {
                        $epsId = $epsConnection->getEpsId();
                        $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $epsId));

                        array_push($jsonResponse, array(
                            //eps
                            Constant::EPS => $eps->getDescription()
                        ));
                    }
                }
            }
        }
        if ($foundStatus == false) {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'invalid user or password'
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return object
     */
    public function loginUserV1(Request $request, $hashAlgorithm)
    {
        //Json Information
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::_EMAIL),
            Constant::PASSWORD => $request->request->get(Constant::_PASSWORD)
        );

        if (isset($data_array[Constant::EMAIL]) && isset($data_array[Constant::PASSWORD])) {

            //Busco todos los usuarios con ese mail
            $allGdu = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            $hashPassword = hash($hashAlgorithm, $data_array[Constant::PASSWORD]);
            $foundStatus = false;
            foreach ($allGdu as $gdu) {

                $edu = $gdu->getUser()->getExtraDataUserId();

                if ($edu->getPassword() == $hashPassword) {
                    $foundStatus = true;
                    if ($gdu->getUser()->getUserTypeId()->getDescription() == Constant::USER_TYPE_DOCTOR) {
                        $generalDataUser = $gdu;
                        $extraDataUser = $edu;
                        //$genderData = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $extraDataUser->getGenderId()));
                        $placeConnections = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $gdu->getUser()));
                        $epsConnections = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUser));

                        $arrayPlaceResponse = array();
                        foreach ($placeConnections as $placeConnection) {
                            $placeId = $placeConnection->getId();
                            $place = $this->repositoryPlace->findOneBy(array(Constant::GENERAL_ID_COLUMN => $placeId));

                            array_push($arrayPlaceResponse, (object)array(
                                Constant::_ID => $place->getId(),
                                Constant::_USER_ID => $generalDataUser->getUser()->getId(),
                                Constant::_ADDRESS => $place->getAddress(),
                                Constant::_PHONE => $place->getPhone()
                            ));
                        }

                        $arrayEpsResponse = array();
                        foreach ($epsConnections as $epsConnection) {
                            $epsId = $epsConnection->getEpsId();
                            $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $epsId));

                            array_push($arrayEpsResponse, (object)array(
                                Constant::_ID => $eps->getId(),
                                Constant::_NAME => $eps->getDescription()
                            ));
                        }

                        return $this->buildSingleDataObject(true, $generalDataUser, $extraDataUser,
                            $arrayPlaceResponse, $arrayEpsResponse, null, null);

                    } elseif ($gdu->getUser()->getUserTypeId()->getDescription() == Constant::USER_TYPE_ASSISTANT) {

                        $userConnectionOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_CONNECTION_COLUMN => $gdu->getUser()->getId()));
                        $doctor = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userConnectionOneTwo->getUserOneId()));

                        //assistant's data
                        $generalDataUser = $gdu;

                        //doctor's data
                        $generalDataUserDoctor = $this->repositoryGeneralDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctor->getGeneralDataUserId()));
                        $extraDataUserDoctor = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $doctor->getExtraDataUserId()));
                        //$genderDataDoctor = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $extraDataUserDoctor->getGenderId()));
                        $placeConnectionsDoctor = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $doctor->getId()));
                        $epsConnectionsDoctor = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUserDoctor));

                        $arrayPlaceResponse = array();
                        foreach ($placeConnectionsDoctor as $placeConnection) {
                            $placeId = $placeConnection->getId();
                            $place = $this->repositoryPlace->findOneBy(array(Constant::GENERAL_ID_COLUMN => $placeId));

                            array_push($arrayPlaceResponse, (object)array(
                                Constant::_ID => $place->getId(),
                                Constant::_USER_ID => $generalDataUser->getUser()->getId(),
                                Constant::_ADDRESS => $place->getAddress(),
                                Constant::_PHONE => $place->getPhone()
                            ));
                        }

                        $arrayEpsResponse = array();
                        foreach ($epsConnectionsDoctor as $epsConnection) {
                            $epsId = $epsConnection->getEpsId();
                            $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $epsId));

                            array_push($arrayEpsResponse, (object)array(
                                Constant::_ID => $eps->getId(),
                                Constant::_NAME => $eps->getDescription()
                            ));
                        }

                        return $this->buildSingleDataObject(false, $generalDataUser, null,
                            $arrayPlaceResponse, $arrayEpsResponse, $generalDataUserDoctor, $extraDataUserDoctor);
                    }
                }
            }
            if ($foundStatus == false) {
                return $this->buildErrorObject(10);
            }
        } else {
            return $this->buildErrorObject(2);
        }
    }


    public function buildSingleDataObject($isDoctor, $generalDataUser, $extraDataUser, $arrayPlaceResponse,
                                          $arrayEpsResponse, $generalDataUserDoctor, $extraDataUserDoctor)
    {
        if ($isDoctor) {
            return (object)array(
                Constant::_SINGLE_DATA => (object)array(
                    Constant::_ID => $generalDataUser->getUser()->getId(),
                    Constant::_USER_TYPE_ID => $generalDataUser->getUser()->getUserTypeId()->getId(),
                    Constant::_OWNER_USER_ID => $generalDataUser->getUser()->getId(),
                    Constant::_FIRST_NAME => $generalDataUser->getName(),
                    Constant::_LAST_NAME => $generalDataUser->getLastName(),
                    Constant::_EMAIL => $generalDataUser->getEmail(),
                    Constant::_PASSWORD => null,
                    Constant::_TEMPORARY_PASSWORD => $generalDataUser->getUser()->getTemporary(),
                    Constant::_PERSONAL_TITLE_ID => (int)$extraDataUser->getPersonalTitle(),
                    Constant::_ARRIVAL_TIME => $extraDataUser->getArrivalTime()->format(\DateTime::ISO8601),
                    Constant::_DEPART_TIME => $extraDataUser->getDepartTime()->format(\DateTime::ISO8601),
                    Constant::_SPECIALIZATION_ID => $extraDataUser->getSpecialityId()->getId(),
                    Constant::_TOKEN => null,
                    Constant::_FULL_NAME => $generalDataUser->getName() . ' ' . $generalDataUser->getLastName(),
                    Constant::_PLACES => $arrayPlaceResponse,
                    Constant::_EPS => $arrayEpsResponse
                )
            );
        } else {
            return (object)array(
                Constant::_SINGLE_DATA => (object)array(
                    Constant::_ID => $generalDataUserDoctor->getUser()->getId(),
                    Constant::_USER_TYPE_ID => $generalDataUser->getUser()->getUserTypeId()->getId(),
                    Constant::_OWNER_USER_ID => $generalDataUserDoctor->getUser()->getId(),
                    Constant::_FIRST_NAME => $generalDataUserDoctor->getName(),
                    Constant::_LAST_NAME => $generalDataUserDoctor->getLastName(),
                    Constant::_EMAIL => $generalDataUserDoctor->getEmail(),
                    Constant::_PASSWORD => null,
                    Constant::_TEMPORARY_PASSWORD => $generalDataUser->getUser()->getTemporary(),
                    Constant::_PERSONAL_TITLE_ID => (int)$extraDataUserDoctor->getPersonalTitle(),
                    Constant::_ARRIVAL_TIME => $extraDataUserDoctor->getArrivalTime()->format(\DateTime::ISO8601),
                    Constant::_DEPART_TIME => $extraDataUserDoctor->getDepartTime()->format(\DateTime::ISO8601),
                    Constant::_SPECIALIZATION_ID => $extraDataUserDoctor->getSpecialityId()->getId(),
                    Constant::_TOKEN => null,
                    Constant::_FULL_NAME => $generalDataUserDoctor->getName() . ' ' . $generalDataUserDoctor->getLastName(),
                    Constant::_PLACES => $arrayPlaceResponse,
                    Constant::_EPS => $arrayEpsResponse
                )
            );
        }
    }

    public function buildUserObjectSync($userEntityDoctor, $dateSync)
    {
        try {
            $generalDataUser = $userEntityDoctor->getGeneralDataUserId();
            $extraDataUser = $userEntityDoctor->getExtraDataUserId();
            $arrayPlaceResponse = $this->placeService->getAllPlacesByDoctor($userEntityDoctor->getId());
            $arrayEpsResponse = $this->epsService->getAllEpsByDoctor($userEntityDoctor->getId());
            if ($generalDataUser && $extraDataUser && $extraDataUser->getArrivalTime() &&
                $extraDataUser->getDepartTime()
            ) {
                return (object)array(
                    Constant::_ID => $userEntityDoctor->getId(),
                    Constant::_USER_TYPE_ID => 1,
                    Constant::_OWNER_USER_ID => $userEntityDoctor->getId(),
                    Constant::_FIRST_NAME => $generalDataUser->getName(),
                    Constant::_LAST_NAME => $generalDataUser->getLastName(),
                    Constant::_EMAIL => $generalDataUser->getEmail(),
                    Constant::_PASSWORD => null,
                    Constant::_TEMPORARY_PASSWORD => $userEntityDoctor->getTemporary(),
                    Constant::_PERSONAL_TITLE_ID => (int)$extraDataUser->getPersonalTitle(),
                    Constant::_ARRIVAL_TIME => $extraDataUser->getArrivalTime()->format(\DateTime::ISO8601),
                    Constant::_DEPART_TIME => $extraDataUser->getDepartTime()->format(\DateTime::ISO8601),
                    Constant::_SPECIALIZATION_ID => $extraDataUser->getSpecialityId()->getId(),
                    Constant::_TOKEN => null,
                    Constant::_FULL_NAME => $generalDataUser->getName() . ' ' . $generalDataUser->getLastName(),
                    Constant::_PLACES => $arrayPlaceResponse,
                    Constant::_EPS => $arrayEpsResponse
                );
            } else {
                return $this->buildErrorObject(-1);
            }
        } catch (\Exception $e) {
            error_log('catch buildUserObject');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case 1:
                $text = 'User not found';
                break;
            case 2:
                $text = 'Missing parameters';
                break;
            case 3:
                $text = 'Missing document type';
                break;
            case 4:
                $text = 'Patient already exist';
                break;
            case 5:
                $text = 'Relationship not found';
                break;
            case 6:
                $text = 'Appointments limit';
                break;
            case 7:
                $text = 'Appointment not found';
                break;
            case 8:
                $text = 'Eps not found';
                break;
            case 9:
                $text = 'Place relationship not found';
                break;
            case 10:
                $text = 'Invalid email or password';
                break;
            case 12:
                $text = 'Missing user type';
                break;
            case 13:
                $text = 'Doctor already exist';
                break;
            case 14:
                $text = 'Assistant already exist';
                break;
            case 15:
                $text = 'Invalid data';
                break;
            case 16:
                $text = 'Missing gender';
                break;
            case 17:
                $text = 'User already exist';
                break;
            case 19:
                $text = 'Place can not be removed';
                break;
            case -1:
                $text = 'Internal Server Error';
                break;
        }

        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return array
     */
    public function createUser(Request $request, $hashAlgorithm)
    {
        $jsonResponse = array();

        $data_array = array(

            //Different Entities
            Constant::USER_TYPE => $request->request->get(Constant::USER_TYPE),
            Constant::DOCUMENT_TYPE => $request->request->get(Constant::DOCUMENT_TYPE),
            Constant::GENDER => $request->request->get(Constant::GENDER),
            Constant::GENDER_ID => $request->request->get(Constant::GENDER_ID),
            Constant::SPECIALITY => $request->request->get(Constant::SPECIALITY),
            Constant::RELATION_SHIP => $request->request->get(Constant::RELATION_SHIP),

            //Place  array information
            Constant::PLACE_ARRAY => $request->request->get(Constant::PLACE),

            //Place Individual
            Constant::ADDRESS => $request->request->get(Constant::ADDRESS),
            Constant::PHONE => $request->request->get(Constant::PHONE),
            //Constant::PLACE_NAME => $request->request->get(Constant::PLACE_NAME),

            //Eps
            Constant::EPS_ARRAY => $request->request->get(Constant::EPS),
            Constant::EPS_ID => $request->request->get(Constant::EPS_ID),

            //Information ExtraDataUser Entity
            Constant::BIRTH_DATE => $request->request->get(Constant::BIRTH_DATE),
            Constant::IDENTITY => $request->request->get(Constant::IDENTITY),
            Constant::EMERGENCY_NAME => $request->request->get(Constant::EMERGENCY_NAME),
            Constant::EMERGENCY_NUMBER => $request->request->get(Constant::EMERGENCY_NUMBER),
            Constant::URGENT => $request->request->get(Constant::URGENT),
            Constant::PASSWORD => $request->request->get(Constant::PASSWORD),
            Constant::ARRIVAL_TIME => $request->request->get(Constant::ARRIVAL_TIME),
            Constant::DEPART_TIME => $request->request->get(Constant::DEPART_TIME),
            Constant::COMMENT_EXTRA_DATA_USER => $request->request->get(Constant::COMMENT_EXTRA_DATA_USER),
            Constant::PERSONAL_TITLE => $request->request->get(Constant::PERSONAL_TITLE),

            //Information GeneralDataUser Entity
            Constant::NAME => $request->request->get(Constant::NAME),
            Constant::LAST_NAME => $request->request->get(Constant::LAST_NAME),
            Constant::EMAIL => $request->request->get(Constant::EMAIL),
            Constant::CELL_PHONE => $request->request->get(Constant::CELL_PHONE),
            Constant::HOME_PHONE => $request->request->get(Constant::HOME_PHONE),
            Constant::PHOTO => $request->request->get(Constant::PHOTO),

            //Identity of user type doctor(01), assistant(02), patient(03)
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::IDENTITY_USER_TYPE_PATIENT => $request->request->get(Constant::IDENTITY_USER_TYPE_PATIENT),
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID)

        );

        $userType = null;

        if ($data_array[Constant::USER_TYPE] == null) {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => false,
                Constant::MESSAGE_NAME => 'Missing userType'
            ));
        } else {
            //Entidad UserType
            $userType = $this->repositoryUserType->findOneBy(array(Constant::GENERAL_DESCRIPTION_COLUMN => $data_array[Constant::USER_TYPE]));

            if (count($userType) > 0) {
                //texto descriptivo de UserType
                $descriptiveUserType = $userType->getDescription();

                switch ($descriptiveUserType) {

                    case($descriptiveUserType == Constant::USER_TYPE_DOCTOR):

                        //Buscar si el mail o la identidad ya existen
                        //Busco todos los doctores
                        $allDoctors = $this->repositoryUser->findBy(array('userTypeId' => '1'));

                        //Con la lista de doctores recorro cada uno para validar que no existe el mail y la identidad (tipo y numero de documento)
                        $validation = true;
                        foreach ($allDoctors as $doctor) {
                            $generalDataUser = $doctor->getGeneralDataUserId();
                            $extraDataUser = $doctor->getExtraDataUserId();

                            if ($generalDataUser->getEmail() == $data_array[Constant::EMAIL])
                                $validation = false;

                            if ($data_array[Constant::DOCUMENT_TYPE] && $data_array[Constant::IDENTITY]) {
                                if ($extraDataUser->getDocumentTypeId())
                                    if ($extraDataUser->getDocumentTypeId()->getDescription() == $data_array[Constant::DOCUMENT_TYPE] && $extraDataUser->getIdentity() == $data_array[Constant::IDENTITY])
                                        $validation = false;
                            }
                        }

                        if ($validation) {
                            /**/
                            if ($data_array[Constant::NAME] != null && $data_array[Constant::ARRIVAL_TIME] != null
                                && $data_array[Constant::DEPART_TIME] != null && $data_array[Constant::PASSWORD] != null && $data_array[Constant::EMAIL] != null
                            ) {

                                //$gender = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));
                                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => $data_array[Constant::DOCUMENT_TYPE]));
                                //Se busca especialidad por defecto: No aplica, id 26
                                $specialityData = $this->repositorySpeciality->findOneBy(array(Constant::SPECIALITY_DESCRIPTION => 'No Aplica'));

                                /*ENTITY GENERAL DATA USER*/
                                $generalDataEntity = new GeneralDataUser();
                                $generalDataEntity->setName($data_array[Constant::NAME]);
                                $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);
                                $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                $generalDataEntity->setCellPhone($data_array[Constant::CELL_PHONE]);
                                $generalDataEntity->setHomePhone($data_array[Constant::HOME_PHONE]);
                                $this->em->persist($generalDataEntity);

                                /*ENTITY EXTRA DATA USER*/
                                $extraDataEntity = new ExtraDataUser();
                                if ($data_array[Constant::IDENTITY]) {
                                    $extraDataEntity->setIdentity($data_array[Constant::IDENTITY]);
                                }

                                if ($data_array[Constant::BIRTH_DATE] != null) {
                                    $extraDataEntity->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                }
                                $extraDataEntity->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                                $extraDataEntity->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                                $extraDataEntity->setUrgent($data_array[Constant::URGENT]);
                                $extraDataEntity->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);
                                $extraDataEntity->setAddress($data_array[Constant::ADDRESS]);
                                $extraDataEntity->setArrivalTime(new \DateTime($data_array[Constant::ARRIVAL_TIME]));
                                $extraDataEntity->setDepartTime(new \DateTime($data_array[Constant::DEPART_TIME]));
                                //Cifrado de password
                                $hashPassword = hash($hashAlgorithm, $data_array[Constant::PASSWORD]);
                                $extraDataEntity->setPassword($hashPassword);
                                $extraDataEntity->setPhoto($data_array[Constant::PHOTO]);
                                $extraDataEntity->setDocumentTypeId($documentTypeData);
                                $extraDataEntity->setSpecialityId($specialityData);
                                $extraDataEntity->setPersonalTitle($data_array[Constant::PERSONAL_TITLE]);
                                $this->em->persist($extraDataEntity);

                                /* ENTITY USER*/
                                $userEntity = new User();
                                $userEntity->setExtraDataUserId($extraDataEntity);
                                $userEntity->setGeneralDataUserId($generalDataEntity);
                                $userEntity->setUserTypeId($userType);
                                $userEntity->setTemporary(0);
                                $userEntity->setCreationDate(new \DateTime('now'));
                                $userEntity->setSyncDate(new \DateTime('now'));
                                $this->em->persist($userEntity);
                                $this->em->flush();

                                /*$login = new Login();
                                $login->setUsername($generalDataEntity->getEmail());
                                $login->setPassword($extraDataEntity->getPassword());
                                if ($extraDataEntity->getIdentity())
                                    $login->setIdentity($extraDataEntity->getIdentity());
                                $login->setUserId($userEntity);
                                $this->em->persist($login);
                                $this->em->flush();*/

                                //Informacion de consultorios
                                $arrayUPConnections = array();
                                if ($request->request->get(Constant::PLACE)) {

                                    foreach ($data_array[Constant::PLACE_ARRAY] as $place) {

                                        $places = new Place();
                                        $places->setAddress($place[Constant::ADDRESS]);
                                        $places->setPhone($place[Constant::PHONE]);
                                        $this->em->persist($places);
                                        $this->em->flush();

                                        $userPlaceConnectionEntity = new UserPlaceConnection();
                                        $userPlaceConnectionEntity->setUserId($userEntity);
                                        $userPlaceConnectionEntity->setPlaceId($places);
                                        $this->em->persist($userPlaceConnectionEntity);
                                        $this->em->flush();
                                        array_push($arrayUPConnections, $userPlaceConnectionEntity);
                                    }
                                }

                                //Informacion de EPS's
                                $arrayEpss = array();
                                if ($request->request->get(Constant::EPS)) {
                                    foreach ($data_array[Constant::EPS_ARRAY] as $eps) {
                                        $data_eps = new UserEpsConnection();
                                        if ($eps[Constant::EPS_ID] == "") {
                                            $eps_data = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => Constant::DEFAULT_EPS));
                                        } else {
                                            $eps_data = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $eps));
                                        }
                                        if ($eps_data) {
                                            $data_eps->setEpsId($eps_data);
                                            $data_eps->setExtraDataUserId($extraDataEntity);
                                            $this->em->persist($data_eps);
                                            $this->em->flush();
                                            array_push($arrayEpss, $eps_data);
                                        }
                                    }
                                } else {
                                    //Se almacena EPS por defecto: Particular
                                    if ($request->request->get(Constant::EPS) == null || size($request->request->get(Constant::EPS)) == 0) {
                                        $epsEntity = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => Constant::DEFAULT_EPS));
                                        $data_eps = new UserEpsConnection();
                                        if ($epsEntity) {
                                            $data_eps->setEpsId($epsEntity);
                                            $data_eps->setExtraDataUserId($extraDataEntity);
                                            $this->em->persist($data_eps);
                                            $this->em->flush();
                                            array_push($arrayEpss, $epsEntity);
                                        }
                                    }
                                }

                                $email = new MailService();
                                $email->setEmail($generalDataEntity->getEmail());
                                $email->setName($generalDataEntity->getName());
                                $email->setPersonalTitleDoctor($extraDataEntity->getPersonalTitle());
                                $email->setTipeMail(6);
                                $email->sendEmail();

                                /*
                                 * Una vez creada la data del medico se crea la data de un paciente con la misma informacion
                                 * del medico
                                 */
                                $extraDataUserPatient = new ExtraDataUser();

                                if ($data_array[Constant::BIRTH_DATE] != null) {
                                    $extraDataUserPatient->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                }

                                $extraDataUserPatient->setPhoto($data_array[Constant::PHOTO]);
                                $this->em->persist($extraDataUserPatient);

                                $generalDataUserPatient = new GeneralDataUser();
                                $generalDataUserPatient->setName($data_array[Constant::NAME]);
                                $generalDataUserPatient->setLastName($data_array[Constant::LAST_NAME]);
                                $generalDataUserPatient->setEmail($data_array[Constant::EMAIL]);
                                $generalDataUserPatient->setCellPhone($data_array[Constant::CELL_PHONE]);
                                $this->em->persist($generalDataUserPatient);

                                $userPatient = new User();
                                $userPatient->setExtraDataUserId($extraDataUserPatient);
                                $userPatient->setGeneralDataUserId($generalDataUserPatient);
                                $userTypePatient = $this->repositoryUserType->find('3');
                                $userPatient->setUserTypeId($userTypePatient);
                                $userPatient->setCreationDate(new \DateTime('now'));
                                $userPatient->setTemporary(false);
                                $userPatient->setSyncDate(new \DateTime('now'));
                                $this->em->persist($userPatient);

                                //Relaciono el medico y el paciente
                                //TODO: para el servicio userConnectionOneThree
                                $ucOneThree = new UserConnectionOneThree();
                                $ucOneThree->setUserOneId($userEntity);
                                $ucOneThree->setUserThreeId($userPatient);
                                $this->em->persist($ucOneThree);

                                //Creo la cita
                                $now = new \DateTime('now');
                                $plusOneHour = new \DateTime(strtolower($now->format('Y-m-d H:i:s') . ' +1 day'));
                                $appointment = new Appointment();
                                $appointment->setUserOneId($userEntity);
                                $appointment->setUserThreeId($userPatient);
                                $appointment->setCreationDate($now);
                                $appointment->setDate(new \DateTime($plusOneHour->format('Y-m-d 8' . ':00:00')));
                                $appointment->setAssignedTime(new \DateTime('00:30:00'));
                                $appointment->setSyncDate(new \DateTime('now'));
                                if (sizeof($arrayUPConnections) > 0) {
                                    $appointment->setUserPlaceConnectionId($arrayUPConnections[0]);
                                }

                                $defaultEps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                                $appointment->setEps($defaultEps);

                                $this->em->persist($appointment);
                                $this->em->flush();

                                $this->setDateNotificationForAppointments($appointment, $generalDataUserPatient, $generalDataEntity, $extraDataEntity);

                                array_push($jsonResponse, array(
                                    Constant::STATUS_NAME => true,
                                    Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL,
                                ));

                            } else {
                                array_push($jsonResponse, array(
                                    Constant::STATUS_NAME => false,
                                    Constant::MESSAGE_NAME => 'Missing parameters to doctorType'
                                ));
                            }
                            /*
                            array_push($jsonResponse, array(
                                'hola' => 'registrese'
                            ));*/
                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => false,
                                Constant::MESSAGE_NAME => 'Email already exist'
                            ));
                        }

                        break;

                    case($descriptiveUserType == Constant::USER_TYPE_ASSISTANT):

                        $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

                        //Busco todas las asistentes del doctor
                        $allucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor));

                        $validatorExist = true;
                        foreach ($allucoTwo as $ucoTwo) {
                            if ($ucoTwo->getUserTwoId()->getGeneralDataUserId()->getEmail() == $data_array[Constant::EMAIL])
                                $validatorExist = false;
                        }
                        if ($validatorExist) {
                            if ($data_array[Constant::NAME] != null) {

                                //$userEntity = $identityUser01->getUser();
                                $generalDataEntity = new GeneralDataUser();
                                $generalDataEntity->setName($data_array[Constant::NAME]);
                                $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);

                                $extraDataEntity = new ExtraDataUser();
                                //TODO: generar un password y cifrarlo
                                $code = substr(md5(uniqid(rand(), true)), 0, 10);
                                $hashPassword = hash($hashAlgorithm, $code);
                                $extraDataEntity->setPassword($hashPassword);

                                $user = new User();
                                $user->setGeneralDataUserId($generalDataEntity);
                                $user->setExtraDataUserId($extraDataEntity);
                                $user->setTemporary(1);
                                $user->setCreationDate(new \DateTime('now'));
                                $user->setUserTypeId($userType);
                                $user->setSyncDate(new \DateTime('now'));

                                //$login = new Login();
                                //$login->setUsername($generalDataEntity->getEmail());
                                //$login->setPassword($extraDataEntity->getPassword());
                                //$login->setUserId($user);

                                $userConnectionOneTwo = new UserConnectionOneTwo();
                                $userConnectionOneTwo->setUserOneId($userDoctor);
                                $userConnectionOneTwo->setUserTwoId($user);

                                $this->em->persist($generalDataEntity);
                                $this->em->persist($extraDataEntity);
                                $this->em->persist($user);
                                //$this->em->persist($login);
                                $this->em->persist($userConnectionOneTwo);
                                $this->em->flush();

                                $email = new MailService();
                                $email->setEmail($data_array[Constant::EMAIL]);
                                $email->setPass($code);
                                $email->setName($data_array[Constant::NAME]);
                                $email->setNameDoctor($userDoctor->getGeneralDataUserId()->getName());
                                $email->setLastNameDoctor($userDoctor->getGeneralDataUserId()->getLastName());
                                $email->setPersonalTitleDoctor($userDoctor->getExtraDataUserId()->getPersonalTitle());
                                $email->setTipeMail(1);
                                $email->sendEmail();

                                array_push($jsonResponse, array(
                                    Constant::STATUS_NAME => true,
                                    Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL
                                ));
                            } else {
                                array_push($jsonResponse, array(
                                    Constant::STATUS_NAME => false,
                                    Constant::MESSAGE_NAME => 'Missing parameters'
                                ));
                            }
                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => false,
                                Constant::MESSAGE_NAME => 'Assistant already exist'
                            ));
                        }
                        break;

                    case($descriptiveUserType == Constant::USER_TYPE_PATIENT):

                        $validationDocumentType = false;
                        if ($data_array[Constant::DOCUMENT_TYPE]) {
                            $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => $data_array[Constant::DOCUMENT_TYPE]));
                            if ($documentTypeData) {
                                $validationDocumentType = true;
                            }
                        } else {
                            $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => Constant::DEFAULT_IDENTITY));
                            $validationDocumentType = true;
                        }

                        if ($validationDocumentType && $documentTypeData) {
                            if ($data_array[Constant::DOCTOR_ID]) {

                                //Entidad user del doctor
                                $entityUser = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

                                if ($entityUser) {

                                    //Busco todos los pacientes de ese doctor
                                    $allConnectionsOneThree = $this->repositoryUserConnectionOneThree->findBy(array('userOneId' => $data_array[Constant::DOCTOR_ID]));
                                    $validation = true;
                                    if ($data_array[Constant::DOCUMENT_TYPE] && $data_array[Constant::IDENTITY]) {
                                        foreach ($allConnectionsOneThree as $connectionOneThree) {
                                            $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();

                                            if ($extraDataUser->getDocumentTypeId() && $extraDataUser->getIdentity())
                                                if ($extraDataUser->getDocumentTypeId())
                                                    if ($extraDataUser->getDocumentTypeId()->getDescription() == $data_array[Constant::DOCUMENT_TYPE] && $extraDataUser->getIdentity() == $data_array[Constant::IDENTITY])
                                                        $validation = false;
                                        }
                                    }

                                    if ($validation) {
                                        if ($data_array[Constant::NAME] != null && $data_array[Constant::LAST_NAME] != null &&
                                            $data_array[Constant::CELL_PHONE] != null
                                        ) {

                                            $gender = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));

                                            /*ENTITY GENERAL DATA USER*/
                                            $generalDataEntity = new GeneralDataUser();
                                            $generalDataEntity->setName($data_array[Constant::NAME]);
                                            $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);
                                            if ($data_array[Constant::EMAIL] == null) {
                                                $generalDataEntity->setEmail('mail@mail.com');
                                            } else {
                                                $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                            }

                                            $generalDataEntity->setCellPhone($data_array[Constant::CELL_PHONE]);
                                            $generalDataEntity->setHomePhone($data_array[Constant::HOME_PHONE]);

                                            /*ENTITY EXTRA DATA USER*/
                                            $extraDataEntity = new ExtraDataUser();
                                            $extraDataEntity->setGenderId($gender);

                                            $extraDataEntity->setDocumentTypeId($documentTypeData);

                                            if ($data_array[Constant::IDENTITY]) {
                                                $extraDataEntity->setIdentity($data_array[Constant::IDENTITY]);
                                            }

                                            if ($data_array[Constant::BIRTH_DATE] != null) {
                                                $extraDataEntity->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                            }

                                            $extraDataEntity->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                                            $extraDataEntity->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                                            $extraDataEntity->setAddress($data_array[Constant::ADDRESS]);
                                            $extraDataEntity->setUrgent($data_array[Constant::URGENT]);
                                            $extraDataEntity->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);
                                            //$extraDataEntity->setEnablePersonalTitle(true);

                                            /*ENTITY USER*/
                                            $userEntity = new User();
                                            $userEntity->setUserTypeId($userType);
                                            $userEntity->setExtraDataUserId($extraDataEntity);
                                            $userEntity->setGeneralDataUserId($generalDataEntity);
                                            $userEntity->setTemporary(0);
                                            $userEntity->setCreationDate(new \DateTime('now'));
                                            $userEntity->setSyncDate(new \DateTime('now'));

                                            /*ENTITY USER CONNECTION ONE THREE*/
                                            //$doctorEntity = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $did));
                                            $userConnectionOneThree = new UserConnectionOneThree();
                                            $userConnectionOneThree->setUserOneId($entityUser);
                                            $userConnectionOneThree->setUserThreeId($userEntity);

                                            /*ENTITY USER EPS CONNECTION*/
                                            $userEpsConnection = new UserEpsConnection();
                                            $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));
                                            $userEpsConnection->setEpsId($eps);
                                            $userEpsConnection->setExtraDataUserId($extraDataEntity);


                                            $this->em->persist($extraDataEntity);
                                            $this->em->persist($generalDataEntity);
                                            // $this->em->persist($place);
                                            $this->em->persist($userEntity);
                                            $this->em->persist($userConnectionOneThree);
                                            $this->em->persist($userEpsConnection);
                                            $this->em->flush();
                                            //$idpatient = $user->getId();
                                            array_push($jsonResponse, array(
                                                Constant::STATUS_NAME => true,
                                                Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL,
                                                Constant::USER_ID => $userEntity->getId()
                                            ));

                                        } else {
                                            array_push($jsonResponse, array(
                                                Constant::STATUS_NAME => false,
                                                Constant::MESSAGE_NAME => 'missing parameters'
                                            ));
                                        }

                                    } else {
                                        array_push($jsonResponse, array(
                                            Constant::STATUS_NAME => false,
                                            Constant::MESSAGE_NAME => 'Identificacion ya existente'
                                        ));
                                    }
                                } else {
                                    array_push($jsonResponse, array(
                                        Constant::STATUS_NAME => false,
                                        Constant::MESSAGE_NAME => 'invalid doctor identity or does not exist'
                                    ));
                                }
                                //Si el paciente lo crea la asistente
                            } elseif ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

                                //Busco todos los asistentes
                                $allAssistants = $this->repositoryUser->findBy(array('userTypeId' => 2));
                                $assistant = null;
                                foreach ($allAssistants as $tempAssistant) {
                                    $generalDataUser = $tempAssistant->getGeneralDataUserId();

                                    if ($generalDataUser->getEmail() == $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
                                        $assistant = $tempAssistant;
                                        break;
                                    }
                                }

                                if ($assistant != null) {

                                    $connectionOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array('userTwoId' => $assistant->getId()));

                                    if ($connectionOneTwo) {

                                        $doctorEntity = $this->repositoryUser->find($connectionOneTwo->getUserOneId());

                                        if ($doctorEntity) {

                                            //Busco todos los pacientes de ese doctor
                                            $allConnectionsOneThree = $this->repositoryUserConnectionOneThree->findBy(array('userOneId' => $doctorEntity->getId()));
                                            $validation = true;
                                            if ($data_array[Constant::DOCUMENT_TYPE] && $data_array[Constant::IDENTITY]) {
                                                foreach ($allConnectionsOneThree as $connectionOneThree) {
                                                    $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();

                                                    if ($extraDataUser->getDocumentTypeId() && $extraDataUser->getIdentity())
                                                        if ($extraDataUser->getDocumentTypeId())
                                                            if ($extraDataUser->getDocumentTypeId()->getDescription() == $data_array[Constant::DOCUMENT_TYPE] && $extraDataUser->getIdentity() == $data_array[Constant::IDENTITY])
                                                                $validation = false;
                                                }
                                            }

                                            if ($validation) {

                                                if ($data_array[Constant::NAME] != null && $data_array[Constant::LAST_NAME] != null && $data_array[Constant::CELL_PHONE] != null
                                                ) {
                                                    $gender = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));

                                                    /*ENTITY GENERAL DATA USER*/
                                                    $generalDataEntity = new GeneralDataUser();
                                                    $generalDataEntity->setName($data_array[Constant::NAME]);
                                                    $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);
                                                    if ($data_array[Constant::EMAIL] == null) {
                                                        $generalDataEntity->setEmail('mail@mail.com');
                                                    } else {
                                                        $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                                    }

                                                    $generalDataEntity->setCellPhone($data_array[Constant::CELL_PHONE]);
                                                    $generalDataEntity->setHomePhone($data_array[Constant::HOME_PHONE]);

                                                    /*ENTITY EXTRA DATA USER*/
                                                    $extraDataEntity = new ExtraDataUser();
                                                    $extraDataEntity->setGenderId($gender);

                                                    $extraDataEntity->setDocumentTypeId($documentTypeData);

                                                    if ($data_array[Constant::IDENTITY]) {
                                                        $extraDataEntity->setIdentity($data_array[Constant::IDENTITY]);
                                                    }

                                                    if ($data_array[Constant::BIRTH_DATE] != null) {
                                                        $extraDataEntity->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                                    }

                                                    $extraDataEntity->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                                                    $extraDataEntity->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                                                    $extraDataEntity->setAddress($data_array[Constant::ADDRESS]);
                                                    $extraDataEntity->setUrgent($data_array[Constant::URGENT]);
                                                    $extraDataEntity->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);

                                                    /*ENTITY USER*/
                                                    $userEntity = new User();
                                                    $userEntity->setUserTypeId($userType);
                                                    $userEntity->setExtraDataUserId($extraDataEntity);
                                                    $userEntity->setGeneralDataUserId($generalDataEntity);
                                                    $userEntity->setTemporary(0);
                                                    $userEntity->setCreationDate(new \DateTime('now'));
                                                    $userEntity->setSyncDate(new \DateTime('now'));

                                                    /*ENTITY USER CONNECTION ONE THREE*/
                                                    //$doctorEntity = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $did));
                                                    $userConnectionOneThree = new UserConnectionOneThree();
                                                    $userConnectionOneThree->setUserOneId($doctorEntity);
                                                    $userConnectionOneThree->setUserThreeId($userEntity);

                                                    /*ENTITY USER EPS CONNECTION*/
                                                    $userEpsConnection = new UserEpsConnection();
                                                    $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));
                                                    $userEpsConnection->setEpsId($eps);
                                                    $userEpsConnection->setExtraDataUserId($extraDataEntity);


                                                    $this->em->persist($extraDataEntity);
                                                    $this->em->persist($generalDataEntity);
                                                    // $this->em->persist($place);
                                                    $this->em->persist($userEntity);
                                                    $this->em->persist($userConnectionOneThree);
                                                    $this->em->persist($userEpsConnection);
                                                    $this->em->flush();
                                                    //$idpatient = $user->getId();
                                                    array_push($jsonResponse, array(
                                                        Constant::STATUS_NAME => true,
                                                        Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL,
                                                        Constant::USER_ID => $userEntity->getId()
                                                    ));

                                                } else {
                                                    array_push($jsonResponse, array(
                                                        Constant::STATUS_NAME => false,
                                                        Constant::MESSAGE_NAME => 'missing parameters'
                                                    ));
                                                }

                                            } else {
                                                array_push($jsonResponse, array(
                                                    Constant::STATUS_NAME => false,
                                                    Constant::MESSAGE_NAME => 'Identificacion ya existe'
                                                ));
                                            }
                                        } else {
                                            array_push($jsonResponse, array(
                                                Constant::STATUS_NAME => false,
                                                Constant::MESSAGE_NAME => 'El doctor que corresponde para la asistente no fue encontrado'
                                            ));
                                        }
                                    } else {
                                        array_push($jsonResponse, array(
                                            Constant::STATUS_NAME => false,
                                            Constant::MESSAGE_NAME => 'Conexión con medico no encontrada'
                                        ));
                                    }
                                } else {
                                    array_push($jsonResponse, array(
                                        Constant::STATUS_NAME => false,
                                        Constant::MESSAGE_NAME => 'Asistente no encontrada'
                                    ));
                                }
                            }
                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => false,
                                Constant::MESSAGE_NAME => 'invalid document type'
                            ));
                        }
                        break;
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => false,
                    Constant::MESSAGE_NAME => 'invalid userType'
                ));
            }
        }
        return $jsonResponse;
    }

    /**
     * @param $assistantObject
     * @param $doctorId
     * @param $hashAlgorithm
     * @param $localId
     * @return object
     */
    public function createAssistantSync($assistantObject, $doctorId, $hashAlgorithm, $localId)
    {
        try {
            $data_array = array(
                Constant::_ID => $assistantObject[Constant::_ID],
                Constant::_LOCAL_ID => $assistantObject[Constant::_LOCAL_ID],
                Constant::_FIRST_NAME => $assistantObject[Constant::_FIRST_NAME],
                Constant::_LAST_NAME => $assistantObject[Constant::_LAST_NAME],
                Constant::_EMAIL => $assistantObject[Constant::_EMAIL],
                Constant::_ACTIVE => $assistantObject[Constant::_ACTIVE]
            );

            $localId = $data_array[Constant::_LOCAL_ID];

            $userDoctor = $this->repositoryUser->find($doctorId);

            //Busco todas las asistentes para validar que no haya un mail repetido
            $allAssistants = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => 2));

            $validatorExist = true;
            foreach ($allAssistants as $assistant) {
                $gduAssistant = $assistant->getGeneralDataUserId();
                if ($gduAssistant->getEmail() == $data_array[Constant::_EMAIL])
                    $validatorExist = false;
            }

            //busco todos los medicos para validar que no haya un mail repetido
            $allDoctors = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => 1));
            foreach ($allDoctors as $doctor) {
                $gduDoctor = $doctor->getGeneralDataUserId();
                if ($gduDoctor->getEmail() == $data_array[Constant::_EMAIL])
                    $validatorExist = false;
            }


            if ($validatorExist) {
                if (isset($data_array[Constant::_FIRST_NAME]) && isset($data_array[Constant::_LAST_NAME])) {

                    $generalDataEntity = new GeneralDataUser();
                    $generalDataEntity->setName($data_array[Constant::_FIRST_NAME]);
                    $generalDataEntity->setEmail($data_array[Constant::_EMAIL]);
                    $generalDataEntity->setLastName($data_array[Constant::_LAST_NAME]);

                    $extraDataEntity = new ExtraDataUser();

                    //TODO: generar un password y cifrarlo
                    $code = substr(md5(uniqid(rand(), true)), 0, 10);
                    $hashPassword = hash($hashAlgorithm, $code);
                    $extraDataEntity->setPassword($hashPassword);
                    $extraDataEntity->setIsBanned($data_array[Constant::_ACTIVE]);

                    $userType = $this->repositoryUserType->find(2);
                    $user = new User();
                    $user->setGeneralDataUserId($generalDataEntity);
                    $user->setExtraDataUserId($extraDataEntity);
                    $user->setTemporary(true);
                    $user->setCreationDate(new \DateTime('now'));
                    $user->setUserTypeId($userType);
                    $user->setSyncDate(new \DateTime('now'));

                    //$login = new Login();
                    //$login->setUsername($generalDataEntity->getEmail());
                    //$login->setPassword($extraDataEntity->getPassword());
                    //$login->setUserId($user);

                    $userConnectionOneTwo = new UserConnectionOneTwo();
                    $userConnectionOneTwo->setUserOneId($userDoctor);
                    $userConnectionOneTwo->setUserTwoId($user);

                    $this->em->persist($generalDataEntity);
                    $this->em->persist($extraDataEntity);
                    $this->em->persist($user);
                    //$this->em->persist($login);
                    $this->em->persist($userConnectionOneTwo);
                    $this->em->flush();

                    $email = new MailService();
                    $email->setEmail($data_array[Constant::_EMAIL]);
                    $email->setPass($code);
                    $email->setName($data_array[Constant::_FIRST_NAME]);
                    $email->setNameDoctor($userDoctor->getGeneralDataUserId()->getName());
                    $email->setLastNameDoctor($userDoctor->getGeneralDataUserId()->getLastName());
                    $email->setPersonalTitleDoctor($userDoctor->getExtraDataUserId()->getPersonalTitle());
                    $email->setTipeMail(1);
                    $email->sendEmail();

                    return $this->buildAssistantObject($user->getId(), $localId, $generalDataEntity);
                } else {
                    return $this->buildErrorObject(2);
                }
            } else {
                return $this->buildErrorObject(17);
            }
        } catch (\Exception $e) {
            error_log('catch createAssistant');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $id
     * @param $localId
     * @param $generalDataUser
     * @return object
     */
    public function buildAssistantObject($id, $localId, $generalDataUser)
    {
        return (object)array(
            Constant::_ID => $id,
            Constant::_LOCAL_ID => $localId,
            Constant::_FIRST_NAME => $generalDataUser->getName(),
            Constant::_LAST_NAME => $generalDataUser->getLastName(),
            Constant::_EMAIL => $generalDataUser->getEmail(),
            Constant::_ACTIVE => true
        );
    }

    /**
     * Crear pacientes
     * @param $patientObject
     * @param $doctorId
     * @param $emailAssistant
     * @param $localId
     * @return object
     */
    public function createUserSync($patientObject, $doctorId, $emailAssistant, $localId)
    {
        try {
            if (isset($patientObject)) {
                $data_array = array(

                    //Different Entities
                    Constant::USER_TYPE => Constant::USER_TYPE_PATIENT,
                    Constant::DOCUMENT_TYPE => $patientObject[Constant::_DOCUMENT_TYPE_ID],
                    Constant::GENDER => null,
                    Constant::GENDER_ID => $patientObject[Constant::_GENDER_ID],
                    Constant::SPECIALITY => null,
                    Constant::RELATION_SHIP => null,

                    //Place  array information
                    Constant::PLACE_ARRAY => null,

                    //Place Individual
                    Constant::ADDRESS => null,
                    Constant::PHONE => null,

                    //Eps
                    Constant::EPS_ARRAY => null,
                    Constant::EPS_ID => null,

                    //Information ExtraDataUser Entity
                    Constant::BIRTH_DATE => $patientObject[Constant::_BIRTH_DATE],
                    Constant::IDENTITY => $patientObject[Constant::_DOCUMENT_ID],
                    Constant::EMERGENCY_NAME => $patientObject[Constant::_EMERGENCY_NAME],
                    Constant::EMERGENCY_NUMBER => $patientObject[Constant::_EMERGENCY_NUMBER],
                    Constant::URGENT => null,
                    Constant::PASSWORD => null,
                    Constant::ARRIVAL_TIME => null,
                    Constant::DEPART_TIME => null,
                    Constant::COMMENT_EXTRA_DATA_USER => $patientObject[Constant::_COMMENTS],
                    Constant::PERSONAL_TITLE => null,

                    //Information GeneralDataUser Entity
                    Constant::NAME => $patientObject[Constant::_FIRST_NAME],
                    Constant::LAST_NAME => $patientObject[Constant::_LAST_NAME],
                    Constant::EMAIL => $patientObject[Constant::_EMAIL],
                    Constant::CELL_PHONE => $patientObject[Constant::_CELLPHONE],
                    Constant::HOME_PHONE => $patientObject[Constant::_HOMEPHONE],
                    Constant::PHOTO => null,

                    //Identity of user type doctor(01), assistant(02), patient(03)
                    Constant::DOCTOR_ID => $doctorId,
                    Constant::IDENTITY_USER_TYPE_DOCTOR => null,
                    Constant::EMAIL_USER_TYPE_ASSISTANT => $emailAssistant,
                    Constant::IDENTITY_USER_TYPE_PATIENT => null,
                );
            } else {
                return $this->buildErrorObject(2);
            }

            $validationDocumentType = false;
            if ($data_array[Constant::DOCUMENT_TYPE]) {
                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::ID => $data_array[Constant::DOCUMENT_TYPE]));
                if ($documentTypeData) {
                    $validationDocumentType = true;
                }
            } else {
                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => Constant::DEFAULT_IDENTITY));
                $validationDocumentType = true;
            }

            if ($validationDocumentType && $documentTypeData) {

                if ($data_array[Constant::DOCTOR_ID]) {

                    //Entidad user del doctor
                    $entityUser = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

                    //Busco todos los pacientes de ese doctor
                    $allConnectionsOneThree = $this->repositoryUserConnectionOneThree->findBy(array('userOneId' => $data_array[Constant::DOCTOR_ID]));
                    $validation = true;
                    if ($data_array[Constant::DOCUMENT_TYPE] && $data_array[Constant::IDENTITY]) {
                        foreach ($allConnectionsOneThree as $connectionOneThree) {
                            $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();

                            if ($extraDataUser->getDocumentTypeId() && $extraDataUser->getIdentity())
                                if ($extraDataUser->getDocumentTypeId()->getId() == $data_array[Constant::DOCUMENT_TYPE] &&
                                    $extraDataUser->getIdentity() == $data_array[Constant::IDENTITY]
                                )
                                    $validation = false;
                        }
                    }

                    if ($validation) {
                        if (isset($data_array[Constant::NAME]) && isset($data_array[Constant::LAST_NAME]) &&
                            isset($data_array[Constant::CELL_PHONE])
                        ) {

                            $gender = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));
                            if ($gender) {

                                /*ENTITY GENERAL DATA USER*/
                                $generalDataEntity = new GeneralDataUser();
                                $generalDataEntity->setName($data_array[Constant::NAME]);
                                $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);
                                if (isset($data_array[Constant::EMAIL])) {
                                    $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                } else {
                                    $generalDataEntity->setEmail('mail@mail.com');
                                }

                                $generalDataEntity->setCellPhone($data_array[Constant::CELL_PHONE]);
                                $generalDataEntity->setHomePhone($data_array[Constant::HOME_PHONE]);

                                /*ENTITY EXTRA DATA USER*/
                                $extraDataEntity = new ExtraDataUser();
                                $extraDataEntity->setGenderId($gender);

                                $extraDataEntity->setDocumentTypeId($documentTypeData);

                                if ($data_array[Constant::IDENTITY]) {
                                    $extraDataEntity->setIdentity($data_array[Constant::IDENTITY]);
                                }

                                if ($data_array[Constant::BIRTH_DATE] != null) {
                                    $extraDataEntity->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                }

                                $extraDataEntity->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                                $extraDataEntity->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                                $extraDataEntity->setAddress($data_array[Constant::ADDRESS]);
                                $extraDataEntity->setUrgent($data_array[Constant::URGENT]);
                                $extraDataEntity->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);
                                //$extraDataEntity->setEnablePersonalTitle(true);

                                /*ENTITY USER*/
                                $userType = $this->repositoryUserType->find(3);
                                $userEntity = new User();
                                $userEntity->setUserTypeId($userType);
                                $userEntity->setExtraDataUserId($extraDataEntity);
                                $userEntity->setGeneralDataUserId($generalDataEntity);
                                $userEntity->setTemporary(0);
                                $userEntity->setCreationDate(new \DateTime('now'));
                                $userEntity->setSyncDate(new \DateTime('now'));

                                /*ENTITY USER CONNECTION ONE THREE*/
                                //$doctorEntity = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $did));
                                $userConnectionOneThree = new UserConnectionOneThree();
                                $userConnectionOneThree->setUserOneId($entityUser);
                                $userConnectionOneThree->setUserThreeId($userEntity);

                                /*ENTITY USER EPS CONNECTION*/
                                $userEpsConnection = new UserEpsConnection();
                                $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));
                                $userEpsConnection->setEpsId($eps);
                                $userEpsConnection->setExtraDataUserId($extraDataEntity);


                                $this->em->persist($extraDataEntity);
                                $this->em->persist($generalDataEntity);
                                // $this->em->persist($place);
                                $this->em->persist($userEntity);
                                $this->em->persist($userConnectionOneThree);
                                $this->em->persist($userEpsConnection);
                                $this->em->flush();

                                $patientService = new PatientService($this->em);

                                return $patientService->buildPatientObject($entityUser->getId(),
                                    $userEntity, $generalDataEntity, $extraDataEntity, $localId);
                            } else {
                                return $this->buildErrorObject(16);
                            }
                        } else {
                            return $this->buildErrorObject(2);
                        }
                    } else {
                        return $this->buildErrorObject(4);
                    }
                    //Si el paciente lo crea la asistente
                } elseif ($data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

                    //Busco todos los asistentes
                    $allAssistants = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => 2));
                    $assistant = null;
                    foreach ($allAssistants as $tempAssistant) {
                        $generalDataUser = $tempAssistant->getGeneralDataUserId();

                        if ($generalDataUser->getEmail() == $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
                            $assistant = $tempAssistant;
                            break;
                        }
                    }

                    if (isset($assistant)) {

                        $connectionOneTwo = $this->repositoryUserConnectionOneTwo->findOneBy(array(Constant::USER_TWO_ID => $assistant->getId()));

                        if ($connectionOneTwo) {

                            $doctorEntity = $connectionOneTwo->getUserOneId();

                            if ($doctorEntity) {

                                //Busco todos los pacientes de ese doctor
                                $allConnectionsOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_ID => $doctorEntity->getId()));
                                $validation = true;
                                if ($data_array[Constant::DOCUMENT_TYPE] && $data_array[Constant::IDENTITY]) {
                                    foreach ($allConnectionsOneThree as $connectionOneThree) {
                                        $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();

                                        if ($extraDataUser->getDocumentTypeId() && $extraDataUser->getIdentity())
                                            if ($extraDataUser->getDocumentTypeId()->getDescription() == $data_array[Constant::DOCUMENT_TYPE] && $extraDataUser->getIdentity() == $data_array[Constant::IDENTITY])
                                                $validation = false;
                                    }
                                }

                                if ($validation) {

                                    if (isset($data_array[Constant::NAME]) && isset($data_array[Constant::LAST_NAME]) &&
                                        isset($data_array[Constant::CELL_PHONE])
                                    ) {
                                        $gender = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));

                                        /*ENTITY GENERAL DATA USER*/
                                        $generalDataEntity = new GeneralDataUser();
                                        $generalDataEntity->setName($data_array[Constant::NAME]);
                                        $generalDataEntity->setLastName($data_array[Constant::LAST_NAME]);
                                        if (isset($data_array[Constant::EMAIL])) {
                                            $generalDataEntity->setEmail($data_array[Constant::EMAIL]);
                                        } else {
                                            $generalDataEntity->setEmail('mail@mail.com');
                                        }

                                        $generalDataEntity->setCellPhone($data_array[Constant::CELL_PHONE]);
                                        $generalDataEntity->setHomePhone($data_array[Constant::HOME_PHONE]);

                                        /*ENTITY EXTRA DATA USER*/
                                        $extraDataEntity = new ExtraDataUser();
                                        $extraDataEntity->setGenderId($gender);

                                        $extraDataEntity->setDocumentTypeId($documentTypeData);

                                        if ($data_array[Constant::IDENTITY]) {
                                            $extraDataEntity->setIdentity($data_array[Constant::IDENTITY]);
                                        }

                                        if ($data_array[Constant::BIRTH_DATE] != null) {
                                            $extraDataEntity->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                                        }

                                        $extraDataEntity->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                                        $extraDataEntity->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                                        $extraDataEntity->setAddress($data_array[Constant::ADDRESS]);
                                        $extraDataEntity->setUrgent($data_array[Constant::URGENT]);
                                        $extraDataEntity->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);

                                        /*ENTITY USER*/
                                        $userType = $this->repositoryUserType->find(3);
                                        $userEntity = new User();
                                        $userEntity->setUserTypeId($userType);
                                        $userEntity->setExtraDataUserId($extraDataEntity);
                                        $userEntity->setGeneralDataUserId($generalDataEntity);
                                        $userEntity->setTemporary(0);
                                        $userEntity->setCreationDate(new \DateTime('now'));
                                        $userEntity->setSyncDate(new \DateTime('now'));

                                        /*ENTITY USER CONNECTION ONE THREE*/
                                        //$doctorEntity = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $did));
                                        $userConnectionOneThree = new UserConnectionOneThree();
                                        $userConnectionOneThree->setUserOneId($doctorEntity);
                                        $userConnectionOneThree->setUserThreeId($userEntity);

                                        /*ENTITY USER EPS CONNECTION*/
                                        $userEpsConnection = new UserEpsConnection();
                                        $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));
                                        $userEpsConnection->setEpsId($eps);
                                        $userEpsConnection->setExtraDataUserId($extraDataEntity);


                                        $this->em->persist($extraDataEntity);
                                        $this->em->persist($generalDataEntity);
                                        // $this->em->persist($place);
                                        $this->em->persist($userEntity);
                                        $this->em->persist($userConnectionOneThree);
                                        $this->em->persist($userEpsConnection);
                                        $this->em->flush();

                                        $patientService = new PatientService($this->em);

                                        return $patientService->buildPatientObject($doctorEntity->getId(),
                                            $userEntity, $generalDataEntity, $extraDataEntity, $localId);
                                    } else {
                                        return $this->buildErrorObject(2);
                                    }
                                } else {
                                    return $this->buildErrorObject(4);
                                }
                            }
                        } else {
                            return $this->buildErrorObject(5);
                        }
                    } else {
                        return $this->buildErrorObject(1);
                    }
                }
            } else {
                return $this->buildErrorObject(3);
            }
        } catch (\Exception $e) {
            error_log('catch createUserSync');
            error_log('Error:' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $identity
     * @param $documentTypeId
     * @param $firstName
     * @param $lastName
     * @param $cellPhone
     * @param $email
     * @param $idDoctor
     * @return null|object
     */
    public function validateUserSync($identity, $documentTypeId, $firstName, $lastName, $cellPhone, $email,
                                     $idDoctor, $localId)
    {
        try {
            //Busco todos los pacientes de ese doctor
            $allConnectionsOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_ID => $idDoctor));

            //Primera instancia: se busca el paciente por identificacion y tipo de documento para el medico respectivo
            if (isset($identity) && isset($documentTypeId)) {

                if (isset($documentTypeId) && isset($identity)) {
                    foreach ($allConnectionsOneThree as $connectionOneThree) {
                        $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();
                        if ($extraDataUser->getDocumentTypeId() && $extraDataUser->getIdentity()) {
                            if ($extraDataUser->getDocumentTypeId()->getId() == $documentTypeId &&
                                $extraDataUser->getIdentity() == $identity
                            ) {
                                $userEntity = $connectionOneThree->getUserThreeId();
                                $generalDataUser = $connectionOneThree->getUserThreeId()->getGeneralDataUserId();

                                $patientService = new PatientService($this->em);
                                return $patientService->buildPatientObject($idDoctor, $userEntity, $generalDataUser,
                                    $extraDataUser, $localId);
                            }
                        }
                    }
                }
                return null;
            } else {
                //Segunda instancia los otros 4 datos
                if (isset($firstName) && isset($lastName)) {

                    //proceso los datos que llegan en la request
                    $firstName = preg_replace('[\s+]', '', $firstName);
                    $lastName = preg_replace('[\s+]', '', $lastName);

                    if ($cellPhone == null) {
                        $cellPhone = '';
                    } else {
                        $cellPhone = preg_replace('[\s+]', '', $cellPhone);
                    }

                    if ($email == null) {
                        $email = '';
                    } else {
                        $email = preg_replace('[\s+]', '', $email);
                    }

                    //concateno los 4 campos que llegan por request
                    $stringRequest = $firstName . $lastName . $cellPhone . $email;
                    //error_log('String Request: ' . $stringRequest);

                    //recorro los pacientes y busco coincidencias
                    foreach ($allConnectionsOneThree as $connectionOneThree) {
                        $generalDataUser = $connectionOneThree->getUserThreeId()->getGeneralDataUserId();

                        //proceso los datos de base de datos
                        $nameDB = $generalDataUser->getName();
                        $lastNameDB = $generalDataUser->getLastName();
                        $phoneDB = $generalDataUser->getCellPhone();
                        $emailDB = $generalDataUser->getEmail();

                        /*Si hay algun valor en base de datos nulo, lo convierto a cadena sin espacio para poder
                        concatenar los 4 campos sin inconvenientes. De lo contrario elimino los espacios en blanco*/
                        if ($nameDB == null) {
                            $nameDB = '';
                        } else {
                            $nameDB = preg_replace('[\s+]', '', $nameDB);
                        }

                        if ($lastNameDB == null) {
                            $lastNameDB = '';
                        } else {
                            $lastNameDB = preg_replace('[\s+]', '', $lastNameDB);
                        }

                        if ($phoneDB == null) {
                            $phoneDB = '';
                        } else {
                            $phoneDB = preg_replace('[\s+]', '', $phoneDB);
                        }

                        if ($emailDB == null || $emailDB == 'mail@mail.com') {
                            $emailDB = '';
                        } else {
                            $emailDB = preg_replace('[\s+]', '', $emailDB);
                        }

                        //concateno los 4 campos de base de datos
                        $stringDB = $nameDB . $lastNameDB . $phoneDB . $emailDB;
                        //error_log('String DB: ' . $stringDB);

                        if (strcasecmp($stringDB, $stringRequest) == 0) {
                            $userEntity = $connectionOneThree->getUserThreeId();
                            $extraDataUser = $connectionOneThree->getUserThreeId()->getExtraDataUserId();

                            $patientService = new PatientService($this->em);
                            return $patientService->buildPatientObject($idDoctor, $userEntity, $generalDataUser,
                                $extraDataUser, $localId);
                        }
                    }
                }
                return null;
            }
        } catch (\Exception $e) {
            error_log('catch validateUserSync');
            error_log('Error:' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return object
     */
    public function registerDoctorV1(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            //Constant::_ID => $request->request->get(Constant::_SINGLE_DATA)[Constant::_ID],
            //Constant::_USER_TYPE_ID => $request->request->get(Constant::_SINGLE_DATA)[Constant::_USER_TYPE_ID],
            //Constant::_OWNER_USER_ID => $request->request->get(Constant::_SINGLE_DATA)[Constant::_OWNER_USER_ID],
            Constant::_FIRST_NAME => $request->request->get(Constant::_FIRST_NAME),
            Constant::_LAST_NAME => $request->request->get(Constant::_LAST_NAME),
            Constant::_EMAIL => $request->request->get(Constant::_EMAIL),
            Constant::_PASSWORD => $request->request->get(Constant::_PASSWORD),
            //Constant::_TEMPORARY_PASSWORD => $request->request->get(Constant::_SINGLE_DATA)[Constant::_TEMPORARY_PASSWORD],
            Constant::_PERSONAL_TITLE_ID => $request->request->get(Constant::_PERSONAL_TITLE_ID),
            Constant::_ARRIVAL_TIME => $request->request->get(Constant::_ARRIVAL_TIME),
            Constant::_DEPART_TIME => $request->request->get(Constant::_DEPART_TIME),
            Constant::_SPECIALIZATION_ID => $request->request->get(Constant::_SPECIALIZATION_ID),
            //Constant::_TOKEN => $request->request->get(Constant::_SINGLE_DATA)[Constant::_TOKEN],
            //Constant::_FULL_NAME => $request->request->get(Constant::_SINGLE_DATA)[Constant::_FULL_NAME],
            Constant::_PLACES => $request->request->get(Constant::_PLACES),
            Constant::_EPS => $request->request->get(Constant::_EPS)
        );


        if (isset($data_array[Constant::_FIRST_NAME]) && isset($data_array[Constant::_LAST_NAME]) &&
            isset($data_array[Constant::_EMAIL]) && isset($data_array[Constant::_PASSWORD]) &&
            isset($data_array[Constant::_PERSONAL_TITLE_ID]) && isset($data_array[Constant::_ARRIVAL_TIME]) &&
            isset($data_array[Constant::_DEPART_TIME]) && isset($data_array[Constant::_PLACES]) &&
            isset($data_array[Constant::_EPS])
        ) {

            //Buscar si el mail o la identidad ya existen
            //Busco todos los doctores
            $allDoctors = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => 1));

            //Con la lista de doctores recorro cada uno para validar que no existe el mail y la identidad (tipo y numero de documento)
            $validation = true;
            foreach ($allDoctors as $doctor) {
                $generalDataUser = $doctor->getGeneralDataUserId();

                if ($generalDataUser->getEmail() == $data_array[Constant::_EMAIL])
                    $validation = false;
            }

            //Busco los asistentes y los recorro para validar que el emial no exista
            $allAssistants = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => 2));

            foreach ($allAssistants as $assistant) {
                $gduAssistant = $assistant->getGeneralDataUserId();
                if ($gduAssistant->getEmail() == $data_array[Constant::_EMAIL])
                    $validation = false;
            }


            if ($validation) {
                if ($data_array[Constant::_SPECIALIZATION_ID]) {
                    $specialityData = $this->repositorySpeciality->findOneBy(array(Constant::ID => $data_array[Constant::_SPECIALIZATION_ID]));
                } else {
                    //Se busca especialidad por defecto: No aplica, id 26
                    $specialityData = $this->repositorySpeciality->findOneBy(array(Constant::ID => 26));
                }

                /*ENTITY GENERAL DATA USER*/
                $generalDataEntity = new GeneralDataUser();
                $generalDataEntity->setName($data_array[Constant::_FIRST_NAME]);
                $generalDataEntity->setLastName($data_array[Constant::_LAST_NAME]);
                $generalDataEntity->setEmail($data_array[Constant::_EMAIL]);
                $this->em->persist($generalDataEntity);

                /*ENTITY EXTRA DATA USER*/
                $extraDataEntity = new ExtraDataUser();
                $extraDataEntity->setArrivalTime(new \DateTime($data_array[Constant::_ARRIVAL_TIME]));
                $extraDataEntity->setDepartTime(new \DateTime($data_array[Constant::_DEPART_TIME]));
                //Cifrado de password
                $hashPassword = hash($hashAlgorithm, $data_array[Constant::_PASSWORD]);
                $extraDataEntity->setPassword($hashPassword);
                $extraDataEntity->setSpecialityId($specialityData);

                if ($data_array[Constant::_PERSONAL_TITLE_ID] == 0 || $data_array[Constant::_PERSONAL_TITLE_ID] == 1 ||
                    $data_array[Constant::_PERSONAL_TITLE_ID] == 2
                ) {

                    $extraDataEntity->setPersonalTitle($data_array[Constant::_PERSONAL_TITLE_ID]);
                } else {
                    return $this->buildErrorObject(15);
                }

                $this->em->persist($extraDataEntity);

                /* ENTITY USER*/
                //Entidad UserType
                $userType = $this->repositoryUserType->find(1);
                $userEntity = new User();
                $userEntity->setExtraDataUserId($extraDataEntity);
                $userEntity->setGeneralDataUserId($generalDataEntity);
                $userEntity->setUserTypeId($userType);
                $userEntity->setTemporary(false);
                $userEntity->setCreationDate(new \DateTime('now'));
                $userEntity->setSyncDate(new \DateTime('now'));
                $this->em->persist($userEntity);
                $this->em->flush();

                //Informacion de consultorios
                $placeService = new PlaceService($this->em);
                $arrayPlacesResponse = array();
                $arrayUPConnections = array();
                foreach ($data_array[Constant::_PLACES] as $place) {
                    $places = new Place();
                    $places->setAddress($place[Constant::_ADDRESS]);
                    $places->setPhone($place[Constant::_PHONE]);
                    $this->em->persist($places);
                    $this->em->flush();

                    $userPlaceConnectionEntity = new UserPlaceConnection();
                    $userPlaceConnectionEntity->setUserId($userEntity);
                    $userPlaceConnectionEntity->setPlaceId($places);
                    $this->em->persist($userPlaceConnectionEntity);
                    $this->em->flush();
                    array_push($arrayUPConnections, $userPlaceConnectionEntity);
                    array_push($arrayPlacesResponse, $placeService->buildPlaceObject($places, 0, $userEntity->getId()));
                }

                //Informacion de EPS's
                $epsService = new EpsService($this->em);
                $arrayEpsResponse = array();
                $arrayEpss = array();
                if (count($data_array[Constant::_EPS]) > 0) {
                    foreach ($data_array[Constant::_EPS] as $eps) {
                        $data_eps = new UserEpsConnection();

                        $eps_data = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $eps[Constant::_ID]));

                        if ($eps_data) {
                            $data_eps->setEpsId($eps_data);
                            $data_eps->setExtraDataUserId($extraDataEntity);
                            $this->em->persist($data_eps);
                            $this->em->flush();
                            array_push($arrayEpss, $eps_data);
                            array_push($arrayEpsResponse, $epsService->buildHealthPromoterObject($eps_data));
                        }
                    }
                } else {
                    //Se almacena EPS por defecto: Particular
                    if (count($data_array[Constant::_EPS]) == 0) {
                        $epsEntity = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => Constant::DEFAULT_EPS));
                        $data_eps = new UserEpsConnection();
                        if ($epsEntity) {
                            $data_eps->setEpsId($epsEntity);
                            $data_eps->setExtraDataUserId($extraDataEntity);
                            $this->em->persist($data_eps);
                            $this->em->flush();
                            array_push($arrayEpss, $epsEntity);
                            array_push($arrayEpsResponse, $epsService->buildHealthPromoterObject($epsEntity));
                        }
                    }
                }

                $email = new MailService();
                $email->setEmail($generalDataEntity->getEmail());
                $email->setName($generalDataEntity->getName());
                $email->setPersonalTitleDoctor($extraDataEntity->getPersonalTitle());
                $email->setTipeMail(6);
                $email->sendEmail();

                /*
                 * Una vez creada la data del medico se crea la data de un paciente con la misma informacion
                 * del medico
                 */
                $extraDataUserPatient = new ExtraDataUser();
                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => Constant::DEFAULT_IDENTITY));
                $extraDataUserPatient->setDocumentTypeId($documentTypeData);

                $genderId = null;
                if ($data_array[Constant::_PERSONAL_TITLE_ID] == 0) {
                    //Masculino
                    $genderId = $this->repositoryGender->find(2);
                } elseif ($data_array[Constant::_PERSONAL_TITLE_ID] == 1) {
                    //Masculino
                    $genderId = $this->repositoryGender->find(2);
                } elseif ($data_array[Constant::_PERSONAL_TITLE_ID] == 2) {
                    //Femenino
                    $genderId = $this->repositoryGender->find(1);
                } else {
                    return $this->buildErrorObject(15);
                }

                $extraDataUserPatient->setGenderId($genderId);
                $this->em->persist($extraDataUserPatient);

                $generalDataUserPatient = new GeneralDataUser();
                if (count($arrayPlacesResponse) > 0) {
                    $generalDataUserPatient->setCellPhone($arrayPlacesResponse[0]->Phone);
                }
                $generalDataUserPatient->setName($data_array[Constant::_FIRST_NAME]);
                $generalDataUserPatient->setLastName($data_array[Constant::_LAST_NAME]);
                $generalDataUserPatient->setEmail($data_array[Constant::_EMAIL]);
                $this->em->persist($generalDataUserPatient);

                $userPatient = new User();
                $userPatient->setExtraDataUserId($extraDataUserPatient);
                $userPatient->setGeneralDataUserId($generalDataUserPatient);
                $userTypePatient = $this->repositoryUserType->find('3');
                $userPatient->setUserTypeId($userTypePatient);
                $userPatient->setCreationDate(new \DateTime('now'));
                $userPatient->setTemporary(false);
                $userPatient->setSyncDate(new \DateTime('now'));
                $this->em->persist($userPatient);

                //Relaciono el medico y el paciente
                //TODO: para el servicio userConnectionOneThree
                $ucOneThree = new UserConnectionOneThree();
                $ucOneThree->setUserOneId($userEntity);
                $ucOneThree->setUserThreeId($userPatient);
                $this->em->persist($ucOneThree);

                //Creo la cita
                $now = new \DateTime('now');
                $plusOneHour = new \DateTime(strtolower($now->format('Y-m-d H:i:s') . ' +1 day'));
                $appointment = new Appointment();
                $appointment->setUserOneId($userEntity);
                $appointment->setUserThreeId($userPatient);
                $appointment->setCreationDate($now);
                $appointment->setDate(new \DateTime($plusOneHour->format('Y-m-d 8' . ':00:00')));
                $appointment->setSyncDate(new \DateTime('now'));
                $appointment->setAssignedTime(new \DateTime('00:30:00'));
                if (sizeof($arrayUPConnections) > 0) {
                    $appointment->setUserPlaceConnectionId($arrayUPConnections[0]);
                }

                $defaultEps = $this->repositoryEps->find(Constant::DEFAULT_EPS);
                $appointment->setEps($defaultEps);

                $this->em->persist($appointment);
                $this->em->flush();

                $this->setDateNotificationForAppointments($appointment, $generalDataUserPatient, $generalDataEntity, $extraDataEntity);

                //return $this->buildSingleDataObject(true, $generalDataEntity, $extraDataEntity, $arrayUPConnections,
                //    $arrayEpss, null, null);

                return $userObjectResponse = (object)array(
                    Constant::_SINGLE_DATA => (object)array(
                        Constant::_ID => $userEntity->getId(),
                        Constant::_USER_TYPE_ID => $userEntity->getUserTypeId()->getId(),
                        Constant::_OWNER_USER_ID => $userEntity->getId(),
                        Constant::_FIRST_NAME => $generalDataEntity->getName(),
                        Constant::_LAST_NAME => $generalDataEntity->getLastName(),
                        Constant::_EMAIL => $generalDataEntity->getEmail(),
                        Constant::_PASSWORD => null,
                        Constant::_TEMPORARY_PASSWORD => $userEntity->getTemporary(),
                        Constant::_PERSONAL_TITLE_ID => (int)$extraDataEntity->getPersonalTitle(),
                        Constant::_ARRIVAL_TIME => $extraDataEntity->getArrivalTime()->format(\DateTime::ISO8601),
                        Constant::_DEPART_TIME => $extraDataEntity->getDepartTime()->format(\DateTime::ISO8601),
                        Constant::_SPECIALIZATION_ID => $extraDataEntity->getSpecialityId()->getId(),
                        Constant::_TOKEN => null,
                        Constant::_FULL_NAME => $generalDataEntity->getName() . ' ' . $generalDataEntity->getLastName(),
                        Constant::_PLACES => $arrayPlacesResponse,
                        Constant::_EPS => $arrayEpsResponse
                    )
                );

            } else {
                return $this->buildErrorObject(17);
            }
        } else {
            return $this->buildErrorObject(2);
        }
    }

    /**
     * @param Appointment $appointment
     * @param GeneralDataUser $generalDataPatient
     * @param GeneralDataUser $generaDataDoctor
     * @param ExtraDataUser $extraDataDoctor
     */
    private function setDateNotificationForAppointments(Appointment $appointment, GeneralDataUser $generalDataPatient, GeneralDataUser $generaDataDoctor, ExtraDataUser $extraDataDoctor)
    {
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $appointmentDate = new \DateTime($appointment->getDate()->format("Y-m-d"));

        //Regla de negocio
        //Cita creada para el mísmo día o para el día siguiente a la cita
        if ($appointmentDate->format("Y-m-d") == $today->format("Y-m-d") ||
            $appointmentDate->format("Y-m-d") == $tomorrow->format("Y-m-d")
        ) {

            //Notificacion SMS
            //Se obtiene el id del registro type=6 de la tabla NotificationType
            $smsNotificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 6));
            $notificationSMS = new Notification();
            $notificationSMS->setAppointment($appointment);
            $notificationSMS->setDateToSend($today);
            $notificationSMS->setIsSend(false);
            $notificationSMS->setType($smsNotificationType);
            $notificationSMS->setSyncDate(new \DateTime('now'));
            $this->em->persist($notificationSMS);
            $this->em->flush();

            //Notificacion email
            //Se obtiene el id del registro type=2 de la tabla NotificationType
            $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
            $notification = new Notification();
            $notification->setAppointment($appointment);
            $notification->setDateToSend($today);
            $notification->setIsSend(false);
            $notification->setType($notificationType);
            $notification->setSyncDate(new \DateTime('now'));
            $this->em->persist($notification);
            $this->em->flush();

            $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

        } else {

            //Valores para pruebas
            //$today = new \DateTime('2016-08-27');

            $differenceDate = date_diff($today, $appointmentDate);

            //Segunda regla de negocio
            //Si la cita se crea el sabado y la cita es para el Lunes, se debe enviar mail de confirmacion inmediatamente
            if ($differenceDate->format('%a') == 2 && $today->format('w') == 6 && $appointmentDate->format('w') == 1) {
                //Se obtiene el id del registro type=2 de la tabla NotificationType
                $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));

                if (count($notificationType) > 0) {
                    $notification = new Notification();
                    $notification->setAppointment($appointment);
                    $notification->setDateToSend($today);
                    $notification->setIsSend(false);
                    $notification->setType($notificationType);
                    $notification->setSyncDate(new \DateTime('now'));
                    $this->em->persist($notification);
                    $this->em->flush();

                    $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                    //TODO: aca se puede implementar una logica para que en caso de que no se envie el mail se notifique a otro mail
                } else {
                    //TODO: inconsistencia de datos
                }
            } else {

                //Tercera regla de negocio
                //dos o mas dias y menos de un mes
                $todayPlusTwoDays = new \DateTime(strtolower($today->format('Y-m-d') . ' +2 days'));
                $todayPlusOneMonth = new \DateTime(strtolower($today->format('Y-m-d') . ' +1 month'));

                if ($appointmentDate >= $todayPlusTwoDays && $appointmentDate <= $todayPlusOneMonth) {
                    //Notificacion SMS 1 dia antes
                    //Se obtiene el id del registro type=5 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 5));

                    if ($notificationType) {
                        $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                        //Si la notificacion cae un Domingo se corre para el Sabado
                        if ($appointmentDateLessOneDay->format('w') == 0)
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($appointmentDateLessOneDay);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();
                    }

                    //Notificacion email
                    //Se obtiene el id del registro type=1 de la tabla NotificationType
                    $notificationType = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                    if ($notificationType) {
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($today);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $notification->setSyncDate(new \DateTime('now'));
                        $this->em->persist($notification);
                        $this->em->flush();

                        $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                        //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                        //Setear fecha para el segundo email (de confirmacion)
                        //Se obtiene el id del registro type=2 de la tabla NotificationType
                        $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                        if (count($notificationTypeTwo) > 0) {
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                            //Si la notificacion cae un Domingo se corre para el Sabado
                            if ($appointmentDateLessOneDay->format('w') == 0)
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessOneDay);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeTwo);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                        } else {
                            //TODO: inconsistencia de datos
                        }
                    } else {
                        //TODO: inconsistencia de datos
                    }
                } else {

                    //Cuarta regla de negocio. Citas para mas de 1 mes
                    if ($appointmentDate > $todayPlusOneMonth) {

                        //Se obtiene el id del registro type=1 de la tabla NotificationType
                        $notificationTypeOne = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 1));

                        if (count($notificationTypeOne) > 0) {
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($today);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();
                            //Se envia el email uno
                            $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                            //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                            //Setear fecha para otro mail de informacion y sms 10 dias antes
                            $appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -10 days'));
                            //Si la tonificacion cae un Domingo
                            if ($appointmentDateLessTenDays->format('w') == 0)
                                $appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDateLessTenDays->format('Y-m-d') . ' -1 days'));
                            //Para Email
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessTenDays);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $notification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($notification);
                            $this->em->flush();

                            //Para SMS
                            $notificationTypeSMS = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 3));
                            $SMSNotification = new Notification();
                            $SMSNotification->setAppointment($appointment);
                            $SMSNotification->setDateToSend($appointmentDateLessTenDays);
                            $SMSNotification->setIsSend(false);
                            $SMSNotification->setType($notificationTypeSMS);
                            $SMSNotification->setSyncDate(new \DateTime('now'));
                            $this->em->persist($SMSNotification);
                            $this->em->flush();

                            //Setear fecha para el segundo email (de confirmacion)
                            //Se obtiene el id del registro type=2 de la tabla NotificationType
                            $notificationTypeTwo = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 2));
                            if (count($notificationTypeTwo) > 0) {
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                                //Si la notificacion cae un Domingo se corre para el Sabado
                                if ($appointmentDateLessOneDay->format('w') == 0)
                                    $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                                $notification = new Notification();
                                $notification->setAppointment($appointment);
                                $notification->setDateToSend($appointmentDateLessOneDay);
                                $notification->setIsSend(false);
                                $notification->setType($notificationTypeTwo);
                                $notification->setSyncDate(new \DateTime('now'));
                                $this->em->persist($notification);
                                $this->em->flush();
                            } else {
                                //TODO: inconsistencia de datos
                            }
                        } else {

                        }
                    }
                }
            }
        }
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     */
    public function sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification)
    {

        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setNewUser(true);

        if ($today->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
            $email->setIsToday(true);
        } else {
            if ($tomorrow->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
                $email->setIsTomorrow(true);
            } else {
                $email->setDay($appointment->getDate()->format('d'));
                $email->setDayLetter($days[$appointment->getDate()->format('w')]);
                $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
                $email->setYear($appointment->getDate()->format('Y'));
            }
        }

        $email->setDate($appointment->getDate());

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));

        $email->setDateToEnd($dateToEnd);
        $email->setDateid($appointment->getId());
        $email->setTipeMail(3);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $gduPatient
     * @param $eduDoctor
     * @param $appointment
     * @param $gduDoctor
     * @param $notification
     */
    public function sendInformationEmail($gduPatient, $eduDoctor, $appointment, $gduDoctor, $notification)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($gduPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($gduDoctor->getName());
        $email->setLastNameDoctor($gduDoctor->getLastName());
        $email->setEmailDoctor($gduDoctor->getEmail());
        $email->setPersonalTitleDoctor($eduDoctor->getPersonalTitle());
        $email->setEmail($gduPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate());
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        $email->setDateToEnd($dateToEnd);

        $email->setGduPatient($gduPatient);
        $email->setEdUDoctor($eduDoctor);
        $email->setAppointment($appointment);
        $email->setGduDoctor($gduDoctor);
        $email->setTipeMail(2);

        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    /**
     * @param Request $request
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @return array|null
     */
    public function getUser(Request $request, $freeAppointments, $timeLapseForFreeAppointments)
    {
        //Json Information
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::EMAIL),
            Constant::USER_ID => $request->request->get(Constant::USER_ID)
        );
        $jsonResponse = array();
        $negativeResponse = null;

        if ($data_array[Constant::USER_ID]) {
            $userFinder = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::USER_ID]));

            if (count($userFinder) > 0) {
                $userIdentity = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $userFinder->getExtraDataUserId()));

                $user = $userIdentity->getUser();
                $user_id_e = $userIdentity->getId();
                $userTypeId = $user->getUserTypeId();
                $user_id = $user->getId();
                $documentData = $userIdentity->getDocumentTypeId();
                $documentDescription = null;
                $genderDescription = null;

                //UserType Entity
                $userTypeIfo = $this->repositoryUserType->find($userTypeId);
                $descriptionUserType = $userTypeIfo->getDescription();
                //GeneralDataUser Entity
                $generalDataUserId = $user->getGeneralDataUserId();
                $generalDataUserInfo = $this->repositoryGeneralDataUser->find($generalDataUserId);
                //place
                //returns an array with the validated content
                $data_place_connection = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_PLACE_USER_ID => $user_id), array());

                $data_eps_connection = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $user_id_e), array());

                if ($descriptionUserType == Constant::USER_TYPE_DOCTOR) {
                    //traverses the array to get the id in each iteration
                    foreach ($data_place_connection as $values) {
                        $place_id = $values->getPlaceId();
                        $place_data = $this->repositoryPlace->find($place_id);
                        $placesPushResponse[] = array(
                            //place
                            Constant::GENERAL_ID_COLUMN => $place_id->getId(),
                            Constant::ADDRESS => $place_data->getAddress(),
                            Constant::PHONE => $place_data->getPhone(),
                        );
                    }

                    $epsResponse = [];
                    foreach ($data_eps_connection as $value) {
                        if ($value == null) {
                            $epsResponse[] = array(
                                Constant::EPS_NAME => Constant::STATUS_NOT_FOUND
                            );
                        } else {
                            $eps_id = $value->getEpsId();
                            $eps = $this->repositoryEps->find($eps_id);
                            $epsConnection = $this->repositoryUserEpsConnection->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $userIdentity->getId(), Constant::EPS_ID => $value->getEpsId()));
                            $epsResponse[] = array(
                                Constant::GENERAL_ID_COLUMN => $eps->getId(),
                                Constant::EPS_NAME => $eps->getDescription(),
                                Constant::USER_EPS_CONNECTION => $epsConnection->getId()
                            );
                        }
                    }

                    $this->validateMembership($user, $freeAppointments, $timeLapseForFreeAppointments);

                    $membership = $this->repositoryMembership->findOneBy(array('user' => $user->getId()));

                    $personalTitle = $userIdentity->getPersonalTitle();
                    if ($personalTitle == null) {
                        $personalTitle = 0;
                    }

                    if ($documentData) {
                        $documentDescription = $documentData->getDescription();
                    }
                    //TODO: poner password null
                    array_push($jsonResponse, array(
                        'isAssistant' => false,
                        //unique user identity
                        Constant::GENERAL_ID_COLUMN => $user->getId(),
                        //ExtraDataUser Entity
                        Constant::PASSWORD => null,
                        Constant::BIRTH_DATE => $userIdentity->getBirthDate(),
                        Constant::IDENTITY => $userIdentity->getIdentity(),
                        Constant::DOCUMENT_TYPE => $documentDescription,
                        Constant::ARRIVAL_TIME => $userIdentity->getArrivalTime(),
                        Constant::DEPART_TIME => $userIdentity->getDepartTime(),
                        Constant::PHOTO => $userIdentity->getPhoto(),

                        Constant::PERSONAL_TITLE => $personalTitle,
                        //Entities
                        Constant::SPECIALITY => $userIdentity->getSpecialityId()->getDescription(),
                        //GeneralDataUser Entity
                        Constant::NAME => $generalDataUserInfo->getName(),
                        Constant::LAST_NAME => $generalDataUserInfo->getLastName(),
                        Constant::EMAIL => $generalDataUserInfo->getEmail(),
                        Constant::CELL_PHONE => $generalDataUserInfo->getCellPhone(),
                        Constant::HOME_PHONE => $generalDataUserInfo->getHomePhone(),
                        Constant::PLACES => $placesPushResponse,
                        Constant::EPS => $epsResponse,
                        'membership' => (object)array(
                            'payMembership' => $membership->getPayMembership(),
                            'startPeriod' => $membership->getStartPeriod(),
                            'endPeriod' => $membership->getEndPeriod(),
                            'currentAppointments' => $membership->getCurrentAppointments())
                    ));

                    return $jsonResponse;

                } else if ($descriptionUserType == Constant::USER_TYPE_PATIENT) {
                    //$epsConnection = $this->repositoryUserEpsConnection->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $userIdentity->getId()));
                    //$eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $epsConnection->getEpsId()));

                    $documentTypeDescription = null;
                    if ($userIdentity->getDocumentTypeId()) {
                        $documentTypeDescription = $userIdentity->getDocumentTypeId()->getDescription();
                    } else {
                        $documentTypeDescription = Constant::DEFAULT_IDENTITY;
                    }

                    array_push($jsonResponse, array(
                        //user unique id
                        Constant::GENERAL_ID_COLUMN => $user->getId(),
                        //ExtraDataUser Entity
                        Constant::IDENTITY => $userIdentity->getIdentity(),
                        Constant::BIRTH_DATE => $userIdentity->getBirthDate(),
                        Constant::EMERGENCY_NAME => $userIdentity->getEmergencyName(),
                        Constant::EMERGENCY_NUMBER => $userIdentity->getEmergencyNumber(),
                        Constant::URGENT => $userIdentity->getUrgent(),
                        Constant::COMMENT_EXTRA_DATA_USER => $userIdentity->getComment(),
                        //Constant::EPS => $eps->getDescription(),

                        //Entities
                        Constant::DOCUMENT_TYPE => $documentTypeDescription,
                        Constant::GENDER => $genderDescription,
                        //Constant::PLACE_ID => $userIdentity->getPlaceId()->getAddress(),

                        //GeneralDataUser Entity
                        Constant::NAME => $generalDataUserInfo->getName(),
                        Constant::LAST_NAME => $generalDataUserInfo->getLastName(),
                        Constant::EMAIL => $generalDataUserInfo->getEmail(),
                        Constant::CELL_PHONE => $generalDataUserInfo->getCellPhone(),
                        Constant::HOME_PHONE => $generalDataUserInfo->getHomePhone()

                    ));
                    return $jsonResponse;
                }
            } else {
                return $negativeResponse;
            }
        } else if ($data_array[Constant::EMAIL]) {

            $gduAssistant = $this->repositoryGeneralDataUser->findOneBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            if ($gduAssistant && $gduAssistant->getUser()->getUserTypeId()->getId() == Constant::TYPE_ASSISTANT) {

                $user = $gduAssistant->getUser();

                array_push($jsonResponse, array(
                    //GeneralDataUser Entity
                    'isAssistant' => true,
                    Constant::NAME => $gduAssistant->getName(),
                    Constant::EMAIL => $gduAssistant->getEmail()
                ));
                return $jsonResponse;
            } else {
                return $negativeResponse;
            }
        }
    }

    /**
     * @param $user
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     */
    public function validateMembership($user, $freeAppointments, $timeLapseForFreeAppointments)
    {
        //Busco el registro en la membresia
        $membership = $this->repositoryMembership->findOneBy(array('user' => $user->getId()));

        $today = new \DateTime('today');

        if ($membership) {

            if ($today > $membership->getEndPeriod()) {
                //Se genera un nuevo periodo
                $flagWhile = true;
                $startPeriod = new \DateTime($membership->getEndPeriod()->format('Y-m-d'));
                do {
                    $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                    if ($today < $endPeriod) {
                        $flagWhile = false;
                    } else {
                        $startPeriod = $endPeriod;
                    }
                } while ($flagWhile);

                if ($startPeriod && $endPeriod) {
                    $membership->setCurrentAppointments(0);
                    $membership->setEndPeriod($endPeriod);
                    $membership->setStartPeriod($startPeriod);
                    $membership->setPayMembership(false);
                    $membership->setAppointmentsLimit($freeAppointments);
                    $this->em->flush();
                }
            }
        } else {
            //Deberia tener membresia pero si no la tiene se crea

            //Deberia tener fecha de creacion pero si no lo tiene se toma hoy
            if ($user->getCreationDate() == null) {
                $user->setCreationDate($today);
            }

            $flagWhile = true;
            $startPeriod = new \DateTime($user->getCreationDate()->format('Y-m-d'));
            do {
                $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                if ($today < $endPeriod) {
                    $flagWhile = false;
                } else {
                    $startPeriod = $endPeriod;
                }
            } while ($flagWhile);

            if ($startPeriod && $endPeriod) {
                $membership = new Membership();
                $membership->setUser($user);
                $membership->setCurrentAppointments(0);
                $membership->setEndPeriod($endPeriod);
                $membership->setStartPeriod($startPeriod);
                $membership->setPayMembership(false);
                $membership->setAppointmentsLimit($freeAppointments);
                $this->em->persist($membership);
                $this->em->flush();
            }
        }
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return bool
     */
    public function updateUser(Request $request, $hashAlgorithm)
    {
        /*
         *the commented responses  like "array_push" wasnt erased, can serve for future API test
         * */
        $placeArray = $request->request->get(Constant::PLACE);
        $epsArray = $request->request->get(Constant::EPS);
        //print_r($placeArray[0]['placeName']);

        //$enablePersonalTitle = true;
        //$flag = $request->request->get(Constant::PERSONAL_TITLE);
        //if ($flag = null) {
        //    $enablePersonalTitle = 3;
        //}

        //Json Information
        $data_array = array(

            //Unique user identifier
            Constant::USER_ID => $request->request->get(Constant::USER_ID),
            //doctor's Unique user identifier
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            //Different Entities
            Constant::USER_TYPE => $request->request->get(Constant::USER_TYPE),
            Constant::DOCUMENT_TYPE => $request->request->get(Constant::DOCUMENT_TYPE),
            Constant::GENDER => $request->request->get(Constant::GENDER),
            Constant::GENDER_ID => $request->request->get(Constant::GENDER_ID),
            Constant::SPECIALITY => $request->request->get(Constant::SPECIALITY),
            Constant::RELATION_SHIP => $request->request->get(Constant::RELATION_SHIP),

            //Place  array information
            Constant::PLACE_ARRAY => $placeArray,

            //Place Individual
            Constant::ADDRESS => $request->request->get(Constant::ADDRESS),
            Constant::PHONE => $request->request->get(Constant::PHONE),
            //Constant::PLACE_NAME => $request->request->get(Constant::PLACE_NAME),

            //Eps
            Constant::EPS_ARRAY => $epsArray,
            Constant::EPS_ID => $request->request->get(Constant::EPS_ID),

            //Information ExtraDataUser Entity
            Constant::BIRTH_DATE => $request->request->get(Constant::BIRTH_DATE),
            Constant::IDENTITY => $request->request->get(Constant::IDENTITY),
            Constant::EMERGENCY_NAME => $request->request->get(Constant::EMERGENCY_NAME),
            Constant::EMERGENCY_NUMBER => $request->request->get(Constant::EMERGENCY_NUMBER),
            Constant::URGENT => $request->request->get(Constant::URGENT),
            Constant::PASSWORD => $request->request->get(Constant::PASSWORD),
            Constant::ARRIVAL_TIME => $request->request->get(Constant::ARRIVAL_TIME),
            Constant::DEPART_TIME => $request->request->get(Constant::DEPART_TIME),
            Constant::COMMENT_EXTRA_DATA_USER => $request->request->get(Constant::COMMENT_EXTRA_DATA_USER),
            Constant::PERSONAL_TITLE => $request->request->get(Constant::PERSONAL_TITLE),

            //Information GeneralDataUser Entity
            Constant::NAME => $request->request->get(Constant::NAME),
            Constant::LAST_NAME => $request->request->get(Constant::LAST_NAME),
            Constant::EMAIL => $request->request->get(Constant::EMAIL),
            Constant::CELL_PHONE => $request->request->get(Constant::CELL_PHONE),
            Constant::HOME_PHONE => $request->request->get(Constant::HOME_PHONE),
            Constant::PHOTO => $request->request->get(Constant::PHOTO),

            //Identity of user type doctor(01), assistant(02), patient(03)
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::IDENTITY_USER_TYPE_PATIENT => $request->request->get(Constant::IDENTITY_USER_TYPE_PATIENT)

        );
        $jsonResponse = array();
        $negativeResponse = null;

        //Se busca el usuario por el userId
        $user = $this->repositoryUser->find($data_array[Constant::USER_ID]);

        if ($user) {

            //Si el usuario a actualizar es medico
            if ($user->getUserTypeId()->getId() == 1 && $data_array[Constant::USER_ID] == $data_array[Constant::DOCTOR_ID]) {

                $allGdu = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

                $validator = true;

                foreach ($allGdu as $gdu) {
                    if ($gdu->getUser()->getUserTypeId()->getId() == 1 && $gdu->getUser()->getId() != $user->getId()) {
                        if ($gdu->getEmail() == $data_array[Constant::EMAIL])
                            $validator = false;
                    }
                }

                if ($validator) {

                    $user->setSyncDate(new \DateTime('now'));

                    $generalDataUser = $user->getGeneralDataUserId();

                    //GENERAL DATA UPDATES
                    $generalDataUser->setName($data_array[Constant::NAME]);
                    $generalDataUser->setLastName($data_array[Constant::LAST_NAME]);
                    $generalDataUser->setCellPhone($data_array[Constant::CELL_PHONE]);
                    $generalDataUser->setHomePhone($data_array[Constant::HOME_PHONE]);
                    $generalDataUser->setEmail($data_array[Constant::EMAIL]);

                    $extraDataUser = $user->getExtraDataUserId();
                    //EXTRA DATA UPDATES
                    if ($data_array[Constant::BIRTH_DATE] != null) {
                        $extraDataUser->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                    }

                    $extraDataUser->setIdentity($data_array[Constant::IDENTITY]);

                    //TODO: cifrar password
                    if ($data_array[Constant::PASSWORD]) {
                        $hashPassword = hash($hashAlgorithm, $data_array[Constant::PASSWORD]);
                        $extraDataUser->setPassword($hashPassword);
                    }
                    $extraDataUser->setArrivalTime(new \DateTime($data_array[Constant::ARRIVAL_TIME]));
                    $extraDataUser->setDepartTime(new \DateTime($data_array[Constant::DEPART_TIME]));
                    $extraDataUser->setPhoto($data_array[Constant::PHOTO]);
                    $extraDataUser->setPersonalTitle($data_array[Constant::PERSONAL_TITLE]);
                    //DOCUMENT UPDATE
                    if ($data_array[Constant::DOCUMENT_TYPE]) {
                        $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => $data_array[Constant::DOCUMENT_TYPE]));
                        if ($documentTypeData)
                            $extraDataUser->setDocumentTypeId($documentTypeData);
                    }
                    //SPECIALITY UPDATE
                    if ($data_array[Constant::SPECIALITY]) {
                        $specialityData = $this->repositorySpeciality->findOneBy(array(Constant::SPECIALITY_DESCRIPTION => $data_array[Constant::SPECIALITY]));
                        if ($specialityData)
                            $extraDataUser->setSpecialityId($specialityData);
                    }

                    $this->em->flush();

                    //Place UPDATES
                    if ($request->request->get(Constant::PLACE) || sizeof($data_array[Constant::PLACE_ARRAY]) == 0) {

                        foreach ($data_array[Constant::PLACE_ARRAY] as $place) {

                            $userPlaceConnection = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $place[Constant::GENERAL_ID_COLUMN]));
                            if ($userPlaceConnection) {
                                $placeUpdate = $this->repositoryPlace->findOneBy(array(Constant::GENERAL_ID_COLUMN => $place[Constant::GENERAL_ID_COLUMN]));
                                $placeUpdate->setAddress($place[Constant::ADDRESS]);
                                $placeUpdate->setPhone($place[Constant::PHONE]);
                                $this->em->persist($placeUpdate);
                                $this->em->flush();
                            } else {
                                $places = new Place();
                                $places->setAddress($place[Constant::ADDRESS]);
                                $places->setPhone($place[Constant::PHONE]);
                                $this->em->persist($places);
                                $this->em->flush();
                                $places->getId();

                                $userPlaceConnectionEntity = new UserPlaceConnection();
                                $userPlaceConnectionEntity->setUserId($user);
                                $userPlaceConnectionEntity->setPlaceId($places);
                                $this->em->persist($userPlaceConnectionEntity);
                                $this->em->flush();
                            }
                        }
                    } else {
                        //Eliminar todas las placeConnection
                        $allPlaceConnection = $this->repositoryUserPlaceConnection->findBy(array('userId' => $user));

                        if (sizeof($allPlaceConnection) > 0) {
                            foreach ($allPlaceConnection as $placeConection) {
                                $this->em->remove($placeConection);

                            }
                            $this->em->flush();
                        }
                    }

                    //EPS updates
                    if ($request->request->get(Constant::EPS)) {
                        //Busco todas las conexiones de ese usuario
                        $allEpsConnections = $this->repositoryUserEpsConnection->findBy(array('extraDataUserId' => $extraDataUser->getId()));
                        //Ciclo para actualizar o agregar
                        foreach ($data_array[Constant::EPS_ARRAY] as $eps) {
                            $updateFlag = false;
                            foreach ($allEpsConnections as $actualEpsConnections) {

                                //Busco lo que llega con lo que hay
                                //Si alguna que llega es igual a la que hay la actualizo
                                if (!empty($eps['userEpsConnection'])) {
                                    if ($actualEpsConnections->getId() == $eps['userEpsConnection']) {
                                        //Buscar la EPS por el ID y por el nombre
                                        $epsFinder = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $eps[Constant::EPS_ID], Constant::GENERAL_DESCRIPTION_COLUMN => $eps[Constant::EPS_NAME]));
                                        if ($epsFinder) {
                                            $actualEpsConnections->setEpsId($epsFinder);
                                            $this->em->persist($actualEpsConnections);
                                            $this->em->flush();
                                        }
                                        $updateFlag = true;
                                    }
                                } else {
                                    if ($actualEpsConnections->getEpsId()->getId() == $eps[Constant::EPS_ID]) {
                                        //Buscar la EPS por el ID y por el nombre
                                        $epsFinder = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $eps[Constant::EPS_ID], Constant::GENERAL_DESCRIPTION_COLUMN => $eps[Constant::EPS_NAME]));
                                        if ($epsFinder) {
                                            $actualEpsConnections->setEpsId($epsFinder);
                                            $this->em->persist($actualEpsConnections);
                                            $this->em->flush();
                                        }
                                        $updateFlag = true;
                                    }
                                }
                            }

                            //Si no la encontro se agrega
                            if (!$updateFlag) {
                                //Crea las nuevas conexiones
                                $userEpsConnectionNew = new UserEpsConnection();
                                //Busca la eps por ID
                                $eps_data = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $eps[Constant::EPS_ID], Constant::GENERAL_DESCRIPTION_COLUMN => $eps[Constant::EPS_NAME]));
                                if (count($eps_data) > 0) {
                                    $userEpsConnectionNew->setEpsId($eps_data);
                                    $userEpsConnectionNew->setExtraDataUserId($extraDataUser);
                                    $this->em->persist($userEpsConnectionNew);
                                    $this->em->flush();
                                } else {
                                    /*array_push($jsonResponse, array(
                                        Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION,
                                        Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL
                                    ));*/
                                }
                            }
                        }

                        //Ciclo para eliminar
                        foreach ($allEpsConnections as $actualEpsConnections) {
                            $updateFlag = false;
                            foreach ($data_array[Constant::EPS_ARRAY] as $eps) {
                                //Busco lo que llega con lo que hay
                                //Si alguna que llega es igual a la que hay la actualizo
                                if (!empty($eps['userEpsConnection'])) {
                                    if ($actualEpsConnections->getId() == $eps['userEpsConnection']) {
                                        $updateFlag = true;
                                    }
                                } else {
                                    if ($actualEpsConnections->getEpsId()->getId() == $eps[Constant::EPS_ID]) {
                                        $updateFlag = true;
                                    }
                                }
                            }
                            //Si no la encontro se eliminar
                            if (!$updateFlag) {
                                $this->em->remove($actualEpsConnections);
                                $this->em->flush();
                            }
                        }
                    } else {
                        $allEpsConnections = $this->repositoryUserEpsConnection->findBy(array('extraDataUserId' => $extraDataUser->getId()));
                        foreach ($allEpsConnections as $actualEpsConnections) {
                            $this->em->remove($actualEpsConnections);
                        }
                        $this->em->flush();
                    }
                    return true;
                } else {
                    return false;
                }
            } //Si el usuario que se va a actualizar es paciente
            elseif ($user->getUserTypeId()->getId() == 3) {
                //Se busca la conexion de el medico con los pacientes
                $usersConnectionOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $data_array[Constant::DOCTOR_ID]));

                if (count($usersConnectionOneThree) > 0) {

                    $restricter = array();
                    //Se recorren las conexiones
                    foreach ($usersConnectionOneThree as $usersConnection) {
                        //$patientRelated = $usersConnection->getUserThreeId();
                        //Se obtiene la entidad user para el paciente
                        $patientUser = $this->repositoryUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $usersConnection->getUserThreeId()));
                        //Se obtiene la entidad extraDataUser para el paciente
                        $extraDataPatient = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $patientUser->getExtraDataUserId()));
                        if ($patientUser->getId() != $data_array[Constant::USER_ID])
                            //Se agregan los ids al arreglo
                            $restricter[] = $extraDataPatient->getId();
                    }

                    $conn = 0;

                    if ($data_array[Constant::IDENTITY] && $data_array[Constant::DOCUMENT_TYPE]) {
                        //Se recorre el arreglo de id's de pacientes
                        foreach ($restricter as $res) {
                            //Entidad extraDataUser de los pacientes
                            $comparer = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $res));

                            //Aca se comparan las cedulas que hay con las que llegan
                            if ($comparer->getIdentity() == $data_array[Constant::IDENTITY] && $comparer->getDocumentTypeId()->getDescription() == $data_array[Constant::DOCUMENT_TYPE]) {
                                $conn++;
                            }
                        }
                    }

                    //TODO: ajustar esta logica
                    if ($conn > 0) {
                        array_push($jsonResponse, array(
                            'message' => 'ya existe',
                            'conn ' => $conn
                        ));
                    } else {

                        $genderData = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));
                        if (count($genderData) != 0) {

                            $user->setSyncDate(new \DateTime('now'));

                            $generalDataUser = $this->repositoryGeneralDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $user->getGeneralDataUserId()));
                            //GeneralDataUser Entity

                            if ($data_array[Constant::EMAIL] == null) {
                                $generalDataUser->setEmail('mail@mail.com');
                            } else {
                                $generalDataUser->setEmail($data_array[Constant::EMAIL]);
                            }

                            $generalDataUser->setName($data_array[Constant::NAME]);
                            $generalDataUser->setLastName($data_array[Constant::LAST_NAME]);
                            $generalDataUser->setCellPhone($data_array[Constant::CELL_PHONE]);
                            $generalDataUser->setHomePhone($data_array[Constant::HOME_PHONE]);
                            $this->em->persist($generalDataUser);
                            $extraDataUser = $this->repositoryExtraDataUser->findOneBy(array(Constant::GENERAL_ID_COLUMN => $user->getExtraDataUserId()));
                            //ExtraDataUser Entity
                            if ($data_array[Constant::BIRTH_DATE] != null) {
                                $extraDataUser->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                            }

                            $extraDataUser->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                            $extraDataUser->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                            $extraDataUser->setUrgent($data_array[Constant::URGENT]);
                            $extraDataUser->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);

                            if ($data_array[Constant::DOCUMENT_TYPE]) {
                                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => $data_array[Constant::DOCUMENT_TYPE]));
                            } else {
                                $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => Constant::DEFAULT_IDENTITY));
                            }

                            if ($documentTypeData) {
                                $extraDataUser->setDocumentTypeId($documentTypeData);
                                if ($data_array[Constant::IDENTITY]) {
                                    $extraDataUser->setIdentity($data_array[Constant::IDENTITY]);
                                } else {
                                    $extraDataUser->setIdentity(null);
                                }
                            } else
                                return false;

                            $extraDataUser->setGenderId($genderData);
                            //$extraDataUser->setEnablePersonalTitle($data_array[Constant::ENABLE_PERSONAL_TITLE]);
                            $this->em->persist($extraDataUser);

                            $eps = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));

                            if (count($eps) > 0) {
                                $userEpsConnection = $this->repositoryUserEpsConnection->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUser->getId()));

                                if (count($usersConnection) > 0) {
                                    $userEpsConnection->setEpsId($eps);
                                    $this->em->persist($userEpsConnection);

                                    $this->em->flush();
                                } else {
                                    $useEpsConn = new UserEpsConnection();
                                    $useEpsConn->setEpsId($eps);
                                    $useEpsConn->setExtraDataUserId($extraDataUser);
                                    $this->em->persist($useEpsConn);
                                    //$this->em->flush();
                                }
                            }
                            $this->em->flush();
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        } else {
            return false;
        }
    }

    /**
     * @param $assistantObject
     * @return object
     */
    public function updateAssistantSync($assistantObject)
    {
        try {
            $data_array = array(
                Constant::_ID => $assistantObject[Constant::_ID],
                Constant::_FIRST_NAME => $assistantObject[Constant::_FIRST_NAME],
                Constant::_LAST_NAME => $assistantObject[Constant::_LAST_NAME],
                Constant::_EMAIL => $assistantObject[Constant::_EMAIL],
                Constant::_ACTIVE => $assistantObject[Constant::_ACTIVE]
            );

            //Se busca el asistente por el userId
            $userEntityAssistant = $this->repositoryUser->find($data_array[Constant::_ID]);

            if ($userEntityAssistant) {
                $gduAssistant = $userEntityAssistant->getGeneralDataUserId();
                $eduAssistant = $userEntityAssistant->getExtraDataUserId();

                if ($data_array[Constant::_ACTIVE] == false) {
                    $this->em->remove($gduAssistant);
                    $this->em->remove($eduAssistant);
                    $this->em->remove($userEntityAssistant);
                    $this->em->flush();
                } else {
                    //validamos que el asistente ya no exista
                    //Obtenemos todos los asistentes
                    $allucoTwo = $this->repositoryUserConnectionOneTwo->findAll();

                    if (count($allucoTwo) > 0) {
                        $validatorAssistant = true;
                        foreach ($allucoTwo as $ucoTwoEntity) {
                            if ($ucoTwoEntity->getUserTwoId()->getId() != $data_array[Constant::_ID])
                                if ($ucoTwoEntity->getUserTwoId()->getGeneralDataUserId()->getEmail() == $data_array[Constant::_EMAIL]) {
                                    $validatorAssistant = false;
                                    break;
                                }
                        }
                    }

                    if ($validatorAssistant) {
                        $userEntityAssistant->setSyncDate(new \DateTime('now'));
                        $gduAssistant->setName($data_array[Constant::_FIRST_NAME]);
                        $gduAssistant->setLastName($data_array[Constant::_LAST_NAME]);
                        $gduAssistant->setEmail($data_array[Constant::_EMAIL]);
                        $this->em->flush();
                    } else {
                        return $this->buildErrorObject(14);
                    }
                }
            } else {
                return $this->buildErrorObject(1);
            }
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * Actualizar medico
     * @param Request $request
     * @param $hashAlgorithm
     * @return object
     */
    public function updateDoctorV1(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::_ID => $request->request->get(Constant::_ID),
            Constant::_USER_TYPE_ID => $request->request->get(Constant::_USER_TYPE_ID),
            Constant::_OWNER_USER_ID => $request->request->get(Constant::_OWNER_USER_ID),
            Constant::_FIRST_NAME => $request->request->get(Constant::_FIRST_NAME),
            Constant::_LAST_NAME => $request->request->get(Constant::_LAST_NAME),
            Constant::_EMAIL => $request->request->get(Constant::_EMAIL),
            Constant::_PASSWORD => $request->request->get(Constant::_PASSWORD),
            Constant::_TEMPORARY_PASSWORD => $request->request->get(Constant::_TEMPORARY_PASSWORD),
            Constant::_PERSONAL_TITLE_ID => $request->request->get(Constant::_PERSONAL_TITLE_ID),
            Constant::_ARRIVAL_TIME => $request->request->get(Constant::_ARRIVAL_TIME),
            Constant::_DEPART_TIME => $request->request->get(Constant::_DEPART_TIME),
            Constant::_SPECIALIZATION_ID => $request->request->get(Constant::_SPECIALIZATION_ID),
            Constant::_TOKEN => $request->request->get(Constant::_TOKEN),
            Constant::_FULL_NAME => $request->request->get(Constant::_FULL_NAME),
            Constant::_PLACES => $request->request->get(Constant::_PLACES),
            Constant::_EPS => $request->request->get(Constant::_EPS)
        );

        if (isset($data_array[Constant::_ID]) && isset($data_array[Constant::_USER_TYPE_ID]) &&
            isset($data_array[Constant::_OWNER_USER_ID]) && isset($data_array[Constant::_FIRST_NAME]) &&
            isset($data_array[Constant::_LAST_NAME]) && isset($data_array[Constant::_EMAIL]) &&
            isset($data_array[Constant::_TEMPORARY_PASSWORD]) &&
            isset($data_array[Constant::_PERSONAL_TITLE_ID]) && isset($data_array[Constant::_ARRIVAL_TIME]) &&
            isset($data_array[Constant::_DEPART_TIME]) && isset($data_array[Constant::_FULL_NAME]) &&
            isset($data_array[Constant::_PLACES]) && isset($data_array[Constant::_EPS])
        ) {

            //Se busca el usuario por el userId
            $user = $this->repositoryUser->find($data_array[Constant::_ID]);

            if ($user && $user->getUserTypeId()->getId() == 1) {

                $allGdu = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::_EMAIL]));

                $validator = true;

                //Validamos que el email no exista para un medico
                foreach ($allGdu as $gdu) {
                    if ($gdu->getUser()->getUserTypeId()->getId() == 1 && $gdu->getUser()->getId() != $user->getId()) {
                        if ($gdu->getEmail() == $data_array[Constant::_EMAIL])
                            $validator = false;
                    }
                }

                if ($validator) {

                    //Iniciamos la transaccion
                    $this->em->getConnection()->beginTransaction();

                    $user->setSyncDate(new \DateTime('now'));

                    $generalDataUser = $user->getGeneralDataUserId();

                    //GENERAL DATA UPDATES
                    if ($data_array[Constant::_FIRST_NAME] != '')
                        $generalDataUser->setName($data_array[Constant::_FIRST_NAME]);
                    if ($data_array[Constant::_LAST_NAME] != '')
                        $generalDataUser->setLastName($data_array[Constant::_LAST_NAME]);
                    $generalDataUser->setEmail($data_array[Constant::_EMAIL]);

                    $extraDataUser = $user->getExtraDataUserId();
                    //TODO: cifrar password
                    if ($data_array[Constant::_PASSWORD]) {
                        $hashPassword = hash($hashAlgorithm, $data_array[Constant::_PASSWORD]);
                        $extraDataUser->setPassword($hashPassword);
                    }
                    $extraDataUser->setArrivalTime(new \DateTime($data_array[Constant::_ARRIVAL_TIME]));
                    $extraDataUser->setDepartTime(new \DateTime($data_array[Constant::_DEPART_TIME]));
                    $extraDataUser->setPersonalTitle($data_array[Constant::_PERSONAL_TITLE_ID]);

                    //SPECIALITY UPDATE
                    if ($data_array[Constant::_SPECIALIZATION_ID]) {
                        $specialityData = $this->repositorySpeciality->find($data_array[Constant::_SPECIALIZATION_ID]);
                        if ($specialityData)
                            $extraDataUser->setSpecialityId($specialityData);
                    }

                    $this->em->flush();

                    //Place UPDATES
                    $arrayNewPlacesResponse = array();
                    $placeService = new PlaceService($this->em);
                    if (count($data_array[Constant::_PLACES]) > 0) {
                        //Ciclo para eliminar
                        $allPlaceConnection = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_ID => $user));
                        foreach ($allPlaceConnection as $actualPlaceConnection) {
                            $found = false;
                            foreach ($data_array[Constant::_PLACES] as $place) {
                                if ($place[Constant::_ID] != 0)
                                    if ($actualPlaceConnection->getPlaceId()->getId() == $place[Constant::_ID]) {
                                        $found = true;
                                    }
                            }

                            //elimino la actual
                            if (!$found) {
                                try {
                                    $this->em->remove($actualPlaceConnection->getPlaceId());
                                    $this->em->remove($actualPlaceConnection);
                                    $this->em->flush();
                                } catch (ForeignKeyConstraintViolationException $e) {
                                    error_log('Error en catch updateDoctorV1');
                                    error_log('Error: ' . $e->getMessage());
                                    //Hacemos rollback
                                    $this->em->getConnection()->rollBack();
                                    return $this->buildErrorObject(19);
                                }
                            }
                        }

                        //Ciclo para actualizar
                        foreach ($data_array[Constant::_PLACES] as $place) {
                            //Place nuevo
                            if ($place[Constant::_ID] == 0) {
                                $places = new Place();
                                $places->setAddress($place[Constant::_ADDRESS]);
                                $places->setPhone($place[Constant::_PHONE]);
                                $this->em->persist($places);
                                $this->em->flush();
                                $places->getId();

                                $userPlaceConnectionEntity = new UserPlaceConnection();
                                $userPlaceConnectionEntity->setUserId($user);
                                $userPlaceConnectionEntity->setPlaceId($places);
                                $this->em->persist($userPlaceConnectionEntity);
                                $this->em->flush();
                                array_push($arrayNewPlacesResponse, $placeService->buildPlaceObject($places, $place[Constant::_LOCAL_ID],
                                    $user->getId()));
                            } else {
                                if ($place[Constant::_ID] > 0) {
                                    $userPlaceConnection = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::PLACE_ID => $place[Constant::_ID]));
                                    if ($userPlaceConnection) {
                                        $placeUpdate = $userPlaceConnection->getPlaceId();
                                        $placeUpdate->setAddress($place[Constant::_ADDRESS]);
                                        $placeUpdate->setPhone($place[Constant::_PHONE]);
                                        $this->em->persist($placeUpdate);
                                        $this->em->flush();
                                    }
                                }
                            }
                        }

                        //Una vez actualizados reuno toda la lista de places
                        $tempAllPlaces = $placeService->getAllPlacesByDoctor($user->getId());

                        foreach ($tempAllPlaces as $placeObject) {
                            $found = false;
                            foreach ($arrayNewPlacesResponse as $newPlaceObject) {
                                if ($placeObject->Id == $newPlaceObject->Id) {
                                    $found = true;
                                }
                            }
                            if (!$found) {
                                array_push($arrayNewPlacesResponse, $placeObject);
                            }
                        }
                    } else {
                        //Eliminar todas las placeConnection
                        $allPlaceConnection = $this->repositoryUserPlaceConnection->findBy(array(Constant::USER_ID => $user));

                        if (sizeof($allPlaceConnection) > 0) {
                            try {
                                foreach ($allPlaceConnection as $placeConnection) {
                                    $this->em->remove($placeConnection->getPlaceId());
                                    $this->em->remove($placeConnection);
                                }
                                $this->em->flush();
                            } catch (ForeignKeyConstraintViolationException $e) {
                                error_log('Error en catch updateDoctorV1 - 2');
                                error_log('Error: ' . $e->getMessage());
                                //Hacemos rollback
                                $this->em->getConnection()->rollBack();
                                return $this->buildErrorObject(19);
                            }
                        }
                    }

                    //EPS updates
                    $arrayEpsResponse = array();
                    $epsService = new EpsService($this->em);
                    if (count($data_array[Constant::_EPS]) > 0) {
                        //Busco todas las conexiones de ese usuario
                        $allEpsConnections = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUser->getId()));
                        //Ciclo para actualizar o agregar
                        foreach ($data_array[Constant::_EPS] as $eps) {

                            $updateFlag = false;

                            foreach ($allEpsConnections as $actualEpsConnections) {
                                if ($actualEpsConnections->getEpsId()->getId() == $eps[Constant::_ID]) {
                                    $updateFlag = true;
                                }
                            }

                            //Si no la encontro se agrega
                            if (!$updateFlag) {
                                //Crea las nuevas conexiones
                                $userEpsConnectionNew = new UserEpsConnection();
                                //Busca la eps por ID
                                $eps_data = $this->repositoryEps->find($eps[Constant::_ID]);
                                if ($eps_data) {
                                    $userEpsConnectionNew->setEpsId($eps_data);
                                    $userEpsConnectionNew->setExtraDataUserId($extraDataUser);
                                    $this->em->persist($userEpsConnectionNew);
                                    $this->em->flush();
                                } else {
                                    //TODO: error
                                }
                            }
                        }

                        //Ciclo para eliminar
                        foreach ($allEpsConnections as $actualEpsConnections) {
                            $updateFlag = false;
                            foreach ($data_array[Constant::_EPS] as $eps) {
                                //Busco lo que llega con lo que hay
                                //Si alguna que llega es igual a la que hay la actualizo
                                if ($actualEpsConnections->getEpsId()->getId() == $eps[Constant::_ID]) {
                                    $updateFlag = true;
                                }
                            }
                            //Si no la encontro se elimina
                            if (!$updateFlag) {
                                $this->em->remove($actualEpsConnections);
                                $this->em->flush();
                            }
                        }

                        $arrayEpsResponse = $epsService->getAllEpsByDoctor($user->getId());

                    } else {
                        $allEpsConnections = $this->repositoryUserEpsConnection->findBy(array(Constant::EXTRA_DATA_USER_ID => $extraDataUser->getId()));
                        foreach ($allEpsConnections as $actualEpsConnections) {
                            $this->em->remove($actualEpsConnections);
                        }
                        $this->em->flush();

                        //Agrego la eps 16
                        $epsEntity = $this->repositoryEps->findOneBy(array(Constant::GENERAL_ID_COLUMN => Constant::DEFAULT_EPS));
                        $data_eps = new UserEpsConnection();
                        if ($epsEntity) {
                            $data_eps->setEpsId($epsEntity);
                            $data_eps->setExtraDataUserId($extraDataUser);
                            $this->em->persist($data_eps);
                            $this->em->flush();
                            array_push($arrayEpsResponse, $epsService->buildHealthPromoterObject($epsEntity));
                        }
                    }

                    //Cerramos la transaccion
                    if ($this->em->getConnection()->isTransactionActive())
                        $this->em->getConnection()->commit();

                    return $this->buildSingleDataObject(true, $generalDataUser, $extraDataUser, $arrayNewPlacesResponse,
                        $arrayEpsResponse, null, null);
                } else {
                    return $this->buildErrorObject(13);
                }
            } else {
                return $this->buildErrorObject(1);
            }
        } else {
            return $this->buildErrorObject(2);
        }
    }

    /**
     * @param $patientObject
     * @param $doctorId
     * @param $emailAssistant
     * @param $localId
     * @return object
     */
    public function updatePatientSync($patientObject, $doctorId, $emailAssistant, $localId)
    {
        if (isset($patientObject)) {
            $data_array = array(

                //Different Entities
                Constant::USER_TYPE => Constant::USER_TYPE_PATIENT,
                Constant::DOCUMENT_TYPE => $patientObject[Constant::_DOCUMENT_TYPE_ID],
                Constant::GENDER => null,
                Constant::GENDER_ID => $patientObject[Constant::_GENDER_ID],
                Constant::SPECIALITY => null,
                Constant::RELATION_SHIP => null,

                //Place  array information
                Constant::PLACE_ARRAY => null,

                //Place Individual
                Constant::ADDRESS => null,
                Constant::PHONE => null,

                //Eps
                Constant::EPS_ARRAY => null,
                Constant::EPS_ID => null,

                //Information ExtraDataUser Entity
                Constant::BIRTH_DATE => $patientObject[Constant::_BIRTH_DATE],
                Constant::IDENTITY => $patientObject[Constant::_DOCUMENT_ID],
                Constant::EMERGENCY_NAME => $patientObject[Constant::_EMERGENCY_NAME],
                Constant::EMERGENCY_NUMBER => $patientObject[Constant::_EMERGENCY_NUMBER],
                Constant::URGENT => null,
                Constant::PASSWORD => null,
                Constant::ARRIVAL_TIME => null,
                Constant::DEPART_TIME => null,
                Constant::COMMENT_EXTRA_DATA_USER => $patientObject[Constant::_COMMENTS],
                Constant::PERSONAL_TITLE => null,

                //Information GeneralDataUser Entity
                Constant::NAME => $patientObject[Constant::_FIRST_NAME],
                Constant::LAST_NAME => $patientObject[Constant::_LAST_NAME],
                Constant::EMAIL => $patientObject[Constant::_EMAIL],
                Constant::CELL_PHONE => $patientObject[Constant::_CELLPHONE],
                Constant::HOME_PHONE => $patientObject[Constant::_HOMEPHONE],
                Constant::PHOTO => null,

                //Identity of user type doctor(01), assistant(02), patient(03)
                Constant::DOCTOR_ID => $doctorId,
                Constant::IDENTITY_USER_TYPE_DOCTOR => null,
                Constant::EMAIL_USER_TYPE_ASSISTANT => $emailAssistant,
                Constant::IDENTITY_USER_TYPE_PATIENT => null,
                Constant::USER_ID => $patientObject[Constant::_ID]
            );
        } else {
            return $this->buildErrorObject(2);
        }

        //Se busca el usuario por el userId
        $user = $this->repositoryUser->find($data_array[Constant::USER_ID]);

        if ($user) {

            if ($user->getUserTypeId()->getId() == 3) {

                $genderData = $this->repositoryGender->findOneBy(array(Constant::GENERAL_ID_COLUMN => $data_array[Constant::GENDER_ID]));
                if ($genderData) {

                    $user->setSyncDate(new \DateTime('now'));

                    $generalDataUser = $user->getGeneralDataUserId();
                    //GeneralDataUser Entity

                    if ($data_array[Constant::EMAIL] == null) {
                        $generalDataUser->setEmail('mail@mail.com');
                    } else {
                        $generalDataUser->setEmail($data_array[Constant::EMAIL]);
                    }

                    $generalDataUser->setName($data_array[Constant::NAME]);
                    $generalDataUser->setLastName($data_array[Constant::LAST_NAME]);
                    $generalDataUser->setCellPhone($data_array[Constant::CELL_PHONE]);
                    $generalDataUser->setHomePhone($data_array[Constant::HOME_PHONE]);
                    $this->em->persist($generalDataUser);
                    $extraDataUser = $user->getExtraDataUserId();
                    //ExtraDataUser Entity
                    if ($data_array[Constant::BIRTH_DATE] != null) {
                        $extraDataUser->setBirthDate(new \DateTime($data_array[Constant::BIRTH_DATE]));
                    }

                    $extraDataUser->setEmergencyName($data_array[Constant::EMERGENCY_NAME]);
                    $extraDataUser->setEmergencyNumber($data_array[Constant::EMERGENCY_NUMBER]);
                    $extraDataUser->setUrgent($data_array[Constant::URGENT]);
                    $extraDataUser->setComment($data_array[Constant::COMMENT_EXTRA_DATA_USER]);

                    if ($data_array[Constant::DOCUMENT_TYPE]) {
                        $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::ID => $data_array[Constant::DOCUMENT_TYPE]));
                    } else {
                        $documentTypeData = $this->repositoryDocumentType->findOneBy(array(Constant::DOCUMENT_TYPE_DESCRIPTION => Constant::DEFAULT_IDENTITY));
                    }

                    if ($documentTypeData) {
                        $extraDataUser->setDocumentTypeId($documentTypeData);
                        if ($data_array[Constant::IDENTITY]) {
                            $extraDataUser->setIdentity($data_array[Constant::IDENTITY]);
                        } else {
                            $extraDataUser->setIdentity(null);
                        }
                    }

                    $extraDataUser->setGenderId($genderData);

                    $this->em->persist($extraDataUser);
                    $this->em->persist($user);
                    $this->em->flush();

                    $patientService = new PatientService($this->em);

                    return $patientService->buildPatientObject($doctorId, $user, $generalDataUser, $extraDataUser,
                        0);

                } else {
                    return $this->buildErrorObject(16);
                }
            } else {
                return $this->buildErrorObject(1);
            }
        } else {
            return $this->buildErrorObject(1);
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function userDisable(Request $request)
    {
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::DOCTOR_ID]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {

                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {

                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {

                                    $assistantGeneralData = $gduAssistant;
                                    $assistantExtraData = $userAssistant->getExtraDataUserId();
                                    $connectionData = $ucot;

                                    $this->em->remove($connectionData);//conn
                                    $this->em->remove($assistantGeneralData);//generldata
                                    $this->em->remove($assistantExtraData);//extradata
                                    $this->em->remove($userAssistant);//user
                                    //$this->em->remove($userLoginAssistant);//login
                                    $this->em->flush();
                                    return true;
                                }
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return bool
     */
    public function userRecoveryPassword(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::EMAIL),
            Constant::NEW_PASSWORD => $request->request->get(Constant::NEW_PASSWORD)
        );
        if (isset($data_array[Constant::EMAIL]) && isset($data_array[Constant::NEW_PASSWORD])) {

            $validateUser = false;

            //Se buscan los usuarios por emails y se validan si son medicos o asistentes
            $allgduUsers = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            if (sizeof($allgduUsers) > 0) {

                foreach ($allgduUsers as $gduUser) {
                    if ($gduUser->getUser()->getUserTypeId()->getId() == 1 || $gduUser->getUser()->getUserTypeId()->getId() == 2) {
                        $validateUser = true;
                        $user = $gduUser->getUser();
                        break;
                    }
                }

                if ($validateUser) {
                    $extraData = $user->getExtraDataUserId();
                    $user->setTemporary(0);
                    $user->setSyncDate(new \DateTime('now'));
                    //TODO: cifrar password
                    if (strlen($data_array[Constant::NEW_PASSWORD])) {
                        $hashPassword = hash($hashAlgorithm, $data_array[Constant::NEW_PASSWORD]);
                        $extraData->setPassword($hashPassword);
                        $this->em->flush();
                        return true;
                    }
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return object
     */
    public function userRecoveryPasswordV1(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::_EMAIL),
            Constant::PASSWORD => $request->request->get(Constant::_PASSWORD),
            Constant::NEW_PASSWORD => $request->request->get(Constant::_NEW_PASSWORD)
        );
        if (isset($data_array[Constant::EMAIL]) && isset($data_array[Constant::PASSWORD]) &&
            isset($data_array[Constant::NEW_PASSWORD])
        ) {

            $validateUser = false;

            //Se buscan los usuarios por emails y se validan si son medicos o asistentes
            $allgduUsers = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            if (sizeof($allgduUsers) > 0) {

                foreach ($allgduUsers as $gduUser) {
                    if ($gduUser->getUser()->getUserTypeId()->getId() == 1 || $gduUser->getUser()->getUserTypeId()->getId() == 2) {
                        $validateUser = true;
                        $user = $gduUser->getUser();
                        break;
                    }
                }

                if ($validateUser) {
                    $actualHashPassword = hash($hashAlgorithm, $data_array[Constant::PASSWORD]);
                    $extraData = $user->getExtraDataUserId();
                    if ($extraData->getPassword() == $actualHashPassword) {
                        $user->setTemporary(0);
                        $user->setSyncDate(new \DateTime('now'));
                        //TODO: cifrar password
                        if (strlen($data_array[Constant::NEW_PASSWORD])) {
                            $hashPassword = hash($hashAlgorithm, $data_array[Constant::NEW_PASSWORD]);
                            $extraData->setPassword($hashPassword);
                            $this->em->flush();
                            return (object)array(
                                Constant::_SINGLE_DATA => true
                            );
                        }
                    } else {
                        return $this->buildErrorObject(10);
                    }
                } else {
                    return $this->buildErrorObject(1);
                }
            } else {
                return $this->buildErrorObject(1);
            }
        } else {
            return $this->buildErrorObject(2);
        }
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return array
     */
    public function forgotPassword(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::EMAIL)
        );

        $jsonResponse = array();

        if (isset($data_array[Constant::EMAIL])) {

            $validateUser = false;

            //Se buscan los usuarios por emails y se validan si son medicos o asistentes
            $allgduUsers = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            if (sizeof($allgduUsers) > 0) {

                foreach ($allgduUsers as $gduUser) {
                    if ($gduUser->getUser()->getUserTypeId()->getId() == 1 || $gduUser->getUser()->getUserTypeId()->getId() == 2) {
                        $validateUser = true;
                        $user = $gduUser->getUser();
                        break;
                    }
                }

                if ($validateUser) {
                    $user->setTemporary(1);
                    $user->setSyncDate(new \DateTime('now'));
                    $extraDataUserId = $user->getExtraDataUserId();
                    $extraDataUser = $this->repositoryExtraDataUser->find($extraDataUserId);
                    //TODO: generar y cifrar password
                    $code = substr(md5(uniqid(rand(), true)), 0, 10);
                    $hashPassword = hash($hashAlgorithm, $code);
                    $extraDataUser->setPassword($hashPassword);
                    $this->em->flush();

                    $email = new MailService();
                    $email->setEmail($user->getGeneralDataUserId()->getEmail());
                    $email->setName($user->getGeneralDataUserId()->getName());
                    $email->setPass($code);
                    $email->setTipeMail(5);
                    $email->sendEmail();

                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL,
                        Constant::MESSAGE_NAME => Constant::VALIDATE_BOOL_T
                    ));
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => 'User not found'
                    ));
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'User not found'
                ));
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'Missing parameters'
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return object
     */
    public function forgotPasswordV1(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::EMAIL => $request->request->get(Constant::_EMAIL)
        );

        if (isset($data_array[Constant::EMAIL])) {

            $validateUser = false;

            //Se buscan los usuarios por emails y se validan si son medicos o asistentes
            $allgduUsers = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL]));

            if (sizeof($allgduUsers) > 0) {

                foreach ($allgduUsers as $gduUser) {
                    if ($gduUser->getUser()->getUserTypeId()->getId() == 1 || $gduUser->getUser()->getUserTypeId()->getId() == 2) {
                        $validateUser = true;
                        $user = $gduUser->getUser();
                        break;
                    }
                }

                if ($validateUser) {
                    $user->setTemporary(1);
                    $user->setSyncDate(new \DateTime('now'));
                    $extraDataUserId = $user->getExtraDataUserId();
                    $extraDataUser = $this->repositoryExtraDataUser->find($extraDataUserId);
                    //TODO: generar y cifrar password
                    $code = substr(md5(uniqid(rand(), true)), 0, 10);
                    $hashPassword = hash($hashAlgorithm, $code);
                    $extraDataUser->setPassword($hashPassword);
                    $this->em->flush();

                    $email = new MailService();
                    $email->setEmail($user->getGeneralDataUserId()->getEmail());
                    $email->setName($user->getGeneralDataUserId()->getName());
                    $email->setPass($code);
                    $email->setTipeMail(5);
                    $email->sendEmail();

                    return (object)array(
                        Constant::_SINGLE_DATA => true
                    );
                } else {
                    return $this->buildErrorObject(1);
                }
            } else {
                return $this->buildErrorObject(1);
            }
        } else {
            return $this->buildErrorObject(2);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getListNotConfirmedUser(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        //json information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {

                //consult Entity Appointment
                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                //get rows where user exist

                foreach ($dateId as $appointment) {
                    $appointmentDate = $appointment->getDate()->format('Y-m-d');
                    $needConfirm = new \DateTime($appointmentDate);

                    date_add($needConfirm, date_interval_create_from_date_string('- 1 days'));
                    $confirmPlease = date_format($needConfirm, 'Y-m-d');
                    $isConfirmed = $appointment->getConfirmed();
                    $isCanceled = $appointment->getCanceled();
                    $isWaitingQueue = $appointment->getWaitingQueue();

                    if ((($data_array[Constant::CURRENT_DATE] == $confirmPlease) || ($data_array[Constant::CURRENT_DATE] == $appointmentDate)) && ($isConfirmed != 1) && ($isCanceled != 1) && ($isWaitingQueue != 1)) {

                        // Get patient data
                        $userThree = $appointment->getUserThreeId();
                        //Consult entity User
                        $patientData = $this->repositoryUser->find($userThree);
                        $patientId = $patientData->getGeneralDataUserId();
                        //Consult Entity GeneralDataUser
                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                        //json information output
                        array_push($jsonResponse, array(
                            Constant::DATE_ID => $appointment->getId(),
                            Constant::DATE => $appointment->getDate()->format('Y-m-d H:i'),
                            Constant::PATIENT_NAME => $patient->getName(),
                            Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                            Constant::EMAIL => $patient->getEmail(),
                            Constant::CELL_PHONE => $patient->getCellPhone(),
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'User not found'
                ));
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {

                                    //consult entity Appointment
                                    $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $userDoctor), array());
                                    //get rows where $user exist
                                    foreach ($dateId as $appointment) {
                                        //$date = $appointment->getDate();
                                        $appointmentDate = $appointment->getDate()->format('Y-m-d');
                                        $needConfirm = new \DateTime($appointmentDate);
                                        //$alertConfirm = date_create();
                                        date_add($needConfirm, date_interval_create_from_date_string('- 1 days'));
                                        $confirmPlease = date_format($needConfirm, 'Y-m-d');
                                        $isConfirmed = $appointment->getConfirmed();
                                        $isCanceled = $appointment->getCanceled();
                                        $isWaitingQueue = $appointment->getWaitingQueue();

                                        if ((($data_array[Constant::CURRENT_DATE] == $confirmPlease) || ($data_array[Constant::CURRENT_DATE] == $appointmentDate)) && ($isConfirmed != 1) && ($isCanceled != 1) && ($isWaitingQueue != 1)) {

                                            $date = $appointment->getDate()->format('Y-m-d H:i');
                                            // Get patient data
                                            $userThree = $appointment->getUserThreeId();
                                            //Consult entity User
                                            $patientData = $this->repositoryUser->find($userThree);
                                            $patientId = $patientData->getGeneralDataUserId();
                                            //Consult entity GeneralDataUser
                                            $patient = $this->repositoryGeneralDataUser->find($patientId);

                                            //json information output
                                            array_push($jsonResponse, array(
                                                Constant::DATE_ID => $appointment->getId(),
                                                Constant::DATE => $date,
                                                Constant::PATIENT_NAME => $patient->getName(),
                                                Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                                Constant::EMAIL => $patient->getEmail(),
                                                Constant::CELL_PHONE => $patient->getCellPhone(),
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getListNotArrivedUser(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        //json information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {

                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                foreach ($dateId as $notArrivedDate) {
                    $date = $notArrivedDate->getDate()->format('Y-m-d');
                    $isAttended = $notArrivedDate->getAttended();
                    $isCanceled = $notArrivedDate->getCanceled();
                    $isWaitingQueue = $notArrivedDate->getWaitingQueue();

                    if ($date < ($data_array[Constant::CURRENT_DATE]) && ($isAttended != 1) && ($isCanceled != 1) && ($isWaitingQueue != 1)) {
                        $userThree = $notArrivedDate->getUserThreeId();
                        $patientData = $this->repositoryUser->find($userThree);
                        $patientId = $patientData->getGeneralDataUserId();
                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                        array_push($jsonResponse, array(
                            Constant::DATE_ID => $notArrivedDate->getId(),
                            Constant::DATE => $notArrivedDate->getDate()->format('Y-m-d H:i'),
                            Constant::PATIENT_NAME => $patient->getName(),
                            Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                            Constant::EMAIL => $patient->getEmail(),
                            Constant::CELL_PHONE => $patient->getCellPhone(),
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'invalid doctor identity or does not exist'
                ));
            }

        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {

                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {

                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {

                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {

                                    $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $userDoctor), array());

                                    foreach ($dateId as $notArrivedDate) {
                                        $date = $notArrivedDate->getDate()->format('Y-m-d');
                                        $isAttended = $notArrivedDate->getAttended();
                                        $isCanceled = $notArrivedDate->getCanceled();
                                        $isWaitingQueue = $notArrivedDate->getWaitingQueue();

                                        if ($date < ($data_array[Constant::CURRENT_DATE]) && ($isAttended != 1) && ($isCanceled != 1) && ($isWaitingQueue != 1)) {
                                            $userThree = $notArrivedDate->getUserThreeId();
                                            $patientData = $this->repositoryUser->find($userThree);
                                            $patientId = $patientData->getGeneralDataUserId();
                                            $patient = $this->repositoryGeneralDataUser->find($patientId);

                                            array_push($jsonResponse, array(
                                                Constant::DATE_ID => $notArrivedDate->getId(),
                                                Constant::DATE => $notArrivedDate->getDate()->format('Y-m-d H:i'),
                                                Constant::PATIENT_NAME => $patient->getName(),
                                                Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                                Constant::EMAIL => $patient->getEmail(),
                                                Constant::CELL_PHONE => $patient->getCellPhone()
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'Assistant not found'
                        ));
                    }
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => 'Assistant not found'
                    ));
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'User not found'
                ));
            }
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getListCanceledUser(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        //json information input
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::CURRENT_DATE => $request->request->get(Constant::CURRENT_DATE)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {

                $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $user), array());
                foreach ($dateId as $notArrivedDate) {
                    $date = $notArrivedDate->getDate()->format('Y-m-d');
                    $isAttended = $notArrivedDate->getAttended();
                    $isCanceled = $notArrivedDate->getCanceled();
                    $isWaitingQueue = $notArrivedDate->getWaitingQueue();

                    if ($date < ($data_array[Constant::CURRENT_DATE]) && ($isAttended != 1) && ($isCanceled == 1) && ($isWaitingQueue != 1)) {
                        $userThree = $notArrivedDate->getUserThreeId();
                        $patientData = $this->repositoryUser->find($userThree);
                        $patientId = $patientData->getGeneralDataUserId();
                        $patient = $this->repositoryGeneralDataUser->find($patientId);

                        array_push($jsonResponse, array(
                            Constant::DATE_ID => $notArrivedDate->getId(),
                            Constant::DATE => $notArrivedDate->getDate()->format('Y-m-d H:i'),
                            Constant::PATIENT_NAME => $patient->getName(),
                            Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                            Constant::EMAIL => $patient->getEmail(),
                            Constant::CELL_PHONE => $patient->getCellPhone(),
                        ));
                    }
                }
                //return $jsonResponse;
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'invalid doctor identity or does not exist'
                ));
            }

        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {

                                    $dateId = $this->repositoryAppointment->findBy(array(Constant::USER_ONE_ID => $userDoctor), array());

                                    foreach ($dateId as $notArrivedDate) {
                                        $date = $notArrivedDate->getDate()->format('Y-m-d');
                                        $isAttended = $notArrivedDate->getAttended();
                                        $isCanceled = $notArrivedDate->getCanceled();
                                        $isWaitingQueue = $notArrivedDate->getWaitingQueue();

                                        if ($date < ($data_array[Constant::CURRENT_DATE]) && ($isAttended != 1) && ($isCanceled == 1) && ($isWaitingQueue != 1)) {
                                            $userThree = $notArrivedDate->getUserThreeId();
                                            $patientData = $this->repositoryUser->find($userThree);
                                            $patientId = $patientData->getGeneralDataUserId();
                                            $patient = $this->repositoryGeneralDataUser->find($patientId);

                                            array_push($jsonResponse, array(
                                                Constant::DATE_ID => $notArrivedDate->getId(),
                                                Constant::DATE => $notArrivedDate->getDate()->format('Y-m-d H:i'),
                                                Constant::PATIENT_NAME => $patient->getName(),
                                                Constant::PATIENT_LAST_NAME => $patient->getLastName(),
                                                Constant::EMAIL => $patient->getEmail(),
                                                Constant::CELL_PHONE => $patient->getCellPhone()
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @param $hashAlgorithm
     * @return bool
     */
    public function updatePassword(Request $request, $hashAlgorithm)
    {
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::OLD_PASSWORD => $request->request->get(Constant::OLD_PASSWORD),
            Constant::NEW_PASSWORD => $request->request->get(Constant::NEW_PASSWORD)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {
                $extraData = $user->getExtraDataUserId();

                $passwordExtra = $extraData->getPassword();
                $hashPassword = hash($hashAlgorithm, $data_array[Constant::OLD_PASSWORD]);

                if ($passwordExtra == $hashPassword) {
                    //TODO: cifrar password
                    $hashNewPassword = hash($hashAlgorithm, $data_array[Constant::NEW_PASSWORD]);
                    //$loginData->setPassword($hashNewPassword);
                    $extraData->setPassword($hashNewPassword);
                    $this->em->flush();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } elseif ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {
                                    if ($userAssistant->getExtraDataUserId()) {
                                        //TODO: cifrar password
                                        $hashNewPassword = hash($hashAlgorithm, $data_array[Constant::NEW_PASSWORD]);
                                        //$loginData->setPassword($hashNewPassword);
                                        $userAssistant->getExtraDataUserId()->setPassword($hashNewPassword);
                                        $this->em->flush();
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getSpeciality(Request $request)
    {
        $list_speciality = $this->repositorySpeciality->findAll();
        $jsonResponse = array();
        foreach ($list_speciality as $values) {
            array_push($jsonResponse, array(
                Constant::GENERAL_ID_COLUMN => $values->getId(),
                Constant::SPECIALITY => $values->getDescription()
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function createSpeciality(Request $request)
    {
        $data_array = array(
            Constant::SPECIALITY => $request->request->get(Constant::SPECIALITY),
        );

        if ($data_array[Constant::SPECIALITY]) {

            $data_speciality = $this->repositorySpeciality->findOneBy(array(Constant::GENERAL_DESCRIPTION_COLUMN => $data_array[Constant::SPECIALITY]));

            if (count($data_speciality) > 0) {
                return false;
            } elseif (count($data_speciality) == 0) {


                $speciality = new Speciality();

                $speciality->setDescription($data_array[Constant::SPECIALITY]);

                $this->em->persist($speciality);
                $this->em->flush();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getListAllMyPatients(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;

        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {

                $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $user), array());

                if (count($takeOfConnectionUserOneThree) > 0) {

                    foreach ($takeOfConnectionUserOneThree as $patient) {
                        $patientId = $patient->getUserThreeId();

                        $patientData = $this->repositoryUser->find($patientId);
                        $patientExtra = $patientData->getExtraDataUserId();
                        $patientGeneral = $patientData->getGeneralDataUserId();

                        $patientGeneralData = $this->repositoryGeneralDataUser->find($patientGeneral);
                        $patientExtraData = $this->repositoryExtraDataUser->find($patientExtra);

                        $documentTypeDescription = null;
                        if ($patientExtraData->getdocumentTypeId()) {
                            $documentTypeDescription = $patientExtraData->getdocumentTypeId()->getDescription();
                        } else {
                            $documentTypeDescription = Constant::DEFAULT_IDENTITY;
                        }

                        $genderDescription = null;
                        if ($patientExtraData->getGenderId()) {
                            $genderId = $patientExtraData->getGenderId();
                            $gender = $this->repositoryGender->find($genderId);
                            $genderDescription = $gender->getDescription();
                        }
                        $birthDate = null;
                        if ($patientExtraData->getBirthDate())
                            $birthDate = $patientExtraData->getBirthDate()->format('Y-m-d');

                        array_push($jsonResponse, array(
                            Constant::USER_ID => $patientId->getId(),
                            Constant::PATIENT_NAME => $patientGeneralData->getName(),
                            Constant::PATIENT_LAST_NAME => $patientGeneralData->getLastName(),
                            Constant::DOCUMENT_TYPE => $documentTypeDescription,
                            Constant::IDENTITY => $patientExtraData->getIdentity(),
                            Constant::BIRTH_DATE => $birthDate,
                            Constant::GENDER => $genderDescription,
                            Constant::CELL_PHONE => $patientGeneralData->getCellPhone(),
                            Constant::HOME_PHONE => $patientGeneralData->getHomePhone(),
                            Constant::EMAIL => $patientGeneralData->getEmail(),
                            Constant::EMERGENCY_NAME => $patientExtraData->getEmergencyName(),
                            Constant::EMERGENCY_NUMBER => $patientExtraData->getEmergencyNumber(),
                            Constant::COMMENT_EXTRA_DATA_USER => $patientExtraData->getComment(),
                            Constant::DOCTOR_ID => $user->getId()
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'invalid doctor identity or does not exist'
                ));
            }
            //return $jsonResponse;

        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {
                                    $takeOfConnectionUserOneThree = $this->repositoryUserConnectionOneThree->findBy(array(Constant::USER_ONE_CONNECTION_COLUMN => $userDoctor), array());

                                    if (count($takeOfConnectionUserOneThree) > 0) {
                                        foreach ($takeOfConnectionUserOneThree as $patient) {
                                            $patientId = $patient->getUserThreeId();

                                            $patientData = $this->repositoryUser->find($patientId);
                                            //$userId = $patientData->getId();
                                            $patientExtra = $patientData->getExtraDataUserId();
                                            $patientGeneral = $patientData->getGeneralDataUserId();

                                            $patientGeneralData = $this->repositoryGeneralDataUser->find($patientGeneral);
                                            $patientExtraData = $this->repositoryExtraDataUser->find($patientExtra);

                                            $documentTypeDescription = null;
                                            if ($patientExtraData->getdocumentTypeId()) {
                                                $documentTypeDescription = $patientExtraData->getdocumentTypeId()->getDescription();
                                            } else {
                                                $documentTypeDescription = Constant::DEFAULT_IDENTITY;
                                            }
                                            $genderId = $patientExtraData->getGenderId();
                                            $gender = $this->repositoryGender->find($genderId);

                                            $birthDate = null;
                                            if ($patientExtraData->getBirthDate())
                                                $birthDate = $patientExtraData->getBirthDate()->format('Y-m-d');

                                            array_push($jsonResponse, array(
                                                Constant::USER_ID => $patientData->getId(),
                                                Constant::PATIENT_NAME => $patientGeneralData->getName(),
                                                Constant::PATIENT_LAST_NAME => $patientGeneralData->getLastName(),
                                                Constant::DOCUMENT_TYPE => $documentTypeDescription,
                                                Constant::IDENTITY => $patientExtraData->getIdentity(),
                                                Constant::BIRTH_DATE => $birthDate,
                                                Constant::GENDER => $gender->getDescription(),
                                                Constant::CELL_PHONE => $patientGeneralData->getCellPhone(),
                                                Constant::EMAIL => $patientGeneralData->getEmail(),
                                                Constant::EMERGENCY_NAME => $patientExtraData->getEmergencyName(),
                                                Constant::EMERGENCY_NUMBER => $patientExtraData->getEmergencyNumber(),
                                                Constant::COMMENT_EXTRA_DATA_USER => $patientExtraData->getComment()
                                            ));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'invalid assistant email or does not exist'
                        ));
                    }
                }
            } else {
                array_push($jsonResponse, array(
                    Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                    Constant::MESSAGE_NAME => 'User not found'
                ));
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing doctor or asistant'
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getGender(Request $request)
    {
        $list_gender = $this->repositoryGender->findAll();
        $jsonResponse = array();
        foreach ($list_gender as $values) {
            array_push($jsonResponse, array(
                Constant::GENERAL_ID_COLUMN => $values->getId(),
                Constant::GENDER => $values->getDescription()
            ));
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function removeUserFromPendingList(Request $request)
    {
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::DATE_ID => $request->request->get(Constant::DATE_ID)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($user) {

                $userTypeId = $this->repositoryUser->find($user);
                $userType = $userTypeId->getUserTypeId();
                $isDoctor = $userType->getDescription();

                if ($isDoctor == Constant::USER_TYPE_DOCTOR) {

                    $dateId = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                    if (count($dateId) > 0) {
                        $dateId->setWaitingQueue(1);
                        $dateId->setSyncDate(new \DateTime('now'));
                        $this->em->flush();

                        return true;
                    } else {
                        return false;
                    }
                }

            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {
                                    $dateId = $this->repositoryAppointment->findOneBy(array(Constant::DATE_DESCRIPTION => $data_array[Constant::DATE_ID]));

                                    if (count($dateId) > 0) {
                                        $dateId->setWaitingQueue(1);
                                        $dateId->setSyncDate(new \DateTime('now'));
                                        $this->em->flush();
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function listAssistant(Request $request)
    {
        $jsonResponse = array();
        $negativeResponse = null;
        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID)
        );
        if ($data_array[Constant::DOCTOR_ID]) {
            $user = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);
            if ($user) {
                if (count($user) > 0) {
                    $user_data_id = $user->getId();
                    $assistan_id = $this->repositoryUserConnectionOneTwo->findBy(array('userOneId' => $user_data_id), array());
                    foreach ($assistan_id as $values) {
                        $id_assistan = $values->getUserTwoId();
                        $user_assis_id = $this->repositoryUser->find($id_assistan);
                        $general_id = $user_assis_id->getGeneralDataUserId();
                        $general_data = $this->repositoryGeneralDataUser->find($general_id);
                        array_push($jsonResponse, array(
                            Constant::GENERAL_ID_COLUMN => $general_data->getId(),
                            Constant::NAME => $general_data->getName(),
                            Constant::LAST_NAME => $general_data->getLastName(),
                            Constant::EMAIL => $general_data->getEmail(),
                            Constant::STATUS_NAME => $status = $user_assis_id->getTemporary()
                        ));
                    }
                } else {
                    return $negativeResponse;
                }
            } else {
                return $negativeResponse;
            }
        } else {
            return $negativeResponse;
        }
        return $jsonResponse;
    }

//TODO: no se usa este endpoint
    /**
     * @param Request $request
     * @return array
     */
    public function removeUserEps(Request $request)
    {
        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::EPS_ID => $request->request->get(Constant::EPS_ID)
        );

        $jsonResponse = array();

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $useLogin = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));
            if (count($useLogin) > 0) {

                $userData = $this->repositoryUser->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $useLogin->getUserId()));

                $userTypeId = $userData->getUserTypeId();
                $useTypeData = $this->repositoryUserType->findOneBY(array(Constant::GENERAL_ID_COLUMN => $userTypeId));
                $userTypeDescription = $useTypeData->getDescription();

                if ($userTypeDescription == Constant::USER_TYPE_DOCTOR) {
                    $userEpsConnection = $this->repositoryUserEpsConnection->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $userData->getExtraDataUserId(),
                        Constant::GENERAL_ID_COLUMN => $data_array[Constant::EPS_ID]));

                    $removerInspector = null;
                    if (count($userEpsConnection) > 0) {
                        $this->em->remove($userEpsConnection);
                        $this->em->flush();
                        $removerInspector = $userEpsConnection->getId();
                        if ($removerInspector == null) {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL,
                                Constant::MESSAGE_NAME => Constant::VALIDATE_BOOL_T
                            ));

                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                Constant::MESSAGE_NAME => Constant::VALIDATE_BOOL_F
                            ));
                        }
                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'user and eps are not related'
                        ));
                    }
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => 'user is not a doctor'
                    ));
                }
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing parameters'
            ));
        }
        return $jsonResponse;
    }

//TODO: no se usa este endpoint
    /**
     * @param Request $request
     * @return array
     */
    public function removeOffice(Request $request)
    {
        $data_array = array(
            Constant::IDENTITY_USER_TYPE_DOCTOR => $request->request->get(Constant::IDENTITY_USER_TYPE_DOCTOR),
            Constant::PLACE_ID => $request->request->get(Constant::PLACE_ID)
        );
        $jsonResponse = array();

        if ($data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]) {
            $loginData = $this->repositoryLogin->findOneBy(array(Constant::IDENTITY => $data_array[Constant::IDENTITY_USER_TYPE_DOCTOR]));
            if (count($loginData) > 0) {
                //$extraDataId = $loginData->getId();
                $userData = $this->repositoryUser->findOneBy(array(Constant::EXTRA_DATA_USER_ID => $loginData->getUserId()));

                $userTypeId = $userData->getUserTypeId();
                $useTypeData = $this->repositoryUserType->findOneBY(array(Constant::GENERAL_ID_COLUMN => $userTypeId));
                $userTypeDescription = $useTypeData->getDescription();

                if ($userTypeDescription == Constant::USER_TYPE_DOCTOR) {
                    $userPlaceConnection = $this->repositoryUserPlaceConnection->findOneBy(array(Constant::USER_ID => $userData->getId(), Constant::PLACE_ID => $data_array[Constant::PLACE_ID]));
                    $removerInspector = null;
                    if (count($userPlaceConnection) > 0) {
                        $this->em->remove($userPlaceConnection);
                        $this->em->flush();
                        $removerInspector = $userPlaceConnection->getId();
                        if ($removerInspector == null) {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL,
                                Constant::MESSAGE_NAME => Constant::VALIDATE_BOOL_T
                            ));

                        } else {
                            array_push($jsonResponse, array(
                                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                                Constant::MESSAGE_NAME => Constant::VALIDATE_BOOL_F
                            ));
                        }
                    } else {
                        array_push($jsonResponse, array(
                            Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                            Constant::MESSAGE_NAME => 'user and place are not related'
                        ));
                    }
                } else {
                    array_push($jsonResponse, array(
                        Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                        Constant::MESSAGE_NAME => 'user is not a doctor'
                    ));
                }
            }
        } else {
            array_push($jsonResponse, array(
                Constant::STATUS_NAME => Constant::STATUS_NOT_SUCCESSFUL,
                Constant::MESSAGE_NAME => 'missing parameters'
            ));
        }
        return $jsonResponse;
    }

//TODO: ya no se usa este metodo
    /**
     * @param $hashAlgorithm
     * @return bool
     */
    public
    function encryptPasswords($hashAlgorithm)
    {
        set_time_limit(0);
        //Consultamos todos los registros extraDataUser
        $allExtraDataUserRecords = $this->repositoryExtraDataUser->findAll();
        $allLoginRecords = $this->repositoryLogin->findAll();

        foreach ($allExtraDataUserRecords as $extraDataUserRecord) {

            $actualPassword = $extraDataUserRecord->getPassword();
            if ($actualPassword) {
                $hashPassword = hash($hashAlgorithm, $actualPassword);
                $extraDataUserRecord->setPassword($hashPassword);

                foreach ($allLoginRecords as $loginRecord) {
                    $userEntity = $this->repositoryUser->findOneBy(array('extraDataUserId' => $extraDataUserRecord->getId()));
                    if ($userEntity)
                        if ($userEntity->getId() == $loginRecord->getUserId()->getId()) {
                            $loginRecord->setPassword($hashPassword);
                            //$this->em->persist($loginRecord);
                        }
                }
                //$this->em->persist($extraDataUserRecord);
                $this->em->flush();
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function setCreationDate()
    {
        //set_time_limit(0);
        //listo todos los doctores
        $allDoctors = $this->repositoryUser->findBy(array('userTypeId' => 1));

        foreach ($allDoctors as $doctor) {
            $appointmentQuery = $this->repositoryAppointment->createQueryBuilder('q')
                ->where('q.userOneId=' . $doctor->getId())
                ->orderBy('q.creationDate', 'ASC')
                ->getQuery();
            $appointmentsByDoctor = $appointmentQuery->getResult();

            if (sizeof($appointmentsByDoctor) > 0) {
                $doctor->setCreationDate($appointmentsByDoctor[0]->getCreationDate());
                $this->em->flush();
            }
        }
        return true;
    }

    public function pdfRemembers()
    {
        //Busco todos los medicos
        $allDoctors = $this->repositoryUser->findBy(array(Constant::USER_TYPE_ID => '1'));

        //Para Ejemplos
        //$allDoctors = $this->repositoryUser->findBy(array(Constant::GENERAL_ID_COLUMN => '1'));

        $today = new \DateTime('today');

        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        $fullDate = $days[$today->format('w')] . " " . $today->format('d') . " de " . $months[$today->format('n') - 1] . " de " . $today->format('Y');

        $pdfGenerator = new PdfGenerator();

        foreach ($allDoctors as $doctor) {

            $appointments = array();

            $personalTitle = null;
            if ($doctor->getExtraDataUserId()->getPersonalTitle() == 2) {
                $personalTitle = "Doctora ";
            } elseif ($doctor->getExtraDataUserId()->getPersonalTitle() == 1) {
                $personalTitle = "Doctor ";
            } else {
                $personalTitle = "";
            }

            $fullDoctorName = $personalTitle . '' . $doctor->getGeneralDataUserId()->getName() . ' ' . $doctor->getGeneralDataUserId()->getLastName();

            //Busco las citas del medico para hoy
            $appointmentQuery = $this->repositoryAppointment->createQueryBuilder('q')
                ->where('q.' . Constant::USER_ONE_ID . '=' . $doctor->getId()
                    . ' and q.' . Constant::DATE . '>= \'' . $today->format('Y-m-d H:i')
                    . '\' and q.' . Constant::DATE . '<= \'' . $today->format('Y-m-d') . ' 23:59' . '\'')
                ->orderBy('q.' . Constant::DATE)
                ->getQuery();

            //error_log($appointmentQuery);

            $allDates = $appointmentQuery->getResult();

            if (sizeof($allDates) > 0) {
                foreach ($allDates as $date) {

                    if ($date->getCanceled() == null || $date->getCanceled() == false) {

                        $canceled = false;

                        if ($date->getCanceled() || $date->getCanceledByPatient())
                            $canceled = true;

                        $eps = 'No registra';
                        if ($date->getEps() != null)
                            $eps = $date->getEps()->getDescription();


                        $endHour = clone $date->getDate();
                        $endHour->add(new \DateInterval('PT' . $date->getAssignedTime()->format('H') . 'H' . $date->getAssignedTime()->format('i') . 'M'));
                        array_push($appointments, (object)array('hour' => $date->getDate()->format('h:i A'),
                            'endHour' => $endHour->format('h:i A'),
                            'patientName' => $date->getUserThreeId()->getGeneralDataUserId()->getName() . ' ' . $date->getUserThreeId()->getGeneralDataUserId()->getLastName(),
                            'observations' => $date->getComment(),
                            'confirmed' => $date->getConfirmed(),
                            'canceled' => $canceled,
                            'eps' => $eps));
                    }
                }
                if (sizeof($appointments) > 0)
                    if ($pdfGenerator->pdfGenerator($fullDoctorName, $fullDate, $appointments)) {
                        $email = new MailService();
                        $email->setEmail($doctor->getGeneralDataUserId()->getEmail());
                        $email->setPersonalTitle($doctor->getExtraDataUserId()->getPersonalTitle());
                        $email->setNameDoctor($doctor->getGeneralDataUserId()->getName());
                        $email->setLastNameDoctor($doctor->getGeneralDataUserId()->getLastName());
                        $email->setName($doctor->getGeneralDataUserId()->getName());
                        $email->setPersonalTitleDoctor($doctor->getExtraDataUserId()->getPersonalTitle());
                        $email->setTipeMail(8);
                        $email->sendEmail();
                    }
            }
        }
    }

    /**
     * @param $request
     * @return object
     */
    public function recommendApp($request)
    {
        $jsonResponse = array();

        $data_array = array(
            Constant::DOCTOR_ID => $request->request->get(Constant::DOCTOR_ID),
            Constant::EMAIL_USER_TYPE_ASSISTANT => $request->request->get(Constant::EMAIL_USER_TYPE_ASSISTANT),
            Constant::EMAILS_RECOMMEND => $request->request->get(Constant::EMAILS_RECOMMEND)
        );

        if ($data_array[Constant::DOCTOR_ID] && !$data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {

            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {

                $toRecommend = $data_array[Constant::EMAILS_RECOMMEND];

                if (sizeof($toRecommend) > 0) {

                    foreach ($toRecommend as $recommended) {

                        if ($recommended['userName'] && ($recommended[Constant::EMAIL] || $recommended[Constant::PHONE])) {
                            $recommendedApp = new RecommendApp();
                            $recommendedApp->setUser($userDoctor);
                            $recommendedApp->setNameRecommended($recommended['userName']);
                            $recommendedApp->setEmailRecommended($recommended[Constant::EMAIL]);
                            $recommendedApp->setPhoneRecommended($recommended[Constant::PHONE]);
                            $recommendedApp->setDateRecommended(new \DateTime('today'));
                            $this->em->persist($recommendedApp);
                            $this->em->flush();

                            if ($recommended['email']) {
                                $email = new MailService();
                                $email->setEmail($recommended['email']);
                                $email->setName($recommended['userName']);
                                $email->setPersonalTitleDoctor($userDoctor->getExtraDataUserId()->getPersonalTitle());
                                $email->setNameDoctor($userDoctor->getGeneralDataUserId()->getName());
                                $email->setLastNameDoctor($userDoctor->getGeneralDataUserId()->getLastName());
                                $email->setTipeMail(10);
                                $email->sendEmail();
                            }
                            $jsonResponse = (object)array(
                                Constant::STATUS_NAME => true,
                                Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL
                            );
                        } else {
                            $jsonResponse = (object)array(
                                Constant::STATUS_NAME => false,
                                Constant::MESSAGE_NAME => 'Error validating data'
                            );
                        }
                    }
                } else {
                    $jsonResponse = (object)array(
                        Constant::STATUS_NAME => false,
                        Constant::MESSAGE_NAME => 'Not recommended information'
                    );
                }
            } else {
                $jsonResponse = (object)array(
                    Constant::STATUS_NAME => false,
                    Constant::MESSAGE_NAME => 'User not found'
                );
            }
        } else if ($data_array[Constant::DOCTOR_ID] && $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]) {
            $userDoctor = $this->repositoryUser->find($data_array[Constant::DOCTOR_ID]);

            if ($userDoctor) {
                $ucoTwo = $this->repositoryUserConnectionOneTwo->findBy(array(Constant::USER_ONE_ID => $userDoctor->getId()));

                if (sizeof($ucoTwo) > 0) {
                    $gduAssistants = $this->repositoryGeneralDataUser->findBy(array(Constant::EMAIL => $data_array[Constant::EMAIL_USER_TYPE_ASSISTANT]));

                    if (sizeof($gduAssistants) > 0) {
                        foreach ($ucoTwo as $ucot) {
                            foreach ($gduAssistants as $gduAssistant) {
                                $userAssistant = $gduAssistant->getUser();
                                if ($ucot->getUserTwoId() == $userAssistant) {

                                    $toRecommend = $data_array[Constant::EMAILS_RECOMMEND];

                                    if (sizeof($toRecommend) > 0) {

                                        foreach ($toRecommend as $recommended) {

                                            if ($recommended['userName'] && ($recommended[Constant::EMAIL] || $recommended[Constant::PHONE])) {
                                                $recommendedApp = new RecommendApp();
                                                $recommendedApp->setUser($userDoctor);
                                                $recommendedApp->setNameRecommended($recommended['userName']);
                                                $recommendedApp->setEmailRecommended($recommended[Constant::EMAIL]);
                                                $recommendedApp->setPhoneRecommended($recommended[Constant::PHONE]);
                                                $recommendedApp->setDateRecommended(new \DateTime('today'));
                                                $this->em->persist($recommendedApp);
                                                $this->em->flush();

                                                if ($recommended['email']) {
                                                    $email = new MailService();
                                                    $email->setEmail($recommended['email']);
                                                    $email->setName($recommended['userName']);
                                                    $email->setPersonalTitleDoctor($userDoctor->getExtraDataUserId()->getPersonalTitle());
                                                    $email->setLastNameDoctor($userDoctor->getGeneralDataUserId()->getName());
                                                    $email->setTipeMail(10);
                                                    $email->sendEmail();
                                                }
                                                $jsonResponse = (object)array(
                                                    Constant::STATUS_NAME => true,
                                                    Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL
                                                );
                                            } else {
                                                $jsonResponse = (object)array(
                                                    Constant::STATUS_NAME => false,
                                                    Constant::MESSAGE_NAME => 'Error validating data'
                                                );
                                            }
                                        }
                                    } else {
                                        $jsonResponse = (object)array(
                                            Constant::STATUS_NAME => false,
                                            Constant::MESSAGE_NAME => 'Not recommended information'
                                        );
                                    }
                                }
                            }
                        }
                    } else {
                        $jsonResponse = (object)array(
                            Constant::STATUS_NAME => false,
                            Constant::MESSAGE_NAME => 'User not found'
                        );
                    }
                } else {
                    $jsonResponse = (object)array(
                        Constant::STATUS_NAME => false,
                        Constant::MESSAGE_NAME => 'Relation not found'
                    );
                }
            } else {
                $jsonResponse = (object)array(
                    Constant::STATUS_NAME => false,
                    Constant::MESSAGE_NAME => 'User not found'
                );
            }
        } else {
            $jsonResponse = (object)array(
                Constant::STATUS_NAME => false,
                Constant::MESSAGE_NAME => 'Error validating data'
            );
        }
        return $jsonResponse;
    }

    /**
     * @param $recommendObject
     * @param $userDoctor
     * @return bool|object
     */
    public function recommendAppSync($recommendObject, $userDoctor)
    {
        try {
            $data_array = array(
                Constant::_ID => $recommendObject[Constant::_ID],
                Constant::_NAME => $recommendObject[Constant::_NAME],
                Constant::_CELLPHONE => $recommendObject[Constant::_CELLPHONE],
                Constant::_EMAIL => $recommendObject[Constant::_EMAIL]
            );

            if (isset($data_array[Constant::_NAME]) && (isset($data_array[Constant::_EMAIL]) || isset($data_array[Constant::_CELLPHONE]))) {
                $recommendedApp = new RecommendApp();
                $recommendedApp->setUser($userDoctor);
                $recommendedApp->setNameRecommended($data_array[Constant::_NAME]);
                $recommendedApp->setEmailRecommended($data_array[Constant::_EMAIL]);
                $recommendedApp->setPhoneRecommended($data_array[Constant::_CELLPHONE]);
                $recommendedApp->setDateRecommended(new \DateTime('today'));
                $this->em->persist($recommendedApp);
                $this->em->flush();

                if ($data_array[Constant::_EMAIL]) {
                    $email = new MailService();
                    $email->setEmail($data_array[Constant::_EMAIL]);
                    $email->setName($data_array[Constant::_NAME]);
                    $email->setPersonalTitleDoctor($userDoctor->getExtraDataUserId()->getPersonalTitle());
                    $email->setNameDoctor($userDoctor->getGeneralDataUserId()->getName());
                    $email->setLastNameDoctor($userDoctor->getGeneralDataUserId()->getLastName());
                    $email->setTipeMail(10);
                    $email->sendEmail();
                }
                $recommendService = new RecommendService();
                return $recommendService->buildRecommendObject($recommendedApp->getId(), (object)$recommendObject);
            } else {
                return $this->buildErrorObject(2);
            }
        } catch (\Exception $e) {
            error_log('catch recommendAppSync');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return bool
     */
    public function setPersonalTitle()
    {

        //Traigo todos los medicos
        $allUserDoctors = $this->repositoryUser->findBy(array('userTypeId' => 1));

        if (sizeof($allUserDoctors) > 0) {
            foreach ($allUserDoctors as $userDoctor) {
                if ($userDoctor->getExtraDataUserId()->getGenderId()) {
                    if ($userDoctor->getExtraDataUserId()->getGenderId()->getId() == 1) {
                        $userDoctor->getExtraDataUserId()->setPersonalTitle(2);
                    } elseif ($userDoctor->getExtraDataUserId()->getGenderId()->getId() == 2) {
                        $userDoctor->getExtraDataUserId()->setPersonalTitle(1);
                    } else {
                        $userDoctor->getExtraDataUserId()->setPersonalTitle(0);
                    }
                } else {
                    $userDoctor->getExtraDataUserId()->setPersonalTitle(0);
                }
                $this->em->persist($userDoctor->getExtraDataUserId());
                $this->em->flush();
            }
        }
        return true;
    }

    /**
     * @param Request $req
     * @return array|object
     */
    public function registerCalendarToken(Request $req)
    {
        $jsonResponse = array();
        $data = array(
            Constant::IS_GOOGLE_TOKEN => $req->request->get(Constant::IS_GOOGLE_TOKEN),
            Constant::IS_OUTLOOK_TOKEN => $req->request->get(Constant::IS_OUTLOOK_TOKEN),
            Constant::DOCTOR_ID => $req->request->get(Constant::DOCTOR_ID),
            Constant::ACCESS_TOKEN => $req->request->get(Constant::ACCESS_TOKEN),
            Constant::TOKEN_TYPE => $req->request->get(Constant::TOKEN_TYPE),
            Constant::EXPIRES_IN => $req->request->get(Constant::EXPIRES_IN),
            Constant::REFRESH_TOKEN => $req->request->get(Constant::REFRESH_TOKEN),
            Constant::ID_TOKEN => $req->request->get(Constant::ID_TOKEN),
            Constant::CREATED => $req->request->get(Constant::CREATED)
        );

        if (isset($data[Constant::DOCTOR_ID])) {

            //Buscamos el medico
            $userDoctor = $this->repositoryUser->find($data[Constant::DOCTOR_ID]);
            $this->var_error_log(isset($userDoctor));

            if (isset($userDoctor) && $userDoctor->getUserTypeId()->getId() == 1) {

                if (isset($data[Constant::IS_GOOGLE_TOKEN])) {

                    if (isset($data[Constant::ACCESS_TOKEN]) && isset($data[Constant::TOKEN_TYPE])
                        && isset($data[Constant::EXPIRES_IN]) && isset($data[Constant::ID_TOKEN])
                        && isset($data[Constant::CREATED])
                    ) {

                        $gCalendarToken = $this->gCalendarTokenService->findOneBy(array(Constant::USER => $userDoctor->getId()));

                        if ($gCalendarToken == null) {
                            $gCalendarToken = new GCalendarToken();
                            $gCalendarToken->setUser($userDoctor);
                            $gCalendarToken->setAccessToken($data[Constant::ACCESS_TOKEN]);
                            $gCalendarToken->setTokenType($data[Constant::TOKEN_TYPE]);
                            $gCalendarToken->setExpiresIn($data[Constant::EXPIRES_IN]);
                            $gCalendarToken->setRefreshToken($data[Constant::REFRESH_TOKEN]);
                            $gCalendarToken->setIdToken($data[Constant::ID_TOKEN]);
                            $gCalendarToken->setCreated($data[Constant::CREATED]);
                            $this->em->persist($gCalendarToken);
                            $this->em->flush();
                            $jsonResponse = (object)array(
                                Constant::STATUS => true,
                                Constant::MESSAGE => 'successful'
                            );
                        } else {
                            $jsonResponse = (object)array(
                                Constant::STATUS => false,
                                Constant::MESSAGE => 'token already exist'
                            );
                        }
                    } else {
                        $jsonResponse = (object)array(
                            Constant::STATUS => false,
                            Constant::MESSAGE => 'parameters missing'
                        );
                    }
                } else {
                    if (isset($data[Constant::IS_OUTLOOK_TOKEN])) {

                    }
                }
            } else {
                $jsonResponse = (object)array(
                    Constant::STATUS => false,
                    Constant::MESSAGE => 'user not found'
                );
            }
        } else {
            $jsonResponse = (object)array(
                Constant::STATUS => false,
                Constant::MESSAGE => 'parameters missing'
            );
        }
        return $jsonResponse;
    }

    public function find($userId)
    {
        try {
            return $this->repositoryUser->find($userId);
        } catch (ORMException $e) {
            error_log('catch find de UserService');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }
}
