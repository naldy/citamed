<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Notification;
use BwStudios\CitaMed\Utility\MailService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\Config\Definition\Exception\Exception;

class NotificationService
{

    private $repositoryNotification;

    private $em;

    /**
     * NotificationService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryNotification = $this->em->getRepository(Constant::ENTITY_NOTIFICATION);
    }

    /**
     * @param $appointments
     * @return array|object
     */
    public function buildArrayNotifications($appointments)
    {
        try {
            $arrayNotifications = array();

            if (count($appointments) > 0)
                foreach ($appointments as $appointment) {

                    $allNotificationsEntity = $this->repositoryNotification->findBy(array(Constant::APPOINTMENT => $appointment->Id));

                    if (count($allNotificationsEntity) > 0)
                        foreach ($allNotificationsEntity as $notificationEntity) {

                            $notificationType = $notificationEntity->getType();
                            if ($notificationType) {
                                $today = new \DateTime('today');
                                if (($notificationType->getId() == 3 || $notificationType->getId() == 5 ||
                                        $notificationType->getId() == 6) && $notificationEntity->getIsSend() == false
                                    && $notificationEntity->getDateToSend() >= $today
                                ) {
                                    $tempResponse = $this->buildNotificationObject($notificationEntity);
                                    if (isset($tempResponse->Error)) {
                                        return $tempResponse;
                                    } else {
                                        array_push($arrayNotifications, $tempResponse);
                                    }
                                }
                            } else {
                                return $this->buildErrorObject(-1);
                            }
                        }
                }

            return $arrayNotifications;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    /**
     * @param $appointments
     * @param $dateSync
     * @return array|object
     */
    public function buildArrayNotificationsSync($appointments, $dateSync)
    {
        try {
            $arrayNotifications = array();

            $dateSyncRequest = new \DateTime($dateSync);

            foreach ($appointments as $appointment) {

                $allNotificationsEntity = $this->repositoryNotification->findBy(array(Constant::APPOINTMENT => $appointment->Id));

                foreach ($allNotificationsEntity as $notificationEntity) {
                    $notificationType = $notificationEntity->getType();
                    $now = new \DateTime('now');
                    if (($notificationType->getId() == 3 || $notificationType->getId() == 5 ||
                            $notificationType->getId() == 6) && $notificationEntity->getIsSend() == false
                        && $notificationEntity->getDateToSend() >= $now
                    )
                        if ($notificationEntity->getSyncDate() > $dateSyncRequest) {
                            $tempResponse = $this->buildNotificationObject($notificationEntity);
                            if (isset($tempResponse->Error)) {
                                return $tempResponse;
                            } else {
                                array_push($arrayNotifications, $tempResponse);
                            }
                        }
                }
            }
            return $arrayNotifications;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Notification $notification
     * @return object
     */
    public function buildNotificationObject(Notification $notification)
    {
        try {
            if ($notification->getType()) {
                $notificationType = $notification->getType();
            } else {
                return $this->buildErrorObject(-1);
            }

            return (object)array(
                Constant::_ID => $notification->getId(),
                Constant::_APPOINTMENT_ID => $notification->getAppointment()->getId(),
                Constant::_NOTIFICATION_TYPE_ID => $notificationType->getId(),
                Constant::_IS_SEND => $notification->getIsSend(),
                Constant::_DATE => $notification->getDateToSend()->format(\DateTime::ISO8601)
            );
        } catch (Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * Obtiene las notificaciones tipo 1, 2 y 4 para hoy y que no se hayan enviado
     * @param $appointmentId
     * @return array|object
     */
    public function getNotificationsEmail($appointmentId)
    {
        try {
            $notificationsResponse = array();
            $today = new \DateTime('today');
            $queryNotification = $this->repositoryNotification->createQueryBuilder('q');
            $queryNotification->where('q.appointment=' . $appointmentId);
            $queryNotification->andWhere('q.type=1 OR q.type=2 OR q.type=4');
            $queryNotification->andWhere('q.isSend=false');
            $queryNotification->andWhere('q.dateToSend=\'' . $today->format('Y-m-d H:i:s') . '\'');

            $notifications = $queryNotification->getQuery()->getResult();

            if (isset($notifications) && count($notifications) > 0) {
                $notificationsResponse = $notifications;
            }
            return $notificationsResponse;
        } catch (ORMException $e) {
            error_log('Catch getNotificationsEmail');
            error_log($e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $appointmentsObject
     * @return object
     */
    public function sendEmailsNewAppointments($appointmentsObject)
    {
        try {
            foreach ($appointmentsObject as $appointmentObject) {
                //busco las notificaciones para esa cita
                $notifications = $this->getNotificationsEmail($appointmentObject->appointmentId);
                if (isset($notifications->Error)) {
                    return $notifications;
                } else {
                    if (count($notifications) > 0) {
                        foreach ($notifications as $notification) {
                            if ($notification->getAppointment() &&
                                $notification->getAppointment()->getUserOneId() &&
                                $notification->getAppointment()->getUserOneId()->getGeneralDataUserId() &&
                                $notification->getAppointment()->getUserOneId()->getExtraDataUserId() &&
                                $notification->getAppointment()->getUserThreeId() &&
                                $notification->getAppointment()->getUserThreeId()->getGeneralDataUserId() &&
                                $notification->getAppointment()->getUserThreeId()->getExtraDataUserId()
                            ) {
                                $generalDataPatient = $notification->getAppointment()->getUserThreeId()->getGeneralDataUserId();
                                $generaDataDoctor = $notification->getAppointment()->getUserOneId()->getGeneralDataUserId();
                                $extraDataDoctor = $notification->getAppointment()->getUserOneId()->getExtraDataUserId();
                                $appointment = $notification->getAppointment();
                                $isAppointmentEdit = false;
                                $previousDate = null;
                                if (isset($appointmentObject->isEdit)) {
                                    if ($appointmentObject->isEdit == true) {
                                        $isAppointmentEdit = true;
                                        $previousDate = $appointmentObject->previousDate;
                                    }
                                }
                                $sendNotifications = $appointment->getSendNotifications();
                                //Si es tipo 1
                                if ($notification->getType()->getId() == 1)
                                    if ($sendNotifications)
                                        $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate);
                                //Si es tipo 2
                                if ($notification->getType()->getId() == 2)
                                    if ($sendNotifications)
                                        $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate);
                                //Si es tipo 4 Cancelacion
                                if ($notification->getType()->getId() == 4)
                                    if (!$appointment->getCanceledByPatient() && $appointment->getSendNotifications() == true) {
                                        if ($sendNotifications)
                                            $this->sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);
                                    }
                            } else {
                                return $this->buildErrorObject(-1);
                            }
                        }
                    }
                }

            }
        } catch (\Exception $e) {
            error_log('Catch sendEmailsNewAppointments');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     */
    public function sendCancellationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate()->format('Y-m-d'));
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());
        $email->setTipeMail(7);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     * @param $isAppointmentEdit
     */
    public function sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit, $previousDate)
    {

        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        if ($previousDate) {
            $email->setPreviousMonth($months[$previousDate->format('m') - 1]);
            $email->setPreviousDayLetter($days[$previousDate->format('w')]);
            $email->setPreviousDay($previousDate->format('d'));
            $email->setPreviousHour($previousDate->format('h:i a'));

            $email->setDay($appointment->getDate()->format('d'));
            $email->setDayLetter($days[$appointment->getDate()->format('w')]);
            $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
            $email->setYear($appointment->getDate()->format('Y'));
        }

        if ($today->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
            $email->setIsToday(true);
        } else {
            if ($tomorrow->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
                $email->setIsTomorrow(true);
            } else {
                $email->setDay($appointment->getDate()->format('d'));
                $email->setDayLetter($days[$appointment->getDate()->format('w')]);
                $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
                $email->setYear($appointment->getDate()->format('Y'));
            }
        }

        $email->setDate($appointment->getDate());

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));

        $email->setDateToEnd($dateToEnd);
        $email->setDateid($appointment->getId());
        $email->setTipeMail(3);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $gduPatient
     * @param $eduDoctor
     * @param $appointment
     * @param $gduDoctor
     * @param $notification
     * @param $isAppointmentEdit
     * @param $previousDate
     */
    public function sendInformationEmail($gduPatient, $eduDoctor, $appointment, $gduDoctor, $notification, $isAppointmentEdit, $previousDate)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($gduPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($gduDoctor->getName());
        $email->setLastNameDoctor($gduDoctor->getLastName());
        $email->setEmailDoctor($gduDoctor->getEmail());
        $email->setPersonalTitleDoctor($eduDoctor->getPersonalTitle());
        $email->setEmail($gduPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate());
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        if ($previousDate) {
            $email->setPreviousMonth($months[$previousDate->format('m') - 1]);
            $email->setPreviousDayLetter($days[$previousDate->format('w')]);
            $email->setPreviousDay($previousDate->format('d'));
            $email->setPreviousHour($previousDate->format('h:i a'));
        }

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        $email->setDateToEnd($dateToEnd);

        $email->setGduPatient($gduPatient);
        $email->setEdUDoctor($eduDoctor);
        $email->setAppointment($appointment);
        $email->setGduDoctor($gduDoctor);
        $email->setTipeMail(2);

        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}