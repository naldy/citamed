<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Speciality;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class SpecialityService
{

    private $repositorySpeciality;

    private $em;

    /**
     * SpecialityService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
    }

    /**
     * @return array|object
     */
    public function findAll()
    {
        try {
            return $this->repositorySpeciality->findAll();
        } catch (ORMException $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @return array|object
     */
    public function buildArraySpecializations()
    {
        try {
            $arraySpecializationsResponse = array();
            $allSpecialityEntity = $this->findAll();
            if (isset($allSpecialityEntity->Error)) {
                return $allSpecialityEntity;
            } else {
                if (count($allSpecialityEntity) > 0)
                    foreach ($allSpecialityEntity as $specialityEntity) {
                        $tempResponse = $this->buildSpecializationObject($specialityEntity);
                        if (isset($tempResponse->Error)) {
                            return $tempResponse;
                        } else {
                            array_push($arraySpecializationsResponse, $tempResponse);
                        }
                    }
            }

            return $arraySpecializationsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $dateSync
     * @return array|object
     */
    public function buildArraySpecializationsSync($dateSync)
    {
        try {
            $dateSyncRequest = new \DateTime($dateSync);
            $arraySpecializationsResponse = array();
            $tempQuery = $this->repositorySpeciality->createQueryBuilder('q')
                ->where('q.sync_date >= \'' . $dateSyncRequest->format('Y-m-d H:i:s') . '\'')
                ->getQuery();
            $allSpecialityEntity = $tempQuery->getResult();
            if (count($allSpecialityEntity) > 0) {
                foreach ($allSpecialityEntity as $specialityEntity) {
                    $tempResponse = $this->buildSpecializationObject($specialityEntity);
                    if (isset($tempResponse->Error)) {
                        return $tempResponse;
                    } else {
                        array_push($arraySpecializationsResponse, $tempResponse);
                    }
                }
            }
            return $arraySpecializationsResponse;
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param Speciality $speciality
     * @return object
     */
    public function buildSpecializationObject(Speciality $speciality)
    {
        try {
            return (object)array(
                Constant::_ID => $speciality->getId(),
                Constant::_NAME => $speciality->getDescription()
            );
        } catch (\Exception $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}