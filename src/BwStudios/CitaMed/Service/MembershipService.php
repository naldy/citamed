<?php

namespace BwStudios\CitaMed\Service;

//Constants
use BwStudios\CitaMed\Constant\Constant;

//Entities
use BwStudios\CitaMed\Entity\Login;
use BwStudios\CitaMed\Entity\Membership;
use BwStudios\CitaMed\Entity\User;
use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\UserType;
use BwStudios\CitaMed\Entity\Speciality;
use BwStudios\CitaMed\Entity\Gender;
use BwStudios\CitaMed\Entity\DocumentType;
use BwStudios\CitaMed\Entity\RelationShip;
use BwStudios\CitaMed\Entity\Eps;
use BwStudios\CitaMed\Entity\UserConnectionOneTwo;
use BwStudios\CitaMed\Entity\UserConnectionOneThree;
use BwStudios\CitaMed\Entity\UserConnectionThreeFour;
use BwStudios\CitaMed\Entity\UserPlaceConnection;
use BwStudios\CitaMed\Entity\Place;
use BwStudios\CitaMed\Entity\UserEpsConnection;

//Symfony utilities
use BwStudios\CitaMed\Utility\MailService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use DoctrineExtensions\Query\Mysql\Cos;
//use Proxies\__CG__\BwStudios\CitaMed\Entity\UserEpsConnection;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;


class MembershipService
{
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryPlace;
    private $repositorySpeciality;
    private $repositoryGender;
    private $repositoryDocumentType;
    private $repositoryRelationShip;
    private $repositoryEps;
    private $repositoryAppointment;
    private $repositoryUserConnectionOneTwo;
    private $repositoryUserConnectionOneThree;
    private $repositoryUserConnectionThreeFour;
    private $repositoryUserPlaceConnection;
    private $repositoryUserEpsConnection;
    private $repositoryMembership;
    private $em;
    private $mailService;

    /**
     * MembershipService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUser = $this->em->getRepository(Constant::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(Constant::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(Constant::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(Constant::ENTITY_USER_TYPE);
        $this->repositoryPlace = $this->em->getRepository(Constant::ENTITY_PLACE);
        $this->repositorySpeciality = $this->em->getRepository(Constant::ENTITY_SPECIALITY);
        $this->repositoryGender = $this->em->getRepository(Constant::ENTITY_GENDER);
        $this->repositoryDocumentType = $this->em->getRepository(Constant::ENTITY_DOCUMENT_TYPE);
        $this->repositoryRelationShip = $this->em->getRepository(Constant::ENTITY_RELATION_SHIP);
        $this->repositoryEps = $this->em->getRepository(Constant::ENTITY_EPS);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryUserConnectionOneTwo = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_TWO);
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
        $this->repositoryUserConnectionThreeFour = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_THREE_FOUR);
        $this->repositoryUserPlaceConnection = $this->em->getRepository(Constant:: ENTITY_PLACE_CONNECTION);
        $this->repositoryUserEpsConnection = $this->em->getRepository(Constant:: ENTITY_USER_EPS_CONNECTION);
        $this->repositoryMembership = $this->em->getRepository(Constant::ENTITY_MEMBERSHIP);
        $this->mailService = new MailService();
    }

    public function setPeriods($timeLapseForFreeAppointments)
    {
        //Busco todos los medicos
        $allDoctors = $this->repositoryUser->findBy(array('userTypeId' => 1));

        foreach ($allDoctors as $doctor) {
            if ($doctor->getCreationDate()) {
                $flagWhile = true;
                $startPeriod = new \DateTime($doctor->getCreationDate()->format('Y-m-d'));
                $today = new \DateTime('today');
                do {
                    $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                    if ($today < $endPeriod) {
                        $flagWhile = false;
                    } else {
                        $startPeriod = $endPeriod;
                    }
                } while ($flagWhile);

                if ($startPeriod && $endPeriod) {
                    //Busco las citas para ese periodo y para ese medico
                    $appointmentsQuery = $this->repositoryAppointment->createQueryBuilder('q')
                        ->where('q.userOneId=' . $doctor->getId() . ' and q.creationDate>= \'' . $startPeriod->format('Y-m-d H:i:s') . '\' and q.creationDate<= \'' . $endPeriod->format('Y-m-d H:i:s') . '\'')
                        ->getQuery();
                    $appointmentsForDoctor = $appointmentsQuery->getResult();
                    $numberOfAppointments = sizeof($appointmentsForDoctor);

                    $membership = new Membership();
                    $membership->setUser($doctor);
                    $membership->setCurrentAppointments($numberOfAppointments);
                    $membership->setEndPeriod($endPeriod);
                    $membership->setStartPeriod($startPeriod);
                    $membership->setPayMembership(false);
                    $this->em->persist($membership);
                    $this->em->flush();
                }
            }
        }
        return true;
    }

    /**
     * @param Request $request
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @return array|object
     */
    public function getInfo(Request $request, $freeAppointments, $timeLapseForFreeAppointments)
    {
        if ($request->request->get('doctorId')) {
            $jsonResponse = array();
            $doctorEntity = $this->repositoryUser->find($request->request->get('doctorId'));
            $today = new \DateTime('today');

            if ($doctorEntity) {
                $membership = $this->repositoryMembership->findOneBy(array('user' => $doctorEntity->getId()));

                if (!$membership) {

                    //Si no existe una membresia se crea
                    if ($doctorEntity->getCreationDate() == null) {
                        $doctorEntity->setCreationDate($today);
                    }

                    $flagWhile = true;
                    $startPeriod = new \DateTime($doctorEntity->getCreationDate()->format('Y-m-d'));
                    do {

                        $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                        if ($today < $endPeriod) {
                            $flagWhile = false;
                        } else {
                            $startPeriod = $endPeriod;
                        }
                    } while ($flagWhile);

                    if ($startPeriod && $endPeriod) {
                        $membership = new Membership();
                        $membership->setUser($doctorEntity);
                        $membership->setCurrentAppointments(0);
                        $membership->setEndPeriod($endPeriod);
                        $membership->setStartPeriod($startPeriod);
                        $membership->setPayMembership(false);
                        $membership->setAppointmentsLimit($freeAppointments);
                        $this->em->persist($membership);
                        $this->em->flush();
                    }
                }

                $jsonResponse = (object)array(
                    'payMembership' => $membership->getPayMembership(),
                    'startPeriod' => $membership->getStartPeriod(),
                    'endPeriod' => $membership->getEndPeriod(),
                    'currentAppointments' => $membership->getCurrentAppointments()
                );

            } else {
                $jsonResponse = (object)array(
                    'status' => false,
                    'message' => 'User not found'
                );
            }
        }
        return $jsonResponse;
    }

    /**
     * @param $doctorId
     * @param $freeAppointments
     * @param $timeLapseForFreeAppointments
     * @return object
     */
    public function getInfoSync($doctorId, $freeAppointments, $timeLapseForFreeAppointments)
    {
        try {
            $doctorEntity = $this->repositoryUser->find($doctorId);
            $today = new \DateTime('today');

            if ($doctorEntity) {
                $membership = $this->repositoryMembership->findOneBy(array(Constant::USER => $doctorEntity->getId()));

                if (!$membership) {

                    //Si no existe una membresia se crea
                    if ($doctorEntity->getCreationDate() == null) {
                        $doctorEntity->setCreationDate($today);
                    }

                    $flagWhile = true;
                    $startPeriod = new \DateTime($doctorEntity->getCreationDate()->format('Y-m-d'));
                    do {

                        $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                        if ($today < $endPeriod) {
                            $flagWhile = false;
                        } else {
                            $startPeriod = $endPeriod;
                        }
                    } while ($flagWhile);

                    if ($startPeriod && $endPeriod) {
                        $membership = new Membership();
                        $membership->setUser($doctorEntity);
                        $membership->setCurrentAppointments(0);
                        $membership->setEndPeriod($endPeriod);
                        $membership->setStartPeriod($startPeriod);
                        $membership->setPayMembership(false);
                        $membership->setAppointmentsLimit($freeAppointments);
                        $this->em->persist($membership);
                        $this->em->flush();
                    }
                }

                return (object)array(
                    Constant::_EXPIRATION_DATE => $membership->getEndPeriod()->format(\DateTime::ISO8601),
                    Constant::_IS_PAID => $membership->getPayMembership(),
                    Constant::_APPOINTMENTS_LIMIT => $membership->getAppointmentsLimit(),
                    Constant::_APPOINTMENTS_COUNT => $membership->getCurrentAppointments()
                );
            } else {
                return $this->buildErrorObject(1);
            }
        } catch (ORMException $e) {
            error_log('catch getInfoSync ORMException');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        } catch (\Exception $e2) {
            error_log('catch getInfoSync Exception');
            error_log('Error: ' . $e2->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $notificationAppointmentsPercent
     * @param $emailsNotificationAppointmentsPercent
     * @param $userEntityDoctor
     */
    public function verifyAppointmentsPercent($notificationAppointmentsPercent,
                                              $emailsNotificationAppointmentsPercent, $userEntityDoctor)
    {
        try {
            $membership = $this->repositoryMembership->findOneBy(array(Constant::USER => $userEntityDoctor->getId()));

            if (isset($membership) && $membership->getPayMembership() == false) {
                $currentAppointments = $membership->getCurrentAppointments();
                $appointmentsLimit = $membership->getAppointmentsLimit();
                if ($currentAppointments && $appointmentsLimit) {
                    $currentPercent = ($currentAppointments * 100) / $appointmentsLimit;
                    if ($currentPercent == $notificationAppointmentsPercent) {
                        $doctorName = $userEntityDoctor->getGeneralDataUserId()->getName() . ' ' .
                            $userEntityDoctor->getGeneralDataUserId()->getLastName();
                        $this->mailService->sendNotificationAppointmentPercent($doctorName, $currentAppointments,
                            $appointmentsLimit, $emailsNotificationAppointmentsPercent, $notificationAppointmentsPercent);
                    }
                }
            }
        } catch (\Exception $e) {
            error_log('catch verifyAppointmentsPercent');
            error_log('Error: ' . $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $freeAppointments
     * @return object
     */
    public function purchase(Request $request, $freeAppointments)
    {
        try {
            $userId = $request->request->get('doctorId');
            $amount = $request->request->get('amount');
            $period = $request->request->get('period');
            if (isset($userId) && isset($amount) && isset($period)) {

                $userDoctor = $this->repositoryUser->find($userId);

                if ($userDoctor) {
                    $membership = $this->repositoryMembership->findOneBy(array(Constant::USER => $userId));

                    if ($membership) {
                        $now = new \DateTime('today');
                        $endPeriod = new \DateTime(strtolower($now->format('Y-m-d') . ' +' . $period . 'month'));

                        $membership->setCurrentAppointments(0);
                        $membership->setEndPeriod($endPeriod);
                        $membership->setStartPeriod($now);
                        $membership->setPayMembership(true);
                        $membership->setAppointmentsLimit($freeAppointments);
                        $this->em->flush();

                        return (object)array(
                            Constant::STATUS => true,
                            Constant::MESSAGE => 'successful'
                        );
                    } else {
                        return $this->buildErrorObject(15);
                    }
                } else {
                    return $this->buildErrorObject(1);
                }
            } else {
                return $this->buildErrorObject(2);
            }
        } catch (\Exception $e) {
            error_log('catch purchase');
            error_log('Error: ' . $e->getMessage());
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case 1:
                $text = 'User not found';
                break;
            case 2:
                $text = 'Missing parameters';
                break;
            case 3:
                $text = 'Missing document type';
                break;
            case 4:
                $text = 'Patient already exist';
                break;
            case 5:
                $text = 'Relationship not found';
                break;
            case 6:
                $text = 'Appointments limit';
                break;
            case 7:
                $text = 'Appointment not found';
                break;
            case 8:
                $text = 'Eps not found';
                break;
            case 9:
                $text = 'Place relationship not found';
                break;
            case 10:
                $text = 'Invalid email or password';
                break;
            case 12:
                $text = 'Missing user type';
                break;
            case 13:
                $text = 'Doctor already exist';
                break;
            case 14:
                $text = 'Assistant already exist';
                break;
            case 15:
                $text = 'Invalid data';
                break;
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}
