<?php

namespace BwStudios\CitaMed\Service;

use BwStudios\CitaMed\Constant\Constant;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class UserConnectionOneThreeService
{

    private $repositoryUserConnectionOneThree;

    private $em;

    /**
     * UserConnectionOneThreeService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryUserConnectionOneThree = $this->em->getRepository(Constant::ENTITY_USER_CONNECTION_ONE_THREE);
    }

    public function findAll()
    {
        return $this->repositoryUserConnectionOneThree->findAll();
    }

    public function findBy($criteria)
    {
        try {
            return $this->repositoryUserConnectionOneThree->findBy($criteria);
        } catch (ORMException $e) {
            return $this->buildErrorObject(-1);
        }
    }

    /**
     * @param $code
     * @return object
     */
    public function buildErrorObject($code)
    {
        $text = '';
        switch ($code) {
            case -1:
                $text = 'Internal Server Error';
                break;
        }
        return (object)array(
            Constant::_ERROR => (object)array(
                Constant::_CODE => $code,
                Constant::_TEXT => $text
            ));
    }
}