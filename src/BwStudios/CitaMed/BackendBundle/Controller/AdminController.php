<?php

namespace BwStudios\CitaMed\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session;

class AdminController extends Controller
{
    public function loginAdminAction() {
        $request = $this->get("request");

        $user = $request->get("user");
        $password = $request->get("password");

        if (!isset($user, $password)) {
            return new JsonResponse([
                "code" => JsonResponse::HTTP_NOT_FOUND,
                "message" => "Not found"
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        $em = $this->get('doctrine')->getManager();
        $login= $em->getRepository("Entity:Login")->findOneBy(array("user" => $user ,"password" => $password));
        if (count($login) == 0) {
            return new JsonResponse([
                "code" => JsonResponse::HTTP_OK,
                "message" => "User does not exists"
            ], JsonResponse::HTTP_OK);
        }
        return $this->render("BwStudiosCitaMedBackendBundle:login:login.json.twig", array('login' => $login));
    }


}







