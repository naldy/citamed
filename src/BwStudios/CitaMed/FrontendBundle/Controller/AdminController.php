<?php

namespace BwStudios\CitaMed\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function loginAdminAction()
    {
        return $this->render("BwStudiosCitaMedFrontendBundle:loginAdmin:login.html.twig");
    }
}





