<?php

namespace BwStudios\CitaMed\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function getDashboardAction()
    {
        return $this->render("BwStudiosCitaMedFrontendBundle:DashBoard:dashboard.html.twig");
    }
}





