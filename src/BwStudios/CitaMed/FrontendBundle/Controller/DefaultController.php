<?php

namespace BwStudios\CitaMed\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BwStudiosCitaMedFrontendBundle:Default:index.html.twig', array('name' => $name));
    }
}
