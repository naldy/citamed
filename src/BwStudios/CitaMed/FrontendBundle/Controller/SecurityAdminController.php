<?php

namespace BwStudios\CitaMed\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityAdminController extends Controller
{
    public function securityAdminAction()
    {
        return $this->render("BwStudiosCitaMedFrontendBundle:securityAdmin:security.html.twig");
    }
}
