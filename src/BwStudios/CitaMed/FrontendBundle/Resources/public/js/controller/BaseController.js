

if (localStorage.getItem('user') == null) {
    window.location.href = server + "front/login_admin";
}

function logout() {
    if (localStorage.getItem('user') != null) {
        localStorage.removeItem('user');
        window.location.href = server + "front/login_admin";
    }
}