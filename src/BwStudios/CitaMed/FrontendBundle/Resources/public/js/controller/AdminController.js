
var app;
app = angular.module('Admin', []).controller('AdminController', ['$scope', '$http', function ($scope, $http) {

    if(localStorage.getItem('user')!= null ){
        window.location.href = server + "front/dashboard";}

$scope.user="";
$scope.password="";
$('#alert-danger').hide();
$('#alert-warning').hide();
    $scope.login = function() {

        if($scope.user == null || $scope.user.length == 0 || /^\s+$/.test($scope.user) ) {

            $('#alert-danger').show();
            setTimeout(function () {
                $('#alert-danger').hide();
            }, 1000);
            return false;
        }

        if($scope.password == null || $scope.password.length == 0 || /^\s+$/.test($scope.password) ) {
            $('#alert-danger').show();
            setTimeout(function () {
                $('#alert-danger').hide();
            }, 1000);
            return false;
        }

        $http({
            method: 'GET',
            url: server + 'api/login_admin?user=' + $scope.user + "&password=" + $scope.password
        }).success(function (data, status,headers, config) {
            console.log("success: " + JSON.stringify(data));

           if ( data.login != undefined) {
               localStorage.setItem('user', document.getElementById('user').value);console.log("setItem");
                window.location.href = server + "front/dashboard";
            } else {
                $('#alert-warning').show();
                setTimeout(function () {
                    $('#alert-warning').hide();
                }, 2500);
            }
        }).error(function (data, status,headers, config) {
            console.log("error: " + data.message);

        });
    };
}]);



