<page>
    <style type="text/css">
        .auto-style1 {
            font-size: 28pt;
            font-family: Roboto;
            color: #282626;
        }

        .auto-style2 {
            font-size: 13pt;
            font-family: Roboto;
            color: #282626;
        }

        .auto-style4 {
            font-size: 14pt;
            font-family: Roboto;
            color: #282626;
        }

        .auto-style5 {
            width: 90px;
            border-radius: 8px;
            border: 1px solid;
            display: inline-block;
            cursor: pointer;
            color: #282626;
            font-family: Roboto;
            font-size: 13px;
            font-weight: bold;
            padding: 6px 12px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #5b8a3c;
            text-align: center;
        }

        .div-green {
            background-color: #9DC95C;
        }

        .div-red {
            background-color: #FF7575;
        }
    </style>

    <table style="width: 100%">
        <tr>
            <td style="width: 540px" class="auto-style2">Informe diario de citas</td>
            <td rowspan="3" valign="top">
                <img height="50" src="<?php echo dirname(__FILE__) . '/logotodoc.png' ?>" width="150"/></td>
        </tr>
        <tr>
            <td style="width: 540px" class="auto-style1"><strong><?php echo $fullDoctorName ?></strong></td>
        </tr>
        <tr>
            <td style="width: 540px" class="auto-style2"><strong><?php echo $fullDate ?></strong></td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <table style="width: 100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="auto-style4" style="width: 35px"><strong>No.</strong></td>
            <td class="auto-style4" style="width: 90px"><strong>Inicio</strong></td>
            <td class="auto-style4" style="width: 80px"><strong>Fin</strong></td>
            <td class="auto-style4" style="width: 160px"><strong>Paciente</strong></td>
            <td class="auto-style4" style="width: 200px"><strong>Razón de la cita</strong></td>
            <td class="auto-style4" style="width: 30px"><strong>Estado</strong></td>
        </tr>
        <?php if ($appointments != null) {
            $count = 1;
            foreach ($appointments as $appointment) {

                if ($appointment->canceled) {
                    $value = 'CANCELADA';
                    $style = "auto-style5 div-red";
                } else {
                    if ($appointment->confirmed) {
                        if ($appointment->confirmed == true) {
                            $value = 'CONFIRMADA';
                            $style = "auto-style5 div-green";
                        }
                    } else {
                        $value = 'PENDIENTE';
                        $style = "auto-style5";
                    }
                }
                ?>
                <tr>
                    <td class="auto-style2" style="width: 35px"><?php echo $count ?></td>
                    <td class="auto-style2" style="width: 90px"><?php echo $appointment->hour ?></td>
                    <td class="auto-style2" style="width: 80px"><?php echo $appointment->endHour ?></td>
                    <td class="auto-style2" style="width: 160px"><?php echo $appointment->patientName ?></td>
                    <td class="auto-style2" style="width: 200px"><?php echo 'EPS: ' . $appointment->eps . '<br>' .
                            $appointment->observations ?></td>
                    <td style="width: 30px">
                        <div class="<?php echo $style ?>">
                            <strong>
                                <?php echo $value ?>
                            </strong>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <hr>
                    </td>
                </tr>
                <?php
                $count++;
            }
        } ?>
    </table>
    <p class="auto-style4">&nbsp;</p>
    <p class="auto-style4"><span class="auto-style2">Si desea ver las estadisticas de gestión de su consultorio, haga clic en el
ícono de </span>
        <img height="14" src="<?php echo dirname(__FILE__) . '/navicon-round.png' ?>" width="18"/>
        <span class="auto-style2"> y seleccióne "ver estadísticas".</span>
    </p>
</page>