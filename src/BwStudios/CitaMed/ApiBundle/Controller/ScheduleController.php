<?php


namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ScheduleController extends Controller
{

    /**
     * @ApiDoc(
     *  description="Get daysMorning"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getDaysMorningAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.scheduleService')->availableTime($request);
            if (isset($userService)) {

                $jsonResponse = /*json_encode(*/$userService/*)*/;

            } else {

                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get list all dates"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListAllDatesAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)){
            $scheduleService = $this->get('api.scheduleService')->getListAllDates($request);
            if ($scheduleService != null){
                $jsonResponse = json_encode($scheduleService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }


}
