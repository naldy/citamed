<?php

namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class StatisticController extends Controller
{
    /**
     * @ApiDoc(
     *  description="Get current patient number"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getCurrentPatientNumberAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $scheduleService = $this->get('api.statisticService')->getCurrentPatientNumber($request);
            if ($scheduleService != null) {
                $jsonResponse = json_encode($scheduleService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get attended users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAttendedUsersAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getAttendedUsers($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get Average"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAverageAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.statisticService')->getAverage($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));


        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get Canceled"
     * )
     * @param Request $request
     * @return JsonResponse
     */

    public function getCanceledAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.statisticService')->getCanceled($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get canceled users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getCanceledUsersAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getCanceledUsers($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);

            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get Attend"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAttendAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.statisticService')->getAttend($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);

            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get not arrived users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getNotArrivedAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getNotArrived($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);

            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get patient's gender"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getPatientGenderAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getPatientGender($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * Calcular edades de pacientes
     * @param Request $request
     * @return Response
     */
    public function getPatientAgeAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getPatientAge($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get not arrived users all"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllNotArrivedAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getAllNotArrived($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get one time patient number"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getOneTimePatientAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->getOneTimePatient($request);
            if ($userStatisticService != null) {
                $jsonResponse = json_encode($userStatisticService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    //TODO: pendiente
    public function datesPerDayLastThirtyAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userStatisticService = $this->get('api.statisticService')->datesPerDayLastThirty($request);
            return new Response(json_encode($userStatisticService));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $req
     * @return Response
     */
    public function statisticsPerMonthAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($req)) {
            $statisticsPerMonth = $this->get('api.statisticService')->statisticsPerMonth($req);
            $response = new Response(json_encode($statisticsPerMonth));
        } else {
            $jsonValidate = (object)array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            $response = new Response(json_encode($jsonValidate));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $req
     * @return Response
     */
    public function statisticsPerDayAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($req)) {
            $statisticsPerDay = $this->get('api.statisticService')->statisticsPerDay($req);
            $response = new Response(json_encode($statisticsPerDay));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            $response = new Response(json_encode($jsonValidate));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    public function mainStatisticsPerMonthAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($req)) {
            $statisticsPerDay = $this->get('api.statisticService')->mainStatisticsPerMonth($req);
            $response = new Response(json_encode($statisticsPerDay));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            $response = new Response(json_encode($jsonValidate));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function mainStatisticsPerYearAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($req)) {
            $statisticsPerDay = $this->get('api.statisticService')->mainStatisticsPerYear($req);
            $response = new Response(json_encode($statisticsPerDay));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            $response = new Response(json_encode($jsonValidate));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function mainStatisticsAllTimesAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($req)) {
            $statisticsPerDay = $this->get('api.statisticService')->mainStatisticsAllTimes($req);
            $response = new Response(json_encode($statisticsPerDay));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            $response = new Response(json_encode($jsonValidate));
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}