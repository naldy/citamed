<?php
namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PayController extends Controller
{
    public function payUStepOneAction(Request $req)
    {
        $payService = $this->get('api.payService');
        return $this->render(dirname(__DIR__) . '/Resources/views/payU/payU-step-one.html.twig', $payService->payUStepOne($req));
    }
}