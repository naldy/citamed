<?php

namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PlaceController extends Controller
{

    /**
     * @ApiDoc(
     *  description="Get Address"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getAddressAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.placeService')->getAddress($request);
            if (isset($userService)) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get eps"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getEpsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.placeService')->getEps($request);
            if (isset($userService)) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get eps"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserEpsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.placeService')->getUserEps($request);
            if (isset($userService)) {

                $jsonResponse = json_encode($userService);

            } else {

                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="tell Friend"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function tellFriendAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.placeService')->tellFriend($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

}