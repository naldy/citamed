<?php
namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserController extends Controller
{
    /**
     * @ApiDoc(
     *  description="Validate Login user"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function loginUserAction(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->loginUser($request, $hashAlgorithm);
            if (isset($userService)) {

                $jsonResponse = json_encode($userService);

            } else {
                $jsonResponse = array(Constant::VALIDATE => Constant::VALIDATE_BOOL_F);
            }

            //return new Response(json_encode($validate),201,array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'));
            return new Response(json_encode($jsonResponse));
            /**$response = new Response(json_encode($validate));
             * $response->headers->set('Access-Control-Allow-Origin', '*');
             * return $response;**/

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            //return new Response(json_encode($jsonValidate),201,array('Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json'));
            return new Response(json_encode($jsonValidate));
            /**$response = new Response(json_encode($jsonValidate));
             * $response->headers->set('Access-Control-Allow-Origin', '*');
             * return $response;**/
        }

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function loginUserV1Action(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');
            return new Response(json_encode($userService->loginUserV1($request, $hashAlgorithm)));
        } else {
            return new Response(json_encode((object)array(
                Constant::_ERROR => (object)array(
                    Constant::_CODE => 11,
                    Constant::_TEXT => 'JSON bad format'
                )
            )));
        }
    }

    /**
     * @ApiDoc(
     *  description="Create user"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function createUserAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->createUser($request, $hashAlgorithm);
            if (isset($userService)) {

                $jsonResponse = json_encode((object)$userService[0]);

            } else {

                $jsonResponse = array(Constant::STATUS_NAME => false, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response($jsonResponse);

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function registerDoctorV1Action(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        if ($jsonFormatter->jsonFormatter($request)) {
            $jsonResponse = $this->get('api.userService')->registerDoctorV1($request, $hashAlgorithm);
            return new Response(json_encode($jsonResponse));
        } else {
            return new Response(json_encode((object)array(
                Constant::_ERROR => (object)array(
                    Constant::_CODE => 11,
                    Constant::_TEXT => 'JSON bad format'
                )
            )));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getUserAction(Request $request)
    {
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getUser($request, $freeAppointments,
                $timeLapseForFreeAppointments);
            if (isset($userService)) {

                $jsonResponse = json_encode($userService);

            } else {

                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }


    /**
     * @ApiDoc(
     *  description="update user"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserAction(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->updateUser($request, $hashAlgorithm)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateDoctorV1Action(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            return new Response(json_encode($userService->updateDoctorV1($request, $hashAlgorithm)));

        } else {
            return new Response(json_encode((object)array(
                Constant::_ERROR => (object)array(
                    Constant::_CODE => 11,
                    Constant::_TEXT => 'JSON bad format'
                )
            )));
        }
    }


    /**
     * @ApiDoc(
     *  description="User Recovery Password "
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function userRecoveryPasswordAction(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->userRecoveryPassword($request, $hashAlgorithm)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function userRecoveryPasswordV1Action(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');
            return new Response(json_encode($userService->userRecoveryPasswordV1($request, $hashAlgorithm)));
        } else {
            return new Response(json_encode((object)array(
                Constant::_ERROR => (object)array(
                    Constant::_CODE => 11,
                    Constant::_TEXT => 'JSON bad format'
                )
            )));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function forgotPasswordAction(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->forgotPassword($request, $hashAlgorithm);
            if (isset($userService)) {

                $jsonResponse = json_encode($userService);

            } else {

                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }

            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function forgotPasswordV1Action(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');
            return new Response(json_encode($userService->forgotPasswordV1($request, $hashAlgorithm)));
        } else {
            return new Response(json_encode((object)array(
                Constant::_ERROR => (object)array(
                    Constant::_CODE => 11,
                    Constant::_TEXT => 'JSON bad format'
                )
            )));
        }
    }

    /**
     * @ApiDoc(
     *  description="Create date"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function createDateAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->createDate($request)) {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }


    /**
     * @ApiDoc(
     *  description="user disable"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function userDisableAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');
            if ($userService->userDisable($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);
            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     *  description="Get list not confirmed users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListNotConfirmedUserAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getListNotConfirmedUser($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get list not arrived users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListNotArrivedUserAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getListNotArrivedUser($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get list canceled users"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListCanceledUserAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getListCanceledUser($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="update password"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePasswordAction(Request $request)
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->updatePassword($request, $hashAlgorithm)) {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get list all specialit"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getSpecialityAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getSpeciality($request);
            if (isset($userService)) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Create date"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function createSpecialityAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->createSpeciality($request)) {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @ApiDoc(
     *  description="Get list all my patients"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListAllMyPatientsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getListAllMyPatients($request);
            if (isset($userService)) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }


    /**
     * @ApiDoc(
     *  description="Get Gender"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getGenderAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->getGender($request);
            if (isset($userService)) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        }
    }

    /**
     * @ApiDoc(
     *  description="Remove user from pending list"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function removeUserFromPendingListAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService');

            if ($userService->removeUserFromPendingList($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);
            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));


        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }

    }

    /**
     *  description="list assistant"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function listAssistantAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->listAssistant($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     *  description="remove Eps"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function removeUserEpsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->removeUserEps($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     *  description="remove Eps"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function removeOfficeAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.userService')->removeOffice($request);
            if ($userService != null) {
                $jsonResponse = json_encode($userService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @return Response
     */
    public function encryptPasswordsAction()
    {
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        return new Response(json_encode($this->get('api.userService')->encryptPasswords($hashAlgorithm)));
    }

    /**
     * @return Response
     */
    public function setCreationDateAction()
    {
        return new Response(json_encode($this->get('api.userService')->setCreationDate()));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function pdfRememberAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            return new Response(json_encode($this->get('api.userService')->pdfRemembers()));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function recommendAppAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $response = new Response(json_encode($this->get('api.userService')->recommendApp($request)));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return new Response(json_encode((object)array(
                'status' => false,
                'message' => 'Error validating request'
            )));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function setPersonalTitleAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $response = new Response(json_encode($this->get('api.userService')->setPersonalTitle()));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return new Response(json_encode((object)array(
                'status' => false,
                'message' => 'Error validating request'
            )));
        }
    }

    public function registerCalendarTokenAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $response = new Response(json_encode($this->get('api.userService')->registerCalendarToken($request)));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return new Response(json_encode((object)array(
                'status' => false,
                'message' => 'Error validating request'
            )));
        }
    }
}