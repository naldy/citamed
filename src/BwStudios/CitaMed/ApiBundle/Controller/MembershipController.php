<?php
namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;

class MembershipController extends Controller
{
    /**
     * @return Response
     */
    public function setPeriodsAction()
    {
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');
        return new Response(json_encode($this->get('api.membershipService')->setPeriods($timeLapseForFreeAppointments)));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getInfoAction(Request $request)
    {
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $data = $this->get('api.membershipService')->getInfo($request, $freeAppointments, $timeLapseForFreeAppointments);
            if ($data != null) {
                $jsonResponse = $data;
            } else {
                $jsonResponse = (object)array();
            }
            $response = new Response(json_encode($jsonResponse));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode((object)$jsonValidate));
        }
    }

    public function purchaseAction(Request $request)
    {
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $data = $this->get('api.membershipService')->purchase($request, $freeAppointments);
            if ($data != null) {
                $jsonResponse = $data;
            } else {
                $jsonResponse = (object)array();
            }
            $response = new Response(json_encode($jsonResponse));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode((object)$jsonValidate));
        }
    }
}