<?php
namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BwStudios\CitaMed\Constant\Constant;

class DateController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function createDateAction(Request $request)
    {
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService')->createDate($request, $freeAppointments, $timeLapseForFreeAppointments);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function updateDateAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.dateService');

            if ($userService->updateDate($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {

                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);

            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getDateAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService')->getDate($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createTimeFrameAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService');

            if ($dateService->createTimeFrame($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function confirmDateAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');

        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService');

            if ($dateService->confirmDate($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cancelDateAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');

        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService');

            if ($dateService->cancelDate($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function appointmentPaymentAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');

        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService');

            if ($dateService->appointmentPayment($request)) {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_SUCCESSFUL, Constant::MESSAGE_NAME => Constant::STATUS_SUCCESSFUL);

            } else {
                $validate = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_SUCCESSFUL);
            }
            return new Response(json_encode($validate));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function mailTwoDaysAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.dateService')->mailTwoDays($request);
            if ($dateService != null) {
                $jsonResponse = json_encode($dateService);
            } else {
                $jsonResponse = array(Constant::STATUS_NAME => Constant::STATUS_DESCRIPTION, Constant::MESSAGE_NAME => Constant::STATUS_NOT_FOUND);
            }
            return new Response(json_encode($jsonResponse));

        } else {
            $jsonValidate = array(Constant::VALIDATE => Constant::WRONG_JSON_FORMATTER);
            return new Response(json_encode($jsonValidate));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function sendNotificationsAction(Request $request)
    {
        $dateService = $this->get('api.dateService');
        $dateService->sendNotifications();
        return new Response(json_encode('Endpoint called successful'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function confirmedDataEmailAction(Request $request)
    {
        $userService = $this->get('api.dateService');
        $statusConfirmed = $userService->confirmedDataEmail($request);
        if ($statusConfirmed == 1) {
            $message = 'Tu cita ha sido confirmada :)';
            return $this->render('date/confirmacion.html.twig', array('message' => $message));
        } elseif ($statusConfirmed == 2) {
            $message = 'Esta cita ya no existe';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusConfirmed == 3) {
            $message = 'Esta cita ya caduco';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusConfirmed == 4) {
            $message = 'Esta cita ya fue confirmada';
            return $this->render('date/confirmacion.html.twig', array('message' => $message));
        } elseif ($statusConfirmed == 5) {
            $message = 'Esta cita ya fue cancelada';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cancelDataEmailAction(Request $request)
    {
        //$jsonFormatter = $this->get('utility.jsonService');
        // if ($jsonFormatter->jsonFormatter($request)) {
        $userService = $this->get('api.dateService');
        $statusCanceled = $userService->cancelDataEmail($request);
        if ($statusCanceled == 1) {
            $message = 'Tu cita ha sido cancelada. Si quieres reagendarla, no dudes en contactar al consultorio.';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusCanceled == 2) {
            $message = 'Esta cita ya no existe';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusCanceled == 3) {
            $message = 'Esta cita ya caduco';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusCanceled == 4) {
            $message = 'Esta cita ya fue confirmada';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        } elseif ($statusCanceled == 5) {
            $message = 'Esta cita ya fue cancelada';
            return $this->render('date/cancelacion.html.twig', array('message' => $message));
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cancelDataAppAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        if ($jsonFormatter->jsonFormatter($request)) {
            $userService = $this->get('api.dateService');
            $statusCanceled = $userService->cancelDataApp($request);

            if ($statusCanceled == 1) {
                $validate = 'Su cita a sido cancelada correctamente';
            } elseif ($statusCanceled == 2) {
                $validate = 'esta cita ya no existe';
            } elseif ($statusCanceled == 3) {
                $validate = 'esta cita ya caduco';
            } elseif ($statusCanceled == 4) {
                $validate = 'esta cita ya fue confirmada';
            } elseif ($statusCanceled == 5) {
                $validate = 'Esta cita ya fue cancelada';
            } elseif ($statusCanceled == 6) {
                $validate = 'Operacion no permitida';
            }
            return new Response(json_encode($validate));
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }

    /**
     * @param Request $request
     * @param $userId
     * @return Response
     */
    public function resendsmsAction(Request $request, $userId)
    {
        $dateService = $this->get('api.dateService');
        $responseService = $dateService->resendsms($userId);
        //$this->var_error_log($responseService);
        if ($responseService[Constant::STATUS_NAME]) {
            $response = new Response();
            $response->setContent(json_encode($responseService['data']));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $response = new Response();
            $response->setContent(json_encode($responseService));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function setToSendSMSAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($request);
        $dateService = $this->get('api.dateService');
        if ($dateService->setToSendSMS($request)) {
            $response = new Response();
            $response->setContent(json_encode('successful'));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            $response = new Response();
            $response->setContent(json_encode('notSuccessful'));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAllByPatientAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($request);
        $dateService = $this->get('api.dateService');

        $response = new Response();
        $response->setContent(json_encode($dateService->listAllByPatient($request)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @return Response
     */
    public function setEpsIdAction()
    {
        $dateService = $this->get('api.dateService');
        $response = new Response();
        $response->setContent(json_encode($dateService->setEpsId()));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($request);
        $dateService = $this->get('api.dateService');
        $response = new Response(json_encode($dateService->edit($request)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function removeNotificationsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($request);
        $dateService = $this->get('api.dateService');
        $response = new Response(json_encode($dateService->removeNotifications()));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function setSendNotificationsAction(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($request);
        $dateService = $this->get('api.dateService');
        $response = new Response(json_encode($dateService->setSendNotifications($request)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}

