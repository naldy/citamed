<?php
namespace BwStudios\CitaMed\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SyncController extends Controller
{
    public function dataV1Action(Request $request)
    {
        $notificationAppointmentsPercent = $this->container->getParameter('notificationAppointmentsPercent');
        $emailsNotificationAppointmentsPercent = $this->container->getParameter('emailsNotificationAppointmentsPercent');
        $hashAlgorithm = $this->container->getParameter('hashAlgorithm');
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        if ($jsonFormatter->jsonFormatter($request)) {
            $jsonResponse = $this->get('api.syncService')->syncDataV1($request, $freeAppointments, $timeLapseForFreeAppointments, $hashAlgorithm,
                $notificationAppointmentsPercent, $emailsNotificationAppointmentsPercent);
        } else {
            //TODO: error en la validacion
        }
        $response = new Response(json_encode($jsonResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}

