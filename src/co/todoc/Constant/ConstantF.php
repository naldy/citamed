<?php

namespace co\todoc\Constant;

class ConstantF
{

    //Constantes para Google Calendar API
    const APPLICATION_NAME = 'todoc-dev';
    const CLIENT_SECRET_PATH = 'Resources/calendars/gCalendar/client_secret.json';
    const CREDENTIALS_PATH = 'src/co/todoc/PublicAPIBundle/Resources/calendars/gCalendar/calendar-php-quickstart.json';
    const ENTITY_GOOGLE_CALENDAR_TOKEN = 'Calendar:GCalendarToken';
    const ENTITY_USER = 'Entity:User';
    const ENTITY_USER_TYPE = 'Entity:UserType';
    const ENTITY_EXTRA_DATA_USER = 'Entity:ExtraDataUser';
    const ENTITY_GENERAL_DATA_USER = 'Entity:GeneralDataUser';
    const ENTITY_APPOINTMENT = 'Entity:Appointment';
    const ENTITY_MEMBERSHIP = 'Entity:Membership';
    const ENTITY_USER_CONNECTION_ONE_THREE = 'Entity:UserConnectionOneThree';
    const ENTITY_USER_PLACE_CONNECTION = 'Entity:UserPlaceConnection';
    const ENTITY_EPS = 'Entity:Eps';
    const ENTITY_NOTIFICATION = 'Entity:Notification';
    const ENTITY_NOTIFICATION_TYPE = 'Entity:NotificationType';
    const EMAIL = 'email';
    const USER = 'user';
    const EMAIL_UNR = 'emailUNR';
    const ACCESS_TOKEN = 'access_token';
    const TOKEN_TYPE = 'token_type';
    const EXPIRES_IN = 'expires_in';
    const REFRESH_TOKEN = 'refresh_token';
    const ID_TOKEN = 'id_token';
    const CREATED = 'created';
    const USER_ID = 'userId';
    const STATUS = 'status';
    const MESSAGE = 'message';
    const USER_ONE_ID = 'userOneId';
    const USER_THREE_ID = 'userThreeId';
    const APPOINTMENTS = 'appointments';
    const DATE_ID = 'dateId';
    const DATE = 'date';
    const ASSIGNED_TIME = 'assignedTime';
    const CONFIRMED = 'confirmed';
    const ATTENDED = 'attended';
    const TIME_FRAME = 'timeFrame';
    const PLACE = 'place';
    const CREATION_DATE = 'creationDate';
    const PATIENT = 'patient';
    const NAME = 'name';
    const LAST_NAME = 'lastName';
    const CELL_PHONE = 'cellPhone';
    const HOME_PHONE = 'homePhone';
    const ID = 'id';
    const COMMENT = 'comment';
    const EPS_NAME = 'epsName';
    const CANCELED = 'canceled';
    const DATA = 'data';
    const CODE = 'code';
    const ERROR = 'error';
    const TYPE = 'type';

    //GOOGLE CALENDAR
    const OFFLINE = 'offline';
    const REDIRECT_URI_DEV = 'http://todoc.co/public_api_dev/calendar/oauth_google_callback';
    const REDIRECT_URI_PROD = 'http://todoc.co/public_api/calendar/oauth_google_callback';
    const REDIRECT_URI = self::REDIRECT_URI_DEV;
    const GOOGLE = 'google';

    //OUTLOOK
    const OUTLOOK_CLIENT_ID = '7a7c2029-24b3-4ecd-bec6-cce0256e4009';
    const OUTLOOK_CLIENT_SECRET = 'OfhNXpbCjLniv27AjQi1mVX';
    const OUTLOOK_AUTHORITY = "https://login.microsoftonline.com";
    const OUTLOOK_REDIRECT_URI = 'http://todoc.co/public_api/calendar/oauth_outlook_callback';

    const SUMMARY = 'summary';
    const DESCRIPTION = 'description';
    const LOCATION = 'location';
    const START = 'start';
    const END = 'end';
    const USER_EMAIL = 'userEmail';
    const DOCTOR_ID = 'doctorId';
    const PLACE_ID = 'placeId';
    const EPS_ID = 'epsId';
    const TYPE_DOCTOR = 1;
    const TODAY = 'today';
    const TYPE_PATIENT = 3;
    const DEFAULT_EPS = 16;
    const SEND_NOTIFICATIONS = 'sendNotifications';
    const ATTENDEES_EMAIL = 'attendeesEmial';
}