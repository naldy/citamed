<?php

namespace co\todoc\PublicAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalendarController extends Controller
{
    public function authAction()
    {
        $calendarService = $this->get('public_api.calendarService');
        $urlServices = $calendarService->auth();
        if ($urlServices != null) {
            $authGoogleUrl = $urlServices['google'];
            $outlookLoginUri = $urlServices['outlook'];
        }

        return $this->render('cotodocPublicAPIBundle:calendars:calendars_oauth.html.twig', array('authGoogleUrl' => $authGoogleUrl,
            'outlookLoginUri' => $outlookLoginUri));
    }

    public function oauthGoogleCallbackAction(Request $req)
    {
        $calendarService = $this->get('public_api.calendarService');
        $message = $calendarService->oauthGoogleCallback($req);

        return $this->render('cotodocPublicAPIBundle:calendars:calendars_oauth_result.html.twig', array('message' => $message));
    }

    public function oauthOutlookCallbackAction(Request $req)
    {
        $calendarService = $this->get('public_api.calendarService');
        $message = $calendarService->oauthOutlookCallback($req);

        return $this->render('cotodocPublicAPIBundle:calendars:calendars_oauth_result.html.twig', array('message' => $message));
    }

    public function getEventsUNRAction(Request $req)
    {

        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($req);

        $calendarService = $this->get('public_api.calendarService');
        $jsonResponse = $calendarService->getEventsUNR($req);

        $response = new Response(json_encode($jsonResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function newEventUNRAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($req);

        $calendarService = $this->get('public_api.calendarService');
        $jsonResponse = $calendarService->newEventUNR($req);

        $response = new Response(json_encode($jsonResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
