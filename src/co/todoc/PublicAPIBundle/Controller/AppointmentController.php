<?php

namespace co\todoc\PublicAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AppointmentController extends Controller
{

    /**
     * @param Request $req
     * @return Response
     */
    public function getAppointmentsURAction(Request $req)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($req);

        $appointmentService = $this->get('public_api.AppointmentService');
        $jsonResponse = $appointmentService->getAppointmentsUR($req);

        $response = new Response(json_encode($jsonResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param Request $req
     * @return Response
     */
    public function newURAction(Request $req)
    {
        $freeAppointments = $this->container->getParameter('freeAppointments');
        $timeLapseForFreeAppointments = $this->container->getParameter('timeLapseForFreeAppointments');

        $jsonFormatter = $this->get('utility.jsonService');
        $jsonFormatter->jsonFormatter($req);

        $appointmentService = $this->get('public_api.AppointmentService');
        $jsonResponse = $appointmentService->newUR($req, $timeLapseForFreeAppointments, $freeAppointments);

        $response = new Response(json_encode($jsonResponse));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
