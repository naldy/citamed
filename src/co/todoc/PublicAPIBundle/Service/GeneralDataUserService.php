<?php

namespace co\todoc\PublicAPIBundle\Service;

use co\todoc\Constant\ConstantF;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BwStudios\CitaMed\Entity\GeneralDataUser;

class GeneralDataUserService
{
    private $repositoryGeneralDataUser;

    private $em;

    /**
     * GeneralDataUserService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryGeneralDataUser = $this->em->getRepository(ConstantF::ENTITY_GENERAL_DATA_USER);
    }

    public function findOneBy($criteria)
    {
        return $this->repositoryGeneralDataUser->findOneBy($criteria);
    }

    public function find($id)
    {
        return $this->repositoryGeneralDataUser->find($id);
    }

    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}