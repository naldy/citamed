<?php

namespace co\todoc\PublicAPIBundle\Service;

use BwStudios\CitaMed\Entity\ExtraDataUser;
use BwStudios\CitaMed\Entity\GeneralDataUser;
use BwStudios\CitaMed\Entity\Notification;
use BwStudios\CitaMed\Utility\MailService;
use co\todoc\Constant\ConstantF;
use co\todoc\Entity\GCalendarToken;
use Doctrine\ORM\EntityManager;
use Google_Service_Calendar;
use Google_Service_Plus;
use Google_Service_Drive;
use Google_Client;
use Google_Service_Calendar_Event;
use Symfony\Component\HttpFoundation\Request;
use BwStudios\CitaMed\Entity\Appointment;

class AppointmentService
{
    private $repositoryGCalendarToken;
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryAppointment;

    //Servicios
    private $membershipService;
    private $calendarService;
    private $userService;
    private $generalDataUserService;
    private $extraDataUserService;
    private $userConnectionOneThreeService;
    private $userPlaceConnectionService;
    private $epsService;
    private $notificationService;
    private $notificationTypeService;

    private $em;

    /**
     * AppointmentService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryGCalendarToken = $this->em->getRepository(ConstantF::ENTITY_GOOGLE_CALENDAR_TOKEN);
        $this->repositoryUser = $this->em->getRepository(ConstantF::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(ConstantF::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(ConstantF::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(ConstantF::ENTITY_USER_TYPE);
        $this->repositoryAppointment = $this->em->getRepository(ConstantF::ENTITY_APPOINTMENT);

        //Servicios
        $this->membershipService = new MembershipService($this->em);
        $this->calendarService = new CalendarService($this->em);
        $this->userService = new UserService($this->em);
        $this->generalDataUserService = new GeneralDataUserService($this->em);
        $this->extraDataUserService = new ExtraDataUserService($this->em);
        $this->userConnectionOneThreeService = new UserConnectionOneThreeService($this->em);
        $this->userPlaceConnectionService = new UserPlaceConnectionService($this->em);
        $this->epsService = new EpsService($this->em);
        $this->notificationService = new NotificationService($this->em);
        $this->notificationTypeService = new NotificationTypeService($this->em);

    }

    /**
     * @param Request $req
     * @return object
     */
    public function getAppointmentsUR(Request $req)
    {
        if ($req->request->get(ConstantF::USER_ID)) {

            $today = new \DateTime('today');

            $doctorId = $req->request->get(ConstantF::USER_ID);

            //Buscamos el medico
            $userDoctor = $this->repositoryUser->find($doctorId);

            if ($userDoctor) {
                //Busco las citas del medico para hoy
                $appointmentQuery = $this->repositoryAppointment->createQueryBuilder('q')
                    ->where('q.' . ConstantF::USER_ONE_ID . '=' . $userDoctor->getId()
                        . ' and q.' . ConstantF::DATE . '>= \'' . $today->format('Y-m-d H:i')
                        . '\' and q.' . ConstantF::DATE . '<= \'' . $today->format('Y-m-d') . ' 23:59' . '\'')
                    ->orderBy('q.' . ConstantF::DATE)
                    ->getQuery();

                $allAppointments = $appointmentQuery->getResult();

                if (sizeof($allAppointments) > 0) {

                    $appointmentsArray = array();
                    foreach ($allAppointments as $appointment) {
                        if ($appointment->getCanceled() == false || $appointment->getCanceled() == null) {
                            $assignedTime = (integer)$appointment->getAssignedTime()->format('H') * 60 + (integer)$appointment->getAssignedTime()->format('i');
                            array_push($appointmentsArray, array(
                                ConstantF::DATE_ID => $appointment->getId(),
                                ConstantF::DATE => $appointment->getDate(),
                                ConstantF::ASSIGNED_TIME => $assignedTime,
                                ConstantF::CONFIRMED => $appointment->getConfirmed(),
                                ConstantF::ATTENDED => $appointment->getAttended(),
                                ConstantF::TIME_FRAME => $appointment->getTimeFrame(),
                                ConstantF::PLACE => $appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress(),
                                ConstantF::CREATION_DATE => $appointment->getCreationDate(),
                                ConstantF::PATIENT => (object)array(
                                    ConstantF::NAME => $appointment->getUserThreeId()->getGeneralDataUserId()->getName(),
                                    ConstantF::LAST_NAME => $appointment->getUserThreeId()->getGeneralDataUserId()->getLastName(),
                                    ConstantF::CELL_PHONE => $appointment->getUserThreeId()->getGeneralDataUserId()->getCellPhone(),
                                    ConstantF::HOME_PHONE => $appointment->getUserThreeId()->getGeneralDataUserId()->getHomePhone(),
                                    ConstantF::EMAIL => $appointment->getUserThreeId()->getGeneralDataUserId()->getEmail(),
                                    ConstantF::ID => $appointment->getUserThreeId()->getId(),
                                ),
                                ConstantF::COMMENT => $appointment->getComment(),
                                ConstantF::EPS_NAME => $appointment->getEps()->getDescription(),
                                ConstantF::CANCELED => $appointment->getCanceledByPatient()
                            ));
                        }
                    }

                    //Me traigo los eventos del calendario de google
                    $appointmentsArray = $this->calendarService->getGoogleEvents($userDoctor, null, $appointmentsArray);
                    //array_push($appointmentsArray, $googleEvents);

                    $jsonResponse = (object)array(
                        ConstantF::STATUS => true,
                        ConstantF::DATA => $appointmentsArray
                    );
                } else {
                    $jsonResponse = (object)array(
                        ConstantF::STATUS => true,
                        ConstantF::APPOINTMENTS => array()
                    );
                }
            } else {
                $jsonResponse = (object)array(
                    ConstantF::STATUS => false,
                    ConstantF::MESSAGE => 'user not found'
                );
            }
        } else {
            $jsonResponse = (object)array(
                ConstantF::STATUS => false,
                ConstantF::MESSAGE => 'parameters missing'
            );
        }
        return $jsonResponse;
    }

    public function newUR($req, $timeLapseForFreeAppointments, $freeAppointments)
    {
        $data = array(
            ConstantF::DATE => $req->request->get(ConstantF::DATE),
            ConstantF::COMMENT => $req->request->get(ConstantF::COMMENT),
            ConstantF::ASSIGNED_TIME => $req->request->get(ConstantF::ASSIGNED_TIME),
            ConstantF::DOCTOR_ID => $req->request->get(ConstantF::DOCTOR_ID),
            ConstantF::PLACE_ID => $req->request->get(ConstantF::PLACE_ID),
            ConstantF::EPS_ID => $req->request->get(ConstantF::EPS_ID),
            ConstantF::USER_ID => $req->request->get(ConstantF::USER_ID),
            ConstantF::SEND_NOTIFICATIONS => $req->request->get(ConstantF::SEND_NOTIFICATIONS)
        );

        if (isset($data[ConstantF::DATE]) && isset($data[ConstantF::ASSIGNED_TIME]) && isset($data[ConstantF::DOCTOR_ID])
            && isset($data[ConstantF::PLACE_ID]) && isset($data[ConstantF::EPS_ID])
            && isset($data[ConstantF::USER_ID])
        ) {

            //Buscamos el doctor por el id
            $userDoctor = $this->userService->find($data[ConstantF::DOCTOR_ID]);

            if ($userDoctor && $userDoctor->getUserTypeId()->getId() == ConstantF::TYPE_DOCTOR) {
                //Validator es true si el medico puede crear citas
                $validatorMembership = false;
                //Valido que el medico tenga una membresia valida
                $this->membershipService->validateMembership($userDoctor, $timeLapseForFreeAppointments);
                $membership = $this->membershipService->findOneBy(array(ConstantF::USER => $userDoctor->getId()));

                //Se valida si el medico es soft o heavy
                if ($membership->getPayMembership()) {
                    $validatorMembership = true;
                } else {
                    if ($membership->getCurrentAppointments() < $freeAppointments) {
                        $validatorMembership = true;
                    }
                }

                if ($validatorMembership) {

                    $generaDataDoctor = $this->generalDataUserService->findOneBy(array(ConstantF::ID => $userDoctor->getGeneralDataUserId()));
                    $extraDataDoctor = $this->extraDataUserService->findOneBy(array(ConstantF::ID => $userDoctor->getExtraDataUserId()));

                    //Se busca el paciente
                    $patientUser = $this->userService->find($data[ConstantF::USER_ID]);

                    //Si el paciente se encontro
                    if ($patientUser) {

                        //Se valida que el usuario sea de tipo paciente
                        if ($patientUser->getUserTypeId()->getId() == ConstantF::TYPE_PATIENT) {

                            //Se valida que el usuario y el doctor esten relacionados
                            $isInRelation = $this->userConnectionOneThreeService->findOneBy(array(ConstantF::USER_ONE_ID => $userDoctor->getId(), ConstantF::USER_THREE_ID => $patientUser->getId()));

                            if ($isInRelation) {

                                $generalDataPatient = $patientUser->getGeneralDataUserId();

                                $appointment = new Appointment();

                                $appointment->setUserOneId($userDoctor);
                                $appointment->setUserThreeId($patientUser);
                                $appointment->setDate(new \DateTime($data[ConstantF::DATE]));
                                $appointment->setComment($data[ConstantF::COMMENT]);
                                $appointment->setAssignedTime(new \DateTime($data[ConstantF::ASSIGNED_TIME]));

                                $userPlaceId = $this->userPlaceConnectionService->findOneBy(array(ConstantF::PLACE_ID => $data[ConstantF::PLACE_ID]));
                                $appointment->setUserPlaceConnectionId($userPlaceId);

                                $eps = $this->epsService->findOneBy(array(ConstantF::ID => $data[ConstantF::EPS_ID]));
                                if ($eps) {
                                    $appointment->setEps($eps);
                                } else {
                                    $eps = $this->epsService->find(ConstantF::DEFAULT_EPS);
                                    $appointment->setEps($eps);
                                }
                                $appointment->setCreationDate(new \DateTime());
                                $appointment->setSendNotifications($data[ConstantF::SEND_NOTIFICATIONS]);
                                $this->em->persist($appointment);
                                $this->em->flush();

                                //Sumo la cita a las citas actuales en la tabla membership
                                $membership->setCurrentAppointments($membership->getCurrentAppointments() + 1);
                                $this->em->flush($membership);

                                $this->setNotificationForAppointments($appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, false, $appointment->getSendNotifications());

                                //Se crea la cita en los calendarios que tenga habilitados
                                $calendarService = new CalendarService($this->em);
                                //createCalendarEventUserRegister
                                $calendarService->createCalendarEvent($userDoctor, null, $appointment, null);

                                $jsonResponse = (object)array(
                                    ConstantF::STATUS => true,
                                    ConstantF::MESSAGE => 'successful'
                                );

                            } else {
                                $jsonResponse = (object)array(
                                    ConstantF::STATUS => false,
                                    ConstantF::MESSAGE => 'user and doctor are not in relationship'
                                );
                            }
                        } else {
                            $jsonResponse = (object)array(
                                ConstantF::STATUS => false,
                                ConstantF::MESSAGE => 'user type not match'
                            );
                        }
                    } else {
                        $jsonResponse = (object)array(
                            ConstantF::STATUS => false,
                            ConstantF::MESSAGE => 'patient not found'
                        );
                    }
                } else {
                    $jsonResponse = (object)array(
                        ConstantF::STATUS => false,
                        ConstantF::MESSAGE => 'appointments limit reached'
                    );
                }
            } else {
                $jsonResponse = (object)array(
                    ConstantF::STATUS => false,
                    ConstantF::MESSAGE => 'user not found'
                );
            }
        } else {
            $jsonResponse = (object)array(
                ConstantF::STATUS => false,
                ConstantF::MESSAGE => 'Missing parameters'
            );
        }
        return $jsonResponse;
    }

    /**
     * @param Appointment $appointment
     * @param $generalDataPatient
     * @param $generaDataDoctor
     * @param $extraDataDoctor
     * @param $isAppointmentEdit
     * @param $sendNotifications
     */
    private function setNotificationForAppointments(Appointment $appointment, $generalDataPatient, $generaDataDoctor, $extraDataDoctor, $isAppointmentEdit, $sendNotifications)
    {
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $appointmentDate = new \DateTime($appointment->getDate()->format("Y-m-d"));

        //Regla de negocio
        //Cita creada para el mísmo día o para el día siguiente a la cita
        if ($appointmentDate->format("Y-m-d") == $today->format("Y-m-d") ||
            $appointmentDate->format("Y-m-d") == $tomorrow->format("Y-m-d")
        ) {
            //Se obtiene el id del registro type=2 de la tabla NotificationType
            $notificationType = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 2));

            if ($notificationType) {
                $notification = new Notification();
                $notification->setAppointment($appointment);
                $notification->setDateToSend($today);
                $notification->setIsSend(false);
                $notification->setType($notificationType);
                $this->em->persist($notification);
                $this->em->flush();

                if ($sendNotifications)
                    $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);

                //TODO: aca se puede implementar una logica para que en caso de que no se envie el mail se notifique a otro mail
            } else {
                //TODO: inconsistencia de datos
            }
        } else {

            //Valores para pruebas
            //$today = new \DateTime('2016-08-27');

            $differenceDate = date_diff($today, $appointmentDate);

            //Segunda regla de negocio
            //Si la cita se crea el sabado y la cita es para el Lunes, se debe enviar mail de confirmacion inmediatamente
            if ($differenceDate->format('%a') == 2 && $today->format('w') == 6 && $appointmentDate->format('w') == 1) {
                //Se obtiene el id del registro type=2 de la tabla NotificationType
                $notificationType = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 2));

                if ($notificationType) {
                    $notification = new Notification();
                    $notification->setAppointment($appointment);
                    $notification->setDateToSend($today);
                    $notification->setIsSend(false);
                    $notification->setType($notificationType);
                    $this->em->persist($notification);
                    $this->em->flush();

                    if ($sendNotifications)
                        $this->sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);

                    //TODO: aca se puede implementar una logica para que en caso de que no se envie el mail se notifique a otro mail
                } else {
                    //TODO: inconsistencia de datos
                }
            } else {

                //Tercera regla de negocio
                //dos o mas dias y menos de un mes
                $todayPlusTwoDays = new \DateTime(strtolower($today->format('Y-m-d') . ' +2 days'));
                $todayPlusOneMonth = new \DateTime(strtolower($today->format('Y-m-d') . ' +1 month'));

                if ($appointmentDate >= $todayPlusTwoDays && $appointmentDate <= $todayPlusOneMonth) {
                    //Notificacion SMS 1 dia antes
                    //Se obtiene el id del registro type=5 de la tabla NotificationType
                    $notificationType = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 5));

                    if ($notificationType) {
                        $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                        //Si la notificacion cae un Domingo se corre para el Sabado
                        if ($appointmentDateLessOneDay->format('w') == 0)
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($appointmentDateLessOneDay);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $this->em->persist($notification);
                        $this->em->flush();
                    }

                    //Notificacion email
                    //Se obtiene el id del registro type=1 de la tabla NotificationType
                    $notificationType = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 1));

                    if ($notificationType) {
                        $notification = new Notification();
                        $notification->setAppointment($appointment);
                        $notification->setDateToSend($today);
                        $notification->setIsSend(false);
                        $notification->setType($notificationType);
                        $this->em->persist($notification);
                        $this->em->flush();

                        if ($sendNotifications)
                            $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);
                        //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                        //Setear fecha para el segundo email (de confirmacion)
                        //Se obtiene el id del registro type=2 de la tabla NotificationType
                        $notificationTypeTwo = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 2));
                        if ($notificationTypeTwo) {
                            $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                            //Si la notificacion cae un Domingo se corre para el Sabado
                            if ($appointmentDateLessOneDay->format('w') == 0)
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessOneDay);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeTwo);
                            $this->em->persist($notification);
                            $this->em->flush();
                        } else {
                            //TODO: inconsistencia de datos
                        }
                    } else {
                        //TODO: inconsistencia de datos
                    }
                } else {

                    //Cuarta regla de negocio. Citas para mas de 1 mes
                    if ($appointmentDate > $todayPlusOneMonth) {

                        //Se obtiene el id del registro type=1 de la tabla NotificationType
                        $notificationTypeOne = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 1));

                        if ($notificationTypeOne) {
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($today);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $this->em->persist($notification);
                            $this->em->flush();
                            //Se envia el email uno
                            if ($sendNotifications)
                                $this->sendInformationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit);
                            //$this->sendIcsEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification);

                            //Setear fecha para otro mail de informacion y sms 10 dias antes
                            /*$appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -10 days'));
                            //Si la tonificacion cae un Domingo
                            if ($appointmentDateLessTenDays->format('w') == 0)
                                $appointmentDateLessTenDays = new \DateTime(strtolower($appointmentDateLessTenDays->format('Y-m-d') . ' -1 days'));
                            //Para Email
                            $notification = new Notification();
                            $notification->setAppointment($appointment);
                            $notification->setDateToSend($appointmentDateLessTenDays);
                            $notification->setIsSend(false);
                            $notification->setType($notificationTypeOne);
                            $this->em->persist($notification);
                            $this->em->flush();

                            //Para SMS
                            $notificationTypeSMS = $this->repositoryNotificationType->findOneBy(array(Constant::NOTIFICATION_TYPE_TYPE => 3));
                            $SMSNotification = new Notification();
                            $SMSNotification->setAppointment($appointment);
                            $SMSNotification->setDateToSend($appointmentDateLessTenDays);
                            $SMSNotification->setIsSend(false);
                            $SMSNotification->setType($notificationTypeSMS);
                            $this->em->persist($SMSNotification);
                            $this->em->flush();*/

                            //Setear fecha para el segundo email (de confirmacion)
                            //Se obtiene el id del registro type=2 de la tabla NotificationType
                            $notificationTypeTwo = $this->notificationTypeService->findOneBy(array(ConstantF::TYPE => 2));
                            if ($notificationTypeTwo) {
                                $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDate->format('Y-m-d') . ' -1 days'));
                                //Si la notificacion cae un Domingo se corre para el Sabado
                                if ($appointmentDateLessOneDay->format('w') == 0)
                                    $appointmentDateLessOneDay = new \DateTime(strtolower($appointmentDateLessOneDay->format('Y-m-d') . ' -1 days'));
                                $notification = new Notification();
                                $notification->setAppointment($appointment);
                                $notification->setDateToSend($appointmentDateLessOneDay);
                                $notification->setIsSend(false);
                                $notification->setType($notificationTypeTwo);
                                $this->em->persist($notification);
                                $this->em->flush();
                            } else {
                                //TODO: inconsistencia de datos
                            }
                        } else {

                        }
                    }
                }
            }
        }
    }

    /**
     * @param $generalDataPatient
     * @param $extraDataDoctor
     * @param $appointment
     * @param $generaDataDoctor
     * @param $notification
     * @param $isAppointmentEdit
     */
    public function sendConfirmationEmail($generalDataPatient, $extraDataDoctor, $appointment, $generaDataDoctor, $notification, $isAppointmentEdit)
    {

        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");
        $today = new \DateTime('today');
        $tomorrow = new \DateTime('tomorrow');
        $email = new MailService();
        $email->setName($generalDataPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($generaDataDoctor->getName());
        $email->setLastNameDoctor($generaDataDoctor->getLastName());
        $email->setEmailDoctor($generaDataDoctor->getEmail());
        $email->setPersonalTitleDoctor($extraDataDoctor->getPersonalTitle());
        $email->setEmail($generalDataPatient->getEmail());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        if ($today->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
            $email->setIsToday(true);
        } else {
            if ($tomorrow->format('Y-m-d') === $appointment->getDate()->format('Y-m-d')) {
                $email->setIsTomorrow(true);
            } else {
                $email->setDay($appointment->getDate()->format('d'));
                $email->setDayLetter($days[$appointment->getDate()->format('w')]);
                $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
                $email->setYear($appointment->getDate()->format('Y'));
            }
        }

        $email->setDate($appointment->getDate());

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));

        $email->setDateToEnd($dateToEnd);
        $email->setDateid($appointment->getId());
        $email->setTipeMail(3);
        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param $gduPatient
     * @param $eduDoctor
     * @param $appointment
     * @param $gduDoctor
     * @param $notification
     * @param $isAppointmentEdit
     */
    public function sendInformationEmail(GeneralDataUser $gduPatient, ExtraDataUser $eduDoctor, Appointment $appointment, GeneralDataUser $gduDoctor, Notification $notification, $isAppointmentEdit)
    {
        //Envio de mail
        $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $days = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado");

        $email = new MailService();
        $email->setName($gduPatient->getName());
        $email->setTime($appointment->getDate()->format('h:i a'));
        $email->setAddress($appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress());
        $email->setPhone($appointment->getUserPlaceConnectionId()->getPlaceId()->getPhone());
        $email->setNameDoctor($gduDoctor->getName());
        $email->setLastNameDoctor($gduDoctor->getLastName());
        $email->setEmailDoctor($gduDoctor->getEmail());
        $email->setPersonalTitleDoctor($eduDoctor->getPersonalTitle());
        $email->setEmail($gduPatient->getEmail());
        $email->setDayLetter($days[$appointment->getDate()->format('w')]);
        $email->setDay($appointment->getDate()->format('d'));
        $email->setDate($appointment->getDate());
        $email->setMonth($months[$appointment->getDate()->format('m') - 1]);
        $email->setYear($appointment->getDate()->format('Y'));
        $email->setDateid($appointment->getId());
        $email->setIsAppointmentEdit($isAppointmentEdit);

        $appointmentDate = new \DateTime($appointment->getDate()->format('Y-m-d H:i:s'));
        $timeToSum = 'PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M';
        $dateToEnd = $appointmentDate->add(new \DateInterval($timeToSum));
        $email->setDateToEnd($dateToEnd);

        $email->setGduPatient($gduPatient);
        $email->setEduDoctor($eduDoctor);
        $email->setAppointment($appointment);
        $email->setGduDoctor($gduDoctor);
        $email->setTipeMail(2);

        if ($email->sendEmail()) {
            $notification->setIsSend(true);
            $this->em->flush();
        }
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}