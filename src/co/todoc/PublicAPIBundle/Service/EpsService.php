<?php

namespace co\todoc\PublicAPIBundle\Service;

use co\todoc\Constant\ConstantF;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BwStudios\CitaMed\Entity\Eps;

class EpsService
{
    private $repositoryEps;

    private $em;

    /**
     * EpsService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryEps = $this->em->getRepository(ConstantF::ENTITY_EPS);
    }

    /**
     * @param $criteria
     * @return null|object
     */
    public function findOneBy($criteria)
    {
        return $this->repositoryEps->findOneBy($criteria);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->repositoryEps->find($id);
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}