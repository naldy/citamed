<?php

namespace co\todoc\PublicAPIBundle\Service;

use co\todoc\Constant\ConstantF;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BwStudios\CitaMed\Entity\Notification;

class NotificationService
{
    private $repositoryNotification;

    private $em;

    /**
     * NotificationService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryNotification = $this->em->getRepository(ConstantF::ENTITY_NOTIFICATION);
    }

    public function findOneBy($criteria)
    {
        return $this->repositoryUser->findOneBy($criteria);
    }

    public function find($id)
    {
        return $this->repositoryUser->find($id);
    }

    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}