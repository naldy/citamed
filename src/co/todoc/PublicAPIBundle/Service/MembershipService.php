<?php

namespace co\todoc\PublicAPIBundle\Service;

use co\todoc\Constant\ConstantF;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BwStudios\CitaMed\Entity\Membership;

class MembershipService
{
    private $repositoryMembership;

    private $em;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryMembership = $this->em->getRepository(ConstantF::ENTITY_MEMBERSHIP);
    }

    /**
     * @param $user
     * @param $timeLapseForFreeAppointments
     * @return null|object|Membership
     */
    public function validateMembership($user, $timeLapseForFreeAppointments)
    {
        //Busco el registro en la membresia
        $membership = $this->repositoryMembership->findOneBy(array(ConstantF::USER => $user->getId()));

        $today = new \DateTime(ConstantF::TODAY);

        if ($membership) {

            if ($today > $membership->getEndPeriod()) {
                //Se genera un nuevo periodo
                $flagWhile = true;
                $startPeriod = new \DateTime($membership->getEndPeriod()->format('Y-m-d'));
                do {
                    $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                    if ($today < $endPeriod) {
                        $flagWhile = false;
                    } else {
                        $startPeriod = $endPeriod;
                    }
                } while ($flagWhile);

                if ($startPeriod && $endPeriod) {
                    $membership->setCurrentAppointments(0);
                    $membership->setEndPeriod($endPeriod);
                    $membership->setStartPeriod($startPeriod);
                    $membership->setPayMembership(false);
                    $this->em->flush();
                }
            }
        } else {
            //Deberia tener membresia pero si no la tiene se crea

            //Deberia tener fecha de creacion pero si no lo tiene se toma hoy
            if ($user->getCreationDate() == null) {
                $user->setCreationDate($today);
            }

            $flagWhile = true;
            $startPeriod = new \DateTime($user->getCreationDate()->format('Y-m-d'));
            do {
                $endPeriod = new \DateTime(strtolower($startPeriod->format('Y-m-d') . ' +' . $timeLapseForFreeAppointments . 'month'));

                if ($today < $endPeriod) {
                    $flagWhile = false;
                } else {
                    $startPeriod = $endPeriod;
                }
            } while ($flagWhile);

            if ($startPeriod && $endPeriod) {
                $membership = new Membership();
                $membership->setUser($user);
                $membership->setCurrentAppointments(0);
                $membership->setEndPeriod($endPeriod);
                $membership->setStartPeriod($startPeriod);
                $membership->setPayMembership(false);
                $this->em->persist($membership);
                $this->em->flush();
            }
        }
    }

    public function findOneBy($criteria)
    {
        return $this->repositoryMembership->findOneBy($criteria);
    }

    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}