<?php

namespace co\todoc\PublicAPIBundle\Service;

use BwStudios\CitaMed\Constant\Constant;
use BwStudios\CitaMed\Entity\Appointment;
use co\todoc\Constant\ConstantF;
use co\todoc\Entity\CalendarEvent;
use co\todoc\Entity\GCalendarToken;
use Doctrine\ORM\EntityManager;
use Google_Service_Calendar;
use Google_Service_Plus;
use Google_Client;
use Google_Service_Calendar_Event;
use Symfony\Component\HttpFoundation\Request;

class CalendarService
{
    private $repositoryGCalendarToken;
    private $repositoryUser;
    private $repositoryExtraDataUser;
    private $repositoryGeneralDataUser;
    private $repositoryUserType;
    private $repositoryAppointment;
    private $repositoryCalendarEvent;
    private $SCOPES;
    private $em;

    /**
     * CalendarService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->repositoryGCalendarToken = $this->em->getRepository(ConstantF::ENTITY_GOOGLE_CALENDAR_TOKEN);
        $this->repositoryUser = $this->em->getRepository(ConstantF::ENTITY_USER);
        $this->repositoryExtraDataUser = $this->em->getRepository(ConstantF::ENTITY_EXTRA_DATA_USER);
        $this->repositoryGeneralDataUser = $this->em->getRepository(ConstantF::ENTITY_GENERAL_DATA_USER);
        $this->repositoryUserType = $this->em->getRepository(ConstantF::ENTITY_USER_TYPE);
        $this->repositoryAppointment = $this->em->getRepository(Constant::ENTITY_APPOINTMENT);
        $this->repositoryCalendarEvent = $this->em->getRepository(Constant::ENTITY_CALENDAR_EVENT);

        $this->SCOPES = implode(' ', array(
                Google_Service_Calendar::CALENDAR, Google_Service_Plus::USERINFO_EMAIL)
        );
    }

    /**
     * @return array
     */
    public function auth()
    {
        $urlServices = array();

        //GOOGLE
        $googleClient = new Google_Client();
        $googleClient->setAccessType(ConstantF::OFFLINE);
        $googleClient->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
        $googleClient->setScopes($this->SCOPES);
        $googleClient->setRedirectUri(ConstantF::REDIRECT_URI);
        $authGoogleUrl = $googleClient->createAuthUrl();
        $urlServices[ConstantF::GOOGLE] = $authGoogleUrl;
        //GOOGLE

        //OUTLOOK
        $clientId = ConstantF::OUTLOOK_CLIENT_ID;
        $clientSecret = ConstantF::OUTLOOK_CLIENT_SECRET;
        $authority = ConstantF::OUTLOOK_AUTHORITY;
        $authorizeUrl = '/common/oauth2/v2.0/authorize?client_id=%1$s&redirect_uri=%2$s&response_type=code&scope=%3$s';
        $tokenUrl = "/common/oauth2/v2.0/token";
        $redirectUri = ConstantF::OUTLOOK_REDIRECT_URI;

        // The app only needs openid (for user's ID info), and Mail.Read
        $scopes = array("User.Read",
            "Calendars.ReadWrite");

        // Build scope string. Multiple scopes are separated
        // by a space
        $scopestr = implode(" ", $scopes);

        $outlookLoginUrl = $authority . sprintf($authorizeUrl, $clientId, urlencode($redirectUri), urlencode($scopestr));

        $urlServices['outlook'] = $outlookLoginUrl;
        //OUTLOOK

        return $urlServices;
    }

    /**
     * @param $req
     * @return null|string
     */
    public function oauthGoogleCallback($req)
    {
        $message = null;

        $code = $req->query->get(ConstantF::CODE);
        $error = $req->query->get(ConstantF::ERROR);

        if (isset($code)) {

            $client = new Google_Client();
            $client->setAccessType(ConstantF::OFFLINE);
            $client->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
            $client->setScopes($this->SCOPES);
            $client->setRedirectUri(ConstantF::REDIRECT_URI);
            $client->authenticate($code);
            $accessToken = $client->getAccessToken();
            $plusService = new Google_Service_Plus($client);
            $person = $plusService->people->get('me');

            //Obtengo el mail del usuario
            $emails = $person->getEmails();

            //Busco el email en la base de datos de todoc
            $isTodocUser = false;
            $userTodoc = null;
            if ($emails[0]->value) {
                $allgduUsers = $this->repositoryGeneralDataUser->findBy(array(ConstantF::EMAIL => $emails[0]->value));

                if (sizeof($allgduUsers) > 0) {
                    foreach ($allgduUsers as $gduUser) {
                        if ($gduUser->getUser()->getUserTypeId()->getId() == 1) {
                            $user = $gduUser->getUser();
                            $isTodocUser = true;
                        }
                    }
                }

                $status = false;
                if ($isTodocUser) {

                    $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::USER => $user));

                    if ($gCalendarToken == null) {
                        $gCAuth = new GCalendarToken();
                        $gCAuth->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                        $gCAuth->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                        $gCAuth->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                        if (isset($accessToken[ConstantF::REFRESH_TOKEN]))
                            $gCAuth->setRefreshToken($accessToken[ConstantF::REFRESH_TOKEN]);
                        $gCAuth->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                        $gCAuth->setCreated($accessToken[ConstantF::CREATED]);
                        $gCAuth->setUser($user);
                        $this->em->persist($gCAuth);
                    }
                    $status = true;
                } else {

                    $calendarRegister = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::EMAIL_UNR => $emails[0]->value));

                    if ($calendarRegister == null) {
                        $gCAuth = new GCalendarToken();
                        $gCAuth->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                        $gCAuth->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                        $gCAuth->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                        if (isset($accessToken[ConstantF::REFRESH_TOKEN]))
                            $gCAuth->setRefreshToken($accessToken[ConstantF::REFRESH_TOKEN]);
                        $gCAuth->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                        $gCAuth->setCreated($accessToken[ConstantF::CREATED]);
                        $gCAuth->setEmailUNR($emails[0]->value);
                        $this->em->persist($gCAuth);
                    }
                    $status = true;
                }

                if ($status) {
                    $this->em->flush();
                    $message = 'Autenticacion correta';
                } else {
                    $message = 'Problema en la autenticacion';
                }
            } else {
                $message = 'Problema en la autenticacion';
            }
        } else {
            if (isset($error)) {
                $message = 'Problema en la autenticacion';
            }
        }
        return $message;
    }

    /**
     * @param $req
     * @return null|string
     */
    public function oauthOutlookCallback($req)
    {

    }


    public function createCalendarEvent($userDoctor, $userEmail, $appointment, $dataEvent)
    {
        if (isset($userDoctor)) {
            $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::USER => $userDoctor));
        } else {
            if (isset($userEmail)) {
                $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::EMAIL_UNR => $userEmail));
            }
        }

        if ($gCalendarToken) {

            $client = new Google_Client();
            $client->setAccessType(ConstantF::OFFLINE);
            $client->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
            $client->setScopes($this->SCOPES);
            $accessToken = array(
                ConstantF::ACCESS_TOKEN => $gCalendarToken->getAccessToken(),
                ConstantF::TOKEN_TYPE => $gCalendarToken->getTokenType(),
                ConstantF::EXPIRES_IN => $gCalendarToken->getExpiresIn(),
                ConstantF::REFRESH_TOKEN => $gCalendarToken->getRefreshToken(),
                ConstantF::ID_TOKEN => $gCalendarToken->getIdToken(),
                ConstantF::CREATED => $gCalendarToken->getCreated());
            $client->setAccessToken($accessToken);

            //Si el token esta expirado lo actualizamos
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                $accessToken = $client->getAccessToken();
                $gCalendarToken->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                $gCalendarToken->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                $gCalendarToken->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                //$gCalendarToken->setRefreshToken($accessToken[ConstantF::REFRESH_TOKEN]);
                $gCalendarToken->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                $gCalendarToken->setCreated($accessToken[ConstantF::CREATED]);
                $this->em->persist($gCalendarToken);
                $this->em->flush();
            }

            $googleCalendarService = new Google_Service_Calendar($client);

            if (isset($appointment)) {
                $dateEnd = clone $appointment->getDate();
                $dateEnd->add(new \DateInterval('PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M'));
                $summary = $appointment->getUserThreeId()->getGeneralDataUserId()->getName() . ' ' . $appointment->getUserThreeId()->getGeneralDataUserId()->getLastName();
                $location = $appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress();
                $description = $appointment->getComment();
                $startDateTime = $appointment->getDate()->format(\DateTime::ISO8601);
                $endDateTime = $dateEnd->format(\DateTime::ISO8601);
                $attendees = array();
            } else {
                if ($dataEvent) {
                    $summary = $dataEvent[ConstantF::SUMMARY];
                    $location = $dataEvent[ConstantF::LOCATION];
                    $description = $dataEvent[ConstantF::DESCRIPTION];
                    $startDateTime = new \DateTime($dataEvent[ConstantF::START]);
                    $startDateTime = $startDateTime->format(\DateTime::ISO8601);
                    $endDateTime = new \DateTime($dataEvent[ConstantF::END]);
                    $endDateTime = $endDateTime->format(\DateTime::ISO8601);
                    $attendees = array();
                    foreach ($dataEvent[ConstantF::ATTENDEES_EMAIL] as $attendeeEmail) {
                        array_push($attendees, array('email' => $attendeeEmail));
                    }
                }
            }

            $event = new Google_Service_Calendar_Event(array(
                'summary' => $summary,
                'location' => $location,
                'description' => $description,
                'start' => array(
                    'dateTime' => $startDateTime,
                    'timeZone' => 'America/Bogota',
                ),
                'end' => array(
                    'dateTime' => $endDateTime,
                    'timeZone' => 'America/Bogota',
                ),
                'attendees' => $attendees
            ));

            $calendarId = 'primary';
            $googleEvent = $googleCalendarService->events->insert($calendarId, $event);
            $googleCalendarId = $googleEvent->getId();

            //Se guarda el id en la entidad appointment o en la entidad CalendarEvent
            if (isset($appointment)) {
                $appointment->setGoogleCalendarId($googleCalendarId);
                $this->em->persist($appointment);
                $this->em->flush();
            } else {
                if (isset($userEmail)) {
                    $calendarEvent = new CalendarEvent();
                    $calendarEvent->setUserEmail($userEmail);
                    $calendarEvent->setEventId($googleCalendarId);
                    $this->em->persist($calendarEvent);
                    $this->em->flush();
                }
            }
        }
    }

    public function editGoogleEvent(Appointment $appointment, $userEmail, $dataEvent)
    {
        if (isset($appointment)) {
            $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::USER => $appointment->getUserOneId()));
            $eventId = $appointment->getGoogleCalendarId();
        } else {
            if (isset($userEmail)) {
                $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::EMAIL_UNR => $userEmail));
                $calendarEvent = $this->repositoryCalendarEvent->findOneBy(array(Constant::EVENT_ID => $dataEvent[Constant::ID]));
                $eventId = $dataEvent[Constant::ID];
            }
        }

        if ($gCalendarToken) {

            $client = new Google_Client();
            $client->setAccessType(ConstantF::OFFLINE);
            $client->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
            $client->setScopes($this->SCOPES);
            $accessToken = array(
                ConstantF::ACCESS_TOKEN => $gCalendarToken->getAccessToken(),
                ConstantF::TOKEN_TYPE => $gCalendarToken->getTokenType(),
                ConstantF::EXPIRES_IN => $gCalendarToken->getExpiresIn(),
                ConstantF::REFRESH_TOKEN => $gCalendarToken->getRefreshToken(),
                ConstantF::ID_TOKEN => $gCalendarToken->getIdToken(),
                ConstantF::CREATED => $gCalendarToken->getCreated());
            $client->setAccessToken($accessToken);

            //Si el token esta expirado lo actualizamos
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                $accessToken = $client->getAccessToken();
                $gCalendarToken->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                $gCalendarToken->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                $gCalendarToken->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                $gCalendarToken->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                $gCalendarToken->setCreated($accessToken[ConstantF::CREATED]);
                $this->em->persist($gCalendarToken);
                $this->em->flush();
            }

            $googleCalendarService = new Google_Service_Calendar($client);

            $startDateTime = null;
            $endDateTime = null;
            if (isset($appointment)) {
                $dateEnd = clone $appointment->getDate();
                $dateEnd->add(new \DateInterval('PT' . $appointment->getAssignedTime()->format('H') . 'H' . $appointment->getAssignedTime()->format('i') . 'M'));
                $summary = $appointment->getUserThreeId()->getGeneralDataUserId()->getName() . ' ' . $appointment->getUserThreeId()->getGeneralDataUserId()->getLastName();
                $location = $appointment->getUserPlaceConnectionId()->getPlaceId()->getAddress();
                $description = $appointment->getComment();
                //$startDateTime = $appointment->getDate()->format(\DateTime::ISO8601);
                $startDateTime = $appointment->getDate();
                //$endDateTime = $dateEnd->format(\DateTime::ISO8601);
                $endDateTime = $dateEnd;
                $attendees = array();
            } else {
                if ($userEmail) {
                    $summary = $dataEvent[ConstantF::SUMMARY];
                    $location = $dataEvent[ConstantF::LOCATION];
                    $description = $dataEvent[ConstantF::DESCRIPTION];
                    $startDateTime = new \DateTime($dataEvent[ConstantF::START]);
                    //$startDateTime = $startDateTime->format(\DateTime::ISO8601);
                    $endDateTime = new \DateTime($dataEvent[ConstantF::END]);
                    //$endDateTime = $endDateTime->format(\DateTime::ISO8601);
                    $attendees = array();
                    foreach ($dataEvent[ConstantF::ATTENDEES_EMAIL] as $attendeeEmail) {
                        array_push($attendees, array('email' => $attendeeEmail));
                    }
                }
            }

            $event = $googleCalendarService->events->get('primary', $eventId);

            $event->setSummary($summary);
            $event->setLocation($location);
            $event->setDescription($description);

            $gEventDateTimeStart = new \Google_Service_Calendar_EventDateTime();
            $gEventDateTimeStart->setDateTime($startDateTime->format(\DateTime::ISO8601));
            //$gEventDateTimeStart->setDate($startDateTime);
            $gEventDateTimeStart->setTimeZone('America/Bogota');

            $gEventDateTimeEnd = new \Google_Service_Calendar_EventDateTime();
            $gEventDateTimeEnd->setDateTime($endDateTime->format(\DateTime::ISO8601));
            //$gEventDateTimeEnd->setDate($endDateTime);
            $gEventDateTimeEnd->setTimeZone('America/Bogota');
            //$event->setStart(array('dateTime' => $startDateTime,
            //    'timeZone' => 'America/Bogota'));
            $event->setStart($gEventDateTimeStart);
            //$event->setEnd(array('dateTime' => $endDateTime,
            //    'timeZone' => 'America/Bogota'));
            $event->setEnd($gEventDateTimeEnd);

            $updatedEvent = $googleCalendarService->events->update('primary', $event->getId(), $event);
            $googleCalendarId = $updatedEvent->getId();

            //Se guarda el id en la entidad appointment
            if (isset($appointment)) {
                $appointment->setGoogleCalendarId($googleCalendarId);
                $this->em->persist($appointment);
                $this->em->flush();
            } else {
                if (isset($userEmail)) {
                    $calendarEvent->setUserEmail($userEmail);
                    $calendarEvent->setEventId($googleCalendarId);
                    $this->em->persist($calendarEvent);
                    $this->em->flush();
                }
            }
        }
    }

    /**
     * @param $eventId
     * @param $userDoctor
     * @param $userEmail
     */
    public function deleteGoogleEvent($eventId, $userDoctor, $userEmail)
    {
        if ($userDoctor) {
            $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::USER => $userDoctor));
        } elseif ($userEmail) {
            $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::EMAIL_UNR => $userEmail));
        }

        if ($gCalendarToken) {

            $client = new Google_Client();
            $client->setAccessType(ConstantF::OFFLINE);
            $client->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
            $client->setScopes($this->SCOPES);
            $accessToken = array(
                ConstantF::ACCESS_TOKEN => $gCalendarToken->getAccessToken(),
                ConstantF::TOKEN_TYPE => $gCalendarToken->getTokenType(),
                ConstantF::EXPIRES_IN => $gCalendarToken->getExpiresIn(),
                ConstantF::REFRESH_TOKEN => $gCalendarToken->getRefreshToken(),
                ConstantF::ID_TOKEN => $gCalendarToken->getIdToken(),
                ConstantF::CREATED => $gCalendarToken->getCreated());
            $client->setAccessToken($accessToken);

            //Si el token esta expirado lo actualizamos
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                $accessToken = $client->getAccessToken();
                $gCalendarToken->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                $gCalendarToken->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                $gCalendarToken->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                $gCalendarToken->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                $gCalendarToken->setCreated($accessToken[ConstantF::CREATED]);
                $this->em->persist($gCalendarToken);
                $this->em->flush();
            }

            $googleCalendarService = new Google_Service_Calendar($client);

            $event = $googleCalendarService->events->delete('primary', $eventId);
        }
    }

    /**
     * @param $userDoctor
     * @param $userEmail
     * @param $appointmentsArray
     * @return mixed
     */
    public function getGoogleEvents($userDoctor, $userEmail, $appointmentsArray)
    {
        $gCalendarToken = null;

        if (isset($userDoctor)) {
            $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::USER => $userDoctor));
        } else {
            if (isset($userEmail)) {
                $gCalendarToken = $this->repositoryGCalendarToken->findOneBy(array(ConstantF::EMAIL_UNR => $userEmail));
            }
        }

        if ($gCalendarToken) {

            $client = new Google_Client();
            $client->setAuthConfig(__DIR__ . '/../' . ConstantF::CLIENT_SECRET_PATH);
            $client->setAccessType(ConstantF::OFFLINE);
            $client->setScopes($this->SCOPES);
            $accessToken = array(
                ConstantF::ACCESS_TOKEN => $gCalendarToken->getAccessToken(),
                ConstantF::TOKEN_TYPE => $gCalendarToken->getTokenType(),
                ConstantF::EXPIRES_IN => $gCalendarToken->getExpiresIn(),
                ConstantF::REFRESH_TOKEN => $gCalendarToken->getRefreshToken(),
                ConstantF::ID_TOKEN => $gCalendarToken->getIdToken(),
                ConstantF::CREATED => $gCalendarToken->getCreated());
            $client->setAccessToken($accessToken);

            //Si el token esta expirado lo actualizamos
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                $accessToken = $client->getAccessToken();
                $gCalendarToken->setAccessToken($accessToken[ConstantF::ACCESS_TOKEN]);
                $gCalendarToken->setTokenType($accessToken[ConstantF::TOKEN_TYPE]);
                $gCalendarToken->setExpiresIn($accessToken[ConstantF::EXPIRES_IN]);
                //$gCalendarToken->setRefreshToken($accessToken[ConstantF::REFRESH_TOKEN]);
                $gCalendarToken->setIdToken($accessToken[ConstantF::ID_TOKEN]);
                $gCalendarToken->setCreated($accessToken[ConstantF::CREATED]);
                $this->em->persist($gCalendarToken);
                $this->em->flush();
            }

            $beginDay = new \DateTime('today');
            $endDay = new \DateTime($beginDay->format('Y-m-d') . '23:59');
            $googleCalendarService = new Google_Service_Calendar($client);
            $allListEvents = $googleCalendarService->events->listEvents('primary',
                array('singleEvents' => true,
                    "orderBy" => "startTime",
                    'timeMin' => $beginDay->format(\DateTime::ISO8601),
                    'timeMax' => $endDay->format(\DateTime::ISO8601)));
            if (sizeof($allListEvents->getItems()) > 0) {
                foreach ($allListEvents as $googleEvent) {
                    array_push($appointmentsArray, array(
                        ConstantF::SUMMARY => $googleEvent->getSummary(),
                        ConstantF::DESCRIPTION => $googleEvent->getDescription(),
                        ConstantF::CREATED => $googleEvent->getCreated(),
                        ConstantF::LOCATION => $googleEvent->getLocation(),
                        ConstantF::START => $googleEvent->getStart(),
                        ConstantF::END => $googleEvent->getEnd()
                    ));
                }
                return $appointmentsArray;
            }
        }
    }

    public function getEventsUNR($req)
    {
        if ($req->request->get(ConstantF::USER_EMAIL)) {
            $userEmail = $req->request->get(ConstantF::USER_EMAIL);
            $eventsArray = $this->getGoogleEvents(null, $userEmail, array());
            if (isset($eventsArray)) {
                $jsonResponse = (object)array(
                    ConstantF::STATUS => true,
                    ConstantF::DATA => $eventsArray
                );
            } else {
                $jsonResponse = (object)array(
                    ConstantF::STATUS => false,
                    ConstantF::MESSAGE => 'Error processing request'
                );
            }
        }
        return $jsonResponse;
    }

    public function newEventUNR(Request $req)
    {
        $jsonResponse = null;

        $data = array(
            ConstantF::START => $req->request->get(ConstantF::START),
            ConstantF::END => $req->request->get(ConstantF::END),
            ConstantF::SUMMARY => $req->request->get(ConstantF::SUMMARY),
            ConstantF::DESCRIPTION => $req->request->get(ConstantF::DESCRIPTION),
            ConstantF::USER_EMAIL => $req->request->get(ConstantF::USER_EMAIL),
            ConstantF::ATTENDEES_EMAIL => $req->request->get(ConstantF::ATTENDEES_EMAIL),
            ConstantF::LOCATION => $req->request->get(ConstantF::LOCATION)
        );

        if (isset($data[ConstantF::START]) && isset($data[ConstantF::END])
            && isset($data[ConstantF::SUMMARY]) && isset($data[ConstantF::DESCRIPTION])
            && isset($data[ConstantF::USER_EMAIL]) && isset($data[ConstantF::ATTENDEES_EMAIL])
            && isset($data[ConstantF::LOCATION])
        ) {
            $this->createCalendarEvent(null, $data[ConstantF::USER_EMAIL], null, $data);
            $jsonResponse = (object)array(
                ConstantF::STATUS => true,
                ConstantF::MESSAGE => 'successfull'
            );
        } else {
            $jsonResponse = (object)array(
                ConstantF::STATUS => false,
                ConstantF::MESSAGE => 'missing parameters'
            );
        }
        return $jsonResponse;
    }

    /**
     * @param null $object
     */
    function var_error_log($object = null)
    {
        ob_start();                    // start buffer capture
        var_dump($object);           // dump the values
        $contents = ob_get_contents(); // put the buffer into a variable
        ob_end_clean();                // end capture
        error_log($contents);        // log contents of the result of var_dump( $object )
    }
}