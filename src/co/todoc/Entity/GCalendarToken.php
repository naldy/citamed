<?php

namespace co\todoc\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GCalendarToken
 *
 * @ORM\Entity
 * @ORM\Table(name="GCalendarToken")
 */
class GCalendarToken
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="BwStudios\CitaMed\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $user;

    /**
     *
     * @var string @ORM\Column(name="access_token", type="string", nullable=false)
     */
    private $accessToken;

    /**
     *
     * @var string @ORM\Column(name="token_type", type="string", nullable=false)
     */
    private $tokenType;

    /**
     *
     * @var integer @ORM\Column(name="expires_in", type="integer", nullable=false)
     */
    private $expiresIn;

    /**
     *
     * @var string @ORM\Column(name="refresh_token", type="string", nullable=false)
     */
    private $refreshToken;
    
    /**
     *
     * @var string @ORM\Column(name="id_token", type="string", nullable=false)
     */
    private $idToken;

    /**
     *
     * @var integer @ORM\Column(name="created", type="integer", nullable=false)
     */
    private $created;

    /**
     * Email for users not registered in todoc database
     * @var string @ORM\Column(name="email_unr", type="string", nullable=true)
     */
    private $emailUNR;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     *
     * @return GCalendarToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set tokenType
     *
     * @param string $tokenType
     *
     * @return GCalendarToken
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = $tokenType;

        return $this;
    }

    /**
     * Get tokenType
     *
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * Set expiresIn
     *
     * @param integer $expiresIn
     *
     * @return GCalendarToken
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    /**
     * Get expiresIn
     *
     * @return integer
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * Set refreshToken
     *
     * @param string $refreshToken
     *
     * @return GCalendarToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    /**
     * Get refreshToken
     *
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Set idToken
     *
     * @param string $idToken
     *
     * @return GCalendarToken
     */
    public function setIdToken($idToken)
    {
        $this->idToken = $idToken;

        return $this;
    }

    /**
     * Get idToken
     *
     * @return string
     */
    public function getIdToken()
    {
        return $this->idToken;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return GCalendarToken
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set emailUNR
     *
     * @param string $emailUNR
     *
     * @return GCalendarToken
     */
    public function setEmailUNR($emailUNR)
    {
        $this->emailUNR = $emailUNR;

        return $this;
    }

    /**
     * Get emailUNR
     *
     * @return string
     */
    public function getEmailUNR()
    {
        return $this->emailUNR;
    }

    /**
     * Set user
     *
     * @param \BwStudios\CitaMed\Entity\User $user
     *
     * @return GCalendarToken
     */
    public function setUser(\BwStudios\CitaMed\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BwStudios\CitaMed\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
